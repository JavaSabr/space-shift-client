package com.ss.client;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import rlib.network.NetworkConfig;
import rlib.util.Util;
import rlib.util.VarTable;

/**
 * Класс для конфигурирование игры.
 * 
 * @author Ronn
 */
public abstract class Config {

	/** адресс удаленного логин сервера */
	public static InetSocketAddress LOGIN_HOST;

	/** адресс локального логин сервера */
	public static InetSocketAddress LOGIN_LOCAL_HOST;

	/** настройки сети */
	public static NetworkConfig NETWORK_CONFIG = new NetworkConfig() {

		@Override
		public String getGroupName() {
			return "Network";
		}

		@Override
		public int getGroupSize() {
			return NETWORK_GROUP_SIZE;
		}

		@Override
		public int getReadBufferSize() {
			return NETWORK_READ_BUFFER_SIZE;
		}

		@Override
		public Class<? extends Thread> getThreadClass() {
			return GameThread.class;
		}

		@Override
		public int getThreadPriority() {
			return NETWORK_THREAD_PRIORITY;
		}

		@Override
		public int getWriteBufferSize() {
			return NETWORK_WRITE_BUFFER_SIZE;
		}

		@Override
		public boolean isVesibleReadException() {
			return true;
		}

		@Override
		public boolean isVesibleWriteException() {
			return false;
		}
	};

	/** адресс логин сервера */
	public static String LOGIN_SERVER_HOST;

	/** путь к папке клиента */
	public static String PROGECT_PATH;

	/** порт для подключения к логин серверу */
	public static int LOGIN_SERVER_PORT;

	/** размер читаемого буфера */
	public static int NETWORK_READ_BUFFER_SIZE;
	/** размер записываемого буфера */
	public static int NETWORK_WRITE_BUFFER_SIZE;
	/** размер группы поток сети */
	public static int NETWORK_GROUP_SIZE;
	/** приоритет сетевых потоков */
	public static int NETWORK_THREAD_PRIORITY;

	/** частота обновления объектов в мире */
	public static int WORLD_UPDATE_OBJECT_INTERVAL;

	/** максимальная степень рассинхронизации */
	public static int GAME_MAX_FLY_DESYNC;

	/** отображать дебаг */
	public static boolean DEV_DEBUG;
	/** отображать дебаг серверных пакетов */
	public static boolean DEV_DEBUG_SERVER_PACKETS;
	/** отображать дебаг клиентских пакетов */
	public static boolean DEV_DEBUG_CLIENT_PACKETS;
	/** отображать ли дебаг синхронизации */
	public static boolean DEV_DEBUG_SYNCHRONIZE;

	/** включен ли режим запуска локально */
	public static boolean LOCAL_MODE;

	/**
	 * Загрузка конфига игры.
	 */
	public static void init() throws UnknownHostException {

		final VarTable vars = VarTable.newInstance();

		LOGIN_SERVER_HOST = vars.getString("LOGIN_SERVER_HOST", "93.84.49.215");
		LOGIN_SERVER_PORT = vars.getInteger("LOGIN_SERVER_PORT", 1201);

		LOGIN_HOST = new InetSocketAddress(InetAddress.getByName(LOGIN_SERVER_HOST), LOGIN_SERVER_PORT);
		LOGIN_LOCAL_HOST = new InetSocketAddress("localhost", LOGIN_SERVER_PORT);

		NETWORK_READ_BUFFER_SIZE = vars.getInteger("NETWORK_READ_BUFFER_SIZE", 8388608);
		NETWORK_WRITE_BUFFER_SIZE = vars.getInteger("NETWORK_WRITE_BUFFER_SIZE", 8388608);
		NETWORK_GROUP_SIZE = vars.getInteger("NETWORK_GROUP_SIZE", 1);
		NETWORK_THREAD_PRIORITY = vars.getInteger("NETWORK_THREAD_PRIORITY", 5);

		GAME_MAX_FLY_DESYNC = vars.getInteger("GAME_MAX_FLY_DESYNC", 5);

		WORLD_UPDATE_OBJECT_INTERVAL = vars.getInteger("WORLD_UPDATE_OBJECT_INTERVAL", 100);

		DEV_DEBUG = vars.getBoolean("DEV_DEBUG", false);
		DEV_DEBUG_SERVER_PACKETS = vars.getBoolean("DEV_DEBUG_SERVER_PACKETS", false);
		DEV_DEBUG_CLIENT_PACKETS = vars.getBoolean("DEV_DEBUG_CLIENT_PACKETS", false);
		DEV_DEBUG_SYNCHRONIZE = vars.getBoolean("DEV_DEBUG_SYNCHRONIZE", false);

		PROGECT_PATH = Util.getRootPath();
	}
}
