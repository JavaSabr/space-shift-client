package com.ss.client.model.skills.classes;

import com.ss.client.model.module.Module;
import com.ss.client.model.skills.Skill;
import com.ss.client.template.SkillTemplate;

import de.lessvoid.nifty.elements.Element;

/**
 * Базовая модель скила модуля.
 * 
 * @author Ronn
 */
public abstract class AbstractSkill implements Skill {

	/** темплейт скила */
	protected SkillTemplate template;

	/** элемент отображения скила */
	protected Element element;

	/** уникальный ид скила */
	protected int objectId;

	public AbstractSkill(final SkillTemplate template) {
		this.template = template;
	}

	@Override
	public void finalyze() {
		objectId = 0;
	}

	@Override
	public void fold() {
		template.put(this);
	}

	@Override
	public String getDescription() {
		return template.getDescription();
	}

	@Override
	public Element getElement() {
		return element;
	}

	@Override
	public int getId() {
		return template.getId();
	}

	@Override
	public int getObjectId() {
		return objectId;
	}

	@Override
	public SkillTemplate getTemplate() {
		return template;
	}

	@Override
	public int getTemplateId() {
		return template.getId();
	}

	@Override
	public void reinit() {
	}

	@Override
	public void setElement(final Element element) {
		this.element = element;
	}

	@Override
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	@Override
	public String toString() {
		return "AbstractSkill [template=" + template + ", objectId=" + objectId + "]";
	}

	@Override
	public void useSkill(final Module module, final int active) {
	}
}
