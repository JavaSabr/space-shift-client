package com.ss.client.model.skills;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.ss.client.model.skills.classes.BlasterSkill;
import com.ss.client.model.skills.classes.ModuleActivate;
import com.ss.client.model.skills.classes.RocketSkill;
import com.ss.client.template.SkillTemplate;

/**
 * Перечисление типов скилов.
 * 
 * @author Ronn
 */
public enum SkillType {
	MODULE_ACTIVATE(ModuleActivate.class),
	BLASTER(BlasterSkill.class),
	ENGINE(ModuleActivate.class),
	ROCKET(RocketSkill.class), ;

	private static final SkillType[] VALUES = values();

	public static final SkillType valueOf(final int index) {
		return VALUES[index];
	}

	/** конструктор скила */
	private Constructor<? extends Skill> constructor;

	private SkillType(final Class<? extends Skill> type) {
		try {
			this.constructor = type.getConstructor(SkillTemplate.class);
		} catch(NoSuchMethodException | SecurityException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @param template шаблон скила.
	 * @return новый экземпляр скила.
	 */
	public Skill newInstance(final SkillTemplate template) {
		try {
			return constructor.newInstance(template);
		} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
