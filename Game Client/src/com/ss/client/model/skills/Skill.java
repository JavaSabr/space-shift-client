package com.ss.client.model.skills;

import rlib.util.pools.Foldable;

import com.ss.client.model.module.Module;
import com.ss.client.template.SkillTemplate;

import de.lessvoid.nifty.elements.Element;

/**
 * Интерфейс для реализации скила.
 * 
 * @author Ronn
 */
public interface Skill extends Foldable {

	/**
	 * Складировать в пул.
	 */
	public void fold();

	/**
	 * @return описание скила.
	 */
	public String getDescription();

	/**
	 * @return элемент, отвечающий за отображение скила.
	 */
	public Element getElement();

	/**
	 * @return ид темплейта.
	 */
	public int getId();

	/**
	 * @return уникальный ид модуля.
	 */
	public int getObjectId();

	/**
	 * @return темплейт скила.
	 */
	public SkillTemplate getTemplate();

	/**
	 * @return ид шаброна умения.
	 */
	public int getTemplateId();

	/**
	 * @param element элемент, отвечающий за отображение скила.
	 */
	public void setElement(Element element);

	/**
	 * @param objectId уникальный ид модуля.
	 */
	public void setObjectId(int objectId);

	/**
	 * Использовать скил.
	 * 
	 * @param module используемый модуль.
	 * @param active статус активности.
	 */
	public void useSkill(Module module, int active);

}
