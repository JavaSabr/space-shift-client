package com.ss.client.model.shots;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.ss.client.util.LocalObjects;

/**
 * Реализация бластерного выстрела.
 * 
 * @author Ronn
 */
public class BlasterShot extends AbstractShot {

	/** целевая точка попадания */
	private final Vector3f targetLoc;

	public BlasterShot() {
		this.targetLoc = new Vector3f();
	}

	/**
	 * @return целевая точка попадания.
	 */
	public Vector3f getTargetLoc() {
		return targetLoc;
	}

	/**
	 * @param targetLoc целевая точка попадания.
	 */
	public void setTargetLoc(Vector3f targetLoc) {
		this.targetLoc.set(targetLoc);
	}

	@Override
	public boolean update(long currentTime, LocalObjects local) {

		float done = getDone();

		try {

			if(done > getMaxDistance() * 3) {
				return true;
			}

			float speed = getSpeed();

			long diff = currentTime - getLastTime();

			done += (diff * speed / 1000F);
			float percent = done / getMaxDistance();

			Vector3f startLoc = getStartLoc();
			Vector3f targetLoc = getTargetLoc();
			Vector3f location = getLocation();

			// рассчитываем новую точку текущей позиции
			float newX = startLoc.getX() + ((targetLoc.getX() - startLoc.getX()) * percent);
			float newY = startLoc.getY() + ((targetLoc.getY() - startLoc.getY()) * percent);
			float newZ = startLoc.getZ() + ((targetLoc.getZ() - startLoc.getZ()) * percent);

			location.set(newX, newY, newZ);

			Spatial model = getModel();
			model.setLocalTranslation(location);

			return false;

		} finally {
			setLastTime(currentTime);
			setDone(done);
		}
	}
}
