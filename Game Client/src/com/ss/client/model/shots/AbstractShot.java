package com.ss.client.model.shots;

import rlib.util.array.Array;
import rlib.util.pools.FoldablePool;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.jme.util.NodeUtils;
import com.ss.client.manager.UpdateShotManager;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.skills.Skill;
import com.ss.client.model.space.SpaceLocation;

/**
 * Базовая реализация выстрела.
 * 
 * @author Ronn
 */
public abstract class AbstractShot implements Shot {

	protected static final UpdateShotManager UPDATE_SHOT_MANAGER = UpdateShotManager.getInstance();
	protected static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** стартовая позиция выстрела */
	private final Vector3f startLoc;
	/** позиция выстрела */
	private final Vector3f location;

	/** разворот выстела */
	private final Quaternion rotation;

	/** пул, в который сложится выстрел после выполнения задачи */
	private FoldablePool<Shot> pool;
	/** контецнер, в котором находится выстрел для обновлений */
	private Array<Shot> updateShots;
	/** стреляющий корабль */
	private SpaceShip shooter;
	/** модуль, из которого был произведен выстрел */
	private ShotModule module;
	/** стреляющий скил */
	private Skill skill;

	/** модель выстрела */
	private Spatial model;
	/** узел, в котром будет находится выстрел */
	private Node node;

	/** пролетевшее расстояние */
	private float done;
	/** текущая скорость выстрела */
	private float speed;

	/** время последнего обновления рассчета */
	private long lastTime;
	/** время запуска выстрела */
	private long startTime;

	/** уникаьный ид выстрела */
	private int objectId;
	/** максимальная дальность полета выстрела */
	private int maxDistance;
	/** индекс ствола, из которого был выстрел */
	private int index;

	public AbstractShot() {
		this.startLoc = new Vector3f();
		this.location = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	public void bind(Array<Shot> updateShots) {
		this.updateShots = updateShots;
	}

	@Override
	public void bind(FoldablePool<Shot> pool) {
		this.pool = pool;
	}

	@Override
	public void finalyze() {
		shooter = null;
		pool = null;
		updateShots = null;
		skill = null;
	}

	@Override
	public void finish() {

		NodeUtils.removeToNode(getNode(), getModel());

		SPACE_LOCATION.removeShot(this);

		if(pool != null) {
			pool.put(this);
		}
	}

	@Override
	public float getDone() {
		return done;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public long getLastTime() {
		return lastTime;
	}

	@Override
	public Vector3f getLocation() {
		return location;
	}

	@Override
	public int getMaxDistance() {
		return maxDistance;
	}

	@Override
	public Spatial getModel() {
		return model;
	}

	@Override
	public ShotModule getModule() {
		return module;
	}

	@Override
	public Node getNode() {
		return node;
	}

	@Override
	public int getObjectId() {
		return objectId;
	}

	@Override
	public Quaternion getRotation() {
		return rotation;
	}

	@Override
	public SpaceShip getShooter() {
		return shooter;
	}

	@Override
	public Skill getSkill() {
		return skill;
	}

	@Override
	public float getSpeed() {
		return speed;
	}

	@Override
	public Vector3f getStartLoc() {
		return startLoc;
	}

	@Override
	public long getStartTime() {
		return startTime;
	}

	@Override
	public void reinit() {
		setDone(0);
	}

	/**
	 * @param done выполненность полета.
	 */
	protected void setDone(float done) {
		this.done = done;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param lastTime время последнего обновления.
	 */
	protected void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

	/**
	 * @param location позиция выстрела.
	 */
	protected void setLocation(Vector3f location) {
		this.location.set(location);
	}

	@Override
	public void setMaxDistance(int maxDistance) {
		this.maxDistance = maxDistance;
	}

	@Override
	public void setModel(Spatial model) {
		this.model = model;
	}

	@Override
	public void setModule(ShotModule module) {
		this.module = module;
	}

	@Override
	public void setNode(Node node) {
		this.node = node;
	}

	@Override
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	@Override
	public void setRotation(Quaternion rotation) {
		this.rotation.set(rotation);
	}

	@Override
	public void setShooter(SpaceShip shooter) {
		this.shooter = shooter;
	}

	@Override
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	@Override
	public void setSpeed(float speed) {
		this.speed = speed;
	}

	@Override
	public void setStartLoc(Vector3f startLoc) {
		this.startLoc.set(startLoc);
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	@Override
	public void start() {

		SpaceShip shooter = getShooter();
		Node node = getNode();
		Spatial model = getModel();

		if(shooter == null || model == null || node == null) {
			throw new RuntimeException("not found shooter or skill or model or node.");
		}

		NodeUtils.addToNode(node, model);

		model.setLocalRotation(rotation);
		model.setLocalTranslation(startLoc);

		setLastTime(System.currentTimeMillis());
		setLocation(getStartLoc());

		UPDATE_SHOT_MANAGER.addShot(this);
		SPACE_LOCATION.addShot(this);
	}

	@Override
	public void stop() {

		if(updateShots != null) {
			updateShots.fastRemove(this);
		}

		finish();
	}
}