package com.ss.client.model;

import java.io.IOException;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.Strings;

import com.jme3.collision.CollisionResult;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.material.Material;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.scene.shape.Box;
import com.ss.client.Config;
import com.ss.client.Game;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.ImageId;
import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.jme.util.NodeUtils;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.template.ObjectTemplate;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;
import com.ss.client.util.SortedCollisionResults;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Базовая модель отображения космического объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceObjectView<T extends ObjectTemplate, O extends SpaceObject> implements SpaceObjectView {

	protected static final Logger LOGGER = Loggers.getLogger(SpaceObjectView.class);
	protected static final Game GAME = Game.getInstance();
	protected static final Nifty NIFTY = GAME.getNifty();

	/** владелец внешности */
	protected O owner;
	/** шаблон объекта */
	protected T template;

	/** пространство модели объекта */
	protected Node node;

	protected Geometry debug;

	/** позиция для надписи */
	protected Vector3f nameLoc;

	public AbstractSpaceObjectView(final Spatial model, final T template) {
		this.node = (Node) model;
		this.nameLoc = new Vector3f();
		this.template = template;

		// model.addControl(this);

		final String niftyId = ElementId.getNextElementId();

		final LabelBuilder nameBuilder = ElementFactory.getLabelBuilder(niftyId, Strings.EMPTY, "200px", "25px");
		nameBuilder.textHAlign(Align.Center);
		nameBuilder.textVAlign(VAlign.Center);

		// debug
		{
			Box box = new Box(template.getSizeX(), template.getSizeY(), template.getSizeZ());
			debug = new Geometry("debug", box);
			debug.setMaterial(new Material(GAME.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md"));
		}
	}

	@Override
	public void addSpatial(final Spatial spatial) {
		GAME.syncLock();
		try {
			NodeUtils.addToNode(getNode(), spatial);
		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void addSpatial(final Spatial[] spatials) {
		GAME.syncLock();
		try {

			final Node parent = getNode();

			for(final Spatial child : spatials) {
				NodeUtils.addToNode(parent, child);
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void addToNode(final Node rootNode) {
		GAME.syncLock();
		try {

			NodeUtils.addToNode(rootNode, getNode());

			if(Config.DEV_DEBUG_SYNCHRONIZE) {
				NodeUtils.addToNode(rootNode, getDebug());
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		return null;
	}

	@Override
	public void destruct() {

		O owner = getOwner();
		GraficEffectTemplate destructEffect = template.getDestructEffect();

		if(destructEffect != null) {
			GraficEffect effect = destructEffect.takeInstance();
			effect.start(owner.getParentNode(), owner.getLocation());
		}
	}

	@Override
	public void finalyze() {
		owner = null;
	}

	@Override
	public String getCollision(Ray ray, Vector3f result, String targetNode) {

		SortedCollisionResults results = SortedCollisionResults.get();
		results.clear();

		Node node = getNode();
		result.set(node.getLocalTranslation());

		try {
			synchronized(node) {
				node.collideWith(ray, results);
			}
		} catch(Exception e) {
			LOGGER.warning(getClass(), e);
		}

		if(results.size() > 0) {

			results.sort();

			CollisionResult collision = null;

			for(int i = 0, length = results.size(); i < length; i++) {

				CollisionResult next = results.getCollision(i);
				Geometry geometry = next.getGeometry();

				if(geometry == null) {
					continue;
				}

				if(targetNode != null && !GameUtil.hasNode(geometry, targetNode)) {
					continue;
				}

				collision = next;
				break;
			}

			if(collision == null) {
				collision = results.getClosestCollision();
			}

			if(collision != null) {
				result.set(collision.getContactPoint());
			}
		}

		return targetNode;
	}

	@Override
	public Geometry getDebug() {
		return debug;
	}

	@Override
	public Vector3f getLocation() {
		return getNode().getLocalTranslation();
	}

	@Override
	public String getMapIcon() {
		return ImageId.GAME_MAP_ICON_UNKNOWN.getPath();
	}

	/**
	 * @return слой для размещений имен объектов.
	 */
	protected Element getNameLayer() {
		final Screen screen = NIFTY.getCurrentScreen();
		return screen.findElementByName(NAME_OBJECT_LAYER);
	}

	@Override
	public final Node getNode() {
		return node;
	}

	@Override
	public O getOwner() {
		return owner;
	}

	@Override
	public Quaternion getRotation() {
		return getNode().getLocalRotation();
	}

	@Override
	public void onHit(Vector3f point, String target, LocalObjects local) {
	}

	@Override
	public void read(JmeImporter im) throws IOException {
	}

	@Override
	public void reinit() {
	}

	@Override
	public void removeSpatial(final Spatial spatial) {
		GAME.syncLock();
		try {
			NodeUtils.removeToNode(getNode(), spatial);
		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void removeSpatial(final Spatial[] spatials) {
		GAME.syncLock();
		try {

			final Node parent = getNode();

			for(final Spatial child : spatials) {
				NodeUtils.removeToNode(parent, child);
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void removeToNode(final Node rootNode) {
		GAME.syncLock();
		try {

			if(rootNode != null) {
				NodeUtils.removeToNode(rootNode, getNode());
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void render(RenderManager rm, ViewPort vp) {
	}

	@Override
	public void setLocation(final Vector3f loc) {
		node.setLocalTranslation(loc);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setOwner(final SpaceObject owner) {
		this.owner = (O) owner;
	}

	@Override
	public void setRotation(final Quaternion rotate) {
		node.setLocalRotation(rotate);
	}

	@Override
	public void setSpatial(Spatial spatial) {
	}

	@Override
	public void update(float tpf) {
	}

	@Override
	public void write(JmeExporter ex) throws IOException {
	}
}
