package com.ss.client.model.space;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.IntKey;
import rlib.util.table.LongKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.math.Vector3f;
import com.ss.client.gui.model.listeners.AddRemoveObjectListener;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.shots.Shot;

/**
 * Модель космической локации.
 * 
 * @author Ronn
 */
public class SpaceLocation {

	private static final Logger LOGGER = Loggers.getLogger(SpaceLocation.class);

	public static final int LOCAL_SECTOR_SIZE = 100;
	public static final int REGION_SECTOR_SIZE = 10000;
	public static final int GLOBAL_SECTOR_SIZE = Integer.MAX_VALUE;

	/** размеры одной локации */
	public static final int LOC_MIN_X = -100_000;
	public static final int LOC_MAX_X = 100_000;
	public static final int LOC_MIN_Y = -100_000;
	public static final int LOC_MAX_Y = 100_000;
	public static final int LOC_MIN_Z = -100_000;
	public static final int LOC_MAX_Z = 100_000;

	public static final int LOCATION_X_SIZE = Math.abs(LOC_MIN_X) + LOC_MAX_X;
	public static final int LOCATION_Y_SIZE = Math.abs(LOC_MIN_Y) + LOC_MAX_Y;
	public static final int LOCATION_Z_SIZE = Math.abs(LOC_MIN_Z) + LOC_MAX_Z;

	/** размер секторов */
	private static final int SECTOR_SIZE = 5_000;

	/** рассчет смещения */
	private static final int OFFSET_X = Math.abs(LOC_MIN_X / SECTOR_SIZE);
	private static final int OFFSET_Y = Math.abs(LOC_MIN_Y / SECTOR_SIZE);
	private static final int OFFSET_Z = Math.abs(LOC_MIN_Z / SECTOR_SIZE);

	/** кол-во секторов в локации */
	private static final int SECTORS_X = LOC_MAX_X / SECTOR_SIZE + OFFSET_X;
	private static final int SECTORS_Y = LOC_MAX_Y / SECTOR_SIZE + OFFSET_Y;
	private static final int SECTORS_Z = LOC_MAX_Z / SECTOR_SIZE + OFFSET_Z;

	private static final SpaceLocation INSTANCE = new SpaceLocation();

	public static final SpaceLocation getInstance() {
		return INSTANCE;
	}

	/** таблица всех объектов */
	private final Table<LongKey, SpaceObject> objectTable;
	/** таблица активных выстрелов */
	private final Table<IntKey, Shot> shotTable;

	/** список всех объектов */
	private final Array<SpaceObject> objects;

	/** слушатели добавления/удаления объектов в космосе */
	private final Array<AddRemoveObjectListener> addRemoveObjectListeners;

	/** все доступные сектора в локации */
	private final SpaceSector[][][] sectors;

	/** корабль игрока */
	private PlayerShip playerShip;

	public SpaceLocation() {
		this.objectTable = Tables.newLongTable();
		this.shotTable = Tables.newConcurrentIntegerTable();
		this.objects = Arrays.toArray(SpaceObject.class);
		this.addRemoveObjectListeners = Arrays.toConcurrentArray(AddRemoveObjectListener.class);
		this.sectors = new SpaceSector[SECTORS_X + 1][SECTORS_Y + 1][SECTORS_Z + 1];
	}

	/**
	 * Добавить близко окружающих объектов.
	 * 
	 * @param container контейнер объектов.
	 * @param center относительно кого они близко.
	 */
	public <T extends SpaceObject> void addAroundTo(final Class<? super T> type, final Array<T> container, final T center) {

		final SpaceSector sector = center.getCurrentSector();

		if(sector == null) {
			return;
		}

		final Vector3f location = center.getLocation();

		final VisibleType visibleType = center.getVisibleType();

		final int size = visibleType.getSectorSize();

		final int x = (int) location.getX() / size;
		final int y = (int) location.getY() / size;
		final int z = (int) location.getZ() / size;

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.addAroundTo(type, container, center, visibleType, size, x, y, z);
		}
	}

	/**
	 * Добавить слушателя добавлений и удалений объектов из космоса.
	 * 
	 * @param listener слушатель объектов.
	 */
	public void addListener(final AddRemoveObjectListener listener) {
		addRemoveObjectListeners.add(listener);
	}

	/**
	 * Добавление нового объекта.
	 * 
	 * @param object новый объект.
	 */
	public void addNewObject(final SpaceObject object) {

		LOGGER.info("add new object " + object);

		final SpaceObject result = objectTable.get(object.getObjectId());

		if(result != null) {
			LOGGER.warning("found duplicate object " + object);
			result.deleteMe();
		}

		objectTable.put(object.getObjectId(), object);
		objects.add(object);

		final Array<AddRemoveObjectListener> listeners = getAddRemoveObjectListeners();

		if(!listeners.isEmpty()) {

			listeners.readLock();
			try {

				for(final AddRemoveObjectListener listener : listeners.array()) {

					if(listener == null) {
						break;
					}

					listener.addObject(object);
				}
			} finally {
				listeners.readUnlock();
			}
		}
	}

	/**
	 * Добавление нового выстрела в пространство.
	 * 
	 * @param shot новый выстрел.
	 */
	public void addShot(Shot shot) {
		shotTable.put(shot.getObjectId(), shot);
	}

	/**
	 * Добавление объекта в локацию.
	 * 
	 * @param object добавляемый объект.
	 */
	public void addVisibleObject(final SpaceObject object) {

		final SpaceSector sector = getSector(object);
		final SpaceSector currentSector = object.getCurrentSector();

		// если сектор не менялся
		if(sector == null || currentSector != null && currentSector == sector) {
			return;
		}

		sector.addObject(object);
		object.setCurrentSector(sector);

		// если объект только появился в локации
		if(currentSector == null && object.getVisibleType() == VisibleType.REGION) {
			for(final SpaceSector neighbor : sector.getNeighbors()) {
				neighbor.onEnter(object, getPlayerShip());
			}
		}
		// если объект сменил сектор
		else if(currentSector != null) {

			final PlayerShip playerShip = getPlayerShip();

			if(object.getVisibleType() == VisibleType.REGION || object == getPlayerShip()) {

				final SpaceSector[] oldNeighbors = currentSector.getNeighbors();
				final SpaceSector[] newNeighbors = sector.getNeighbors();

				for(final SpaceSector neighbor : oldNeighbors) {
					if(!Arrays.contains(newNeighbors, neighbor)) {
						neighbor.onExit(object, playerShip);
					}
				}

				for(final SpaceSector neighbor : newNeighbors) {
					if(!Arrays.contains(oldNeighbors, neighbor)) {
						neighbor.onEnter(object, playerShip);
					}
				}
			}

			currentSector.removeObject(object);
		}
	}

	public Array<AddRemoveObjectListener> getAddRemoveObjectListeners() {
		return addRemoveObjectListeners;
	}

	/**
	 * Получить список соседних секторов вокруг указанного сектора.
	 * 
	 * @param locationId ид локации.
	 * @param x индекс сектора.
	 * @param y индекс сектора.
	 * @param z индекс сектора.
	 * @return список соседних секторов.
	 */
	public SpaceSector[] getNeighbors(final int x, final int y, final int z) {

		final Array<SpaceSector> array = Arrays.toArray(SpaceSector.class, 27);

		final SpaceSector[][][] sectors = getSectors();

		for(int a = -1; a <= 1; a++) {
			for(int b = -1; b <= 1; b++) {
				for(int c = -1; c <= 1; c++) {

					final int tileX = x + a;
					final int tileY = y + b;
					final int tileZ = z + c;

					if(validSector(tileX, tileY, tileZ)) {

						SpaceSector sector = sectors[tileX][tileY][tileZ];

						if(sector == null)
							synchronized(this) {
								sector = sectors[tileX][tileY][tileZ];

								if(sector == null) {
									sector = new SpaceSector(this, tileX, tileY, tileZ);
									sectors[tileX][tileY][tileZ] = sector;
								}
							}

						array.add(sector);
					}
				}
			}
		}

		array.trimToSize();

		return array.array();
	}

	/**
	 * Получение объекта по его ид.
	 * 
	 * @param objectId уникальный ид объекта.
	 * @return искомый объект.
	 */
	@SuppressWarnings("unchecked")
	public <T extends SpaceObject> T getObject(final long objectId) {
		return (T) objectTable.get(objectId);
	}

	/**
	 * @return список всех объектов.
	 */
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * Получение сектора, в котором находится точка.
	 * 
	 * @param x координата.
	 * @param y координата.
	 * @param z координата.
	 * @return искомый сектор.
	 */
	public SpaceSector getSector(final float x, final float y, final float z) {

		final int newX = (int) x / SECTOR_SIZE + OFFSET_X;
		final int newY = (int) y / SECTOR_SIZE + OFFSET_Y;
		final int newZ = (int) z / SECTOR_SIZE + OFFSET_Z;

		return getSector(newX, newY, newZ);
	}

	/**
	 * Получение сектора по индексам.
	 * 
	 * @param x индекс.
	 * @param y индекс.
	 * @param z индекс.
	 * @return искомый сектор.
	 */
	public SpaceSector getSector(final int x, final int y, final int z) {

		final SpaceSector[][][] sectors = getSectors();

		SpaceSector sector = null;

		if(validSector(x, y, z)) {

			sector = sectors[x][y][z];

			if(sector == null) {
				synchronized(this) {
					sector = sectors[x][y][z];

					if(sector == null) {
						sector = new SpaceSector(this, x, y, z);
						sectors[x][y][z] = sector;
					}
				}
			}
		}

		return sector;
	}

	/**
	 * Получение сектора, в котром находися указанный объект.
	 * 
	 * @param object объект.
	 * @return искомый сектор.
	 */
	public SpaceSector getSector(final SpaceObject object) {
		final Vector3f vector = object.getLocation();
		return getSector(vector.getX(), vector.getY(), vector.getZ());
	}

	/**
	 * @return массив секторов в локации.
	 */
	public SpaceSector[][][] getSectors() {
		return sectors;
	}

	/**
	 * @param objectId уникальный ид выстрела.
	 * @return выстрел.
	 */
	public Shot getShot(int objectId) {
		return shotTable.get(objectId);
	}

	/**
	 * Удаление старого объекта.
	 * 
	 * @param object старый объект.
	 */
	public void removeOldObject(final SpaceObject object) {

		LOGGER.info("remove old object " + object);

		final SpaceObject result = objectTable.remove(object.getObjectId());

		if(result == null) {
			LOGGER.warning("not found object for remove " + object);
		}

		objects.fastRemove(object);

		final Array<AddRemoveObjectListener> listeners = getAddRemoveObjectListeners();

		if(result != null && !listeners.isEmpty()) {

			listeners.readLock();
			try {

				for(final AddRemoveObjectListener listener : listeners.array()) {

					if(listener == null) {
						break;
					}

					listener.removeObject(object);
				}
			} finally {
				listeners.readUnlock();
			}
		}
	}

	/**
	 * Удаление неактуального выстрела из пространства.
	 * 
	 * @param shot неактуальный выстрел.
	 */
	public void removeShot(Shot shot) {
		shotTable.remove(shot.getObjectId());
	}

	/**
	 * Удаление из локации объекта.
	 * 
	 * @param object удаляемый объект.
	 */
	public void removeVisibleObject(final SpaceObject object) {

		final SpaceSector currentSector = object.getCurrentSector();

		if(currentSector == null) {
			return;
		}

		for(final SpaceSector neighbor : currentSector.getNeighbors()) {
			neighbor.onExit(object, getPlayerShip());
		}

		currentSector.removeObject(object);
	}

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(final PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	/**
	 * Проверка на корректность индексов сектора.
	 * 
	 * @param x индекс сектора.
	 * @param y индекс сектора.
	 * @param z индекс сектора.
	 * @return корректные ли индексы сектора.
	 */
	public boolean validSector(final int x, final int y, final int z) {
		return x >= 0 && x < SECTORS_X && y >= 0 && y < SECTORS_Y && z >= 0 && z < SECTORS_Z;
	}
}
