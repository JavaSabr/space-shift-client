package com.ss.client.model.space;

import rlib.concurrent.AsynReadSynWriteLock;
import rlib.concurrent.Locks;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.math.Vector3f;
import com.ss.client.model.SpaceObject;

/**
 * Модель сектора в косомсе.
 * 
 * @author Ronn
 */
public class SpaceSector {

	/** синхронизатор */
	private final AsynReadSynWriteLock lock;

	/** ид локации, к которому принадлежит он */
	private final SpaceLocation location;

	/** список объктов в секторе */
	private final Array<SpaceObject> objects;

	/** координаты сектора */
	private final int x;
	private final int y;
	private final int z;

	/** соседние сектора */
	private volatile SpaceSector[] neighbors;

	public SpaceSector(final SpaceLocation location, final int x, final int y, final int z) {
		this.lock = Locks.newARSWLock();
		this.objects = Arrays.toArray(SpaceObject.class);
		this.location = location;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Добавить в контейнер всех ближних объектов к указанному объекту в
	 * секторе.
	 * 
	 * @param type тип интересуемых объектов.
	 * @param container контейнер объектов.
	 * @param center центральны й объект.
	 * @param visibleType тип расстояния.
	 * @param size размер квадрата.
	 * @param x координата квадрата.
	 * @param y координата квадрата.
	 * @param z координата квадрата.
	 */
	@SuppressWarnings("unchecked")
	public <T extends SpaceObject> void addAroundTo(final Class<? super T> type, final Array<T> container, final T center, final VisibleType visibleType, final int size, final int x, final int y,
			final int z) {

		final AsynReadSynWriteLock lock = getLock();

		lock.asynLock();
		try {

			final Array<SpaceObject> objects = getObjects();

			for(final SpaceObject object : objects.array()) {

				if(object == null) {
					break;
				}

				if(!type.isInstance(object) || object == center) {
					continue;
				}

				final VisibleType targetType = object.getVisibleType();

				if(targetType == visibleType && isAround(object, size, x, y, z)) {
					container.add((T) object);
				} else if(targetType != visibleType && isAround(center, object, targetType)) {
					container.add((T) object);
				}
			}

		} finally {
			lock.asynUnlock();
		}
	}

	/**
	 * Добавление объекта в сектор.
	 * 
	 * @param object добавляемый объект.
	 */
	public void addObject(final SpaceObject object) {

		final AsynReadSynWriteLock lock = getLock();

		lock.synLock();
		try {
			getObjects().add(object);
		} finally {
			lock.synUnlock();
		}
	}

	@Override
	public boolean equals(final Object obj) {

		if(this == obj) {
			return true;
		}

		if(obj == null) {
			return false;
		}

		if(getClass() != obj.getClass()) {
			return false;
		}

		final SpaceSector other = (SpaceSector) obj;

		if(x != other.x) {
			return false;
		}
		if(y != other.y) {
			return false;
		}
		if(z != other.z) {
			return false;
		}

		return true;
	}

	/**
	 * @return синхронизатор.
	 */
	public AsynReadSynWriteLock getLock() {
		return lock;
	}

	/**
	 * @return массив соседних регионов.
	 */
	public SpaceSector[] getNeighbors() {

		if(neighbors == null) {
			synchronized(this) {
				if(neighbors == null) {
					neighbors = location.getNeighbors(x, y, z);
				}
			}
		}

		return neighbors;
	}

	/**
	 * @return список всех объектов в секторе.
	 */
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	/**
	 * Определение, находится ли объект близко к указанной позиции.
	 * 
	 * @param object проверяемый объект.
	 * @param size размер квадрата.
	 * @param x координата квадрата.
	 * @param y координата квадрата.
	 * @param z координата квадрата.
	 * @return находится ли рядом с указанным квадратом.
	 */
	private boolean isAround(final SpaceObject object, final int size, final int x, final int y, final int z) {

		final Vector3f location = object.getLocation();

		final int targetX = (int) location.getX() / size;
		final int targetY = (int) location.getY() / size;
		final int targetZ = (int) location.getZ() / size;

		return Math.abs(x - targetX) < 2 && Math.abs(y - targetY) < 2 && Math.abs(z - targetZ) < 2;
	}

	/**
	 * Определние, находятся ли 2 объекта рядом друг с другом.
	 * 
	 * @param first первый объект.
	 * @param second второй объект.
	 * @param visibleType рамзамер квадрата, по которому проверяем.
	 * @return находятся ли рядом они друг с другом.
	 */
	private boolean isAround(final SpaceObject first, final SpaceObject second, final VisibleType visibleType) {

		final int size = visibleType.getSectorSize();

		final Vector3f location = second.getLocation();

		final int x = (int) location.getX() / size;
		final int y = (int) location.getY() / size;
		final int z = (int) location.getZ() / size;

		return isAround(first, size, x, y, z);
	}

	/**
	 * Отобразить объект для игрока.
	 * 
	 * @param object отображаемый объект.
	 */
	public void onEnter(final SpaceObject object, final SpaceObject playerShip) {

		if(object != playerShip && object.isVisible()) {
			return;
		}

		final AsynReadSynWriteLock lock = getLock();

		lock.asynLock();
		try {

			final Array<SpaceObject> objects = getObjects();

			if(objects.isEmpty()) {
				return;
			}

			if(object == playerShip) {

				for(final SpaceObject target : objects.array()) {

					if(target == null) {
						break;
					}

					if(target == playerShip || target.isVisible() || target.getVisibleType() != VisibleType.REGION) {
						continue;
					}

					target.show();
				}

			} else if(object.getVisibleType() == VisibleType.REGION && objects.contains(playerShip)) {
				object.show();
			}

		} finally {
			lock.asynUnlock();
		}
	}

	/**
	 * Скрыть объект от игрока.
	 * 
	 * @param object скрываемый объект.
	 */
	public void onExit(final SpaceObject object, final SpaceObject playerShip) {

		if(object != playerShip && !object.isVisible()) {
			return;
		}

		final AsynReadSynWriteLock lock = getLock();

		lock.asynLock();
		try {

			final Array<SpaceObject> objects = getObjects();

			if(objects.isEmpty()) {
				return;
			}

			if(object == playerShip) {

				for(final SpaceObject target : objects.array()) {

					if(target == null) {
						break;
					}

					if(target == playerShip || !target.isVisible() || target.getVisibleType() != VisibleType.REGION) {
						continue;
					}

					target.hide();
				}

			} else if(object.getVisibleType() == VisibleType.REGION && objects.contains(playerShip)) {
				object.hide();
			}

		} finally {
			lock.asynUnlock();
		}
	}

	/**
	 * Удалить объект из сектора.
	 * 
	 * @param object удаляемый объект.
	 */
	public void removeObject(final SpaceObject object) {
		final AsynReadSynWriteLock lock = getLock();

		lock.synLock();
		try {
			getObjects().fastRemove(object);
		} finally {
			lock.synUnlock();
		}
	}

	@Override
	public String toString() {
		return "SpaceSector x = " + x + ",  y = " + y + ",  z = " + z;
	}
}
