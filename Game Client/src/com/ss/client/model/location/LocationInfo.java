package com.ss.client.model.location;

import java.util.Arrays;

import rlib.util.VarTable;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

/**
 * Модель описания локации.
 * 
 * @author Ronn
 */
public class LocationInfo {

	public static final String VECTOR_STAR = "vectorStar";
	public static final String AMBIENT_COLOR = "ambientColor";
	public static final String STAR_LIGHT_COLOR = "starLightColor";
	public static final String SHADOW_LIGHTCOLOR = "shadowLightColor";
	public static final String LIGHTS = "lights";
	public static final String BACKGROUND = "background";
	public static final String ID = "id";

	/** информация о космическом фоне */
	protected final BackgroundInfo backgroundInfo;

	/** характеристики звезд в локации */
	protected final LightInfo[] lights;

	/** позиция звезды в локации */
	private final Vector3f vectorStar;

	/** цвет света усиливанрия звезд */
	protected final ColorRGBA starLightColor;
	/** цвет света в тени */
	protected final ColorRGBA shadowLightColor;
	/** цвет подсветки фона локации */
	protected final ColorRGBA ambientColor;

	/** ид локации */
	protected final int id;

	public LocationInfo(final VarTable vars) {
		this.id = vars.getInteger(ID);
		this.backgroundInfo = vars.getGeneric(BACKGROUND, BackgroundInfo.class);
		this.lights = vars.getGeneric(LIGHTS, LightInfo[].class);
		this.vectorStar = vars.getGeneric(VECTOR_STAR, Vector3f.class);
		this.shadowLightColor = vars.getGeneric(SHADOW_LIGHTCOLOR, ColorRGBA.class);
		this.starLightColor = vars.getGeneric(STAR_LIGHT_COLOR, ColorRGBA.class);
		this.ambientColor = vars.getGeneric(AMBIENT_COLOR, ColorRGBA.class);
	}

	/**
	 * @return цвет подсветки фона локации.
	 */
	public ColorRGBA getAmbientColor() {
		return ambientColor;
	}

	/**
	 * @return информация о текстурах фона.
	 */
	public final BackgroundInfo getBackgroundInfo() {
		return backgroundInfo;
	}

	/**
	 * @return ид локации.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * @return характеристики звезд в локации.
	 */
	public LightInfo[] getLights() {
		return lights;
	}

	/**
	 * @return цвет подсветки тени.
	 */
	public ColorRGBA getShadowLightColor() {
		return shadowLightColor;
	}

	/**
	 * @return цвет подсветки света звезд.
	 */
	public ColorRGBA getStartLightColor() {
		return starLightColor;
	}

	/**
	 * @return позиция звезды в локации.
	 */
	public Vector3f getVectorStar() {
		return vectorStar;
	}

	@Override
	public String toString() {
		return "LocationInfo backgroundInfo = " + backgroundInfo + ",  lights = " + Arrays.toString(lights) + ",  id = " + id;
	}
}
