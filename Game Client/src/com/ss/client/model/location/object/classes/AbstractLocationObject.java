package com.ss.client.model.location.object.classes;

import com.ss.client.model.AbstractSpaceObject;
import com.ss.client.model.location.object.LocationObject;
import com.ss.client.model.location.object.LocationObjectView;
import com.ss.client.template.LocationObjectTemplate;

/**
 * Базовая реализация локационного объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractLocationObject<V extends LocationObjectView> extends AbstractSpaceObject<LocationObjectTemplate, V> implements LocationObject {

	public AbstractLocationObject(long objectId, LocationObjectTemplate template) {
		super(objectId, template);
	}

	@Override
	public boolean isNeedIndicator() {
		return false;
	}
}
