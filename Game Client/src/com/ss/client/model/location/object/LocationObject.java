package com.ss.client.model.location.object;

import com.ss.client.model.SpaceObject;

/**
 * Интерфейс для реализации локационного объекта.
 * 
 * @author Ronn
 */
public interface LocationObject extends SpaceObject {

}
