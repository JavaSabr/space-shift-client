package com.ss.client.model.location.object.classes;

import com.ss.client.model.location.object.view.AsteroidView;
import com.ss.client.template.LocationObjectTemplate;

/**
 * Реализация локационного астеройда.
 * 
 * @author Ronn
 */
public class Asteroid extends AbstractLocationObject<AsteroidView> {

	public Asteroid(long objectId, LocationObjectTemplate template) {
		super(objectId, template);
	}
}
