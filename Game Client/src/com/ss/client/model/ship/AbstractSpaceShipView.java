package com.ss.client.model.ship;

import rlib.util.array.Array;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.ss.client.model.AbstractSpaceObjectView;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ModuleView;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.template.ship.ShipTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Базовая модель внешности корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceShipView<T extends ShipTemplate, S extends SpaceShip> extends AbstractSpaceObjectView<T, S> implements SpaceShipView {

	public AbstractSpaceShipView(final Spatial spatial, final T template) {
		super(spatial, template);
		getNode().setName(SPACE_SHIP_NODE);
	}

	@Override
	public void onHit(Vector3f point, String target, LocalObjects local) {
		super.onHit(point, target, local);

		S owner = getOwner();

		if(owner == null) {
			LOGGER.warning(getClass(), "not found owner.");
			return;
		}

		ModuleSystem system = owner.getModuleSystem();

		if(system == null) {
			LOGGER.warning(getClass(), "not found module system.");
			return;
		}

		Array<Module> modules = system.getModules(local.getNextModuleList());

		for(Module module : modules.array()) {

			if(module == null) {
				break;
			}

			ModuleView view = module.getView();

			if(view == null) {
				continue;
			}

			view.onHit(point, target, local);
		}
	}
}
