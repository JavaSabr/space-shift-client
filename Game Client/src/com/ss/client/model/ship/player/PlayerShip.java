package com.ss.client.model.ship.player;

import com.jme3.math.Vector2f;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.AbstractSpaceShip;
import com.ss.client.template.ship.PlayerShipTemplate;

/**
 * Модель корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShip extends AbstractSpaceShip<PlayerShipTemplate, PlayerShipView> {

	/** вектор разворота */
	private final Vector2f vectorRotation;

	/** текущая цель корабля */
	private SpaceObject target;

	/** количество опыта игрока */
	private long exp;

	/** текущее кол-во энергии */
	private int currentEnergy;
	/** максимальный уровень энергии */
	private int maxEnergy;
	/** текущее кол-во прочности */
	private int currentStrength;
	/** максимальный уровень прочности */
	private int maxStrength;
	/** скорость генерации энергии */
	private int energyRegen;

	public PlayerShip(final long objectId, final PlayerShipTemplate template) {
		super(objectId, template);

		this.vectorRotation = new Vector2f();
	}

	/**
	 * @return текущее кол-во энергии.
	 */
	public int getCurrentEnergy() {
		return currentEnergy;
	}

	/**
	 * @return текущее кол-во прочности.
	 */
	public int getCurrentStrength() {
		return currentStrength;
	}

	/**
	 * @return скорость генерации энергии.
	 */
	public int getEnergyRegen() {
		return energyRegen;
	}

	/**
	 * @return количество опыта игрока.
	 */
	public long getExp() {
		return exp;
	}

	/**
	 * @return максимальный уровень энергии.
	 */
	public int getMaxEnergy() {
		return maxEnergy;
	}

	/**
	 * @return максимальный уровень прочности.
	 */
	public int getMaxStrength() {
		return maxStrength;
	}

	@Override
	public PlayerShip getPlayerShip() {
		return this;
	}

	/**
	 * @return текущая цель корабля.
	 */
	public SpaceObject getTarget() {
		return target;
	}

	/**
	 * @return вектор разворота.
	 */
	public Vector2f getVectorRotation() {
		return vectorRotation;
	}

	@Override
	public boolean isPlayerShip() {
		return true;
	}

	@Override
	public boolean isVisibleOnMap() {
		final GameUIController controller = GameUIController.getInstance();
		return this == controller.getPlayerShip();
	}

	/**
	 * @param currentEnergy текущее кол-во энергии.
	 */
	public void setCurrentEnergy(int currentEnergy) {
		this.currentEnergy = currentEnergy;
	}

	/**
	 * @param currentStrength текущее кол-во прочности.
	 */
	public void setCurrentStrength(int currentStrength) {
		this.currentStrength = currentStrength;
	}

	/**
	 * @param energyRegen скорость генерации энергии.
	 */
	public void setEnergyRegen(int energyRegen) {
		this.energyRegen = energyRegen;
	}

	/**
	 * @param exp количество опыта игрока.
	 */
	public void setExp(long exp) {
		this.exp = exp;
	}

	/**
	 * @param maxEnergy максимальный уровень энергии.
	 */
	public void setMaxEnergy(int maxEnergy) {
		this.maxEnergy = maxEnergy;
	}

	/**
	 * @param maxStrength максимальный уровень прочности.
	 */
	public void setMaxStrength(int maxStrength) {
		this.maxStrength = maxStrength;
	}

	/**
	 * @param target текущая цель корабля.
	 */
	public void setTarget(SpaceObject target) {
		this.target = target;
	}

	/**
	 * Вектор разворота.
	 */
	public void setVectorRotation(float x, float y) {
		this.vectorRotation.set(x, y);
	}

	@Override
	public String toString() {
		return "PlayerShip name = " + name;
	}
}
