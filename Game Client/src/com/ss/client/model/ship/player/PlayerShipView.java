package com.ss.client.model.ship.player;


import com.jme3.scene.Spatial;
import com.ss.client.model.ship.AbstractSpaceShipView;
import com.ss.client.template.ship.PlayerShipTemplate;

/**
 * Модель внешности корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipView extends AbstractSpaceShipView<PlayerShipTemplate, PlayerShip> {

	public PlayerShipView(final Spatial spatial, final PlayerShipTemplate template) {
		super(spatial, template);
	}
}
