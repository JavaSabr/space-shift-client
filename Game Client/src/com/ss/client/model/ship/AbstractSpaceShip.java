package com.ss.client.model.ship;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.gui.controller.game.hud.selector.SelectorType;
import com.ss.client.manager.UpdateObjectManager;
import com.ss.client.model.AbstractSpaceObject;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.space.VisibleType;
import com.ss.client.tasks.FlyTask;
import com.ss.client.tasks.RotationTask;
import com.ss.client.tasks.SyncFlyTask;
import com.ss.client.template.ship.ShipTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Базовая модель корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceShip<T extends ShipTemplate, A extends SpaceShipView> extends AbstractSpaceObject<T, A> implements SpaceShip {

	/** система модулей корабля */
	protected final ModuleSystem moduleSystem;

	/** задача для обработки полета */
	protected final FlyTask flyTask;
	/** задача для обработки поворота */
	protected final RotationTask rotateTask;
	/** задача по синхронизации полета */
	protected final SyncFlyTask syncFlyTask;

	/** направление корабля */
	protected final Vector3f direction;

	/** скорость разворота */
	protected float rotateSpeed;
	/** ускорение корабля */
	protected float flyAccel;
	/** текущая скорость полета корабля */
	protected float flyCurrentSpeed;
	/** максимальная скорость корабля */
	protected float flyMaxSpeed;

	/** процент прочности корабля */
	protected float strengthPercent;
	/** процент энергии корабля */
	protected float energyPercent;

	public AbstractSpaceShip(final long objectId, final T template) {
		super(objectId, template);

		this.moduleSystem = template.takeModuleSystem();
		this.moduleSystem.setOwner(this);
		this.direction = new Vector3f();
		this.flyTask = new FlyTask(this);
		this.rotateTask = new RotationTask(this);
		this.syncFlyTask = new SyncFlyTask(this);
	}

	@Override
	public void deleteMe() {
		flyTask.stop();
		rotateTask.stop();
		super.deleteMe();
	}

	@Override
	public void finalyze() {
		getModuleSystem().clear();
		setRotateSpeed(0);
		super.finalyze();
	}

	@Override
	public void fly(Vector3f location, float accel, float speed, float maxSpeed, float maxCurrentSpeed, boolean active, boolean force) {
		flyTask.update(location, accel, speed, maxSpeed, maxCurrentSpeed, active, force);
	}

	@Override
	public Vector3f getDirection() {
		return direction;
	}

	@Override
	public float getEnergyPercent() {
		return energyPercent;
	}

	@Override
	public float getFlyAccel() {
		return flyAccel;
	}

	@Override
	public float getFlyCurrentSpeed() {
		return flyCurrentSpeed;
	}

	@Override
	public float getFlyMaxSpeed() {
		return flyMaxSpeed;
	}

	@Override
	public ModuleSystem getModuleSystem() {
		return moduleSystem;
	}

	@Override
	public float getRotateSpeed() {
		return rotateSpeed;
	}

	@Override
	public SelectorType getSelectorType() {
		return SelectorType.SPACE_SHIP;
	}

	@Override
	public SpaceShip getSpaceShip() {
		return this;
	}

	@Override
	public float getStrengthPercent() {
		return strengthPercent;
	}

	@Override
	public Quaternion getTargetRotation() {
		return rotateTask.getEndRotation();
	}

	@Override
	public VisibleType getVisibleType() {
		return VisibleType.LOCAL;
	}

	@Override
	public boolean isActiveEngine() {
		return flyTask.isActive();
	}

	@Override
	public boolean isRotation() {
		return rotateTask.isRotation();
	}

	@Override
	public boolean isSpaceShip() {
		return true;
	}

	@Override
	public void reinit() {
		super.reinit();
	}

	@Override
	public void rotate(LocalObjects local, Quaternion start, final Quaternion target, float step, float done, boolean infinity, boolean force) {
		rotateTask.update(local, start, target, step, done, infinity, force);
	}

	@Override
	public void rotate(Quaternion target, float modiff) {
		rotateTask.update(target, modiff);
	}

	@Override
	public void setEnergyPercent(float percent) {
		this.energyPercent = percent;
	}

	@Override
	public void setFlyAccel(float accel) {
		this.flyAccel = accel;
	}

	@Override
	public void setFlyCurrentSpeed(float currentSpeed) {
		this.flyCurrentSpeed = currentSpeed;
	}

	@Override
	public void setFlyMaxSpeed(float flyMaxSpeed) {
		this.flyMaxSpeed = flyMaxSpeed;
	}

	@Override
	public void setRotateSpeed(float rotateSpeed) {
		this.rotateSpeed = rotateSpeed;
	}

	@Override
	public void setRotation(final Quaternion rotate) {
		rotate.getRotationColumn(2, direction);
		super.setRotation(rotate);
	}

	@Override
	public void setStrengthPercent(float percent) {
		this.strengthPercent = percent;
	}

	@Override
	public void spawnMe() {
		super.spawnMe();

		UpdateObjectManager updateObjectManager = UpdateObjectManager.getInstance();
		updateObjectManager.addObject(this);

		long currentTime = Game.getCurrentTime();

		rotateTask.reinit(currentTime);
		flyTask.reinit(currentTime);
		syncFlyTask.reinit(currentTime);
	}

	@Override
	public void stopRotate() {
		rotateTask.stop();
	}

	@Override
	public void teleportTo(Vector3f position, Quaternion rotation, LocalObjects local) {

		rotateTask.stop();
		flyTask.stop();
		syncFlyTask.stop();

		super.teleportTo(position, rotation, local);

		long currentTime = Game.getCurrentTime();

		rotateTask.reinit(currentTime);
		flyTask.reinit(currentTime);
		syncFlyTask.reinit(currentTime);
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {
		rotateTask.update(local, currentTime);
		flyTask.update(local, currentTime);
		syncFlyTask.update(local, currentTime);
		return false;
	}
}
