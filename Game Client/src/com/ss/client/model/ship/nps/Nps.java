package com.ss.client.model.ship.nps;

import com.ss.client.model.ship.SpaceShip;

/**
 * Интерфейс для реализации Nps.
 * 
 * @author Ronn
 */
public interface Nps extends SpaceShip {
}
