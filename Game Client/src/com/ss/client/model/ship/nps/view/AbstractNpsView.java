package com.ss.client.model.ship.nps.view;


import com.jme3.scene.Spatial;
import com.ss.client.model.ship.AbstractSpaceShipView;
import com.ss.client.model.ship.nps.Nps;
import com.ss.client.model.ship.nps.NpsView;
import com.ss.client.template.ship.NpsTemplate;

/**
 * Базовая реализация внешности Nps.
 * 
 * @author Ronn
 */
public abstract class AbstractNpsView extends AbstractSpaceShipView<NpsTemplate, Nps> implements NpsView {

	public AbstractNpsView(Spatial spatial, NpsTemplate template) {
		super(spatial, template);
	}
}
