package com.ss.client.model.ship.nps.classes;

import com.ss.client.model.ship.AbstractSpaceShip;
import com.ss.client.model.ship.nps.Nps;
import com.ss.client.model.ship.nps.NpsView;
import com.ss.client.template.ship.NpsTemplate;

/**
 * Базовая реализация Nps.
 * 
 * @author Ronn
 */
public class AbstractNps extends AbstractSpaceShip<NpsTemplate, NpsView> implements Nps {

	public AbstractNps(long objectId, NpsTemplate template) {
		super(objectId, template);
		setName(template.getName());
	}

	@Override
	public int getLevel() {
		return template.getLevel();
	}

	@Override
	public void reinit() {
		super.reinit();
		setName(template.getName());
	}
}
