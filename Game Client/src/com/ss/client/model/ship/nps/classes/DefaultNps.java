package com.ss.client.model.ship.nps.classes;

import com.ss.client.template.ship.NpsTemplate;

/**
 * Базовая реализация Nps.
 * 
 * @author Ronn
 */
public class DefaultNps extends AbstractNps {

	public DefaultNps(long objectId, NpsTemplate template) {
		super(objectId, template);
	}
}
