package com.ss.client.model.state;

import rlib.logging.Logger;
import rlib.logging.Loggers;

import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppState;

/**
 * Дополнительная прослойка для реализации стадий.
 * 
 * @author Ronn
 */
public abstract class AbstractState extends AbstractAppState {

	protected static final Logger LOGGER = Loggers.getLogger(AppState.class);
}
