package com.ss.client.model.state;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import com.ss.client.Game;
import com.ss.client.gui.InterfaceId;
import com.ss.client.gui.model.BackgroundSound;
import com.ss.client.model.NodeRegion;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.table.BackgroundSoundTable;

/**
 * Стадия ангара с выбором используемого корабля.
 * 
 * @author Ronn
 */
public class HangarState extends AbstractAppState implements NodeRegion {

	private static final Logger LOGGER = Loggers.getLogger(HangarState.class);

	private static final Game GAME = Game.getInstance();

	/** позиция камеры в этой стадии */
	private static final Vector3f CAMERA_LOCATION = new Vector3f(-3.2067614F, 32.400135F, 106.27154F);
	/** разворот камеры в этой стадии */
	private static final Quaternion CAMERA_ROTATION = new Quaternion(1.5971136E-4F, 0.99945307F, -0.032708548F, 0.0048801787F);

	/** точка вращение выбранного корабля */
	private static final Vector3f CENTER = new Vector3f(0F, 30F, 0F);

	/** позиции платформ */
	private static final Vector3f[] POINTS = {
		new Vector3f(0, 5F, -142.465F),
		new Vector3f(66.883F, 5F, -125.789F),
		new Vector3f(-66.883F, 5F, -125.789F),
		new Vector3f(129.117F, 5F, -60.208F),
		new Vector3f(-129.117F, 5F, -60.208F)
	};

	/** направление кораблей на платформах */
	private static final Quaternion[] QUATERNIONS = {
		new Quaternion(0F, 0F, 0F, 1F),
		new Quaternion(0F, -0.15F, 0F, 1F),
		new Quaternion(0F, 0.15F, 0F, 1F),
		new Quaternion(0F, -0.3F, 0F, 1F),
		new Quaternion(0F, 0.3F, 0F, 1F)
	};

	private static final String HANGAR_BACKGROUND_EFFECT_1 = "hangar_1";
	private static final String HANGAR_BACKGROUND_EFFECT_2 = "hangar_2";

	/** узел этой стадии */
	private final Node stateNode;
	/** сцена ангара */
	private final Node scene;

	/** список кораблей игрока */
	private final Array<PlayerShip> playerShips;

	/** выбранный корабль */
	private PlayerShip selected;

	/** фоновые звуки */
	private BackgroundSound[] backgroundSounds;

	public HangarState() {
		this.playerShips = Arrays.toArray(PlayerShip.class, POINTS.length);

		final AssetManager asset = GAME.getAssetManager();

		scene = (Node) asset.loadModel("scenes/hangar.j3o");
		stateNode = new Node("Hangar");

		final TextureKey key = new TextureKey("textures/hangar/space.png");
		key.setGenerateMips(true);
		key.setAsCube(true);

		final Texture texture = asset.loadTexture(key);

		stateNode.attachChild(SkyFactory.createSky(asset, texture, texture, texture, texture, texture, texture));
		stateNode.attachChild(scene);
	}

	/**
	 * Добавление нового корабля.
	 * 
	 * @param playerShip
	 * @return успешно ли добавлен корабль.
	 */
	public boolean addPlayerShip(final PlayerShip playerShip) {
		final Array<PlayerShip> playerShips = getPlayerShips();

		if(playerShips.size() < POINTS.length) {
			playerShip.setParentNode(scene);
			playerShip.setLocation(POINTS[playerShips.size()], Vector3f.ZERO);
			playerShip.setRotation(QUATERNIONS[playerShips.size()]);
			playerShip.show();
			playerShip.getView().setLocation(POINTS[playerShips.size()]);
			playerShip.getView().setRotation(QUATERNIONS[playerShips.size()]);
			playerShips.add(playerShip);
			return true;
		}

		selectedPlayerShip(0);

		return false;
	}

	@Override
	public void addVisibleObject(final SpaceObject object) {
	}

	@Override
	public void cleanup() {
		try {
			playerShipClear();

			GAME.getRootNode().detachChild(stateNode);

			for(final BackgroundSound backgroundSound : backgroundSounds)
				backgroundSound.stop();

			backgroundSounds = null;

			super.cleanup();
		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	@Override
	public Node getNode() {
		return scene;
	}

	/**
	 * @return список кораблей на аккаунте.
	 */
	public final Array<PlayerShip> getPlayerShips() {
		return playerShips;
	}

	/**
	 * @return выбранный корабль.
	 */
	public PlayerShip getSelected() {
		return selected;
	}

	@Override
	public void initialize(final AppStateManager stateManager, final Application app) {
		try {

			super.initialize(stateManager, app);

			GAME.getRootNode().attachChild(stateNode);
			GAME.gotoScreen(InterfaceId.HANGAR);

			// game.getFlyByCamera().setEnabled(true);
			// game.getFlyByCamera().setMoveSpeed(100);

			final Camera camera = GAME.getCamera();
			camera.setLocation(CAMERA_LOCATION);
			camera.setRotation(CAMERA_ROTATION);

			final BackgroundSoundTable soundTable = BackgroundSoundTable.getInstance();

			backgroundSounds = Arrays.toGenericArray(soundTable.getSound(HANGAR_BACKGROUND_EFFECT_1), soundTable.getSound(HANGAR_BACKGROUND_EFFECT_2));

			for(final BackgroundSound backgroundSound : backgroundSounds) {
				backgroundSound.play();
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * Очистка от набора кораблей игрока.
	 */
	public void playerShipClear() {

		final Array<PlayerShip> playerShips = getPlayerShips();

		for(final PlayerShip playerShip : playerShips) {
			playerShip.hide();
			playerShip.deleteMe();
		}

		playerShips.clear();
	}

	@Override
	public void removeVisibleObject(final SpaceObject object) {
	}

	/**
	 * Установка выбранного корабля.
	 * 
	 * @param index индекс корабля.
	 */
	public void selectedPlayerShip(int index) {
		final Array<PlayerShip> playerShips = getPlayerShips();

		if(index == -1) {
			if(selected != null) {
				index = playerShips.indexOf(selected);

				selected.setLocation(POINTS[index], Vector3f.ZERO);
				selected.setRotation(QUATERNIONS[index]);

				selected.getView().setLocation(POINTS[playerShips.size()]);
				selected.getView().setRotation(QUATERNIONS[playerShips.size()]);
				selected = null;
			}
		} else {
			if(selected != null) {
				final int i = playerShips.indexOf(selected);

				selected.setLocation(POINTS[i], Vector3f.ZERO);
				selected.setRotation(QUATERNIONS[i]);

				selected.getView().setLocation(POINTS[playerShips.size()]);
				selected.getView().setRotation(QUATERNIONS[playerShips.size()]);
				selected = null;
			}

			selected = playerShips.get(index);
			selected.setLocation(CENTER, Vector3f.ZERO);
		}
	}

	@Override
	public void update(final float tpf) {
		super.update(tpf);

		final Array<PlayerShip> playerShips = getPlayerShips();

		for(final PlayerShip ship : playerShips.array()) {

			if(ship == null)
				break;
		}

		final PlayerShip selected = getSelected();

		if(selected != null)
			selected.getView().getNode().rotate(0, 0.02F, 0);
	}
}
