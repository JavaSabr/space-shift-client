package com.ss.client.model.state;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.light.Light;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import com.ss.client.Config;
import com.ss.client.Game;
import com.ss.client.gui.InterfaceId;
import com.ss.client.gui.controller.camera.CameraController;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.model.BackgroundSound;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.jme.post.effect.StartLightEffect;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.location.BackgroundInfo;
import com.ss.client.model.location.BackgroundInfo.BackgroundSide;
import com.ss.client.model.location.LightInfo;
import com.ss.client.model.location.LocationInfo;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestInterface;
import com.ss.client.table.BackgroundSoundTable;
import com.ss.client.table.LocationTable;
import com.ss.client.util.Initializable;
import com.ss.client.util.LocalObjects;

/**
 * Стадия самой уже игры.
 * 
 * @author Ronn
 */
public class GameState extends AbstractAppState implements Initializable {

	private static final Logger LOGGER = Loggers.getLogger(GameState.class);

	private static final Game GAME = Game.getInstance();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final LocationTable LOCATION_TABLE = LocationTable.getInstance();
	private static final BackgroundSoundTable SOUND_TABLE = BackgroundSoundTable.getInstance();
	private static final Network NETWORK = Network.getInstance();

	/** список источников света в локации */
	private final Array<Light> lights;
	/** камера экрана */
	private final Camera camera;
	/** узел для размещения планет/звезд */
	private final Node gravityState;
	/** узел для размещения остальных объектов */
	private final Node objectState;
	/** котроллер звездалета */
	private final GameUIController controller;

	/** последняя локация, которую загружали */
	private LocationInfo lastLocation;
	/** фон локации */
	private Spatial background;

	/** управляемый игроком корабль */
	private PlayerShip playerShip;

	/** фоновая музыка в игре */
	private BackgroundSound backgroundSound;

	public GameState() {
		this.camera = GAME.getCamera();
		this.controller = GameUIController.getInstance();
		this.lights = Arrays.toArray(Light.class);

		// настрока узлов
		this.gravityState = new Node(getClass().getName());
		this.objectState = new Node(getClass().getName());
		this.gravityState.attachChild(objectState);
	}

	@Override
	public void cleanup() {
		try {

			super.cleanup();

			if(backgroundSound != null) {
				backgroundSound.stop();
				backgroundSound = null;
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * Создние фона локации.
	 * 
	 * @return новый фон для локации.
	 */
	private Spatial createBackground(final LocationInfo location, final AssetManager asset) {

		if(false) {
			return SkyFactory.createSky(asset, "sphere_map.png", true);
		}
		final BackgroundInfo background = lastLocation.getBackgroundInfo();

		final Texture west = asset.loadTexture(background.getKey(BackgroundSide.WEST));
		final Texture east = asset.loadTexture(background.getKey(BackgroundSide.EAST));
		final Texture north = asset.loadTexture(background.getKey(BackgroundSide.NORTH));
		final Texture south = asset.loadTexture(background.getKey(BackgroundSide.SOUTH));
		final Texture up = asset.loadTexture(background.getKey(BackgroundSide.UP));
		final Texture down = asset.loadTexture(background.getKey(BackgroundSide.DOWN));

		if(Config.DEV_DEBUG_CLIENT_PACKETS) {
			LOGGER.warning(this, west + "\n" + east + "\n" + north + "\n" + south + "\n" + up + "\n" + down);
		}

		return SkyFactory.createSky(asset, west, east, north, south, up, down, Vector3f.UNIT_XYZ, 1000);
	}

	/**
	 * @return фон локации.
	 */
	public Spatial getBackground() {
		return background;
	}

	/**
	 * @return камера.
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @return узел для планет.
	 */
	public Node getGravityState() {
		return gravityState;
	}

	/**
	 * @return последняя активированная локация.
	 */
	public LocationInfo getLastLocation() {
		return lastLocation;
	}

	/**
	 * @return список источников света в локации.
	 */
	public Array<Light> getLights() {
		return lights;
	}

	/**
	 * @return узел для обычных объектов.
	 */
	public Node getObjectState() {
		return objectState;
	}

	/**
	 * @return корабль игрока.
	 */
	public final PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * Переход к указанной локации.
	 * 
	 * @param locationId ид новой локации.
	 */
	public void gotoLocation(final int locationId) {
		GAME.syncLock();
		try {

			final Node stateNode = getGravityState();
			final Array<Light> lights = getLights();

			LocationInfo lastLocation = getLastLocation();

			for(Light light : getLights()) {
				stateNode.removeLight(light);
			}

			lastLocation = LOCATION_TABLE.getLocation(locationId);

			if(lastLocation == null) {
				return;
			}

			final LightInfo[] lightInfos = lastLocation.getLights();

			setLastLocation(lastLocation);

			Vector3f main = null;

			for(LightInfo light : lightInfos) {

				Light result = null;

				switch(light.getLightType()) {
					case DIRECTION: {

						DirectionalLight directionalLight = new DirectionalLight();
						directionalLight.setDirection(light.getPosition());
						result = directionalLight;

						if(light.isMain()) {
							main = light.getPosition().mult(-300000);
						}

						break;
					}
					default: {
						break;
					}
				}

				if(result != null) {
					result.setColor(light.getColor());
					stateNode.addLight(result);
					lights.add(result);
				}
			}

			Spatial background = getBackground();

			if(background != null) {
				stateNode.detachChild(background);
			}

			background = createBackground(lastLocation, GAME.getAssetManager());
			stateNode.attachChild(background);
			setBackground(background);

			StartLightEffect starLight = StartLightEffect.getInstance();
			starLight.updateOffset(main);
			starLight.setEnabled(main != null);

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void initialize(final AppStateManager stateManager, final Application app) {
		super.initialize(stateManager, app);

		LocalObjects local = LocalObjects.get();

		try {

			GAME.getRootNode().attachChild(gravityState);

			backgroundSound = SOUND_TABLE.getNextBackground();

			if(backgroundSound != null) {
				backgroundSound.play();
			}

			GAME.gotoScreen(InterfaceId.GAME);

			NETWORK.sendPacketToGameServer(RequestInterface.getInstance());

			Array<PostEffect> effects = GAME.getPostEffects();

			for(PostEffect effect : effects.array()) {
				effect.activate(local);
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * @param background фон локации.
	 */
	public void setBackground(Spatial background) {
		this.background = background;
	}

	/**
	 * @param lastLocation последняя активированная локация.
	 */
	public void setLastLocation(final LocationInfo lastLocation) {
		this.lastLocation = lastLocation;
	}

	/**
	 * Активирование корабля игрока.
	 */
	public void setPlayerShip(final PlayerShip playerShip) {
		GAME.syncLock();
		try {

			playerShip.setParentNode(getObjectState());

			this.playerShip = playerShip;

			controller.activate(this);

			Array<PostEffect> effects = GAME.getPostEffects();

			for(PostEffect effect : effects.array()) {
				effect.setPlayerShip(playerShip);
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void update(final float tpf) {
		super.update(tpf);

		final LocalObjects local = LocalObjects.get();
		final PlayerShip ship = getPlayerShip();

		if(ship != null) {
			CameraController controller = CameraController.getInstance();
			controller.update();
		}

		final Array<SpaceObject> objects = SPACE_LOCATION.getObjects();
		final SpaceObject[] array = objects.array();

		for(SpaceObject object : array) {

			if(object == null) {
				break;
			}

			object.updateVisibleTo(ship);
		}

		Array<PostEffect> effects = GAME.getPostEffects();

		for(PostEffect effect : effects.array()) {
			effect.update(local);
		}
	}
}
