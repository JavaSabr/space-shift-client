package com.ss.client.model.gravity;

import com.ss.client.model.SpaceObjectView;

/**
 * Интерфейс для реализации внешности гравитационного объекта.
 * 
 * @author Ronn
 */
public interface GravityObjectView extends SpaceObjectView {

}
