package com.ss.client.model.gravity;


import com.jme3.material.Material;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import com.ss.client.model.AbstractSpaceObjectView;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Базовая модель внешности гравитационного объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractGravityObjectView<T extends GravityObject, E extends GravityObjectTemplate> extends AbstractSpaceObjectView<E, T> implements GravityObjectView {

	public AbstractGravityObjectView(final Spatial spatial, final E template) {
		super(spatial, template);

		// debug
		{
			Sphere sphere = new Sphere(50, 50, template.getRadius());
			debug = new Geometry("debug", sphere);
			debug.setMaterial(new Material(GAME.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md"));
		}
	}
}
