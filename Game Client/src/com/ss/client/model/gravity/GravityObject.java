package com.ss.client.model.gravity;

import rlib.util.array.Array;

import com.jme3.math.Quaternion;
import com.ss.client.model.ObjectContainer;
import com.ss.client.model.SpaceObject;

/**
 * Интерфейс для реализации гравитационных объектов.
 * 
 * @author Ronn
 */
public interface GravityObject extends SpaceObject, ObjectContainer {

	/**
	 * @param object добавить объект в гравитационное поле.
	 */
	public void addChild(GravityObject object);

	/**
	 * @return объекты в гравитационном поле.
	 */
	public Array<GravityObject> getChilds();

	/**
	 * @return дистанция от центра.
	 */
	public int getDistance();

	/**
	 * @return разворот орбиты.
	 */
	public Quaternion getOrbitalRotation();

	/**
	 * @return в чьем гравитацинном поле находится.
	 */
	public GravityObject getParent();

	/**
	 * @return радиус объекта.
	 */
	public int getRadius();

	/**
	 * @return время вращения вокруг своей орбиты.
	 */
	public long getTurnAroundTime();

	/**
	 * @return время вращения вокруг своей оси.
	 */
	public int getTurnTime();

	/**
	 * @param object цудалить объект из гравитационного поля.
	 */
	public void removeChild(GravityObject object);

	/**
	 * Установка параметров гравитационного объекта.
	 * 
	 * @param radius радиус объекта.
	 * @param turnAroundTime время оборота вокруг орбиты.
	 * @param turnTime время оборота вокруг оси.
	 * @param distance дистанция от центра.
	 * @param orbitalRotation разворот орбиты.
	 */
	public void setOrbit(int radius, long turnAroundTime, int turnTime, int distance, Quaternion rotation);

	/**
	 * @param object в чьем гравитацинном поле находится.
	 */
	public void setParent(GravityObject object);

	/**
	 * Обновление положения на орбите.
	 * 
	 * @param done процент выполнности оборота.
	 */
	public void updatePosition(double done);
}
