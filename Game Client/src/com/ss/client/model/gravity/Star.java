package com.ss.client.model.gravity;

import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель звезды.
 * 
 * @author Ronn
 */
public class Star extends AbstractGravityObject<GravityObjectTemplate, StartView> {

	public Star(final long objectId, final GravityObjectTemplate template) {
		super(objectId, template);
	}
}
