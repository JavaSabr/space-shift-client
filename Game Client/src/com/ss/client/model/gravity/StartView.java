package com.ss.client.model.gravity;

import com.jme3.light.PointLight;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель внешности звезды.
 * 
 * @author Ronn
 */
public class StartView extends AbstractGravityObjectView<Star, GravityObjectTemplate> {

	/** свет звезды */
	protected PointLight light;

	public StartView(final Spatial spatial, final GravityObjectTemplate template) {
		super(spatial, template);

		light = new PointLight();
		light.setRadius(template.getBrightness());
		light.setColor(light.getColor().mult(0.7F));
	}

	@Override
	public void addToNode(final Node rootNode) {
		GAME.syncLock();
		try {
			rootNode.addLight(light);

			super.addToNode(rootNode);
		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void removeToNode(final Node rootNode) {
		GAME.syncLock();
		try {
			rootNode.removeLight(light);

			super.removeToNode(rootNode);
		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public void setLocation(final Vector3f loc) {
		light.setPosition(loc);
		super.setLocation(loc);
	}
}
