package com.ss.client.model.gravity;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;

/**
 * Перечисление типов гравитационных объектов.
 * 
 * @author Ronn
 */
public enum GravityObjectType implements SpaceObjectType {
	ASTEROID(Asteroid.class, AsteroidView.class),
	PLANET(Planet.class, PlanetView.class),
	MOON(Planet.class, PlanetView.class),
	STAR(Star.class, StartView.class);

	/** список типов грави объектов */
	private static final GravityObjectType[] VALUES = values();

	/**
	 * Получение типа гравитационного объекта по его индексу.
	 * 
	 * @param index индекс типа грави объекта.
	 * @return тип грави объекта.
	 */
	public static final GravityObjectType valueOf(final int index) {
		return VALUES[index];
	}

	/** тип гравитационного объекта */
	private Class<? extends GravityObject> instanceClass;
	/** тип внешности гравитационного объекта */
	private Class<? extends GravityObjectView> appearanceClass;

	private GravityObjectType(final Class<? extends GravityObject> instanceClass, final Class<? extends GravityObjectView> appearanceClass) {
		this.instanceClass = instanceClass;
		this.appearanceClass = appearanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return appearanceClass;
	}
}
