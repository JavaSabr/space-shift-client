package com.ss.client.model.gravity;

import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель астеройда.
 * 
 * @author Ronn
 */
public class Asteroid extends AbstractGravityObject<GravityObjectTemplate, GravityObjectView> {

	public Asteroid(long objectId, GravityObjectTemplate template) {
		super(objectId, template);
	}
}
