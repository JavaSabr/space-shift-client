package com.ss.client.model.station;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;

/**
 * Перечисление типов космических станций.
 * 
 * @author Ronn
 */
public enum SpaceStationType implements SpaceObjectType {
	BASE_STATION(BaseStation.class, BaseStationAppearance.class);

	private static final SpaceStationType[] VALUES = values();

	/**
	 * Получение типа космической станции по индексу типа.
	 * 
	 * @param index индекс типа станции.
	 * @return тип космической станции.
	 */
	public static final SpaceStationType valueOf(final int index) {
		return VALUES[index];
	}

	/** тип объектов */
	private Class<? extends SpaceStation> instanceClass;
	/** тип внешности */
	private Class<? extends SpaceStationView> appearanceClass;

	private SpaceStationType(final Class<? extends SpaceStation> instanceClass, final Class<? extends SpaceStationView> appearanceClass) {
		this.instanceClass = instanceClass;
		this.appearanceClass = appearanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return appearanceClass;
	}
}
