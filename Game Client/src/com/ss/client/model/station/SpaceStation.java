package com.ss.client.model.station;

import com.ss.client.model.gravity.GravityObject;

/**
 * Интерфейс для реализации космической станции.
 * 
 * @author Ronn
 */
public interface SpaceStation extends GravityObject {

}
