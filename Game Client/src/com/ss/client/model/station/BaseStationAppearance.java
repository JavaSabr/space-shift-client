package com.ss.client.model.station;


import com.jme3.scene.Spatial;
import com.ss.client.template.StationTemplate;

/**
 * Модель внешности базовой космической станции.
 * 
 * @author Ronn
 */
public class BaseStationAppearance extends AbstractSpaceStationView<BaseStation> {

	public BaseStationAppearance(final Spatial spatial, final StationTemplate template) {
		super(spatial, template);
	}
}
