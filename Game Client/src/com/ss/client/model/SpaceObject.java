package com.ss.client.model;

import rlib.util.pools.Foldable;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.gui.controller.game.hud.selector.SelectorType;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.model.module.Module;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceSector;
import com.ss.client.model.space.VisibleType;
import com.ss.client.model.util.UpdateableObject;
import com.ss.client.template.ObjectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации космического объекта.
 * 
 * @author Ronn
 */
public interface SpaceObject extends Foldable, UpdateableObject, Comparable<SpaceObject> {

	/**
	 * Удалить из виду.
	 */
	public void decayMe();

	/**
	 * Удаление объекта из мира.
	 */
	public void deleteMe();

	/**
	 * Обработка разрушения объекта.
	 */
	public void destruct();

	/**
	 * Получение дистанции между текущим и указанным объектом.
	 * 
	 * @param object объект, до которого считаем дистанцию.
	 * @return дистанция между объектами.
	 */
	public float distanceTo(SpaceObject object);

	/**
	 * Получение дистанции между текущим объектом и позицией.
	 * 
	 * @param location интересуемая позиция.
	 * @return дистанция между объектом и позицией..
	 */
	public float distanceTo(Vector3f location);

	/**
	 * @return текущий сектор объекта.
	 */
	public SpaceSector getCurrentSector();

	/**
	 * @return модель для дебага.
	 */
	@Deprecated
	public Spatial getDebug();

	/**
	 * @return статус дружелюбности объекта в игроку.
	 */
	public FriendStatus getFriendStatus();

	/**
	 * @return уровень объекта.
	 */
	public int getLevel();

	/**
	 * @return позиция объекта в мире.
	 */
	public Vector3f getLocation();

	/**
	 * @return максимальный размер объекта.
	 */
	public int getMaxSize();

	/**
	 * @return модуль.
	 */
	public Module getModule();

	/**
	 * @return название корабля.
	 */
	public String getName();

	/**
	 * @return уникальный ид объекта.
	 */
	public long getObjectId();

	/**
	 * @return пространство, в котором находится объект.
	 */
	public Node getParentNode();

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip();

	/**
	 * @return получение разворот объекта.
	 */
	public Quaternion getRotation();

	/**
	 * @return тип селектора для данного объекта.
	 */
	public SelectorType getSelectorType();

	/**
	 * @return размер модели по X.
	 */
	public int getSizeX();

	/**
	 * @return размер модели по Y.
	 */
	public int getSizeY();

	/**
	 * @return размер модели по Z.
	 */
	public int getSizeZ();

	/**
	 * @return космический предмет.
	 */
	public SpaceItem getSpaceItem();

	/**
	 * @return космический корабль.
	 */
	public SpaceShip getSpaceShip();

	/**
	 * @return шаблон объекта.
	 */
	public <T extends ObjectTemplate> T getTemplate();

	/**
	 * @return ид шаблона объекта.
	 */
	public int getTemplateId();

	/**
	 * @return 3Д вид объекта.
	 */
	public SpaceObjectView getView();

	/**
	 * @return дистанция отображения.
	 */
	public int getVisibleRange();

	/**
	 * @return тип отображения.
	 */
	public VisibleType getVisibleType();

	/**
	 * Скрыть объект.
	 */
	public void hide();

	/**
	 * Проверка на нахождение объекта на нужном расстоянии.
	 * 
	 * @param object проверяемый объект.
	 * @param range проверяемая дистанция.
	 * @return находится ли в пределах указанной дистанции.
	 */
	public boolean isInRange(SpaceObject object, int range);

	/**
	 * Проверка на нахождение точки на нужном расстоянии.
	 * 
	 * @param point проверяемая точка.
	 * @param range проверяемая дистанция.
	 * @return находится ли в пределах указанной дистанции.
	 */
	public boolean isInRange(Vector3f point, int range);

	/**
	 * @return является ли объект модулем.
	 */
	public boolean isModule();

	/**
	 * @return нужен ли для этого объекта индикатор.
	 */
	public boolean isNeedIndicator();

	/**
	 * @return является ли объект игровым кораблем..
	 */
	public boolean isPlayerShip();

	/**
	 * @return является ли космическим предметом.
	 */
	public boolean isSpaceItem();

	/**
	 * @return является ли объект космическим кораблем.
	 */
	public boolean isSpaceShip();

	/**
	 * @return отспавнен ли объект.
	 */
	public boolean isSpawned();

	/**
	 * @return виден ли объект.
	 */
	public boolean isVisible();

	/**
	 * @return видим ли этот объект на карте.
	 */
	public boolean isVisibleOnMap();

	/**
	 * @param sector текущий сектор объекта.
	 */
	public void setCurrentSector(SpaceSector sector);

	/**
	 * @param friendStatus статус дружелюбности объекта в игроку.
	 */
	public void setFriendStatus(FriendStatus friendStatus);

	/**
	 * @param level уровень объекта.
	 */
	public void setLevel(int level);

	/**
	 * Установка позиции в мире.
	 * 
	 * @param point координаты новой позиции.
	 * @param changed изменение позиции относительно старой.
	 */
	public void setLocation(Vector3f point, Vector3f changed);

	/**
	 * @param name название корабля.
	 */
	public void setName(String name);

	/**
	 * @param objectId уникальный ид объекта.
	 */
	public void setObjectId(long objectId);

	/**
	 * @param stateNode пространство, в котором находится объект.
	 */
	public void setParentNode(Node stateNode);

	/**
	 * Установка нужного разворота объекту.
	 * 
	 * @param newRotation новый разворот.
	 */
	public void setRotation(Quaternion newRotation);

	/**
	 * @param visible виден ли объект.
	 */
	public void setVisible(boolean visible);

	/**
	 * Отобразить объект.
	 */
	public void show();

	/**
	 * Добавить в мир.
	 */
	public void spawnMe();

	/**
	 * Телепортирование объекта на указанную позицию с указанным разворотом.
	 * 
	 * @param position новая позиция объекта.
	 * @param rotation новый разворот объекта.
	 * @param local контейнер локальных объектов.
	 */
	public void teleportTo(Vector3f position, Quaternion rotation, LocalObjects local);

	/**
	 * Обновить объект.
	 */
	public void update(LocalObjects local, long currentTime, float tpf);

	/**
	 * Обновление видимости объекта для игрока.
	 * 
	 * @param ship игрок, относительно которого рассчитывается видимость.
	 */
	public void updateVisibleTo(PlayerShip ship);
}
