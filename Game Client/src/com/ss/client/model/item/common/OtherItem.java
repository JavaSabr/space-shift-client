package com.ss.client.model.item.common;

import com.ss.client.template.item.CommonItemTemplate;

/**
 * Реализация различных предметов.
 * 
 * @author Ronn
 */
public class OtherItem extends CommonItem {

	public OtherItem(final long objectId, final CommonItemTemplate template) {
		super(objectId, template);
	}
}
