package com.ss.client.model.item;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.model.AbstractSpaceObjectView;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;
import com.ss.client.template.item.ItemTemplate;

/**
 * Базовая реализация внешности космического итема.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceItemView<T extends ItemTemplate, O extends SpaceItem> extends AbstractSpaceObjectView<T, O> implements SpaceItemView {

	public AbstractSpaceItemView(final Spatial spatial, final T template) {
		super(spatial, template);
	}

	@Override
	public void finishTeleportEffect() {

		O owner = getOwner();

		if(owner == null) {
			return;
		}

		Node parentNode = owner.getParentNode();

		if(parentNode == null) {
			return;
		}

		GraficEffectTemplate effectTemplate = template.getFinishTeleportEffect();

		if(effectTemplate != null) {
			GraficEffect effect = effectTemplate.takeInstance();
			effect.start(parentNode, owner.getLocation());
		}
	}

	@Override
	public String getIcon() {
		return template.getIcon();
	}

	@Override
	public void startTeleportEffect() {

		O owner = getOwner();

		if(owner == null) {
			return;
		}

		Node parentNode = owner.getParentNode();

		if(parentNode == null) {
			return;
		}

		GraficEffectTemplate effectTemplate = template.getStartTeleportEffect();

		if(effectTemplate != null) {
			GraficEffect effect = effectTemplate.takeInstance();
			effect.start(parentNode, owner.getLocation());
		}
	}
}
