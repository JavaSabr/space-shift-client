package com.ss.client.model.item;

import com.ss.client.model.SpaceObjectView;

/**
 * Интерфейс для реализации внешности космического предмета.
 * 
 * @author Ronn
 */
public interface SpaceItemView extends SpaceObjectView {

	/**
	 * Запуск эффекта завершения телепорта.
	 */
	public void finishTeleportEffect();

	/**
	 * @return путь к иконке предмета.
	 */
	public String getIcon();

	/**
	 * Запуск эффекта старта телепорта.
	 */
	public void startTeleportEffect();
}
