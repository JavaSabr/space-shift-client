package com.ss.client.model.module;

import com.ss.client.model.SpaceObjectView;

/**
 * Интерфейс для реализации внешности модуля.
 * 
 * @author Ronn
 */
public interface ModuleView extends SpaceObjectView {

}
