package com.ss.client.model.module.view;


import com.jme3.scene.Spatial;
import com.ss.client.model.AbstractSpaceObjectView;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ModuleView;
import com.ss.client.template.ModuleTemplate;

/**
 * Базовая модкль внешности модулей.
 * 
 * @author Ronn
 */
public abstract class AbstractModuleView<M extends Module> extends AbstractSpaceObjectView<ModuleTemplate, M> implements ModuleView {

	public AbstractModuleView(final Spatial spatial, final ModuleTemplate template) {
		super(spatial, template);
	}
}
