package com.ss.client.model.module.view;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Spatial;
import com.ss.client.model.module.classes.EngineModule;
import com.ss.client.model.module.info.EngineFireInfo;
import com.ss.client.template.ModuleTemplate;

/**
 * Моделт внешности модуля двигателей.
 * 
 * @author Ronn
 */
public class EngineModuleView extends AbstractModuleView<EngineModule> {

	/** эффект работы двигателя */
	private Spatial[] fires;

	public EngineModuleView(final Spatial spatial, final ModuleTemplate template) {
		super(spatial, template);

		final AssetManager assetNamager = GAME.getAssetManager();

		final EngineFireInfo[] fireInfos = template.getEngineFireInfos();
		final Spatial[] fires = new Spatial[fireInfos.length];

		for(int i = 0, length = fireInfos.length; i < length; i++) {
			final EngineFireInfo fireInfo = fireInfos[i];
			fires[i] = fireInfo.createModel(assetNamager);
		}

		setFires(fires);
	}

	/**
	 * @return набор струй двигателей.
	 */
	public Spatial[] getFires() {
		return fires;
	}

	/**
	 * Спрятать огонь двигателя.
	 */
	public void hideFire() {
		removeSpatial(getFires());
	}

	/**
	 * @param fires набор струй двигателей.
	 */
	public void setFires(final Spatial[] fires) {
		this.fires = fires;
	}

	/**
	 * Отобразить огонь двигателя.
	 */
	public void showFire() {
		addSpatial(getFires());
	}
}
