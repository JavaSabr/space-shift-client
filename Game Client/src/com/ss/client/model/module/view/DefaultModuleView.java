package com.ss.client.model.module.view;


import com.jme3.scene.Spatial;
import com.ss.client.model.module.classes.DefaultModule;
import com.ss.client.template.ModuleTemplate;

/**
 * Стандартная внешность модуля.
 * 
 * @author Ronn
 */
public class DefaultModuleView extends AbstractModuleView<DefaultModule> {

	public DefaultModuleView(Spatial spatial, ModuleTemplate template) {
		super(spatial, template);
	}
}
