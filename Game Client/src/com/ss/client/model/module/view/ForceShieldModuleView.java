package com.ss.client.model.module.view;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.ss.client.jme.control.ForceShieldControl;
import com.ss.client.jme.util.NodeUtils;
import com.ss.client.model.module.classes.ForceShieldModule;
import com.ss.client.model.module.info.ForceShieldInfo;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Реализация внешности модуля силового щита.
 * 
 * @author Ronn
 */
public class ForceShieldModuleView extends AbstractModuleView<ForceShieldModule> {

	public static final String FORCE_SHIELD = "ForceShield";

	/** контролер силового поля */
	private ForceShieldControl forceShield;

	public ForceShieldModuleView(Spatial spatial, ModuleTemplate template) {
		super(spatial, template);

		ForceShieldInfo info = template.getForceShieldInfo();

		if(info == null) {
			LOGGER.warning(getClass(), "not found forceShieldInfo for " + template);
			return;
		}

		AssetManager assetManager = GAME.getAssetManager();

		ForceShieldControl forceShield = new ForceShieldControl(assetManager, 0.3F);

		Spatial model = assetManager.loadModel(info.getModelKey());
		model.setName(FORCE_SHIELD);
		model.addControl(forceShield);

		forceShield.setEffectSize(info.getEffectSize());
		forceShield.setColor(info.getEffectColor());
		forceShield.setVisibility(0);
		forceShield.setTexture(assetManager.loadTexture(info.getTextureKey()));

		setForceShield(forceShield);

		NodeUtils.addToNode(getNode(), model);
	}

	/**
	 * @return контролер силового поля.
	 */
	public ForceShieldControl getForceShield() {
		return forceShield;
	}

	@Override
	public void onHit(Vector3f point, String target, LocalObjects local) {
		super.onHit(point, target, local);

		if(FORCE_SHIELD == target) {
			getForceShield().registerHit(point, local);
		}
	}

	/**
	 * @param forceShield контролер силового поля.
	 */
	public void setForceShield(ForceShieldControl forceShield) {
		this.forceShield = forceShield;
	}
}
