package com.ss.client.model.module.view;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Spatial;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.module.classes.BlasterModule;
import com.ss.client.model.module.info.BlasterRayInfo;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Модель внешности бластерного модуля.
 * 
 * @author Ronn
 */
public class BlasterModuleView extends AbstractModuleView<BlasterModule> {

	/** список графических эффектов */
	private GraficEffectTemplate[] explosions;

	/** модели лучей бластеров */
	private Spatial[] rays;

	public BlasterModuleView(final Spatial spatial, final ModuleTemplate template) {
		super(spatial, template);

		final AssetManager assetNamager = GAME.getAssetManager();

		final BlasterRayInfo[] rayInfos = template.getBlasterRayInfos();

		final GraficEffectTemplate[] effects = new GraficEffectTemplate[rayInfos.length];
		final Spatial[] rays = new Spatial[rayInfos.length];

		for(int i = 0, length = rayInfos.length; i < length; i++) {

			final BlasterRayInfo rayInfo = rayInfos[i];

			final Spatial ray = assetNamager.loadModel(rayInfo.getModel());

			final Material material = assetNamager.loadMaterial(rayInfo.getMaterial());

			final RenderState render = material.getAdditionalRenderState();

			render.setDepthTest(true);
			render.setDepthWrite(false);

			ray.setMaterial(material);
			ray.setQueueBucket(Bucket.Transparent);
			ray.setLocalScale(rayInfo.getScale());

			rays[i] = ray;
			effects[i] = rayInfo.getExplosion();
		}

		setRays(rays);
		setEffects(effects);
	}

	/**
	 * Создание эффекты взрыва от бластера.
	 * 
	 * @param index индекс бластера.
	 * @return модель эффекта.
	 */
	public GraficEffect createExplosion(int index) {
		return explosions[index].takeInstance();
	}

	/**
	 * Модель для выстрела бластера.
	 * 
	 * @param index индекс модели.
	 * @return модель бластера.
	 */
	public Spatial getModel(int index) {
		return rays[index].clone();
	}

	/**
	 * @param effects список шаблонов эффектов взрывов.
	 */
	protected void setEffects(GraficEffectTemplate[] effects) {
		this.explosions = effects;
	}

	/**
	 * @param rays список моделей лучей бластеров.
	 */
	protected void setRays(Spatial[] rays) {
		this.rays = rays;
	}
}
