package com.ss.client.model.module.system;

import com.jme3.math.Vector3f;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ModuleType;

import de.lessvoid.nifty.elements.Element;

/**
 * Модель слота для модуля.
 * 
 * @author Ronn
 */
public final class ModuleSlot {

	/** тип слота */
	private final ModuleType type;

	/** позиция модуля */
	private final Vector3f offset;

	/** индекс слота */
	private final int index;

	/** модуль в слоте */
	private Module module;

	/** ячека в панели */
	private Element cell;

	public ModuleSlot(final int index, final SlotInfo info) {
		this.index = index;
		this.type = info.getType();
		this.offset = info.getOffset();
	}

	/**
	 * @return ячейка модуля на панели.
	 */
	public Element getCell() {
		return cell;
	}

	/**
	 * @return индекс слота.
	 */
	public final int getIndex() {
		return index;
	}

	/**
	 * @return модуль.
	 */
	public final Module getModule() {
		return module;
	}

	/**
	 * @return тип слота.
	 */
	public final ModuleType getType() {
		return type;
	}

	/**
	 * @return пустой ли слот.
	 */
	public boolean isEmpty() {
		return module == null;
	}

	/**
	 * @param cell ячейка модуля на панели.
	 */
	public void setCell(final Element cell) {
		this.cell = cell;
	}

	/**
	 * @param module модуль.
	 */
	public final void setModule(final Module module) {
		this.module = module;

		if(module != null) {
			module.setLocation(offset, Vector3f.ZERO);
			module.getView().setLocation(offset);
		}
	}
}
