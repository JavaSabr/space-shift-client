package com.ss.client.model.module.system;

import java.util.concurrent.locks.Lock;

import rlib.concurrent.Locks;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;

import com.ss.client.model.module.Module;
import com.ss.client.model.ship.SpaceShip;

/**
 * Базовая система модулей корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractModuleSystem<O extends SpaceShip> implements ModuleSystem {

	protected static final Logger LOGGER = Loggers.getLogger(ModuleSystem.class);

	/** блокировщик */
	protected final Lock lock;

	/** набор слотов */
	protected ModuleSlot[] slots;

	/** владелец системы */
	protected O owner;

	public AbstractModuleSystem() {
		this.lock = Locks.newLock();
	}

	@Override
	public void addModule(final Module module) {
		// TODO Auto-generated method stub
	}

	@Override
	public void clear() {

		final ModuleSlot[] slots = getSlots();

		for(int i = 0, length = slots.length; i < length; i++) {

			final ModuleSlot slot = slots[i];

			if(slot.isEmpty()) {
				continue;
			}

			final Module module = slot.getModule();
			module.deleteMe();

			slot.setModule(null);
		}
	}

	@Override
	public void finalyze() {
		clear();
	}

	@Override
	public Module getModule(final long objectId) {

		lock();
		try {

			final ModuleSlot[] slots = getSlots();

			for(int i = 0, length = slots.length; i < length; i++) {

				final ModuleSlot slot = slots[i];
				final Module module = slot.getModule();

				if(module == null || module.getObjectId() != objectId) {
					continue;
				}

				return module;
			}

		} finally {
			unlock();
		}

		return null;
	}

	@Override
	public Array<Module> getModules(Array<Module> container) {

		lock();
		try {

			final ModuleSlot[] slots = getSlots();

			for(int i = 0, length = slots.length; i < length; i++) {

				final ModuleSlot slot = slots[i];
				final Module module = slot.getModule();

				if(module == null) {
					continue;
				}

				container.add(module);
			}

		} finally {
			unlock();
		}

		return container;
	}

	@Override
	public SpaceShip getOwner() {
		return owner;
	}

	@Override
	public ModuleSlot[] getSlots() {
		return slots;
	}

	@Override
	public void init(final SlotInfo[] infos) {

		slots = new ModuleSlot[infos.length];

		for(int i = 0, length = infos.length; i < length; i++) {
			slots[i] = new ModuleSlot(i, infos[i]);
		}
	}

	@Override
	public void lock() {
		lock.lock();
	}

	@Override
	public void reinit() {
	}

	@Override
	public void removeModule(final Module module) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setModule(final int index, final Module module) {

		ModuleSlot[] slots = getSlots();

		lock();
		try {

			ModuleSlot slot = slots[index];

			if(!slot.isEmpty()) {
				LOGGER.warning(getClass(), "found duplicate module for slot " + slot);
			}

			module.addToShip(getOwner());
			slot.setModule(module);

		} finally {
			unlock();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setOwner(final SpaceShip owner) {
		this.owner = (O) owner;
	}

	@Override
	public void unlock() {
		lock.unlock();
	}
}
