package com.ss.client.model.module.system;

import rlib.util.Synchronized;
import rlib.util.array.Array;
import rlib.util.pools.Foldable;

import com.ss.client.model.module.Module;
import com.ss.client.model.ship.SpaceShip;

/**
 * Интерфейс для реализации системы модулей корабля.
 * 
 * @author Ronn
 */
public interface ModuleSystem extends Foldable, Synchronized {

	/**
	 * Установка модуля в корабль.
	 */
	public void addModule(Module module);

	/**
	 * Очистка системы от модулей.
	 */
	public void clear();

	/**
	 * @param objectId уникальный ид модуля.
	 * @return модуль с указанным ид.
	 */
	public Module getModule(long objectId);

	/**
	 * Получение всех установленных модулей.
	 * 
	 * @param container контейнер модулей.
	 * @return список всех модулей.
	 */
	public Array<Module> getModules(Array<Module> container);

	/**
	 * @return владелец системы.
	 */
	public SpaceShip getOwner();

	/**
	 * @return все слоты системы.
	 */
	public ModuleSlot[] getSlots();

	/**
	 * Инициализация системы.
	 */
	public void init(SlotInfo[] info);

	/**
	 * Удаление модуля из системы.
	 */
	public void removeModule(Module module);

	/**
	 * Установка модуля в конкретный слот.
	 * 
	 * @param index иднекс слота.
	 * @param module модуль.
	 */
	public void setModule(int index, Module module);

	/**
	 * @param owner владелец системы.
	 */
	public void setOwner(SpaceShip owner);
}
