package com.ss.client.model.module;

import com.ss.client.model.AbstractSpaceObject;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.template.ModuleTemplate;

/**
 * Базовая модель реализации модуля.
 * 
 * @author Ronn
 */
public abstract class AbstractModule<A extends ModuleView> extends AbstractSpaceObject<ModuleTemplate, A> implements Module {

	/** владелец модуля */
	protected SpaceShip owner;

	/** активирован ли модуль */
	protected boolean activated;

	public AbstractModule(final long objectId, final ModuleTemplate template) {
		super(objectId, template);
	}

	@Override
	public void activate() {
		setActivated(true);
	}

	@Override
	public void active(final int active) {
		if(isActivated() && active == 0) {
			deactivate();
		} else if(!isActivated() && active == 1) {
			activate();
		}
	}

	@Override
	public void addListener(final Object listener) {
	}

	@Override
	public void addToShip(final SpaceShip ship) {

		final SpaceShipView view = ship.getView();
		view.addSpatial(getView().getNode());

		setParentNode(view.getNode());
		setOwner(ship);
	}

	@Override
	public void deactivate() {
		setActivated(false);
	}

	@Override
	public String getIcon() {
		return template.getIcon();
	}

	@Override
	public ModuleType getModuleType() {
		return template.getType();
	}

	@Override
	public SpaceShip getOwner() {
		return owner;
	}

	@Override
	public boolean isActivated() {
		return activated;
	}

	@Override
	public void removeListener(final Object listener) {
	}

	@Override
	public void removeToShip(final SpaceShip ship) {

		final SpaceShipView view = ship.getView();
		view.removeSpatial(getView().getNode());

		setParentNode(null);
		setOwner(null);
	}

	@Override
	public void setActivated(final boolean activated) {
		this.activated = activated;
	}

	@Override
	public void setOwner(final SpaceShip owner) {
		this.owner = owner;
	}
}
