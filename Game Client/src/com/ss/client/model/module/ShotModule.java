package com.ss.client.model.module;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.shots.Shot;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс, для реализации стреляющего модуля.
 * 
 * @author Ronn
 */
public interface ShotModule extends Module {

	/**
	 * Cоздание эффекта для отображения попадания.
	 * 
	 * @param index индекс ствола модуля.
	 * @return новый эффект.
	 */
	public GraficEffect createExplosion(int index);

	/**
	 * Обработка самоуничтожение выстрела.
	 * 
	 * @param shot уничтожающийся выстрел.
	 * @param local контейнер локальных объектов.
	 */
	public void destruct(Shot shot, LocalObjects local);

	/**
	 * Получить экземпляр нового выстрела.
	 * 
	 * @param index индекс ствола модуля, из которого был выстрел.
	 * @return новый выстрел.
	 */
	public Shot getNextShot(int index);

	/**
	 * Отображение попадания укзаанного выстрела в указанный объект.
	 * 
	 * @param shot попавший выстрел.
	 * @param object объект попадания.
	 * @param onShield было ли попадание в щит.
	 * @param local контейнер локальных объектов.
	 */
	public void onHit(Shot shot, SpaceObject object, boolean onShield, LocalObjects local);
}
