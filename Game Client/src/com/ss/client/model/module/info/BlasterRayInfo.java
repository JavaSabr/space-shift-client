package com.ss.client.model.module.info;

import rlib.util.VarTable;

import com.jme3.math.Vector3f;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Модель информации, описывающего луч бластера.
 * 
 * @author Ronn
 */
public final class BlasterRayInfo {

	public static final String EXPLOSION = "explosion";
	public static final String ROTATE = "rotate";
	public static final String SCALE = "scale";
	public static final String OFFSET = "offset";
	public static final String MATERIAL = "material";
	public static final String MODEL = "model";

	/** адресс модели */
	private final String model;
	/** адресс материала */
	private final String material;

	/** маштаб */
	private final Vector3f scale;

	/** шаблон эффекта взрыва */
	private final GraficEffectTemplate explosion;

	public BlasterRayInfo(final VarTable vars) {
		this.model = vars.getString(MODEL);
		this.material = vars.getString(MATERIAL);
		this.scale = vars.getGeneric(SCALE, Vector3f.class);
		this.explosion = vars.get(EXPLOSION, GraficEffectTemplate.class, null);
	}

	/**
	 * @return шаблон эффекта взрыва.
	 */
	public GraficEffectTemplate getExplosion() {
		return explosion;
	}

	/**
	 * @return адресс материала.
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * @return адресс модели.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return маштаб.
	 */
	public Vector3f getScale() {
		return scale;
	}

	@Override
	public String toString() {
		return "BlasterRayInfo model = " + model + ",  material = " + material + ",  scale = " + scale;
	}
}