package com.ss.client.model.module.info;

import rlib.util.VarTable;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Spatial;

/**
 * Модель информации, описывающего огонь двигателя.
 * 
 * @author Ronn
 */
public final class EngineFireInfo {

	public static final String ROTATE = "rotate";
	public static final String SCALE = "scale";
	public static final String OFFSET = "offset";
	public static final String MATERIAL = "material";
	public static final String MODEL = "model";

	/** адресс модели */
	private final String model;
	/** адресс материала */
	private final String material;

	/** относительное положение от позиции модуля */
	private final Vector3f offset;
	/** маштаб */
	private final Vector3f scale;

	/** наклон струи */
	private final Quaternion rotate;

	public EngineFireInfo(final VarTable vars) {
		this.model = vars.getString(MODEL);
		this.material = vars.getString(MATERIAL);
		this.offset = vars.getGeneric(OFFSET, Vector3f.class);
		this.scale = vars.getGeneric(SCALE, Vector3f.class);
		this.rotate = vars.getGeneric(ROTATE, Quaternion.class);
	}

	/**
	 * Создание модели пламени.
	 * 
	 * @return новая модель.
	 */
	public Spatial createModel(AssetManager assetManager) {

		final Spatial model = assetManager.loadModel(getModel());
		final Material material = assetManager.loadMaterial(getMaterial());
		final RenderState render = material.getAdditionalRenderState();

		render.setDepthTest(true);
		render.setDepthWrite(false);

		model.setMaterial(material);
		model.setQueueBucket(Bucket.Transparent);
		model.setLocalRotation(getRotate());
		model.setLocalScale(getScale());
		model.setLocalTranslation(getOffset());

		return model;
	}

	/**
	 * @return адресс материала.
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * @return адресс модели.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return расположение относительно модуля.
	 */
	public Vector3f getOffset() {
		return offset;
	}

	/**
	 * @return наклон относительно модуля.
	 */
	public Quaternion getRotate() {
		return rotate;
	}

	/**
	 * @return маштаб.
	 */
	public Vector3f getScale() {
		return scale;
	}

	@Override
	public String toString() {
		return "EngineFireInfo model = " + model + ",  material = " + material + ",  offset = " + offset + ",  scale = " + scale + ",  rotate = " + rotate;
	}
}