package com.ss.client.model.module;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.SpaceShip;

/**
 * Интерфейс для реализации модуля.
 * 
 * @author Ronn
 */
public interface Module extends SpaceObject {

	/**
	 * Активировать модуль.
	 */
	public void activate();

	/**
	 * Обновить активность на указанную.
	 * 
	 * @param active флаг активности.
	 */
	public void active(int active);

	/**
	 * Добавить слушателя модуля.
	 */
	public void addListener(Object listener);

	/**
	 * Добавление в сам корабль.
	 */
	public void addToShip(SpaceShip ship);

	/**
	 * Деактивировать модуль.
	 */
	public void deactivate();

	/**
	 * @return иконка модуля.
	 */
	public String getIcon();

	/**
	 * @return тип модуля.
	 */
	public ModuleType getModuleType();

	/**
	 * @return владелец модуля.
	 */
	public SpaceShip getOwner();

	@Override
	public ModuleView getView();

	/**
	 * @return активный ли модуль.
	 */
	public boolean isActivated();

	/**
	 * Удалить слушателя модуля.
	 */
	public void removeListener(Object listener);

	/**
	 * Удаление из корабля.
	 */
	public void removeToShip(SpaceShip ship);

	/**
	 * @param activated активный ли модуль.
	 */
	public void setActivated(boolean activated);

	/**
	 * @param owner владелец модуля.
	 */
	public void setOwner(SpaceShip owner);
}
