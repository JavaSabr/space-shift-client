package com.ss.client.model.module.classes;

import com.ss.client.model.module.AbstractModule;
import com.ss.client.model.module.view.ForceShieldModuleView;
import com.ss.client.template.ModuleTemplate;

/**
 * Реализация модуля силового щита.
 * 
 * @author Ronn
 */
public class ForceShieldModule extends AbstractModule<ForceShieldModuleView> {

	public ForceShieldModule(long objectId, ModuleTemplate template) {
		super(objectId, template);
	}
}
