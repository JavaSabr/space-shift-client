package com.ss.client.model.module.classes;

import com.ss.client.model.module.AbstractModule;
import com.ss.client.model.module.view.EngineModuleView;
import com.ss.client.template.ModuleTemplate;

/**
 * Модель модуля двигателя.
 * 
 * @author Ronn
 */
public class EngineModule extends AbstractModule<EngineModuleView> {

	public EngineModule(final long objectId, final ModuleTemplate template) {
		super(objectId, template);
	}

	@Override
	public void activate() {
		super.activate();
		getView().showFire();
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getView().hideFire();
	}
}
