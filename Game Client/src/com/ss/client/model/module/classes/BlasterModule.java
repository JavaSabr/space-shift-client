package com.ss.client.model.module.classes;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.module.AbstractModule;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.module.view.BlasterModuleView;
import com.ss.client.model.module.view.ForceShieldModuleView;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.model.shots.BlasterShot;
import com.ss.client.model.shots.Shot;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Модель бластерного модуля.
 * 
 * @author Ronn
 */
public class BlasterModule extends AbstractModule<BlasterModuleView> implements ShotModule {

	private final FoldablePool<Shot> pool = Pools.newConcurrentFoldablePool(Shot.class);

	public BlasterModule(final long objectId, final ModuleTemplate template) {
		super(objectId, template);
	}

	@Override
	public GraficEffect createExplosion(int index) {

		BlasterModuleView view = getView();

		if(view != null) {
			return view.createExplosion(index);
		}

		return null;
	}

	@Override
	public void destruct(Shot shot, LocalObjects local) {

		if(!(shot instanceof BlasterShot)) {
			LOGGER.warning("incorrect shot " + shot);
			return;
		}

		SpaceShip owner = getOwner();

		if(owner == null) {
			LOGGER.warning(getClass(), "not found module owner.");
			return;
		}

		Node parentNode = owner.getParentNode();

		if(parentNode == null) {
			LOGGER.warning(getClass(), "not found parent node.");
			return;
		}

		GraficEffect explosion = createExplosion(shot.getIndex());

		if(explosion == null) {
			return;
		}

		explosion.start(parentNode, shot.getLocation());
	}

	@Override
	public Shot getNextShot(int index) {

		Shot shot = pool.take();

		if(shot == null) {
			shot = new BlasterShot();
		}

		shot.bind(pool);
		shot.setModel(getView().getModel(index));

		return shot;
	}

	@Override
	public void onHit(Shot shot, SpaceObject object, boolean onShield, LocalObjects local) {

		if(!(shot instanceof BlasterShot)) {
			LOGGER.warning("incorrect shot " + shot);
			return;
		}

		BlasterShot blasterShot = (BlasterShot) shot;

		Node parentNode = object.getParentNode();

		if(parentNode == null) {
			return;
		}

		GraficEffect explosion = createExplosion(blasterShot.getIndex());

		if(explosion == null) {
			return;
		}

		Vector3f startLoc = shot.getStartLoc();
		Vector3f contact = local.getNextVector();
		Vector3f direction = local.getNextVector();
		direction.set(blasterShot.getTargetLoc()).subtractLocal(startLoc).normalizeLocal();

		Ray ray = local.getNextRay();
		ray.setOrigin(startLoc);
		ray.setDirection(direction);

		SpaceObjectView view = object.getView();

		if(view == null) {
			return;
		}

		String targetNode = onShield ? ForceShieldModuleView.FORCE_SHIELD : SpaceShipView.SPACE_SHIP_NODE;
		view.getCollision(ray, contact, targetNode);
		view.onHit(contact, targetNode, local);

		explosion.start(parentNode, contact);
	}
}
