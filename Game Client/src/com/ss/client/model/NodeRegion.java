package com.ss.client.model;

import com.jme3.scene.Node;

/**
 * Интерфейс-маркер, говорящий, что класс имеет свой узел для 3д моделей.
 * 
 * @author Ronn
 */
public interface NodeRegion {

	/**
	 * Добавление видимого объекта.
	 * 
	 * @param object видимый объект.
	 */
	public void addVisibleObject(SpaceObject object);

	/**
	 * @return узел.
	 */
	public Node getNode();

	/**
	 * Удаление видимого объекта.
	 * 
	 * @param object уже не видимый объект.
	 */
	public void removeVisibleObject(SpaceObject object);
}
