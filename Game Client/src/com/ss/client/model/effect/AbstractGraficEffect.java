package com.ss.client.model.effect;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.Game;
import com.ss.client.jme.util.NodeUtils;
import com.ss.client.manager.UpdateGraficEffectManager;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Базовая реализация графического эффекта.
 * 
 * @author Ronn
 */
public abstract class AbstractGraficEffect implements GraficEffect {

	protected static final Logger LOGGER = Loggers.getLogger(GraficEffect.class);
	protected static final Game GAME = Game.getInstance();

	/** узел для размещения компонентов эффекта */
	protected final Node effectNode;

	/** контейнер обновляемых эффектов */
	protected Array<GraficEffect> container;

	/** время старта эфффекта */
	protected long startTime;

	/** состояние эффекта */
	protected int state;

	public AbstractGraficEffect(GraficEffectTemplate template) {
		this.effectNode = new Node(getClass().getName());
	}

	@Override
	public void bind(Array<GraficEffect> container) {
		setContainer(container);
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void finish() {

		setContainer(null);

		Node effectNode = getEffectNode();
		Node parentNode = effectNode.getParent();

		if(parentNode != null) {
			GAME.syncLock();
			try {
				NodeUtils.removeToNode(parentNode, effectNode);
			} finally {
				GAME.syncUnlock();
			}
		}
	}

	/**
	 * @return контейнер обновляемых эффектов.
	 */
	public Array<GraficEffect> getContainer() {
		return container;
	}

	public Node getEffectNode() {
		return effectNode;
	}

	/**
	 * @return время старта эффекта.
	 */
	public long getStartTime() {
		return startTime;
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param container контейнер обновляемых эффектов.
	 */
	public void setContainer(Array<GraficEffect> container) {
		this.container = container;
	}

	/**
	 * @param startTime время старта эффекта.
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	@Override
	public void setState(int state) {
		this.state = state;
	}

	@Override
	public void start(Node parentNode, Vector3f position) {

		GAME.syncLock();
		try {
			Node effectNode = getEffectNode();
			NodeUtils.addToNode(parentNode, effectNode);
			effectNode.setLocalTranslation(position);
		} finally {
			GAME.syncUnlock();
		}

		setStartTime(System.currentTimeMillis());
		setState(EFFECT_STATE_START);

		UpdateGraficEffectManager manager = UpdateGraficEffectManager.getInstance();
		manager.addGraficEffect(this);
	}

	@Override
	public void stop() {

		Array<GraficEffect> container = getContainer();

		if(container != null) {
			container.fastRemove(this);
			finish();
		}
	}

	@Override
	public String toString() {
		return "AbstractGraficEffect effectNode = " + effectNode + ",  state = " + state;
	}
}
