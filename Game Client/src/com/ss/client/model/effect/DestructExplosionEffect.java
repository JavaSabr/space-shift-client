package com.ss.client.model.effect;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.template.grafic.effect.DestructExplosionEffectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Реализация эффекта разрушающего взрыва.
 * 
 * @author Ronn
 */
public class DestructExplosionEffect extends AbstractGraficEffect {

	/** ожидающие эфекты */
	private final Array<ExplosionEffect> waitEffects;
	/** запущнные эффекты */
	private final Array<ExplosionEffect> startEffects;

	public DestructExplosionEffect(DestructExplosionEffectTemplate template) {
		super(template);

		this.waitEffects = Arrays.toArray(ExplosionEffect.class);
		this.startEffects = Arrays.toArray(ExplosionEffect.class);
	}

	@Override
	public void start(Node parentNode, Vector3f position) {
		// TODO Auto-generated method stub
		super.start(parentNode, position);
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {
		// TODO Auto-generated method stub
		return false;
	}
}
