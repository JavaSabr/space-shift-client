package com.ss.client.model.effect;

import rlib.util.array.Array;
import rlib.util.pools.Foldable;


import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации графического эффекта.
 * 
 * @author Ronn
 */
public interface GraficEffect extends Foldable {

	public static final int EFFECT_STATE_START = 0;

	/**
	 * Запоминание контейнера, в котором лежит эффект для обновления.
	 * 
	 * @param container контейнер эффектов.
	 */
	public void bind(Array<GraficEffect> container);

	/**
	 * Завершиние работы эффекта.
	 */
	public void finish();

	/**
	 * @return состояние эффекта.
	 */
	public int getState();

	/**
	 * @param state состояние эффекта.
	 */
	public void setState(int state);

	/**
	 * Запуск эффекта в указанном узле с указанными координатами.
	 * 
	 * @param parentNode узел, в котором нужно отобразить эффект.
	 * @param position местоположение эффекта.
	 */
	public void start(Node parentNode, Vector3f position);

	/**
	 * Остановка эффекта.
	 */
	public void stop();

	/**
	 * Обновление состояния эффекта.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @return завершен ли эффект.
	 */
	public boolean update(LocalObjects local, long currentTime);
}
