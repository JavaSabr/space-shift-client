package com.ss.client.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Анотация, указывающая, что этот класс исполльзуется рефлексией.
 * 
 * @author Ronn
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface ReflectionClass {

}
