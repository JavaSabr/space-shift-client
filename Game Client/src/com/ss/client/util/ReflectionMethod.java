package com.ss.client.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Анатация, помечающий метод, как метод вызываемый из рифлексии.
 * 
 * @author Ronn
 */
@Target(ElementType.METHOD)
public @interface ReflectionMethod {

}
