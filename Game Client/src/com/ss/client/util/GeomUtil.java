package com.ss.client.util;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.model.SpaceObject;

public class GeomUtil {

	private static final int VECTOR_UP = 1;

	public static boolean isInFron(SpaceObject object, SpaceObject target) {

		LocalObjects local = LocalObjects.get();

		Vector3f up = local.getNextVector();

		Quaternion rotation = object.getRotation();
		rotation.getRotationColumn(VECTOR_UP, up);

		return isInFron(object, target, up, local);
	}

	public static boolean isInFron(SpaceObject object, SpaceObject target, Vector3f up, LocalObjects local) {

		Vector3f diff = local.getNextVector();
		diff.set(target.getLocation());
		diff.subtractLocal(object.getLocation());

		Quaternion temp = local.getNextRotation();
		Quaternion rotation = object.getRotation();

		temp.lookAt(diff, up);

		float dot = Math.abs(rotation.dot(temp));

		return dot > 0.95;
	}

	public static boolean isInFron(SpaceObject object, SpaceObject target, Vector3f up, LocalObjects local, float exp) {

		Vector3f diff = local.getNextVector();
		diff.set(target.getLocation());
		diff.subtractLocal(object.getLocation());

		Quaternion temp = local.getNextRotation();
		Quaternion rotation = object.getRotation();

		temp.lookAt(diff, up);

		float dot = Math.abs(rotation.dot(temp));

		return dot > exp;
	}

	public static void lookAt(Quaternion quaternion, Vector3f direction, Vector3f up, LocalObjects local) {

		Vector3f first = local.getNextVector();
		first.set(direction).normalizeLocal();

		Vector3f second = local.getNextVector();
		second.set(up).crossLocal(direction).normalizeLocal();

		Vector3f thrid = local.getNextVector();
		thrid.set(direction).crossLocal(second).normalizeLocal();

		quaternion.fromAxes(second, thrid, first);
		quaternion.normalizeLocal();
	}

	public static Vector3f getVectorUp(SpaceObject object, Vector3f vector) {
		Quaternion rotation = object.getRotation();
		rotation.getRotationColumn(VECTOR_UP, vector);
		return vector;
	}
}
