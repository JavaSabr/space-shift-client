package com.ss.client.util;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

/**
 * Буффер для вычислений.
 * 
 * @author Ronn
 */
public final class LocalThread {

	private static final int DEFAULT_VECTOR_BUFFER_SIZE = 10;

	private static final int DEFAULT_QUATERNION_BUFFER_SIZE = 10;

	/**
	 * Таблица локальных экземпляров.
	 */
	private static final ThreadLocal<LocalThread> locals = new ThreadLocal<LocalThread>() {

		@Override
		protected LocalThread initialValue() {
			return new LocalThread();
		}
	};

	/**
	 * @return получить буфера для текущего потока.
	 */
	public static final LocalThread get() {
		return locals.get().reinit();
	}

	/** буффер векторов */
	private final Vector3f[] vectorBuffer;
	/** буфер кватернионов */
	private final Quaternion[] rotationBuffer;

	/** индекс след. свободного вектора */
	private int vectorIndex;
	/** иднекс след. свободного кватерниона */
	private int rotationIndex;

	public LocalThread() {
		this.vectorBuffer = new Vector3f[DEFAULT_VECTOR_BUFFER_SIZE];

		for(int i = 0, length = DEFAULT_VECTOR_BUFFER_SIZE; i < length; i++)
			vectorBuffer[i] = new Vector3f();

		this.rotationBuffer = new Quaternion[DEFAULT_QUATERNION_BUFFER_SIZE];

		for(int i = 0, length = DEFAULT_QUATERNION_BUFFER_SIZE; i < length; i++)
			rotationBuffer[i] = new Quaternion();
	}

	/**
	 * @return получаем след. свободный квантернион.
	 */
	public Quaternion getNextRotation() {
		return rotationBuffer[rotationIndex++];
	}

	/**
	 * @return получаем след. свободнвый вектор.
	 */
	public Vector3f getNextVector() {
		return vectorBuffer[vectorIndex++];
	}

	/**
	 * @return реинициализация буфера.
	 */
	private LocalThread reinit() {
		vectorIndex = 0;
		rotationIndex = 0;

		return this;
	}
}
