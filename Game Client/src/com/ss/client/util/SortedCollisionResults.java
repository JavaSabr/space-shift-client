package com.ss.client.util;

import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.Arrays;

import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;

/**
 * Реализация отсортированного контейнера коллизий.
 * 
 * @author Ronn
 */
public final class SortedCollisionResults extends CollisionResults {

	private static final ThreadLocal<SortedCollisionResults> LOCAL = new ThreadLocal<SortedCollisionResults>() {

		@Override
		protected SortedCollisionResults initialValue() {
			return new SortedCollisionResults();
		}
	};

	private static final ArrayComparator<CollisionResult> COMPARATOR = new ArrayComparator<CollisionResult>() {

		@Override
		protected int compareImpl(CollisionResult first, CollisionResult second) {
			return first.compareTo(second);
		}
	};

	public static final SortedCollisionResults get() {
		return LOCAL.get();
	}

	/** контейнер результатов колизии */
	private final Array<CollisionResult> container;

	public SortedCollisionResults() {
		this.container = Arrays.toArray(CollisionResult.class);
	}

	@Override
	public void addCollision(CollisionResult result) {
		container.add(result);
	}

	@Override
	public void clear() {
		container.clear();
	}

	@Override
	public CollisionResult getClosestCollision() {
		return container.first();
	}

	@Override
	public CollisionResult getCollision(int index) {
		return container.get(index);
	}

	@Override
	public CollisionResult getCollisionDirect(int index) {
		return container.get(index);
	}

	@Override
	public CollisionResult getFarthestCollision() {
		return container.last();
	}

	@Override
	public int size() {
		return container.size();
	}

	public void sort() {
		container.sort(COMPARATOR);
	}
}
