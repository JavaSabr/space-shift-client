package com.ss.client.util;

/**
 * Интерфейс с пометкой о инициализированности объекта.
 * 
 * @author Ronn
 */
public interface Initializable {

	/**
	 * @return инициализирован ли.
	 */
	public boolean isInitialized();
}
