package com.ss.client.util;

/**
 * Отложенная акетная задача, которая не могла быть выполена на момент получеия
 * пакета.
 * 
 * @author Ronn
 */
public interface PacketTask {

	/**
	 * @return завершео ли выполение задачи.
	 */
	public boolean execute(LocalObjects local, long currentTime);

	/**
	 * Идет сравнения задачи с проверяемым объектом.
	 * 
	 * @param object сравнивый объект.
	 * @return соответствует ли задача объекту.
	 */
	public boolean isMatches(Object object);
}
