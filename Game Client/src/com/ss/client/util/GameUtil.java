package com.ss.client.util;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.tools.SizeValue;

/**
 * Набор полезных утилит для разработки.
 * 
 * @author Ronn
 */
public abstract class GameUtil {

	private static final Table<IntKey, SizeValue> SIZE_PIXEL_TABLE = Tables.newIntegerTable();
	private static final Table<String, SizeValue> SIZE_ALL_TABLE = Tables.newObjectTable();

	private static final SizeValue[] SIZE_PERCENT_TABLE = new SizeValue[101];

	private static final ThreadLocal<SimpleDateFormat> LOCATE_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {

		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("HH:mm:ss:SSS");
		}
	};

	/**
	 * @param path путь к ресурсу.
	 * @return поток ввода.
	 */
	public static InputStream getInputStream(final String path) {
		return Object.class.getResourceAsStream(path);
	}

	public static SizeValue getPixelSize(final int size) {

		SizeValue value = SIZE_PIXEL_TABLE.get(size);

		if(value == null) {
			value = SizeValue.px(size);
			SIZE_PIXEL_TABLE.put(size, value);
		}

		return value;
	}

	public static SizeValue getPercentSize(final int size) {

		SizeValue value = SIZE_PERCENT_TABLE[size];

		if(value == null) {
			value = SizeValue.percent(size);
			SIZE_PERCENT_TABLE[size] = value;
		}

		return value;
	}

	public static SizeValue getSizeValue(String value) {

		SizeValue result = SIZE_ALL_TABLE.get(value);

		if(result == null) {
			result = new SizeValue(value);
			SIZE_ALL_TABLE.put(value, result);
		}

		return result;
	}

	/**
	 * Получение имя пользователя текущей системы.
	 * 
	 * @return имя пользователя системы.
	 */
	public static final String getUserName() {
		return System.getProperty("user.name");
	}

	/**
	 * Получение сдвинутой точки на нужную дистанцию.
	 * 
	 * @param first первая точка.
	 * @param second вторая точка.
	 * @param store контейнер результата.
	 * @param length дистанция сдвига.
	 */
	public static final void movePoint(final Vector3f first, final Vector3f second, final Vector3f store, final int length) {
		store.x = first.x + (second.x - first.x) * length;
		store.y = first.y + (second.y - first.y) * length;
		store.z = first.z + (second.z - first.z) * length;
	}

	/**
	 * Обновление позиции камеры.
	 * 
	 * @param camera камера, чью позицию надо изменить.
	 * @param object объект, за которым надо разместить камеру.
	 */
	public static final void updateCameraPosition(final Camera camera, final SpaceShip object) {

		final LocalThread local = LocalThread.get();

		final Vector3f loc = object.getLocation();
		final Vector3f cameraLoc = local.getNextVector();

		cameraLoc.set(object.getDirection());

		int offset = object.getSizeY();

		cameraLoc.multLocal(offset * -2F);
		cameraLoc.addLocal(loc);

		final Vector3f aiming = local.getNextVector();
		aiming.set(object.getDirection());
		aiming.multLocal(900);
		aiming.addLocal(loc);

		final Vector3f up = local.getNextVector();
		final Quaternion rotation = object.getRotation();

		rotation.getRotationColumn(1, up);

		offset = object.getSizeX();

		cameraLoc.addLocal(up.mult(offset * 1.1F, local.getNextVector()));

		camera.setLocation(cameraLoc);

		final GameUIController control = GameUIController.getInstance();

		if(control.getInputMode() == InputMode.INTERFACE_MODE)
			return;

		camera.setRotation(object.getRotation());
	}

	public static void waitInitialize(final Initializable initializable) {
		while(!initializable.isInitialized())
			try {
				Thread.sleep(100);
			} catch(final InterruptedException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Получение направление на разварот по координатам на экране.
	 * 
	 * @param playerShip корабль игрока.
	 * @param camera камера.
	 * @param local локальные объекты.
	 * @param x координата на экране.
	 * @param y координата на экране.
	 * @return направление по координатам на экране.
	 */
	public static Quaternion getTargetRotation(PlayerShip playerShip, Camera camera, LocalObjects local, int x, int y) {

		synchronized(camera) {

			Quaternion sourceRotation = local.getNextRotation();
			sourceRotation.set(camera.getRotation());
			Quaternion targetRotation = local.getNextRotation();
			targetRotation.set(playerShip.getRotation());

			Vector3f sourceLocation = local.getNextVector();
			sourceLocation.set(camera.getLocation());
			Vector3f targetLocation = local.getNextVector();
			targetLocation.set(playerShip.getLocation());
			Vector3f up = targetRotation.getRotationColumn(1, local.getNextVector());
			Vector3f result = local.getNextVector();

			camera.setRotation(targetRotation);
			camera.setLocation(targetLocation);
			try {

				float distance = camera.getViewToProjectionZ(50);
				camera.getWorldCoordinates(new Vector2f(x, y), distance, result);

				result.subtractLocal(targetLocation);
				targetRotation.lookAt(result, up);

				return targetRotation;

			} finally {
				camera.setRotation(sourceRotation);
				camera.setLocation(sourceLocation);
			}
		}
	}

	public static String timeFormat(long time) {
		SimpleDateFormat format = LOCATE_DATE_FORMAT.get();
		return format.format(new Date(time));
	}

	/**
	 * Проверка существования ресурса по указанному пути.
	 * 
	 * @param path путь к ресрсу.
	 * @return существуетли такой ресурс.
	 */
	public static boolean checkExists(String path) {

		Class<GameUtil> cs = GameUtil.class;

		return cs.getResourceAsStream(path) != null || cs.getResourceAsStream("/" + path) != null;
	}

	/**
	 * Есть ли узел с указанным названии.
	 */
	public static boolean hasNode(Spatial spatial, String nodeName) {

		int i = 0;

		for(Node parent = spatial.getParent(); parent != null; parent = parent.getParent(), i++) {
			if(parent.getName() == nodeName) {
				return true;
			}

			if(i > 0) {
				break;
			}
		}

		return false;
	}

	/**
	 * Видно ли на экране объект с такими экранными координатами.
	 * 
	 * @param position позиция на экране объекта.
	 * @param camera камера экрана
	 * @return видно ли на экране.
	 */
	public static boolean isVisibleOnScreen(Vector3f position, Camera camera) {

		int maxHeight = camera.getHeight();
		int maxWidth = camera.getWidth();

		boolean isBottom = position.getY() < 0;
		boolean isTop = position.getY() > maxHeight;
		boolean isLeft = position.getX() < 0;
		boolean isRight = position.getX() > maxWidth;

		return !isBottom && !isLeft && !isTop && !isRight && position.getZ() < 1F;
	}
}
