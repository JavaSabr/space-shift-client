package com.ss.client.gui.model;

import com.ss.client.gui.controller.position.ElementType;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Модель информации о позиции элемента.
 * 
 * @author Ronn
 */
public final class InterfaceInfo implements Foldable {

	private static final FoldablePool<InterfaceInfo> pool = Pools.newConcurrentFoldablePool(InterfaceInfo.class);

	/**
	 * информация о позиции элемента.
	 */
	public static InterfaceInfo newInstance(final ElementType type, final int x, final int y, final boolean minimized) {
		InterfaceInfo info = pool.take();

		if(info == null)
			info = new InterfaceInfo();

		info.type = type;
		info.x = x;
		info.y = y;
		info.minimized = minimized;

		return info;
	}

	/** тип элемента */
	private ElementType type;

	/** координата */
	private int x;
	/** координата */
	private int y;

	/** свернут ли элемент */
	private boolean minimized;

	private InterfaceInfo() {
		super();
	}

	@Override
	public void finalyze() {
		type = null;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return тип элемента.
	 */
	public ElementType getType() {
		return type;
	}

	/**
	 * @return координата.
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return координата.
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return свернут ли элемент.
	 */
	public boolean isMinimized() {
		return minimized;
	}

	@Override
	public void reinit() {
		minimized = false;
	}

	/**
	 * @param minimized свернут ли элемент.
	 */
	public void setMinimized(final boolean minimized) {
		this.minimized = minimized;
	}
}
