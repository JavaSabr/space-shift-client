package com.ss.client.gui.model;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

/**
 * Перечисление возможных разрешений экрана.
 * 
 * @author Ronn
 */
public class ScreenSize {

	public static final int SCREEN_SIZE_MIN_HEIGHT = 768;
	public static final int SCREEN_SIZE_MIN_WIDTH = 1024;

	/** таблица доступных расширений экрана */
	private static Table<String, ScreenSize> screenSizeTable = Tables.newObjectTable();

	/** список доступных разрешений */
	private static ScreenSize[] values;

	/**
	 * Инициализация списка доступных разрешений экрана.
	 */
	public static void init() {

		final GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		final DisplayMode[] modes = device.getDisplayModes();

		Array<ScreenSize> container = Arrays.toArraySet(ScreenSize.class);

		for(final DisplayMode mode : modes) {

			if(mode.getWidth() < SCREEN_SIZE_MIN_WIDTH || mode.getHeight() < SCREEN_SIZE_MIN_HEIGHT) {
				continue;
			}

			container.add(new ScreenSize(mode.getWidth(), mode.getHeight()));
		}

		container.add(new ScreenSize(SCREEN_SIZE_MIN_WIDTH, SCREEN_SIZE_MIN_HEIGHT));

		for(ScreenSize screenSize : container) {
			screenSizeTable.put(screenSize.size, screenSize);
		}

		values = container.toArray(new ScreenSize[container.size()]);
	}

	/**
	 * @param size строкое представление разрешения.
	 * @return ссылка на инстанс размера.
	 */
	public static ScreenSize sizeOf(final String size) {
		ScreenSize screenSize = screenSizeTable.get(size);
		return screenSize == null ? new ScreenSize(SCREEN_SIZE_MIN_WIDTH, SCREEN_SIZE_MIN_HEIGHT) : screenSize;
	}

	/**
	 * @return список доступных разрешений.
	 */
	public static ScreenSize[] values() {
		return values;
	}

	/** ширина экрана */
	private final int width;
	/** высота кэрана */
	private final int height;

	/** строковый вид */
	private final String size;

	private ScreenSize(final int width, final int height) {
		this.width = width;
		this.height = height;
		this.size = width + "x" + height;
	}

	@Override
	public boolean equals(Object obj) {

		if(this == obj) {
			return true;
		}

		if(obj == null) {
			return false;
		}

		if(getClass() != obj.getClass()) {
			return false;
		}

		ScreenSize other = (ScreenSize) obj;

		if(height != other.height) {
			return false;
		}

		if(width != other.width) {
			return false;
		}

		return true;
	}

	/**
	 * @return высота.
	 */
	public final int getHeight() {
		return height;
	}

	/**
	 * @return ширина.
	 */
	public final int getWidth() {
		return width;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + width;
		return result;
	}

	@Override
	public String toString() {
		return size;
	}
}
