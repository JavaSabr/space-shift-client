package com.ss.client.gui.model;

import de.lessvoid.nifty.controls.Draggable;
import de.lessvoid.nifty.controls.Droppable;
import de.lessvoid.nifty.controls.DroppableDropFilter;

/**
 * Филтр на проверку возможности вставлять элемент в элемент.
 * 
 * @author Ronn
 */
public class DropFilter implements DroppableDropFilter {

	private static final DropFilter instance = new DropFilter();

	public static DropFilter getInstance() {
		return instance;
	}

	@Override
	public boolean accept(final Droppable source, final Draggable draggable, final Droppable target) {
		return true;
	}
}
