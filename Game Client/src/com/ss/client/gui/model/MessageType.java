package com.ss.client.gui.model;

import com.ss.client.table.LangTable;

import de.lessvoid.nifty.tools.Color;

/**
 * Перечисление типов сообщений в чат.
 * 
 * @author Ronn
 */
public enum MessageType {
	MAIN("@interface:chatMessageMain@", "#FFFFFF", false),
	SYSTEM("@interface:chatMessageSystem@", "#ffef96", true),
	PARTY("@interface:chatMessageParty@", "#1229d5", true),
	FRACTION("@interface:chatMessageFraction@", "#24a61a", true),
	FEDERATION("@interface:chatMessageFederation@", "#00c8a0", true),
	ANNOUNCE("@interface:chatMessageAnnounce@", "#00d2ff", true),
	PRIVATE("@interface:chatMessagePrivate@", "3ff2323", true);

	/** цвет сообщения */
	private final Color color;
	/** название типа сообщения */
	private final String typeName;

	/** отображать ли тип сообщения */
	private final boolean visibleType;

	private MessageType(final String typeName, String color, boolean visibleType) {
		final LangTable langTable = LangTable.getInstance();
		this.typeName = langTable.getText(typeName);
		this.visibleType = visibleType;
		this.color = new Color(color);
	}

	/**
	 * @return цвет сообщения.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return название для типа.
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @return отображать ли тип сообщения.
	 */
	public boolean isVisibleType() {
		return visibleType;
	}
}
