package com.ss.client.gui.model.quest.reward.impl;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.builder.game.factory.QuestElementUIFactory;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Реализация награды за задание в виде получения опыта.
 * 
 * @author Ronn
 */
public class ExperienceQuestReward extends AbstractQuestReward {

	private static final FoldablePool<ExperienceQuestReward> POOL = Pools.newConcurrentFoldablePool(ExperienceQuestReward.class);

	public static final ExperienceQuestReward newInstance(long exp) {

		ExperienceQuestReward reward = POOL.take();

		if(reward == null) {
			reward = new ExperienceQuestReward();
		}

		reward.exp = exp;
		return reward;
	}

	/** получаемое кол-во опыта */
	private long exp;

	@Override
	public Element buildTo(Element parent, Nifty nifty, Screen screen, int order) {
		return QuestElementUIFactory.build(this, parent, nifty, screen, exp, order);
	}

	@Override
	public void finalyze() {
		super.finalyze();
		exp = 0;
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return получаемое кол-во опыта.
	 */
	public long getExp() {
		return exp;
	}
}
