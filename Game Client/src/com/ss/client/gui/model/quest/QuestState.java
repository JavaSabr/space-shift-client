package com.ss.client.gui.model.quest;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;
import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.model.quest.reward.QuestReward;

/**
 * Реализация контейнера информации о задании.
 * 
 * @author Ronn
 */
public class QuestState implements Foldable {

	private static final FoldablePool<QuestState> POOL = Pools.newConcurrentFoldablePool(QuestState.class);

	private static final FuncElement<QuestButton> BUTTON_FUNC = new FuncElement<QuestButton>() {

		@Override
		public void apply(QuestButton button) {
			button.fold();
		}
	};

	private static final FuncElement<QuestReward> REWARD_FUNC = new FuncElement<QuestReward>() {

		@Override
		public void apply(QuestReward reward) {
			reward.fold();
		}
	};

	private static final FuncElement<QuestCounter> COUNTER_FUNC = new FuncElement<QuestCounter>() {

		@Override
		public void apply(QuestCounter counter) {
			counter.fold();
		}
	};

	public static final QuestState newInstance() {

		QuestState info = POOL.take();

		if(info == null) {
			info = new QuestState();
		}

		return info;
	}

	/** список действий по заданию */
	private final Array<QuestButton> buttons;
	/** награда за задание */
	private final Array<QuestReward> rewards;
	/** счетчики задания */
	private final Array<QuestCounter> counters;

	/** название задания */
	private String name;
	/** описание задания */
	private String description;

	/** ид задания */
	private int id;
	/** уникальный ид задания */
	private int objectId;

	public QuestState() {
		this.buttons = Arrays.toArray(QuestButton.class);
		this.rewards = Arrays.toArray(QuestReward.class);
		this.counters = Arrays.toArray(QuestCounter.class);
	}

	/**
	 * Добавление новой кнопки действия.
	 * 
	 * @param button новая кнопка.
	 */
	public void addButton(QuestButton button) {
		getButtons().add(button);
	}

	/**
	 * Добавление нового счетчика.
	 * 
	 * @param counter счетчик задания.
	 */
	public void addCounter(QuestCounter counter) {
		getCounters().add(counter);
	}

	/**
	 * Добавления новой награды.
	 * 
	 * @param reward награда за задание.
	 */
	public void addReward(QuestReward reward) {
		getRewards().add(reward);
	}

	@Override
	public void finalyze() {

		Array<QuestButton> buttons = getButtons();
		buttons.apply(BUTTON_FUNC);
		buttons.clear();

		Array<QuestReward> rewards = getRewards();
		rewards.apply(REWARD_FUNC);
		rewards.clear();

		Array<QuestCounter> counters = getCounters();
		counters.apply(COUNTER_FUNC);
		counters.clear();
	}

	/**
	 * @return список действий по заданию.
	 */
	public Array<QuestButton> getButtons() {
		return buttons;
	}

	/**
	 * @return счетчики задания.
	 */
	public Array<QuestCounter> getCounters() {
		return counters;
	}

	/**
	 * @return описание задания.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return ид задания.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название задания.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return уникальный ид задания.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return награда за задание.
	 */
	public Array<QuestReward> getRewards() {
		return rewards;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param description описание задания.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id ид задания.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param name название задания.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param objectId уникальный ид задания.
	 */
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	/**
	 * Поиск счетчика с нужным ID.
	 * 
	 * @param id ид счетчика.
	 * @return искомый счетчик.
	 */
	public QuestCounter findCounter(int id) {

		Array<QuestCounter> counters = getCounters();

		for(QuestCounter counter : counters.array()) {

			if(counter == null) {
				break;
			}

			if(counter.getId() == id) {
				return counter;
			}
		}

		return null;
	}
}
