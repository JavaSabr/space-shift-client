package com.ss.client.gui.model.quest;

import com.ss.client.table.LangTable;

/**
 * Перечисление статусов задания.
 * 
 * @author Ronn
 */
public enum QuestStatus {

	IN_PROGRESS("@quest:statusInProgress@", "game/quest/status/in_progress.png"),
	COMPLETED("@quest:statusCompleted@", "game/quest/status/completed.png"),
	FAILED("@quest:statusFailed@", "game/quest/status/failed.png");

	/** название статуса */
	private String name;
	/** путь к иконке статуса */
	private String image;

	private QuestStatus(String name, String image) {

		LangTable langTable = LangTable.getInstance();

		this.name = langTable.getText(name);
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public String getName() {
		return name;
	}
}
