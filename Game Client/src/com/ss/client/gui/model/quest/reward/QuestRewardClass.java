package com.ss.client.gui.model.quest.reward;

import com.ss.client.gui.model.quest.reward.impl.ExperienceQuestReward;
import com.ss.client.gui.model.quest.reward.impl.ItemQuestReward;
import com.ss.client.network.ServerPacket;

/**
 * Перечисление классов наград.
 * 
 * @author Ronn
 */
public enum QuestRewardClass {

	EXPERIENCE {

		@Override
		public QuestReward readFrom(ServerPacket packet) {
			return ExperienceQuestReward.newInstance(packet.readLong());
		}
	},
	ITEM {

		@Override
		public QuestReward readFrom(ServerPacket packet) {
			return ItemQuestReward.newInstance(packet.readInt(), packet.readLong());
		}
	};

	private static final QuestRewardClass[] VALUES = values();

	public static final QuestRewardClass valueOf(int index) {
		return VALUES[index];
	}

	/**
	 * Прочитать награду из пакета.
	 * 
	 * @param packet серверный пакет.
	 * @return награда за задание.
	 */
	public QuestReward readFrom(ServerPacket packet) {
		return null;
	}
}
