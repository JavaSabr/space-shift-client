package com.ss.client.gui.model.quest.reward;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Интерфейс для реализации награды за задание.
 * 
 * @author Ronn
 */
public interface QuestReward {

	/**
	 * Построение элемента отображающего нвграду.
	 * 
	 * @param parent родительский элемент.
	 * @param nifty контроллер UI.
	 * @param screen текущий экран UI.
	 * @param order порядок элемента.
	 */
	public Element buildTo(Element parent, Nifty nifty, Screen screen, int order);

	/**
	 * Сложить объект в пул.
	 */
	public void fold();

	/**
	 * @return элемент отображающий награду.
	 */
	public Element getElement();
}
