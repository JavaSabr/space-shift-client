package com.ss.client.gui.model.quest;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.element.builder.impl.ButtonUIBuilder;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Информация о действии по заданию.
 * 
 * @author Ronn
 */
public class QuestButton implements Foldable {

	private static final FoldablePool<QuestButton> POOL = Pools.newConcurrentFoldablePool(QuestButton.class);

	public static final QuestButton newInstance(int id, String name) {

		QuestButton button = POOL.take();

		if(button == null) {
			button = new QuestButton();
		}

		button.id = id;
		button.name = name;
		return button;
	}

	/** элемент отображающий кнопку */
	private Element element;

	/** название кнопки */
	private String name;

	/** ид кнопки */
	private int id;

	/**
	 * Построение элемента кнопки.
	 * 
	 * @param parent родительский элемент.
	 * @param nifty контроллер UI.
	 * @param screen текущий экран UI.
	 * @param order порядок кнопки.
	 * @return построенный элемент кнопки.
	 */
	public Element buildElement(Element parent, Nifty nifty, Screen screen, int order) {

		ButtonUIBuilder button = new ButtonUIBuilder("@quest_button_element_" + order);
		button.setBackgroundColor(Color.randomColor());
		button.setText(getName());
		button.setHeight("25px");
		button.setWidth("80%");

		return button.build(nifty, screen, parent);
	}

	@Override
	public void finalyze() {
		name = null;
		element = null;
		id = 0;
	}

	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return элемент отображающий кнопку.
	 */
	public Element getElement() {
		return element;
	}

	/**
	 * @return ид кнопки.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название кнопки.
	 */
	public String getName() {
		return name;
	}

	@Override
	public void reinit() {
	}
}
