package com.ss.client.gui.model;

/**
 * Модель языка в клиенте.
 * 
 * @author Ronn
 */
public class Language {

	/** название языка */
	private final String name;

	/** позиция языка в локализации */
	private final int ordinal;

	public Language(final String name, final int ordinal) {
		this.name = name;
		this.ordinal = ordinal;
	}

	/**
	 * @return название языка.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return порядок языка в хмл локализации.
	 */
	public int getOrdinal() {
		return ordinal;
	}

	@Override
	public String toString() {
		return name;
	}
}
