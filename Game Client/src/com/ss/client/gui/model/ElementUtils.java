package com.ss.client.gui.model;

import java.util.List;

import rlib.util.array.Array;
import rlib.util.linkedlist.LinkedList;
import rlib.util.linkedlist.Node;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.effects.Effect;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.effects.EffectManager;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.effects.impl.Hint;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.tools.SizeValue;

/**
 * Набор утилитных методов по элементам интерфейса.
 * 
 * @author Ronn
 */
public class ElementUtils {

	/**
	 * Добавить внутренние элементы.
	 */
	public static void addElements(final Array<Element> container, final Element element, final String containsId) {

		final Array<Element> elements = element.getFastElements();

		for(final Element child : elements) {

			if(!child.getId().contains(containsId)) {
				addElements(container, child, containsId);
				continue;
			}

			container.add(child);
		}
	}

	/**
	 * Отключение панели.
	 */
	public static void disable(final Element layer) {

		if(layer == null) {
			return;
		}

		final Array<Element> elements = layer.getFastElements();

		layer.disable();

		for(final Element element : elements) {
			element.disable();
		}
	}

	/**
	 * Включение панели.
	 */
	public static void enable(final Element layer) {

		if(layer == null) {
			return;
		}

		final Array<Element> elements = layer.getFastElements();

		layer.enable();

		for(final Element element : elements) {
			element.enable();
		}
	}

	/**
	 * Сравнение родителей 2х элементов.
	 * 
	 * @param first первый элемент.
	 * @param second второй элемент.
	 * @return общий ли у них родитель.
	 */
	public static boolean equalsParent(Element first, Element second) {

		if(first == null || second == null) {
			return false;
		}

		Element firstParent = first.getParent();
		Element secondParent = second.getParent();

		if(firstParent == null || secondParent == null) {
			return false;
		}

		return firstParent.equals(secondParent);
	}

	/**
	 * Поиск элемента.
	 * 
	 * @param element элемент в котором идет поиск.
	 * @param id искомый элемент.
	 */
	public static Element findChild(final Element element, final String id) {

		final Array<Element> elements = element.getFastElements();

		for(final Element child : elements) {
			if(id.equals(child.getId()))
				return child;

			final Element target = findChild(child, id);

			if(target != null)
				return target;
		}

		return null;
	}

	public static String getAllParentsToString(final Element element) {

		final StringBuilder builder = new StringBuilder(toString(element));
		builder.append(" -> ");

		Element parent = element.getParent();

		while(parent != null) {
			builder.append(toString(parent)).append(" -> ");
			parent = parent.getParent();
		}

		builder.replace(builder.length() - 3, builder.length(), "");

		return builder.toString();
	}

	/**
	 * Перемещение элемента в указанную позицию.
	 * 
	 * @param element перемещаемый элемент.
	 * @param x новоя координата.
	 * @param y новоя координата.
	 */
	public static void moveElement(final Element element, final int x, final int y) {

		element.setConstraintX(SizeValue.px(x));
		element.setConstraintY(SizeValue.px(y));

		final Element parent = element.getParent();

		parent.layoutElements();
	}

	public static void remove(LinkedList<Element> elements) {
		for(Node<Element> node = elements.getFirstNode(); node != null; node = node.getNext()) {
			Element element = node.getItem();
			element.markForRemoval();
		}
	}

	/**
	 * Удаление внутренних элементов элемента.
	 * 
	 * @param element элемент, чьи все элементы будем удалять.
	 */
	public static void removeChilds(final Element element) {

		final Array<Element> elements = element.getFastElements();

		for(final Element child : elements) {
			removeChilds(child);

			child.markForRemoval();
		}
	}

	public static void removeEffects(final Element element) {
		EffectManager effectManager = element.getEffectManager();
		effectManager.removeAllEffects();
	}

	public static void removeElement(final Element element) {
		if(element != null) {
			element.markForRemoval();
		}
	}

	public static String toString(final Element element) {
		return element.getId();
	}

	public static void updateButtonText(final Element element, final String text) {
		final Button button = element.getNiftyControl(Button.class);
		button.setText(text);
	}

	public static void updateHintTo(final Element element, final String text) {

		final List<Effect> effects = element.getEffects(EffectEventId.onHover, Hint.class);

		for(final Effect effect : effects) {
			final EffectProperties parametrs = effect.getParameters();
			parametrs.setProperty("hintText", text);
		}
	}

	public static void updateLayout(final Element element) {
		element.layoutElements();
	}

	private ElementUtils() {
		throw new RuntimeException();
	}
}
