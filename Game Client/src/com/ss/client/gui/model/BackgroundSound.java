package com.ss.client.gui.model;

import java.util.concurrent.ScheduledFuture;

import rlib.util.SafeTask;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioContext;
import com.jme3.audio.AudioKey;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioRenderer;
import com.jme3.audio.AudioSource.Status;
import com.ss.client.Game;
import com.ss.client.GameConfig;
import com.ss.client.manager.ExecutorManager;

/**
 * Модель фонового звука.
 * 
 * @author Ronn
 */
public final class BackgroundSound {

	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();
	private static final Game GAME = Game.getInstance();

	/** менеджер дял загрузки звука */
	private final AssetManager asset;
	/** название инстанса */
	private final String name;
	/** клю для загрузки звука */
	private final AudioKey key;
	/** таск перезапуска звука */
	private final SafeTask replayTask;

	/** является ли эффектом */
	private final boolean effect;

	/** обработчик звука */
	private volatile AudioNode sound;

	/** ссылка на активный таск */
	private volatile ScheduledFuture<SafeTask> schedule;

	public BackgroundSound(final AssetManager asset, final String name, final String path, final boolean effect) {
		this.name = name;
		this.effect = effect;
		this.key = new AudioKey(path, true, false);
		this.asset = asset;

		this.replayTask = new SafeTask() {

			@Override
			protected void runImpl() {
				if(sound.getStatus() == Status.Stopped) {
					final AudioKey key = getKey();

					sound = new AudioNode(getAsset().loadAudio(key), key);
					sound.setVolume(isEffect() ? GameConfig.EFFECT_VOLUME : GameConfig.MUSIC_VOLUME);

					final AudioRenderer render = AudioContext.getAudioRenderer();

					if(render == null)
						AudioContext.setAudioRenderer(GAME.getAudioRenderer());

					sound.setPositional(false);
					sound.play();
				}
			}
		};
	}

	/**
	 * @return asset менеджер загрузки контента.
	 */
	protected final AssetManager getAsset() {
		return asset;
	}

	/**
	 * @return key ключ для загрузки трэка.
	 */
	protected final AudioKey getKey() {
		return key;
	}

	/**
	 * @return имя инстанса.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return обработчик звука.
	 */
	public final AudioNode getSound() {
		return sound;
	}

	/**
	 * @return является ли эффетком.
	 */
	public final boolean isEffect() {
		return effect;
	}

	/**
	 * Воспроизведение звука.
	 */
	public synchronized final void play() {
		if(sound != null && sound.getStatus() == Status.Playing)
			return;

		final AudioRenderer render = AudioContext.getAudioRenderer();

		if(render == null)
			AudioContext.setAudioRenderer(GAME.getAudioRenderer());

		final AudioKey key = getKey();

		sound = new AudioNode(getAsset().loadAudio(key), key);
		sound.setVolume(isEffect() ? GameConfig.EFFECT_VOLUME : GameConfig.MUSIC_VOLUME);
		sound.setPositional(false);
		sound.play();

		if(schedule == null)
			schedule = EXECUTOR_MANAGER.scheduleGeneralAtFixedRate(replayTask, 500, 500);
	}

	/**
	 * Остановка возспроизведения звука.
	 */
	public synchronized final void stop() {
		if(sound.getStatus() == Status.Stopped)
			return;

		sound.stop();

		if(schedule != null) {
			schedule.cancel(false);
			schedule = null;
		}
	}

	/**
	 * Обновление громкости звуков.
	 */
	public void updateVolume() {
		if(sound != null)
			sound.setVolume(effect ? GameConfig.EFFECT_VOLUME : GameConfig.MUSIC_VOLUME);
	}
}
