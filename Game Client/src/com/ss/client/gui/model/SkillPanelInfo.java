package com.ss.client.gui.model;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Модель настроек положения скилов на панели.
 * 
 * @author Ronn
 */
public class SkillPanelInfo implements Foldable {

	private static final FoldablePool<SkillPanelInfo> pool = Pools.newConcurrentFoldablePool(SkillPanelInfo.class);

	public static SkillPanelInfo newInstance(final int objectId, final int skillId, final int order) {
		SkillPanelInfo info = pool.take();

		if(info == null)
			info = new SkillPanelInfo();

		info.objectId = objectId;
		info.skillId = skillId;
		info.order = order;

		return info;
	}

	/** уникальный ид модуля */
	private int objectId;
	/** ид темплейта скила */
	private int skillId;
	/** позиция на панели */
	private int order;

	@Override
	public void finalyze() {
	}

	/**
	 * Складирование настроек в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return objectId уникальный ид модуля.
	 */
	public final int getObjectId() {
		return objectId;
	}

	/**
	 * @return order положение на панели.
	 */
	public final int getOrder() {
		return order;
	}

	/**
	 * @return skillId ид темплейта скила.
	 */
	public final int getSkillId() {
		return skillId;
	}

	@Override
	public void reinit() {
	}

	@Override
	public String toString() {
		return "SkillPanelInfo [objectId=" + objectId + ", skillId=" + skillId + ", order=" + order + "]";
	}
}
