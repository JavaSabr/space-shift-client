package com.ss.client.gui.model.listeners;

import com.ss.client.model.SpaceObject;

/**
 * Интерфейс для прослушивания добавления/удаления объектов из космоса.
 * 
 * @author Ronn
 */
public interface AddRemoveObjectListener {

	/**
	 * Добавление объекта в космос.
	 * 
	 * @param object добавленный объект.
	 */
	public void addObject(SpaceObject object);

	/**
	 * Удаление объекта из космоса.
	 * 
	 * @param object удаляемый объект.
	 */
	public void removeObject(SpaceObject object);
}
