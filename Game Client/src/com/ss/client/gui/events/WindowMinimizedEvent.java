package com.ss.client.gui.events;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;
import de.lessvoid.nifty.NiftyEvent;
import de.lessvoid.nifty.elements.Element;

/**
 * Событие о сворачивании/разворачивании окна.
 * 
 * @author Ronn
 */
public class WindowMinimizedEvent implements NiftyEvent<Void>, Foldable {

	private static final FoldablePool<WindowMinimizedEvent> pool = Pools.newConcurrentFoldablePool(WindowMinimizedEvent.class);

	public static final WindowMinimizedEvent getInstance(final Element element, final boolean minimized) {
		WindowMinimizedEvent event = pool.take();

		if(event == null)
			event = new WindowMinimizedEvent();

		event.element = element;
		event.minimized = minimized;

		return event;
	}

	/** ссылка на элемент */
	private Element element;

	/** свернут ли элемент */
	private boolean minimized;

	@Override
	public void finalyze() {
		element = null;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return измененный элемент.
	 */
	public Element getElement() {
		return element;
	}

	/**
	 * @return свернут ли элемент.
	 */
	public boolean isMinimized() {
		return minimized;
	}

	@Override
	public void reinit() {
	}
}
