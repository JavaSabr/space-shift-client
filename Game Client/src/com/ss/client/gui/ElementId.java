package com.ss.client.gui;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.table.Table;
import rlib.util.table.Tables;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.Slider;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.screen.Screen;

/**
 * Набор всех ид всех уникальных элементов интерфейса.
 * 
 * @author Ronn
 */
public enum ElementId {
	/** элементы окна авторизации */
	LOGIN_SCREEN("login_screen", Screen.class),
	LOGIN_AUTH_LAYER("login_auth_layer", null),
	LOGIN_AUTH_FIELD_LOGIN("login_auth_field_login", TextField.class),
	LOGIN_AUTH_FIELD_PASSWORD("login_auth_field_password", TextField.class),
	LOGIN_AUTH_BUTTON_EXIT("login_auth_button_exit", Button.class),
	LOGIN_AUTH_BUTTON_AUTH("login_auth_button_auth", Button.class),

	LOGIN_CONFIG_LAYER("login_config_layer", null),
	LOGIN_CONFIG_DROP_DOWN_LANG("login_config_drop_down_lang", DropDown.class),
	LOGIN_CONFIG_DROP_DOWN_SCREEN("login_config_drop_down_screen", DropDown.class),
	LOGIN_CONFIG_SLIDER_MUSIC("login_config_slider_music", Slider.class),
	LOGIN_CONFIG_SLIDER_EFFECT("login_config_slider_effect", Slider.class),
	LOGIN_WARNING_LAYER("login_warning_layer", null),
	LOGIN_WARNING_LABEL("login_warning_label", Label.class),
	LOGIN_WARNING_BUTTON("login_warning_button", Button.class),
	LOGIN_REGISTER_LAYER("login_register_layer", null),
	LOGIN_REGISTER_FIELD_LOGIN("login_register_field_login", TextField.class),
	LOGIN_REGISTER_FIELD_PASSWORD("login_register_field_password", TextField.class),
	LOGIN_REGISTER_FIELD_EMAIL("login_register_field_email", TextField.class),
	LOGIN_REGISTER_BUTTON_CANCEL("login_register_button_cancel", Button.class),
	LOGIN_REGISTER_BUTTON_REGISTER("login_register_button_register", Button.class),
	LOGIN_SERVER_LIST_LAYER("login_server_list_layer", null),
	LOGIN_SERVER_LIST_LIST_BOX("login_server_list_list_box", ListBox.class),
	LOGIN_SERVER_LIST_BUTTON_ENTER("login_server_list_button_enter", Button.class),

	/** элементы ангара */
	HANGAR_SCREEN("hangar_screen", Screen.class),
	HANGAR_SERVER_LABEL_INFO("hangar_server_label_name", Label.class),
	HANGAR_CREATE_LAYER("hangar_create_layer", null),
	HANGAR_CREATE_BUTTON_CREATE("hangar_create_button_create", Button.class),
	HANGAR_CREATE_DROP_DOWN_TYPE("hangar_create_drop_down_type", DropDown.class),
	HANGAR_CREATE_FIELD_NAME("hangar_create_field_name", TextField.class),
	HANGAR_WARNING_LAYER("hangar_warning_layer", null),
	HANGAR_WARNING_LABEL("hangar_warning_label", Label.class),
	HANGAR_ENTER_LAYER("hangar_enter_layer", null),
	HANGAR_ENTER_DROP_DOWN_SELECTED("hangar_enter_drop_down_selected", null),
	HANGAR_ENTER_BUTTON_ENTER("hangar_enter_button_enter", null),
	HANGAR_DELETE_LAYER("hangar_delete_layer", null),
	HANGAR_DELETE_DROP_DOWN_SELECTED("hangar_delete_drop_down_selected", null),
	HANGAR_DELETE_BUTTON_DELETE("hangar_delete_button_delete", null),

	/** элементы игрового интерфейса */
	GAME_SCREEN("game_screen", null),

	GAME_SELECTOR_LAYER("game_selector_layer", null),
	GAME_PANEL_LAYER("game_panel_layer", null),

	/** окошки с панелями */
	GAME_WINDOW_LAYER("game_window_layer", null),
	GAME_WINDOW_SKILL_LIST("game_window_skill_list", null),
	GAME_WINDOW_QUEST_DIALOG("game_window_quest_dialog", null),
	GAME_WINDOW_QUEST_BOOK("game_window_quest_book", null),
	GAME_WINDOW_SHIP_PANEL("game_window_ship_panel", null),
	GAME_WINDOW_STORAGE_PANEL("game_window_storage_panel", null),
	GAME_WINDOW_MAP_LOCATION("game_window_map_location", null),

	/** сврачиваемы панели */
	GAME_PANEL_RADAR_PANEL("game_panel_radar_panel", null),
	GAME_PANEL_CHAT_PANEL("game_panel_chat_panel", null),
	GAME_PANEL_FASTMENU_PANEL("game_panel_fastmenu_panel", null),
	GAME_PANEL_MAIN_PANEL("game_panel_main_panel", null),

	// FIXME это относится к панелькам
	GAME_WINDOW_ANGAR_INDICATOR("game_window_angar_indicator", null),

	GAME_SELECT_PANEL("game_select_panel", null),

	GAME_MAP_LOCATION_PANEL("game_map_location_panel", null),
	GAME_MAP_LOCATION_HIDE_PANEL("game_map_location_hide_panel", null),
	GAME_MAP_LOCATION_IMAGE("game_map_location_image", null),

	GAME_CHAT_LAYER("game_chat_layer", null),
	GAME_CHAT_LIST_BOX("game_chat_list_box", ListBox.class),
	GAME_CHAT_TEXT_FIELD("game_chat_text_field", TextField.class),
	GAME_CHAT_BUTTON_SEND("game_chat_button_send", Button.class),
	GAME_CHAT_TAB_MAIN("game_chat_tab_main", Button.class),
	GAME_CHAT_TAB_PARTY("game_chat_tab_party", Button.class),
	GAME_CHAT_TAB_FRACTION("game_chat_tab_fraction", Button.class),
	GAME_CHAT_TAB_FEDERATION("game_chat_tab_federation", Button.class),
	GAME_CHAT_TAB_GLOBAL("game_chat_tab_global", Button.class),

	GAME_FASTMENU_LAYER("game_fastmenu_layer", null),
	GAME_FAST_MENU_BUTTON("game_fastmenu_button", null),

	GAME_STORAGE_BUTTON("game_storage_button", null),

	GAME_SKILL_LIST_LAYER("game_skill_list_layer", null),
	GAME_SKILL_LIST_BUTTON("game_skill_list_button", null),

	GAME_VECTOR_INDICATOR_LAYER("game_vector_indicator_layer", null),
	GAME_VECTOR_INDICATOR_IMAGE("game_vector_indicator_image", null),

	GAME_AIM_INDICATOR_LAYER("game_aim_indicator_layer", null),
	GAME_AIM_INDICATOR_IMAGE("game_aim_indicator_image", null),

	GAME_HANGAR_INDICATOR_LAYER("game_hangar_indicator_layer", null),
	GAME_HANGAR_INDICATOR_IMAGE("game_hangar_indicator_image", null),

	GAME_QUEST_DIALOG_LAYER("game_quest_dialog_layer", null),
	GAME_QUEST_DIALOG_LIST("game_quest_dialog_list", null),
	GAME_QUEST_DIALOG_DESCR_LABEL("game_quest_dialog_descr_label", Label.class),
	GAME_QUEST_DIALOG_DESCR("game_quest_dialog_descr", null),
	GAME_QUEST_DIALOG_INTERCACT_LABEL("game_quest_dialog_interact_label", null),
	GAME_QUEST_DIALOG_INTERACT_CONTAINER("game_quest_dialog_intercat_container", null),

	GAME_QUEST_BOOK_LAYER("game_quest_book_layer", null),
	GAME_QUEST_BOOK_LIST("game_quest_book_list", null),
	GAME_QUEST_BOOK_LABEL_DESCR("game_quest_book_label_descr", null),
	GAME_QUEST_BOOK_SCROLL_PANEL("game_quest_book_scroll_panel", null),
	GAME_QUEST_BOOK_QUEST_DESCR_PANEL("game_quest_book_quest_descr_panel", null),
	GAME_QUEST_BOOK_TEXT_AREA("game_quest_book_text_area", null),
	GAME_QUEST_BOOK_LABEL_REWARD("game_quest_book_label_reward", null),
	GAME_QUEST_BOOK_REWARD_PANEL("game_quest_book_reward_panel", null),
	GAME_QUEST_BOOK_LABEL_INTERACT("game_quest_book_label_interact", null),
	GAME_QUEST_BOOK_INTERACT_PANEL("game_quest_book_interact_panel", null),
	GAME_QUEST_BOOK_HIDE_PANEL("game_quest_book_hide_panel", null),

	GAME_SHIP_PANEL_LABEL_MODULES("game_ship_panel_label_modules", null),
	GAME_SHIP_PANEL_LABEL_STATS("game_ship_panel_label_stats", null),
	GAME_SHIP_PANEL_SCROLL_PANEL("game_ship_panel_scroll_panel", null),
	GAME_SHIP_PANEL_MODULES_PANEL("game_ship_panel_modules_panel", null),
	GAME_SHIP_PANEL_STATS_PANEL("game_ship_panel_stats_panel", null),
	GAME_SHIP_PANEL_HIDE("game_ship_panel_shide", null),

	GAME_BUFFER_PANEL_1("game_buffer_panel_1", null),
	GAME_BUFFER_PANEL_2("game_buffer_panel_2", null),
	GAME_BUFFER_PANEL_3("game_buffer_panel_3", null),
	GAME_BUFFER_PANEL_4("game_buffer_panel_4", null),

	GAME_STATUS_LAYER("game_status_panel", null),
	GAME_STATUS_PANEL("game_status_panel", null),
	GAME_STATUS_PANEL_PLAYER("game_status_panel_player", null),
	GAME_STATUS_PANEL_PLAYER_NAME("game_status_panel_player_name", null),
	GAME_STATUS_PANEL_PLAYER_LEVEL("game_status_panel_player_level", null),
	GAME_STATUS_PANEL_HEAL("game_status_panel_heal", null),
	GAME_STATUS_PANEL_TEXT_CP("game_status_panel_text_cp", null),
	GAME_STATUS_PANEL_TEXT_EP("game_status_panel_text_ep", null),

	GAME_SHIP_STATUS_LEVEL_PANEL("game_ship_status_level_panel", null),
	GAME_SHIP_STATUS_NAME_LABEL("game_ship_status_name_label", null),
	GAME_SHIP_STATUS_LEVEL_LABEL("game_ship_status_level_label", null),
	GAME_SHIP_STATUS_CP_EP_PANEL("game_ship_status_cp_ep_panel", null),
	GAME_SHIP_STATUS_CP_LABEL("game_ship_status_cp_label", null),
	GAME_SHIP_STATUS_EP_LABEL("game_ship_status_ep_label", null),

	GAME_FAST_LAYER("game_fast_layer", null),
	GAME_FAST_PANEL("game_fast_panel", null),

	GAME_FAST_PANEL_ROW("game_fast_panel_row", null), ;

	private static final Logger log = Loggers.getLogger(ElementId.class);

	/** массив всех ид элементов гуи */
	public static final ElementId[] VALUES = values();
	/** кол-во всех идов */
	public static final int SIZE = VALUES.length;

	/** таблица элементов по ид */
	private static Table<String, ElementId> elementTable;

	/** значение ид для след. элемента */
	private static long nextElementId;

	/**
	 * @param id ид элемента.
	 * @return тип элемента.
	 */
	public static ElementId findById(final String id) {
		return elementTable.get(id);
	}

	/**
	 * @return ид для элемента.
	 */
	public synchronized static String getNextElementId() {
		nextElementId += 7;

		return String.valueOf(nextElementId);
	}

	/**
	 * Инициализация перечисления.
	 */
	public static void init() {
		elementTable = Tables.newObjectTable();

		nextElementId = Byte.MAX_VALUE;

		for(final ElementId element : VALUES) {
			final String id = element.getId();

			if(elementTable.containsKey(id)) {
				log.warning("fount duplicate for element id " + id);
				continue;
			}

			elementTable.put(id, element);
		}
	}

	/** ид элемента */
	private String id;
	/** тип элемента */
	private Class<?> type;

	private ElementId(final String id, final Class<?> type) {
		this.id = id;
		this.type = type;
	}

	/**
	 * @return ид элемента.
	 */
	public final String getId() {
		return id;
	}

	/**
	 * @return тип элемента.
	 */
	public final Class<?> getType() {
		return type;
	}
}
