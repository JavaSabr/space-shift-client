package com.ss.client.gui.element;

import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.elements.Element;

/**
 * Интерфейс для реализации перемещаемого элемента.
 * 
 * @author Ronn
 */
public interface DraggableElementUI extends ElementUI {

	public static final String PROPERTY_DROPPABLE = "droppable";
	public static final String PROPERTY_REVERTABLE = "revertable";

	/**
	 * @return перемещаемый элемент.
	 */
	public Element getDraggable();

	/**
	 * @return контейнер перемещаемого элемента.
	 */
	public DroppableElementUI getDroppable();

	/**
	 * @return завершение перемещения.
	 */
	public EndNotify getEndNotify();

	/**
	 * @param draggable перемещаемый элемент.
	 */
	public void setDraggable(Element draggable);

	/**
	 * @param droppable контейнер перемещаемого элемента.
	 */
	public void setDroppable(DroppableElementUI droppable);
}
