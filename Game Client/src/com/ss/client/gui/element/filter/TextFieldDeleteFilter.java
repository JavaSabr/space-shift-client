package com.ss.client.gui.element.filter;

/**
 * Интерфейс для реализации филльтра для удаляемых символов.
 * 
 * @author Ronn
 */
public interface TextFieldDeleteFilter {

	/**
	 * Проверка возможности удаления укказанного символа.
	 * 
	 * @param oldChar удаляемый символ.
	 * @param index индекс этого символа.
	 * @return можно ли его удалить.
	 */
	public boolean acceptDelete(char oldChar, int index);

	/**
	 * Проверка возможности удаления указанного набора символов.
	 * 
	 * @param oldSequence удаляемый набор символов.
	 * @param deleteStart стартовая позиция в тексте.
	 * @param deleteEnd конечная позиция в тексте.
	 * @return можно ли удалить эти символы.
	 */
	public boolean acceptDelete(CharSequence oldSequence, int deleteStart, int deleteEnd);
}
