package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.gui.element.effect.builder.EffectUIBuilder;
import com.ss.client.gui.element.effect.impl.ImageOverlayEffectUI;

/**
 * Конструктор эффекта замены изображения.
 * 
 * @author Ronn
 */
public class SimpleImageOverlayEffectUIBuilder extends AbstractSimpleEffectUIBuilder {

	static {
		EffectUIBuilder builder = new SimpleImageOverlayEffectUIBuilder();
		NIFTY.registerEffect(builder.getEffectName(), ImageOverlayEffectUI.class.getName());
	}

	public void setActiveBeforeStartDelay(boolean activeBeforeStartDelay) {
		getBuilder().effectParameter(ImageOverlayEffectUI.PROP_ACTIVE_BEFORE_START_DELAY, String.valueOf(activeBeforeStartDelay));
	}

	public void setAlpha(String alpha) {
		getBuilder().effectParameter(ImageOverlayEffectUI.PROP_ALPHA, alpha);
	}

	public void setCenter(boolean center) {
		getBuilder().effectParameter(ImageOverlayEffectUI.PROP_CENTER, String.valueOf(center));
	}

	public void setHideIfNotEnoughSpace(boolean hideIfNotEnoughSpace) {
		getBuilder().effectParameter(ImageOverlayEffectUI.PROP_HIDE_IF_NOT_ENOUGH_SPACE, String.valueOf(hideIfNotEnoughSpace));
	}
}
