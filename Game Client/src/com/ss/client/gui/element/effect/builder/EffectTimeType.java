package com.ss.client.gui.element.effect.builder;

import rlib.util.Strings;

/**
 * Перечисление типов времени эффектов.
 * 
 * @author Ronn
 */
public enum EffectTimeType {
	INFINITE("infinite"),
	LINEAR("linear"),
	EXP("exp");

	public static final EffectTimeType[] VALUES = values();

	public static final EffectTimeType fromValue(String value) {

		for(EffectTimeType type : VALUES) {
			if(Strings.equals(type.value, value)) {
				return type;
			}
		}

		return null;
	}

	/** xml представление */
	private String value;

	private EffectTimeType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

}
