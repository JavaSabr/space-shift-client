package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.gui.element.effect.builder.EffectUIBuilder;
import com.ss.client.gui.element.effect.impl.HintEffectUI;

/**
 * Реализация конструктора эффекта подсказки.
 * 
 * @author Ronn
 */
public class HoverHintEffectUIBuilder extends AbstractHoverEffectUIBuilder {

	static {
		EffectUIBuilder builder = new HoverHintEffectUIBuilder();
		NIFTY.registerEffect(builder.getEffectName(), HintEffectUI.class.getName());
	}

	public void setHintDelay(int delay) {
		getBuilder().effectParameter(HintEffectUI.PROP_HINT_DELAY, String.valueOf(delay));
	}
}
