package com.ss.client.gui.element.effect.impl;

import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyMouse;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.effects.Falloff;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.screen.Screen;

/**
 * Реализация эффекта подсказки.
 * 
 * @author Ronn
 */
public class HintEffectUI extends AbstractEffectUI {

	public static final String PROP_OFFSET_BOTTOM = "bottom";
	public static final String PROP_OFFSET_TOP = "top";
	public static final String PROP_OFFSET_RIGHT = "right";
	public static final String PROP_OFFSET_LEFT = "left";
	public static final String PROP_OFFSET_CENTER = "center";
	public static final String PROP_HINT_DELAY = "hintDelay";

	public static final String HINT_LAYER_ID = "hint-layer-";

	private static final Table<Screen, Element> LAYER_TABLE = Tables.newObjectTable();

	/** контроллер UI */
	private Nifty nifty;

	/** элемент подсказки */
	private Element element;

	/** отступ эффекта по X */
	private String offsetX;
	/** отступ эффекта по Y */
	private String offsetY;

	/** задержка перед отображением */
	private int hintDelay;

	@Override
	public void activate(final Nifty nifty, final Element element, final EffectProperties parameter) {
		this.nifty = nifty;
		this.hintDelay = Integer.parseInt(parameter.getProperty(PROP_HINT_DELAY, "0"));
		this.offsetX = parameter.getProperty(PROP_OFFSET_X, "0");
		this.offsetY = parameter.getProperty(PROP_OFFSET_Y, "0");
		this.element = build(nifty, nifty.getCurrentScreen(), getLayer(), element, parameter);
	}

	/**
	 * Построение элемента для отображаения подсказки.
	 * 
	 * @param nifty контроллер UI.
	 * @param screen текущий экран UI.
	 * @param parent элемент для размещения подсказки.
	 * @param activator активирующий элемент.
	 * @param properties набор параметров.
	 * @return элемент подсказки.
	 */
	protected Element build(Nifty nifty, Screen screen, Element parent, Element activator, EffectProperties properties) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public void deactivate() {

		final Element hintElement = getElement();

		if(hintElement == null) {
			return;
		}

		if(hintElement.isVisible()) {
			hintElement.startEffect(EffectEventId.onCustom, new EndNotify() {

				@Override
				public void perform() {
					hintElement.markForRemoval();
				}
			});
		} else {
			hintElement.markForRemoval();
		}
	}

	@Override
	public void execute(final Element element, final float normalizedTime, final Falloff falloff, final NiftyRenderEngine renderEngine) {

		if(normalizedTime > 0.0F) {

			final Element hintElement = getElement();

			if(hintElement != null && !hintElement.isVisible()) {

				Nifty nifty = getNifty();
				NiftyMouse niftyMouse = nifty.getNiftyMouse();

				if(niftyMouse.getNoMouseMovementTime() > getHintDelay()) {

					preShowHint();

					hintElement.setConstraintX(GameUtil.getPixelSize(getPosX(element, hintElement, renderEngine.getWidth())));
					hintElement.setConstraintY(GameUtil.getPixelSize(getPosY(element, hintElement, renderEngine.getHeight())));
					hintElement.setVisible(true);

					ElementUtils.updateLayout(hintElement.getParent());
				}
			}
		}
	}

	/**
	 * Выполнение действий перед отображением подсказки.
	 */
	protected void preShowHint() {
	}

	/**
	 * @return элемент подсказки.
	 */
	public Element getElement() {
		return element;
	}

	/**
	 * @return задержка перед отображением.
	 */
	public int getHintDelay() {
		return hintDelay;
	}

	/**
	 * @return слой для построения элементов подсказок.
	 */
	protected Element getLayer() {

		Nifty nifty = getNifty();
		Screen currentScreen = nifty.getCurrentScreen();

		Element layer = LAYER_TABLE.get(currentScreen);

		if(layer == null) {
			synchronized(LAYER_TABLE) {
				layer = LAYER_TABLE.get(currentScreen);

				if(layer == null) {

					LayerBuilder builder = new LayerBuilder(HINT_LAYER_ID + currentScreen.getScreenId());
					builder.childLayout(ChildLayoutType.AbsoluteInside);
					builder.visibleToMouse(false);

					layer = builder.build(nifty, currentScreen, currentScreen.getRootElement());
					LAYER_TABLE.put(currentScreen, layer);
				}
			}
		}

		return layer;
	}

	/**
	 * @return контроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return отступ эффекта по X.
	 */
	public String getOffsetX() {
		return offsetX;
	}

	/**
	 * @return отступ эффекта по Y.
	 */
	public String getOffsetY() {
		return offsetY;
	}

	/**
	 * Получение позиции по X.
	 * 
	 * @param element элемент, для которого вызывается подсказка.
	 * @param hintElement элемент подсказки.
	 * @param screenWidth ширина экрана.
	 * @return позиция подсказки по X.
	 */
	private int getPosX(final Element element, final Element hintElement, final int screenWidth) {

		String offsetX = getOffsetX();

		int pos = 0;

		if(PROP_OFFSET_CENTER.equals(offsetX)) {
			pos = element.getX() + element.getWidth() / 2 - hintElement.getWidth() / 2;
		} else if(PROP_OFFSET_LEFT.equals(offsetX)) {
			pos = element.getX();
		} else if(PROP_OFFSET_RIGHT.equals(offsetX)) {
			pos = element.getX() + element.getWidth() - hintElement.getWidth();
		} else {
			Nifty nifty = getNifty();
			NiftyMouse niftyMouse = nifty.getNiftyMouse();
			pos = niftyMouse.getX() + Integer.parseInt(offsetX);
		}

		if(pos < 0) {
			pos = 0;
		}

		if(pos + hintElement.getWidth() > screenWidth) {
			pos = screenWidth - hintElement.getWidth();
		}

		return pos;
	}

	/**
	 * Получение позиции по Y.
	 * 
	 * @param element элемент, для которого вызывается подсказка.
	 * @param hintElement элемент подсказки.
	 * @param screenWidth ширина экрана.
	 * @return позиция подсказки по Y.
	 */
	private int getPosY(final Element element, final Element hintElement, final int screenHeight) {

		String offsetY = getOffsetY();

		int pos = 0;

		if(PROP_OFFSET_CENTER.equals(offsetY)) {
			pos = element.getY() + element.getHeight() / 2 - hintElement.getHeight() / 2;
		} else if(PROP_OFFSET_TOP.equals(offsetY)) {
			pos = element.getY();
		} else if(PROP_OFFSET_BOTTOM.equals(offsetY)) {
			pos = element.getY() + element.getHeight() - hintElement.getHeight();
		} else {
			Nifty nifty = getNifty();
			NiftyMouse niftyMouse = nifty.getNiftyMouse();
			pos = niftyMouse.getY() + Integer.parseInt(offsetY);
		}

		if(pos < 0) {
			pos = 0;
		}

		if(pos + hintElement.getHeight() > screenHeight) {
			pos = screenHeight - hintElement.getHeight();
		}

		return pos;
	}

}
