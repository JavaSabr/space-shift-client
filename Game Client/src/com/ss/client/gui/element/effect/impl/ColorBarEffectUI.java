package com.ss.client.gui.element.effect.impl;

import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.effects.Falloff;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.loaderv2.types.helper.PaddingAttributeParser;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.nifty.tools.SizeValue;

/**
 * Реализация эффекта.
 * 
 * @author Ronn
 */
public class ColorBarEffectUI extends AbstractEffectUI {

	private final Color tempColor = new Color("#000f");

	private Color color;

	private SizeValue width;
	private SizeValue insetLeft = GameUtil.getPixelSize(0);
	private SizeValue insetRight = GameUtil.getPixelSize(0);
	private SizeValue insetTop = GameUtil.getPixelSize(0);
	private SizeValue insetBottom = GameUtil.getPixelSize(0);

	@Override
	public void activate(Nifty nifty, Element element, EffectProperties parameter) {
		super.activate(nifty, element, parameter);

		this.color = new Color(parameter.getProperty(PROP_COLOR, "#ffffffff"));
		this.width = new SizeValue(parameter.getProperty(PROP_WIDTH));

		if(!parameter.containsKey(PROP_INSET)) {
			return;
		}

		try {

			PaddingAttributeParser parser = new PaddingAttributeParser(parameter.getProperty(PROP_INSET));
			this.insetLeft = GameUtil.getSizeValue(parser.getLeft());
			this.insetRight = GameUtil.getSizeValue(parser.getRight());
			this.insetTop = GameUtil.getSizeValue(parser.getTop());
			this.insetBottom = GameUtil.getSizeValue(parser.getBottom());

		} catch(Exception e) {
			LOGGER.warning(e);
		}
	}

	@Override
	public void deactivate() {
	}

	@Override
	public void execute(final Element element, final float normalizedTime, final Falloff falloff, final NiftyRenderEngine renderEngine) {

		renderEngine.saveState(null);

		if(renderEngine.isColorAlphaChanged()) {

			if(falloff == null) {
				renderEngine.setColorIgnoreAlpha(color);
			} else {
				tempColor.mutiply(color, falloff.getFalloffValue());
				renderEngine.setColorIgnoreAlpha(tempColor);
			}

		} else {

			if(falloff == null) {
				renderEngine.setColor(color);
			} else {
				tempColor.mutiply(color, falloff.getFalloffValue());
				renderEngine.setColor(tempColor);
			}
		}

		int insetOffsetLeft = insetLeft.getValueAsInt(element.getWidth());
		int insetOffsetRight = insetRight.getValueAsInt(element.getWidth());
		int insetOffsetTop = insetTop.getValueAsInt(element.getHeight());
		int insetOffsetBottom = insetBottom.getValueAsInt(element.getHeight());

		int size = (int) width.getValue(element.getParent().getWidth());

		int y = element.getY() + insetOffsetTop;
		int renderHeight = element.getHeight() - insetOffsetTop - insetOffsetBottom;

		if(size == -1) {

			int x = element.getX() + insetOffsetLeft;
			int renderWidth = element.getWidth() - insetOffsetLeft - insetOffsetRight;

			renderEngine.renderQuad(x, y, renderWidth, renderHeight);

		} else {

			int x = (element.getX() + element.getWidth() / 2) - size / 2 + insetOffsetLeft;
			int renderWidth = size - insetOffsetLeft - insetOffsetRight;

			renderEngine.renderQuad(x, y, renderWidth, renderHeight);
		}

		renderEngine.restoreState();
	}
}
