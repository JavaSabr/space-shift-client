package com.ss.client.gui.element.effect;

import de.lessvoid.nifty.effects.EffectImpl;

/**
 * Интерфейс для реализации эффекта елемента.
 * 
 * @author Ronn
 */
public interface EffectUI extends EffectImpl {

	public static final String PROP_POST = "post";
	public static final String PROP_COLOR = "color";
	public static final String PROP_NEVER_STOP_RENDERING = "neverStopRendering";
	public static final String PROP_TIME_TYPE = "timeType";
	public static final String PROP_CUSTOM_KEY = "customKey";
	public static final String PROP_FILENAME = "filename";
	public static final String PROP_HEIGHT = "height";
	public static final String PROP_WIDTH = "width";
	public static final String PROP_INSET = "inset";
	public static final String PROP_OFFSET_Y = "offsetY";
	public static final String PROP_OFFSET_X = "offsetX";
}
