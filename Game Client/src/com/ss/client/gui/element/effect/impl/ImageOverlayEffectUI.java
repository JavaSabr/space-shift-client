package com.ss.client.gui.element.effect.impl;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.effects.Falloff;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.render.image.ImageModeFactory;
import de.lessvoid.nifty.render.image.ImageModeHelper;
import de.lessvoid.nifty.tools.Alpha;
import de.lessvoid.nifty.tools.SizeValue;

/**
 * Реализация эффекта наложения изображения.
 * 
 * @author Ronn
 */
public class ImageOverlayEffectUI extends AbstractEffectUI {

	public static final String PROP_ACTIVE_BEFORE_START_DELAY = "activeBeforeStartDelay";
	public static final String PROP_HIDE_IF_NOT_ENOUGH_SPACE = "hideIfNotEnoughSpace";
	public static final String PROP_CENTER = "center";
	public static final String PROP_ALPHA = "alpha";

	/** изображение */
	private NiftyImage image;
	/** уровень прозрачности */
	private Alpha alpha;

	/** отступы */
	private SizeValue inset;
	/** ширина изображения */
	private SizeValue width;
	/** высота изображения */
	private SizeValue height;

	/** рендерить ли по центу элемента */
	private boolean center;
	private boolean hideIfNotEnoughSpace;
	private boolean activeBeforeStartDelay;

	@Override
	public void activate(final Nifty nifty, final Element element, final EffectProperties parameter) {
		this.image = nifty.getRenderEngine().createImage(nifty.getCurrentScreen(), parameter.getProperty(PROP_FILENAME), false);

		String areaProviderProperty = ImageModeHelper.getAreaProviderProperty(parameter);
		String renderStrategyProperty = ImageModeHelper.getRenderStrategyProperty(parameter);

		if((areaProviderProperty != null) || (renderStrategyProperty != null)) {
			image.setImageMode(ImageModeFactory.getSharedInstance().createImageMode(areaProviderProperty, renderStrategyProperty));
		}

		this.alpha = new Alpha(parameter.getProperty(PROP_ALPHA, "#f"));
		this.inset = new SizeValue(parameter.getProperty(PROP_INSET, "0px"));
		this.width = new SizeValue(parameter.getProperty(PROP_WIDTH, element.getWidth() + "px"));
		this.height = new SizeValue(parameter.getProperty(PROP_HEIGHT, element.getHeight() + "px"));
		this.center = Boolean.valueOf(parameter.getProperty(PROP_CENTER, "false"));
		this.hideIfNotEnoughSpace = Boolean.valueOf(parameter.getProperty(PROP_HIDE_IF_NOT_ENOUGH_SPACE, "false"));
		this.activeBeforeStartDelay = Boolean.valueOf(parameter.getProperty(PROP_ACTIVE_BEFORE_START_DELAY, "false"));
	}

	@Override
	public void deactivate() {
		image.dispose();
	}

	@Override
	public void execute(final Element element, final float normalizedTime, final Falloff falloff, final NiftyRenderEngine renderEngine) {

		if(!isActiveBeforeStartDelay() && normalizedTime <= 0.0) {
			return;
		}

		int insetOffset = inset.getValueAsInt(element.getWidth());
		int imageX = element.getX() + insetOffset;
		int imageY = element.getY() + insetOffset;
		int imageWidth = width.getValueAsInt(element.getWidth()) - insetOffset * 2;
		int imageHeight = height.getValueAsInt(element.getHeight()) - insetOffset * 2;

		if(isHideIfNotEnoughSpace()) {
			if(imageWidth > element.getWidth() || imageHeight > element.getHeight()) {
				return;
			}
		}

		NiftyImage image = getImage();
		Alpha alpha = getAlpha();

		renderEngine.saveState(null);

		if(falloff != null) {
			renderEngine.setColorAlpha(alpha.mutiply(falloff.getFalloffValue()).getAlpha());
		} else {
			if(!renderEngine.isColorAlphaChanged()) {
				renderEngine.setColorAlpha(alpha.getAlpha());
			}
		}

		if(isCenter()) {
			renderEngine.renderImage(image, element.getX() + (element.getWidth() - imageWidth) / 2, element.getY() + (element.getHeight() - imageHeight) / 2, imageWidth, imageHeight);
		} else {
			renderEngine.renderImage(image, imageX, imageY, imageWidth, imageHeight);
		}

		renderEngine.restoreState();
	}

	/**
	 * @return уровень прозрачности.
	 */
	public Alpha getAlpha() {
		return alpha;
	}

	/**
	 * @return изображение.
	 */
	public NiftyImage getImage() {
		return image;
	}

	public boolean isActiveBeforeStartDelay() {
		return activeBeforeStartDelay;
	}

	/**
	 * @return рендерить ли по центу элемента.
	 */
	public boolean isCenter() {
		return center;
	}

	public boolean isHideIfNotEnoughSpace() {
		return hideIfNotEnoughSpace;
	}
}
