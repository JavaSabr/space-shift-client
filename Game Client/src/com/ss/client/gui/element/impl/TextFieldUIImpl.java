package com.ss.client.gui.element.impl;

import java.util.Properties;

import rlib.util.Strings;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.TextFieldUI;
import com.ss.client.gui.element.event.impl.KeyInputEvent;
import com.ss.client.gui.element.filter.TextFieldDeleteFilter;
import com.ss.client.gui.element.filter.TextFieldDisplayFormat;
import com.ss.client.gui.element.filter.TextFieldInputFilter;
import com.ss.client.gui.element.impl.logic.TextFieldUILogic;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.FocusHandler;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.elements.tools.FontHelper;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента для ввода текста.
 * 
 * @author Ronn
 */
public class TextFieldUIImpl extends AbstractElementUI implements TextFieldUI {

	/** локальные ивенты ввода с клавиатуры */
	private final ThreadLocal<KeyInputEvent<ElementUI>> localKeyInputEvent = new ThreadLocal<KeyInputEvent<ElementUI>>() {

		@Override
		protected KeyInputEvent<ElementUI> initialValue() {
			return new KeyInputEvent<>();
		}
	};

	private Nifty nifty;
	private Screen screen;

	/** элемент текста */
	private Element textElement;
	/** элемент поля ввода */
	private Element fieldElement;
	/** элемент курсора */
	private Element cursorElement;

	/** обработчик логики поля ввода */
	private TextFieldUILogic textField;
	/** обработчик фокуса элемента */
	private FocusHandler focusHandler;

	/** индекс первого видимого символа */
	private int firstVisibleCharacterIndex;
	/** индекс последнего видимого символа */
	private int lastVisibleCharacterIndex;
	/** ширина поля ввода */
	private int fieldWidth;
	/** позиция при клике мышкой */
	private int fromClickCursorPos;
	/** позиция при движении кликнутой мышки */
	private int toClickCursorPos;

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties properties, final Attributes attributes) {
		bind(element);

		this.nifty = nifty;
		this.screen = screen;
		this.fromClickCursorPos = -1;
		this.toClickCursorPos = -1;

		this.textField = new TextFieldUILogic(properties.getProperty(PROPERTY_TEXT, Strings.EMPTY), nifty.getClipboard(), this);
		this.textField.toFirstPosition();

		this.textElement = element.findElementByName(TEXT_ELEMENT_ID);
		this.fieldElement = element.findElementByName(TEXT_FIELD_ID);
		this.cursorElement = element.findElementByName(TEXT_CURSOR_ID);

		if(properties.containsKey(PROPERTY_MAX_LENGTH)) {
			setMaxLength(Integer.parseInt(properties.getProperty(PROPERTY_MAX_LENGTH)));
		}
	}

	/**
	 * Рассчет индекса первого видимого символа.
	 */
	private void calcFirstVisibleIndex(final int cursorPos) {
		if(cursorPos > lastVisibleCharacterIndex) {
			final int cursorPosDelta = cursorPos - lastVisibleCharacterIndex;
			firstVisibleCharacterIndex += cursorPosDelta;
		} else if(cursorPos < firstVisibleCharacterIndex) {
			final int cursorPosDelta = firstVisibleCharacterIndex - cursorPos;
			firstVisibleCharacterIndex -= cursorPosDelta;
		}
	}

	/**
	 * Рассчет индекса последнего видимого символа.
	 */
	private void calcLastVisibleIndex(final TextRenderer textRenderer) {

		final CharSequence currentText = getTextField().getDisplayedText();

		if(firstVisibleCharacterIndex < currentText.length()) {
			final CharSequence textToCheck = currentText.subSequence(firstVisibleCharacterIndex, currentText.length());
			final int lengthFitting = FontHelper.getVisibleCharactersFromStart(textRenderer.getFont(), textToCheck, fieldWidth, 1.0f);
			lastVisibleCharacterIndex = lengthFitting + firstVisibleCharacterIndex;
		} else {
			lastVisibleCharacterIndex = firstVisibleCharacterIndex;
		}
	}

	private void checkBounds(final CharSequence text, final TextRenderer textRenderer) {
		final int textLen = text.length();
		if(firstVisibleCharacterIndex > textLen) {
			// re position so that we show at much possible text
			lastVisibleCharacterIndex = textLen;
			firstVisibleCharacterIndex = FontHelper.getVisibleCharactersFromEnd(textRenderer.getFont(), text, fieldWidth, 1.0f);
		}
	}

	/**
	 * Установка позиции курсора в поле ввода.
	 * 
	 * @param mouseX координаты указателя.
	 * @param mouseY координаты указателя.
	 */
	@Override
	public boolean elementPrimaryClick(int mouseX, int mouseY) {

		final CharSequence visibleString = getVisibleText();
		final int indexFromPixel = getCursorPosFromMouse(mouseX, visibleString);

		if(indexFromPixel != -1) {
			setFromClickCursorPos(getFirstVisibleCharacterIndex() + indexFromPixel);
		}

		TextFieldUILogic textField = getTextField();
		textField.resetSelection();
		textField.updateCursorPosition(getFromClickCursorPos());

		updateCursor();

		return super.elementPrimaryClick(mouseX, mouseY);
	}

	/**
	 * Обновление выделение текста курсором.
	 * 
	 * @param mouseX координаты указателя.
	 * @param mouseY координаты указателя.
	 */
	@Override
	public boolean elementPrimaryClickMouseMove(int mouseX, int mouseY) {

		final CharSequence visibleString = getVisibleText();
		final int indexFromPixel = getCursorPosFromMouse(mouseX, visibleString);

		if(indexFromPixel != -1) {
			setToClickCursorPos(getFirstVisibleCharacterIndex() + indexFromPixel);
		}

		TextFieldUILogic textField = getTextField();
		textField.updateCursorPosition(getFromClickCursorPos());
		textField.startSelecting();
		textField.updateCursorPosition(getToClickCursorPos());
		textField.endSelecting();

		updateCursor();

		return super.elementPrimaryClickMouseMove(mouseX, mouseY);
	}

	@Override
	public Color getColor() {
		TextRenderer renderer = getTextElement().getRenderer(TextRenderer.class);
		return renderer.getColor();
	}

	/**
	 * @return элемент курсора.
	 */
	private Element getCursorElement() {
		return cursorElement;
	}

	private int getCursorPosFromMouse(final int mouseX, final CharSequence visibleString) {
		final TextRenderer textRenderer = getTextElement().getRenderer(TextRenderer.class);
		return FontHelper.getCharacterIndexFromPixelPosition(textRenderer.getFont(), visibleString, mouseX - getFieldElement().getX(), 1.0F);
	}

	@Override
	public String getDisplayedText() {
		return String.valueOf(getTextField().getDisplayedText());
	}

	/**
	 * @return элемент поля ввода.
	 */
	private Element getFieldElement() {
		return fieldElement;
	}

	/**
	 * @return ширина поля ввода.
	 */
	private int getFieldWidth() {
		return fieldWidth;
	}

	/**
	 * @return индекс первого видимого символа.
	 */
	private int getFirstVisibleCharacterIndex() {
		return firstVisibleCharacterIndex;
	}

	/**
	 * @return обработчик фокуса элемента.
	 */
	private FocusHandler getFocusHandler() {
		return focusHandler;
	}

	/**
	 * @return позиция при клике мышкой.
	 */
	private int getFromClickCursorPos() {
		return fromClickCursorPos;
	}

	/**
	 * @return индекс послднего видимого символа.
	 */
	public int getLastVisibleCharacterIndex() {
		return lastVisibleCharacterIndex;
	}

	@Override
	public String getRealText() {
		return String.valueOf(getTextField().getRealText());
	}

	/**
	 * @return элемент текста.
	 */
	private Element getTextElement() {
		return textElement;
	}

	/**
	 * @return обработчик логики поля ввода.
	 */
	private TextFieldUILogic getTextField() {
		return textField;
	}

	/**
	 * @return позиция при движении кликнутой мышки.
	 */
	public int getToClickCursorPos() {
		return toClickCursorPos;
	}

	/**
	 * @return видимый текст в рамках поля ввода.
	 */
	private CharSequence getVisibleText() {
		CharSequence displayedText = getTextField().getDisplayedText();
		return displayedText.subSequence(getFirstVisibleCharacterIndex(), getLastVisibleCharacterIndex());
	}

	@Override
	public void init(final Properties parameter, final Attributes attributes) {

		this.focusHandler = screen.getFocusHandler();

		Element textElement = getTextElement();
		Element fieldElement = getFieldElement();
		Element cursorElement = getCursorElement();

		TextRenderer textRenderer = textElement.getRenderer(TextRenderer.class);

		TextFieldUILogic textField = getTextField();
		textField.setTextAndNotify(textElement.getRenderer(TextRenderer.class).getOriginalText());

		setFieldWidth(fieldElement.getWidth() - cursorElement.getWidth());

		RenderFont font = textRenderer.getFont();
		CharSequence displayedText = textField.getDisplayedText();

		setFirstVisibleCharacterIndex(0);
		setLastVisibleCharacterIndex(FontHelper.getVisibleCharactersFromStart(font, displayedText, getFieldWidth(), 1.0f));
		updateCursor();

		super.init(parameter, attributes);
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {

		if(inputEvent == null) {
			return false;
		}

		TextFieldUILogic textField = getTextField();
		FocusHandler focusHandler = getFocusHandler();
		Element fieldElement = getFieldElement();

		switch(inputEvent) {
			case MoveCursorLeft: {
				textField.cursorLeft();
				break;
			}
			case MoveCursorRight: {
				textField.cursorRight();
				break;
			}
			case Delete: {
				textField.delete();
				break;
			}
			case Backspace: {
				textField.backspace();
				break;
			}
			case MoveCursorToLastPosition: {
				textField.toLastPosition();
				break;
			}
			case MoveCursorToFirstPosition: {
				textField.toFirstPosition();
				break;
			}
			case SelectionStart: {
				textField.startSelecting();
				break;
			}
			case SelectionEnd: {
				textField.endSelecting();
				break;
			}
			case Cut: {
				textField.cut();
				break;
			}
			case Copy: {
				textField.copy();
				break;
			}
			case Paste: {
				textField.put();
				break;
			}
			case SelectAll: {
				textField.selectAll();
				break;
			}
			case Character: {
				textField.insert(inputEvent.getCharacter());
				break;
			}
			case NextInputElement: {
				focusHandler.getNext(fieldElement).setFocus();
				break;
			}
			case PrevInputElement: {
				focusHandler.getPrev(fieldElement).setFocus();
				break;
			}
			default: {
				break;
			}
		}

		updateCursor();

		KeyInputEvent<ElementUI> event = localKeyInputEvent.get();
		event.setInputEvent(inputEvent);

		notifyEvent(event);
		return true;
	}

	@Override
	public void layoutCallback() {
		setFieldWidth(getFieldElement().getWidth() - getCursorElement().getWidth());
	}

	@Override
	public void onFocus(final boolean getFocus) {

		Element cursorElement = getCursorElement();

		if(cursorElement != null) {

			super.onFocus(getFocus);

			if(getFocus) {
				cursorElement.startEffect(EffectEventId.onCustom);
			} else {
				cursorElement.stopEffect(EffectEventId.onCustom);
			}

			updateCursor();
		}
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void setColor(Color color) {
		TextRenderer renderer = getTextElement().getRenderer(TextRenderer.class);
		renderer.setColor(color);
	}

	@Override
	public void setCursorPosition(final int position) {
		getTextField().updateCursorPosition(position);
		updateCursor();
	}

	@Override
	public void setDeleteFilter(TextFieldDeleteFilter filter) {
		getTextField().setDeleteFilter(filter);
	}

	/**
	 * @param fieldWidth ширина поля ввода.
	 */
	public void setFieldWidth(int fieldWidth) {
		this.fieldWidth = fieldWidth;
	}

	/**
	 * @param firstVisibleCharacterIndex индекс первого видимого символа.
	 */
	private void setFirstVisibleCharacterIndex(int firstVisibleCharacterIndex) {
		this.firstVisibleCharacterIndex = firstVisibleCharacterIndex;
	}

	@Override
	public void setFormat(final TextFieldDisplayFormat format) {
		getTextField().setFormat(format);
	}

	/**
	 * @param fromClickCursorPos позиция при клике мышкой.
	 */
	public void setFromClickCursorPos(int fromClickCursorPos) {
		this.fromClickCursorPos = fromClickCursorPos;
	}

	@Override
	public void setInputFilter(TextFieldInputFilter filter) {
		getTextField().setInputFilter(filter);
	}

	/**
	 * @param lastVisibleCharacterIndex индекс последнего видимого символа.
	 */
	private void setLastVisibleCharacterIndex(int lastVisibleCharacterIndex) {
		this.lastVisibleCharacterIndex = lastVisibleCharacterIndex;
	}

	@Override
	public void setMaxLength(final int maxLength) {
		getTextField().setMaxLength(maxLength);
		updateCursor();
	}

	@Override
	public void setText(final CharSequence text) {
		getTextField().setText(nifty.specialValuesReplace(text.toString()));
		updateCursor();
	}

	/**
	 * @param toClickCursorPos позиция при движении кликнутой мышки.
	 */
	private void setToClickCursorPos(int toClickCursorPos) {
		this.toClickCursorPos = toClickCursorPos;
	}

	@Override
	public void textChangeEvent(final String newText) {
		updateCursor();
	}

	/**
	 * Обновление курсора поля ввода.
	 */
	private void updateCursor() {

		TextFieldUILogic textField = getTextField();
		Element textElement = getTextElement();

		final TextRenderer textRenderer = textElement.getRenderer(TextRenderer.class);
		RenderFont font = textRenderer.getFont();

		final String text = String.valueOf(textField.getDisplayedText());

		checkBounds(text, textRenderer);
		calcLastVisibleIndex(textRenderer);

		textRenderer.setText(text);
		textRenderer.setSelection(textField.getSelectionStart(), textField.getSelectionEnd());

		// calc cursor position
		final int cursorPos = textField.getCursorPosition();

		// outside, move window to fit cursorPos inside [first,last]
		calcFirstVisibleIndex(cursorPos);
		calcLastVisibleIndex(textRenderer);

		final String substring2 = text.substring(0, getFirstVisibleCharacterIndex());
		final int d = font.getWidth(substring2);
		textRenderer.setXoffsetHack(-d);

		final String substring = text.substring(0, cursorPos);
		final int textWidth = font.getWidth(substring);

		Element cursorElement = getCursorElement();

		final int cursorX = textWidth - d;
		final int cursorY = (getElement().getHeight() - cursorElement.getHeight()) / 2;

		cursorElement.setConstraintX(GameUtil.getPixelSize(cursorX));
		cursorElement.setConstraintY(GameUtil.getPixelSize(cursorY));
		cursorElement.startEffect(EffectEventId.onActive, null);

		ElementUtils.updateLayout(cursorElement.getParent());
	}
}
