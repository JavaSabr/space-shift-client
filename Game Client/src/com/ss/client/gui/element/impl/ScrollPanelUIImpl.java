package com.ss.client.gui.element.impl;

import java.util.Properties;

import rlib.util.array.Array;

import com.ss.client.gui.element.ScrollPanelUI;
import com.ss.client.gui.element.ScrollbarUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.ScrollbarChangeEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.input.NiftyMouseInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента скролируемой панели.
 * 
 * @author Ronn
 */
public class ScrollPanelUIImpl extends PanelUIImpl implements ScrollPanelUI {

	/** слушатель изменений скроллбара */
	private final ElementUIEventListener<ScrollbarUI> scrollbarEventListener = new ElementUIEventListener<ScrollbarUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ScrollbarUI> event) {

			Element container = getContainer();

			if(container == null) {
				return;
			}

			Array<Element> elements = container.getFastElements();

			if(elements.isEmpty()) {
				return;
			}

			ScrollbarChangeEvent changeEvent = (ScrollbarChangeEvent) event;
			ScrollbarUI scrollbar = event.getElement();

			if(scrollbar == getVerticalScrollBar()) {

				final Element scrollElement = findScrollElement(elements);

				if(scrollElement != null) {
					scrollElement.setConstraintY(GameUtil.getPixelSize(-(int) changeEvent.getValue()));
					ElementUtils.updateLayout(container);
				}

			} else if(scrollbar == getHorizontalScrollBar()) {

				final Element scrollElement = findScrollElement(elements);

				if(scrollElement != null) {
					scrollElement.setConstraintX(GameUtil.getPixelSize(-(int) changeEvent.getValue()));
					ElementUtils.updateLayout(container);
				}
			}
		}
	};

	/** контейнер скролируемого контента */
	private Element container;

	/** режим автоскролирования */
	private AutoScroll autoScroll;

	/** вертикальный скроллбар */
	private ScrollbarUI verticalScrollBar;
	/** горизонтальный скроллбар */
	private ScrollbarUI horizontalScrollBar;

	/** размер шага по Х */
	private float stepSizeX;
	/** размер шага по У */
	private float stepSizeY;

	/** размер страницы по Х */
	private float pageSizeX;
	/** размер страницы по У */
	private float pageSizeY;

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties properties, final Attributes attributes) {
		super.bind(element);

		this.container = element.findElementByName(CONTAINER_ID);
		this.stepSizeX = new Float(properties.getProperty(PROPERTY_STEP_SIZE_X, "1.0"));
		this.stepSizeY = new Float(properties.getProperty(PROPERTY_STEP_SIZE_Y, "1.0"));
		this.pageSizeX = new Float(properties.getProperty(PROPERTY_PAGE_SIZE_X, "1.0"));
		this.pageSizeY = new Float(properties.getProperty(PROPERTY_PAGE_SIZE_Y, "1.0"));
		this.autoScroll = AutoScroll.valueOf(properties.getProperty(PROPERTY_AUTO_SCROLL));

		this.horizontalScrollBar = element.findControl(HORIZONTAL_SCROLLBAR_ID, ScrollbarUI.class);

		if(horizontalScrollBar != null) {
			horizontalScrollBar.addElementListener(ScrollbarChangeEvent.EVENT_TYPE, getScrollbarEventListener());
		}

		this.verticalScrollBar = element.findControl(VERTICAL_SCROLLBAR_ID, ScrollbarUI.class);

		if(verticalScrollBar != null) {
			verticalScrollBar.addElementListener(ScrollbarChangeEvent.EVENT_TYPE, getScrollbarEventListener());
		}
	}

	protected Element findScrollElement(Array<Element> elements) {
		return elements.first();
	}

	@Override
	public AutoScroll getAutoScroll() {
		return autoScroll;
	}

	/**
	 * @return контейнер скролируемого контента.
	 */
	protected Element getContainer() {
		return container;
	}

	@Override
	public float getHorizontalPos() {

		ScrollbarUI scrollbar = getHorizontalScrollBar();

		if(scrollbar != null) {
			return scrollbar.getValue();
		}

		return 0.f;
	}

	/**
	 * @return горизонтальный скроллбар.
	 */
	protected ScrollbarUI getHorizontalScrollBar() {
		return horizontalScrollBar;
	}

	/**
	 * @return размер страницы по Х.
	 */
	public float getPageSizeX() {
		return pageSizeX;
	}

	/**
	 * @return размер страницы по У.
	 */
	public float getPageSizeY() {
		return pageSizeY;
	}

	/**
	 * @return слушатель изменений скроллбара.
	 */
	protected ElementUIEventListener<ScrollbarUI> getScrollbarEventListener() {
		return scrollbarEventListener;
	}

	/**
	 * @return размер шага по Х.
	 */
	public float getStepSizeX() {
		return stepSizeX;
	}

	/**
	 * @return размер шага по У.
	 */
	public float getStepSizeY() {
		return stepSizeY;
	}

	@Override
	public float getVerticalPos() {

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {
			return scrollbar.getValue();
		}

		return 0.f;
	}

	/**
	 * @return вертикальный скроллбар.
	 */
	protected ScrollbarUI getVerticalScrollBar() {
		return verticalScrollBar;
	}

	@Override
	public void init(final Properties parameter, final Attributes controlDefinitionAttributes) {
		initializeScrollbars();
		super.init(parameter, controlDefinitionAttributes);
	}

	/**
	 * Настройка скроллбаров.
	 */
	private void initializeScrollbars() {

		Element container = getContainer();

		if(container != null) {

			Array<Element> elements = container.getFastElements();

			if(elements.isEmpty()) {
				return;
			}

			final Element scrollElement = findScrollElement(elements);

			if(scrollElement != null) {

				ScrollbarUI scrollbar = getHorizontalScrollBar();

				if(scrollbar != null) {
					scrollbar.setWorldMax(scrollElement.getWidth());
					updateWorldH();
					scrollbar.setWorldPageSize(scrollbar.getWidth());
					scrollbar.setValue(0.0f);
					scrollbar.setButtonStepSize(getStepSizeX());
					scrollbar.setPageStepSize(getPageSizeX());
				}

				scrollbar = getVerticalScrollBar();

				if(scrollbar != null) {
					scrollbar.setWorldMax(scrollElement.getHeight());
					updateWorldV();
					scrollbar.setWorldPageSize(scrollbar.getHeight());
					scrollbar.setValue(0.0f);
					scrollbar.setButtonStepSize(getStepSizeY());
					scrollbar.setPageStepSize(getPageSizeY());
				}

				scrollElement.setConstraintX(GameUtil.getPixelSize(0));
				scrollElement.setConstraintY(GameUtil.getPixelSize(0));
			}

			ElementUtils.updateLayout(scrollElement.getParent());
		}
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void layoutCallback() {

		Element container = getContainer();

		if(container != null) {

			Array<Element> elements = container.getFastElements();

			if(elements.isEmpty()) {
				return;
			}

			final Element scrollElement = findScrollElement(elements);

			if(scrollElement != null) {

				ScrollbarUI scrollbar = getHorizontalScrollBar();

				if(scrollbar != null) {
					scrollbar.setWorldMax(scrollElement.getWidth());
					scrollbar.setWorldPageSize(scrollbar.getWidth());
					updateWorldH();
				}

				scrollbar = getVerticalScrollBar();

				if(scrollbar != null) {
					scrollbar.setWorldMax(scrollElement.getHeight());
					scrollbar.setWorldPageSize(scrollbar.getHeight());
					updateWorldV();
				}
			}
		}
	}

	public void mouseWheel(final Element e, final NiftyMouseInputEvent inputEvent) {

		int mouseWheel = inputEvent.getMouseWheel();

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {

			float currentValue = scrollbar.getValue();

			if(mouseWheel < 0) {
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
			} else if(mouseWheel > 0) {
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
			}
		}
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void setAutoScroll(final AutoScroll auto) {
		this.autoScroll = auto;

		updateWorldH();
		updateWorldV();
	}

	@Override
	public void setHorizontalPos(final float xPos) {

		ScrollbarUI scrollbar = getHorizontalScrollBar();

		if(scrollbar != null) {
			scrollbar.setValue(xPos);
		}
	}

	@Override
	public void setPageSizeX(final float pageSizeX) {
		this.pageSizeX = pageSizeX;

		ScrollbarUI scrollbar = getHorizontalScrollBar();

		if(scrollbar != null) {
			scrollbar.setPageStepSize(pageSizeX);
		}
	}

	@Override
	public void setPageSizeY(final float pageSizeY) {
		this.pageSizeY = pageSizeY;

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {
			scrollbar.setPageStepSize(pageSizeY);
		}
	}

	@Override
	public void setStepSizeX(final float stepSizeX) {
		this.stepSizeX = stepSizeX;

		ScrollbarUI scrollbar = getHorizontalScrollBar();

		if(scrollbar != null) {
			scrollbar.setButtonStepSize(stepSizeX);
		}
	}

	@Override
	public void setStepSizeY(final float stepSizeY) {
		this.stepSizeY = stepSizeY;

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {
			scrollbar.setButtonStepSize(stepSizeY);
		}
	}

	@Override
	public void setUp(final float stepSizeX, final float stepSizeY, final float pageSizeX, final float pageSizeY, final AutoScroll auto) {
		this.stepSizeX = stepSizeX;
		this.stepSizeY = stepSizeY;
		this.pageSizeX = pageSizeX;
		this.pageSizeY = pageSizeY;
		this.autoScroll = auto;

		initializeScrollbars();
	}

	@Override
	public void setVerticalPos(final float yPos) {

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {
			scrollbar.setValue(yPos);
		}
	}

	@Override
	public void showElementVertical(final int elemCount) {
		showElementVertical(elemCount, VerticalAlign.center);
	}

	private void showElementVertical(final int elemCount, final VerticalAlign valign) {

		float newPos;

		switch(valign) {
			case top:
				newPos = stepSizeY * elemCount;
				break;
			case center:
				newPos = stepSizeY * elemCount - getElement().getHeight() / 2;
				break;
			case bottom:
				newPos = stepSizeY * elemCount - getElement().getHeight();
				break;
			default:
				newPos = 0;
		}

		setVerticalPos(newPos);
	}

	private void updateWorldH() {

		ScrollbarUI scrollbar = getHorizontalScrollBar();

		if(scrollbar != null) {
			if(autoScroll == AutoScroll.RIGHT || autoScroll == AutoScroll.BOTTOM_RIGHT || autoScroll == AutoScroll.TOP_RIGHT) {
				scrollbar.setValue(scrollbar.getWorldMax());
			} else if(autoScroll == AutoScroll.LEFT || autoScroll == AutoScroll.BOTTOM_LEFT || autoScroll == AutoScroll.TOP_LEFT) {
				scrollbar.setValue(0);
			}
		}
	}

	private void updateWorldV() {

		ScrollbarUI scrollbar = getVerticalScrollBar();

		if(scrollbar != null) {
			if(autoScroll == AutoScroll.BOTTOM || autoScroll == AutoScroll.BOTTOM_LEFT || autoScroll == AutoScroll.BOTTOM_RIGHT) {
				scrollbar.setValue(scrollbar.getWorldMax());
			} else if(autoScroll == AutoScroll.TOP || autoScroll == AutoScroll.TOP_LEFT || autoScroll == AutoScroll.TOP_RIGHT) {
				scrollbar.setValue(0);
			}
		}
	}
}
