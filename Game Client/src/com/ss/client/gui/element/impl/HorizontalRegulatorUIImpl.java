package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.ButtonUI;
import com.ss.client.gui.element.RegulatorUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.ChangeElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента горизонтального регулятора.
 * 
 * @author Ronn
 */
public class HorizontalRegulatorUIImpl extends AbstractElementUI implements RegulatorUI {

	private final ThreadLocal<ChangeElementUIEvent<RegulatorUI>> localChangeEvent = new ThreadLocal<ChangeElementUIEvent<RegulatorUI>>() {

		@Override
		protected ChangeElementUIEvent<RegulatorUI> initialValue() {
			return new ChangeElementUIEvent<>();
		}
	};

	/** изорбажение полосок регулятора */
	private String valuePanelImage;
	private String basePanelImage;

	/** цвета полосок регулятора */
	private Color valuePanelColor;
	private Color basePanelColor;

	/** базовая панель расположения полоски значения */
	private Element basePanel;
	/** панель полоски значения */
	private Element valuePanel;

	/** минимальное значение регулятора */
	private float minimum;
	/** максимальное значение регулятора */
	private float maximum;
	/** шаг регулятора */
	private float step;
	/** текущее значение регулятора */
	private float current;

	public HorizontalRegulatorUIImpl() {
		this.minimum = 0;
		this.maximum = 1;
		this.step = 0.1F;
		this.current = 0;
	}

	/**
	 * Приминение изминений.
	 */
	protected void applyChange() {

		Element basePanel = getBasePanel();
		Element valuePanel = getValuePanel();

		float percent = getCurrent() / getMaximum();

		SizeValue width = GameUtil.getPixelSize((int) (basePanel.getWidth() * percent));
		valuePanel.setConstraintWidth(width);

		ElementUtils.updateLayout(basePanel);

		ChangeElementUIEvent<RegulatorUI> event = localChangeEvent.get();
		event.setElement(this);

		notifyEvent(event);
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.basePanel = element.findElementByName(BASE_PANEL_ID);
		this.valuePanel = element.findElementByName(VALUE_PANEL_ID);

		ButtonUI button = element.findNiftyControl(BUTTON_ADD_ID, ButtonUI.class);
		button.addElementListener(PrimaryClickEvent.EVENT_TYPE, new ElementUIEventListener<ButtonUI>() {

			@Override
			public void notifyEvent(ElementUIEvent<ButtonUI> event) {
				regulatorAdd();
			}
		});

		button = element.findNiftyControl(BUTTON_SUB_ID, ButtonUI.class);
		button.addElementListener(PrimaryClickEvent.EVENT_TYPE, new ElementUIEventListener<ButtonUI>() {

			@Override
			public void notifyEvent(ElementUIEvent<ButtonUI> event) {
				regulatorSub();
			}
		});

		this.maximum = Float.parseFloat(parameter.getProperty(MAXIMUM));
		this.minimum = Float.parseFloat(parameter.getProperty(MINIMUM));
		this.step = Float.parseFloat(parameter.getProperty(STEP));

		setCurrent(getMaximum());
	}

	/**
	 * @return базовая панель расположения полоски значения.
	 */
	protected Element getBasePanel() {
		return basePanel;
	}

	@Override
	public float getCurrent() {
		return current;
	}

	@Override
	public float getMaximum() {
		return maximum;
	}

	@Override
	public float getMinimum() {
		return minimum;
	}

	@Override
	public float getStep() {
		return step;
	}

	/**
	 * @return панель полоски значения.
	 */
	protected Element getValuePanel() {
		return valuePanel;
	}

	/**
	 * Обработка увеличения значения.
	 */
	private void regulatorAdd() {
		setCurrent(getCurrent() + getStep());
	}

	/**
	 * Обработка уменьшения значения.
	 */
	private void regulatorSub() {
		setCurrent(getCurrent() - getStep());
	}

	@Override
	public void setCurrent(float current) {

		float old = getCurrent();

		this.current = Math.max(Math.min(current, getMaximum()), getMinimum());

		if(Float.compare(old, getCurrent()) != 0) {
			applyChange();
		}
	}

	@Override
	public void setMaximum(float maximum) {
		this.maximum = maximum;
	}

	@Override
	public void setMinimum(float minimum) {
		this.minimum = minimum;
	}

	@Override
	public void setStep(float step) {
		this.step = step;
	}
}
