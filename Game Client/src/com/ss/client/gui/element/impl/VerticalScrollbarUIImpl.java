package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.ScrollbarUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.MouseWheelEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickMouseMoveEvent;
import com.ss.client.gui.element.event.impl.ScrollbarChangeEvent;
import com.ss.client.gui.element.impl.logic.ScrollbarUILogic;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.NextPrevHelper;
import de.lessvoid.nifty.controls.scrollbar.ScrollbarView;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация вертикального скроллбара.
 * 
 * @author Ronn
 */
public class VerticalScrollbarUIImpl extends AbstractElementUI implements ScrollbarUI, ScrollbarView {

	private final ThreadLocal<ScrollbarChangeEvent> localChangeEvent = new ThreadLocal<ScrollbarChangeEvent>() {

		@Override
		protected ScrollbarChangeEvent initialValue() {
			return new ScrollbarChangeEvent();
		}
	};

	/** слушатель событий кнопок скрола */
	private final ElementUIEventListener<ElementUI> buttonEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {

			if(event.getElement() == getButtonDown()) {
				scrollingDown();
			} else if(event.getElement() == getButtonUp()) {
				scrollingUp();
			}
		}
	};

	/** слушатель событий полоски ползунка */
	private final ElementUIEventListener<ElementUI> backgroundEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {

			PrimaryClickEvent<ElementUI> clickEvent = (PrimaryClickEvent<ElementUI>) event;
			scrollingToBackground(clickEvent.getMouseX(), clickEvent.getMouseY());
		}
	};

	/** слушатель событий ползунка */
	private final ElementUIEventListener<ElementUI> positionEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {

			if(event.getEventType() == PrimaryClickEvent.EVENT_TYPE) {
				PrimaryClickEvent<ElementUI> clickEvent = (PrimaryClickEvent<ElementUI>) event;
				startScrolling(clickEvent.getMouseX(), clickEvent.getMouseY());
			} else if(event.getEventType() == PrimaryClickMouseMoveEvent.EVENT_TYPE) {
				PrimaryClickMouseMoveEvent<ElementUI> mouseMoveEvent = (PrimaryClickMouseMoveEvent<ElementUI>) event;
				processScrolling(mouseMoveEvent.getMouseX(), mouseMoveEvent.getMouseY());
			}
		}
	};

	/** слушатель вращения колесика */
	private final ElementUIEventListener<ElementUI> wheelEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {

			MouseWheelEvent<ElementUI> wheelEvent = (MouseWheelEvent<ElementUI>) event;
			scrollingToWheel(wheelEvent.getValue());
		}
	};

	/** реализация логики скролинга */
	private final ScrollbarUILogic scrollbarLogic;

	/** кнопка скрола вверх */
	private ElementUI buttonUp;
	/** кнопка скрола вниз */
	private ElementUI buttonDown;
	/** фоновая полоса ползунка */
	private ElementUI background;
	/** ползунок */
	private ElementUI position;

	/** элемент фоновой полоски скроллбара */
	protected Element backgroundElement;
	/** элемент ползунка скроллбара */
	protected Element positionElement;

	/** помошник ввода */
	private NextPrevHelper nextPrevHelper;

	private float worldMax;
	private float worldPageSize;
	private float initial;
	private float pageStepSize;
	private float buttonStepSize;

	/** минимальный размер ползунка */
	protected int minHandleSize;

	public VerticalScrollbarUIImpl() {
		this.scrollbarLogic = new ScrollbarUILogic();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.buttonUp = element.findControl(BUTTON_UP_ID, ElementUI.class);
		this.buttonUp.addElementListener(PrimaryClickEvent.EVENT_TYPE, getButtonEventListener());
		this.buttonUp.addElementListener(MouseWheelEvent.EVENT_TYPE, getWheelEventListener());

		this.buttonDown = element.findControl(BUTTON_DOWN_ID, ElementUI.class);
		this.buttonDown.addElementListener(PrimaryClickEvent.EVENT_TYPE, getButtonEventListener());
		this.buttonDown.addElementListener(MouseWheelEvent.EVENT_TYPE, getWheelEventListener());

		this.backgroundElement = element.findElementByName(BACKGROUND_ID);

		this.background = backgroundElement.getControl(ElementUI.class);
		this.background.addElementListener(PrimaryClickEvent.EVENT_TYPE, getBackgroundEventListener());
		this.background.addElementListener(MouseWheelEvent.EVENT_TYPE, getWheelEventListener());

		this.positionElement = element.findElementByName(POSITION_ID);

		this.position = positionElement.getControl(ElementUI.class);
		this.position.addElementListener(PrimaryClickEvent.EVENT_TYPE, getPositionEventListener());
		this.position.addElementListener(PrimaryClickMouseMoveEvent.EVENT_TYPE, getPositionEventListener());
		this.position.addElementListener(MouseWheelEvent.EVENT_TYPE, getWheelEventListener());

		this.nextPrevHelper = new NextPrevHelper(element, screen.getFocusHandler());
		this.minHandleSize = positionElement.getHeight();
		this.worldMax = Float.valueOf(parameter.getProperty(PROPERTY_WORLD_MAX, "100.0"));
		this.worldPageSize = Float.valueOf(parameter.getProperty(PROPERTY_WORLD_PAGE_SIZE, "100.0"));
		this.initial = Float.valueOf(parameter.getProperty(PROPERTY_INITIAL, "0.0"));
		this.buttonStepSize = Float.valueOf(parameter.getProperty(PROPERTY_BUTTON_STEP_SIZE, "1.0"));
		this.pageStepSize = Float.valueOf(parameter.getProperty(PROPERTY_PAGE_STEP_SIZE, "25.0"));
		this.scrollbarLogic.bindToView(this, initial, worldMax, worldPageSize, buttonStepSize, pageStepSize);
	}

	@Override
	public int filter(final int pixelX, final int pixelY) {
		return pixelY - getBackgroundElement().getY();
	}

	@Override
	public int getAreaSize() {
		return getBackgroundElement().getHeight();
	}

	/**
	 * @return элемент фоновой полоски скроллбара.
	 */
	public Element getBackgroundElement() {
		return backgroundElement;
	}

	/**
	 * @return слушатель событий полоски ползунка.
	 */
	public ElementUIEventListener<ElementUI> getBackgroundEventListener() {
		return backgroundEventListener;
	}

	/**
	 * @return кнопка скрола вверх.
	 */
	public ElementUI getButtonDown() {
		return buttonDown;
	}

	/**
	 * @return слушатель событий кнопок скрола.
	 */
	public ElementUIEventListener<ElementUI> getButtonEventListener() {
		return buttonEventListener;
	}

	@Override
	public float getButtonStepSize() {
		return getScrollbarLogic().getButtonStepSize();
	}

	/**
	 * @return кнопка скрола вниз.
	 */
	public ElementUI getButtonUp() {
		return buttonUp;
	}

	@Override
	public int getMinHandleSize() {
		return minHandleSize;
	}

	/**
	 * @return помошник ввода.
	 */
	public NextPrevHelper getNextPrevHelper() {
		return nextPrevHelper;
	}

	@Override
	public float getPageStepSize() {
		return scrollbarLogic.getPageStepSize();
	}

	/**
	 * @return элемент ползунка скроллбара.
	 */
	public Element getPositionElement() {
		return positionElement;
	}

	/**
	 * @return слушатель событий ползунка.
	 */
	public ElementUIEventListener<ElementUI> getPositionEventListener() {
		return positionEventListener;
	}

	/**
	 * @return реализация логики скролинга.
	 */
	public ScrollbarUILogic getScrollbarLogic() {
		return scrollbarLogic;
	}

	@Override
	public float getValue() {
		return getScrollbarLogic().getValue();
	}

	/**
	 * @return слушатель вращения колесика.
	 */
	private ElementUIEventListener<ElementUI> getWheelEventListener() {
		return wheelEventListener;
	}

	@Override
	public float getWorldMax() {
		return getScrollbarLogic().getWorldMax();
	}

	@Override
	public float getWorldPageSize() {
		return getScrollbarLogic().getWorldPageSize();
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {

		NextPrevHelper nextPrevHelper = getNextPrevHelper();

		if(nextPrevHelper.handleNextPrev(inputEvent)) {
			return true;
		}

		ScrollbarUILogic scrollbarLogic = getScrollbarLogic();

		if(inputEvent == NiftyInputEvent.MoveCursorUp || inputEvent == NiftyInputEvent.MoveCursorLeft) {
			scrollbarLogic.stepDown();
			return true;
		} else if(inputEvent == NiftyInputEvent.MoveCursorDown || inputEvent == NiftyInputEvent.MoveCursorRight) {
			scrollbarLogic.stepUp();
			return true;
		}

		return false;
	}

	@Override
	public void layoutCallback() {
		scrollbarLogic.updateView();
	}

	/**
	 * Процесс скролинга через ползунок.
	 */
	public void processScrolling(final int mouseX, final int mouseY) {
		getScrollbarLogic().processMove(filter(mouseX, mouseY));
	}

	/**
	 * Обработка скролированиян а 1 шаг вниз.
	 */
	public void scrollingDown() {
		getScrollbarLogic().stepPageUp();
	}

	/**
	 * Процесс скролинга до указанной позиции на панели скроллбара.
	 */
	public void scrollingToBackground(final int mouseX, final int mouseY) {
		getScrollbarLogic().processClick(filter(mouseX, mouseY));
	}

	/**
	 * Обработка скролинга посредством колеса.
	 */
	public void scrollingToWheel(float value) {

		ScrollbarUILogic scrollbarLogic = getScrollbarLogic();
		float currentValue = scrollbarLogic.getValue();

		if(value < 0) {
			scrollbarLogic.updateValue(currentValue - scrollbarLogic.getButtonStepSize() * value);
		} else if(value > 0) {
			scrollbarLogic.updateValue(currentValue - scrollbarLogic.getButtonStepSize() * value);
		}
	}

	/**
	 * Обработка скролирования на 1 шаг вверх.
	 */
	public void scrollingUp() {
		getScrollbarLogic().stepPageDown();
	}

	@Override
	public void setButtonStepSize(final float stepSize) {
		getScrollbarLogic().setButtonStepSize(stepSize);
	}

	@Override
	public void setHandle(final int pos, final int size) {

		Element backgroundElement = getBackgroundElement();
		Element positionElement = getPositionElement();
		Element element = getElement();

		if(backgroundElement.getHeight() < getMinHandleSize()) {

			if(element.isVisible()) {
				positionElement.hide();
			}

		} else {

			if(element.isVisible()) {
				positionElement.show();
			}

			positionElement.setConstraintY(GameUtil.getPixelSize(pos));
			positionElement.setConstraintHeight(GameUtil.getPixelSize(size));

			ElementUtils.updateLayout(backgroundElement);
		}
	}

	@Override
	public void setPageStepSize(final float stepSize) {
		getScrollbarLogic().setPageStepSize(stepSize);
	}

	@Override
	public void setup(float value, float worldMax, float worldPageSize, float buttonStepSize, float pageStepSize) {
		getScrollbarLogic().setup(value, worldMax, worldPageSize, buttonStepSize, pageStepSize);
	}

	@Override
	public void setValue(float value) {
		getScrollbarLogic().updateValue(value);
	}

	@Override
	public void setWorldMax(final float worldMax) {
		getScrollbarLogic().setWorldMax(worldMax);
	}

	@Override
	public void setWorldPageSize(final float worldPageSize) {
		getScrollbarLogic().setWorldPageSize(worldPageSize);
	}

	/**
	 * Процесс старта скролинга через ползунок.
	 */
	public void startScrolling(final int mouseX, final int mouseY) {
		getScrollbarLogic().processClick(filter(mouseX, mouseY));
	}

	@Override
	public void valueChanged(final float value) {

		ScrollbarChangeEvent event = localChangeEvent.get();
		event.setValue(value);
		event.setElement(this);

		notifyEvent(event);
	}
}
