package com.ss.client.gui.element.impl.list;

import com.ss.client.gui.element.ListElementUI;
import com.ss.client.gui.element.impl.PanelUIImpl;

import de.lessvoid.nifty.effects.EffectEventId;

/**
 * Реализация контрола элемента строки из списка.
 * 
 * @author Ronn
 */
public class ListElementUIImpl<T> extends PanelUIImpl implements ListElementUI<T> {

	/** отображаемый итем */
	private T item;

	/** выбран ли сейчас этот элемент */
	private boolean selected;

	@Override
	public T getItem() {
		return item;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setItem(T item) {
		this.item = item;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;

		if(selected) {
			getElement().startEffect(EffectEventId.onCustom, null, SELECT_EFFECT_KEY);
		} else {
			getElement().resetSingleEffect(EffectEventId.onCustom, SELECT_EFFECT_KEY);
		}
	}
}
