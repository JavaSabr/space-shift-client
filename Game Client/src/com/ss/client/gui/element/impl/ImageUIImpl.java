package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.ImageUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента изображения.
 * 
 * @author Ronn
 */
public class ImageUIImpl extends AbstractElementUI implements ImageUI {

	/** рендер картинки элемента */
	private ImageRenderer renderer;
	/** картинка элемента */
	private NiftyImage image;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.renderer = element.getRenderer(ImageRenderer.class);
		this.image = renderer.getImage();
	}

	@Override
	public NiftyImage getImage() {
		return image;
	}

	@Override
	public void setImage(NiftyImage image) {
		this.image = image;
		this.renderer.setImage(image);
	}

	@Override
	public void setImage(String path) {

		Nifty nifty = GAME.getNifty();
		Screen screen = nifty.getCurrentScreen();

		NiftyRenderEngine renderEngine = nifty.getRenderEngine();
		NiftyImage image = renderEngine.createImage(screen, path, true);

		setImage(image);
	}
}
