package com.ss.client.gui.element.impl;

import java.util.Properties;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация вертикального скроллбара элемента.
 * 
 * @author Ronn
 */
public class HorizontalScrollbarUIImpl extends VerticalScrollbarUIImpl {

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.minHandleSize = positionElement.getWidth();
	}

	@Override
	public int filter(final int pixelX, final int pixelY) {
		return pixelX - backgroundElement.getX();
	}

	@Override
	public int getAreaSize() {
		return backgroundElement.getWidth();
	}

	@Override
	public int getMinHandleSize() {
		return minHandleSize;
	}

	@Override
	public void setHandle(final int pos, final int size) {
		if(backgroundElement.getWidth() < minHandleSize) {
			if(getElement().isVisible()) {
				positionElement.hide();
			}
		} else {
			if(getElement().isVisible()) {
				positionElement.show();
			}
			positionElement.setConstraintX(new SizeValue(pos + "px"));
			positionElement.setConstraintWidth(new SizeValue(size + "px"));
			backgroundElement.layoutElements();
		}
	}
}
