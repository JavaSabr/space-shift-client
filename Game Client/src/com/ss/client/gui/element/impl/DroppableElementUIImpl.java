package com.ss.client.gui.element.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.impl.DroppableDropedEvent;
import com.ss.client.gui.element.filter.DroppableDropFilter;

import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.controls.NiftyInputControl;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.tools.SizeValue;

/**
 * Реализация контейнера перемещаемых элементов.
 * 
 * @author Ronn
 */
public class DroppableElementUIImpl extends AbstractElementUI implements DroppableElementUI {

	protected static final SizeValue ZERO_PIXEL_SIZE = new SizeValue("0px");

	private final ThreadLocal<DroppableDropedEvent<ElementUI>> localDroppableDropEvent = new ThreadLocal<DroppableDropedEvent<ElementUI>>() {

		@Override
		protected DroppableDropedEvent<ElementUI> initialValue() {
			return new DroppableDropedEvent<>();
		}
	};

	/** список фильтров на размещение элемента в контейнере */
	private final Array<DroppableDropFilter> filters;

	/** размещенный элемент в контейнере */
	private DraggableElementUI draggable;

	public DroppableElementUIImpl() {
		this.filters = Arrays.toArray(DroppableDropFilter.class);
	}

	@Override
	public boolean accept(final DroppableElementUI source, final DraggableElementUI draggable) {

		Array<DroppableDropFilter> filters = getFilters();

		if(filters.isEmpty()) {
			return true;
		}

		for(DroppableDropFilter filter : filters.array()) {

			if(filter == null) {
				break;
			}

			if(!filter.accept(source, draggable, this)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void addFilter(final DroppableDropFilter filter) {
		filters.add(filter);
	}

	@Override
	public void drop(final DraggableElementUI draggable, final EndNotify endNotify) {
		drop(draggable, endNotify, true);
	}

	@Override
	public void drop(final DraggableElementUI draggable, final EndNotify endNotify, final boolean notify) {

		setDraggable(draggable);

		Element element = draggable.getElement();
		element.setConstraintX(ZERO_PIXEL_SIZE);
		element.setConstraintY(ZERO_PIXEL_SIZE);
		element.markForMove(getElement(), endNotify);

		DroppableElementUI source = draggable.getDroppable();

		if(source != null && source.getDraggable() == draggable) {
			source.setDraggable(null);
		}

		draggable.setDroppable(this);

		if(notify) {

			DroppableDropedEvent<ElementUI> event = localDroppableDropEvent.get();
			event.setDraggable(draggable);
			event.setSource(source);
			event.setElement(this);

			notifyEvent(event);
		}
	}

	/**
	 * @return перемещаемый элемент.
	 */
	private DraggableElementUI findDraggableChild(final Element element) {

		for(Element child : element.getElements()) {

			if(isDraggable(child)) {
				return child.getControl(DraggableElementUI.class);
			}

			DraggableElementUI draggable = findDraggableChild(child);

			if(draggable != null) {
				return draggable;
			}
		}

		return null;
	}

	@Override
	public DraggableElementUI getDraggable() {
		return draggable;
	}

	/**
	 * @return список фильтров на размещение элемента в контейнере.
	 */
	protected Array<DroppableDropFilter> getFilters() {
		return filters;
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	/**
	 * @return является ли элемент перемещаемым.
	 */
	private boolean isDraggable(final Element element) {

		NiftyInputControl control = element.getAttachedInputControl();

		if(control != null) {
			return control.getController() instanceof DraggableElementUI;
		}

		return false;
	}

	@Override
	public void onStartScreen() {

		DraggableElementUI draggable = findDraggableChild(getElement());

		if(draggable != null) {
			drop(draggable, reactivate(draggable.getElement()), false);
		}
	}

	private EndNotify reactivate(final Element element) {
		return new EndNotify() {

			@Override
			public void perform() {
				element.reactivate();
			}
		};
	}

	@Override
	public void removeFilter(final DroppableDropFilter filter) {
		filters.fastRemove(filter);
	}

	@Override
	public void setDraggable(DraggableElementUI draggable) {
		this.draggable = draggable;
	}
}
