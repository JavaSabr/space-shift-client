package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.DynamicPanelUI;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.layout.BoxConstraints;
import de.lessvoid.nifty.layout.LayoutPart;
import de.lessvoid.nifty.layout.manager.CenterLayout;
import de.lessvoid.nifty.layout.manager.HorizontalLayout;
import de.lessvoid.nifty.layout.manager.LayoutManager;
import de.lessvoid.nifty.layout.manager.VerticalLayout;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация панели с динамическим размером.
 * 
 * @author Ronn
 */
public class DynamicPanelUIImpl extends PanelUIImpl implements DynamicPanelUI {

	/** центральный лаяутинг */
	private boolean center;
	/** горизонтальный лояутинг */
	private boolean horizontal;
	/** вертикальный лояутинг */
	private boolean vertical;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		LayoutManager layoutManager = element.getLayoutManager();

		this.center = layoutManager instanceof CenterLayout;
		this.horizontal = !center && layoutManager instanceof HorizontalLayout;
		this.vertical = !center && !horizontal && layoutManager instanceof VerticalLayout;
	}

	/**
	 * @return центральный лаяутинг.
	 */
	public boolean isCenter() {
		return center;
	}

	/**
	 * @return горизонтальный лояутинг.
	 */
	public boolean isHorizontal() {
		return horizontal;
	}

	/**
	 * @return вертикальный лояутинг.
	 */
	public boolean isVertical() {
		return vertical;
	}

	@Override
	public void updateSize(boolean updateHeight, boolean updateWidth) {

		if(!updateHeight && !updateWidth) {
			return;
		}

		Element element = getElement();

		if(isCenter()) {

			int height = 0;
			int width = 0;

			for(Element child : element.getArrayElements()) {

				if(child == null) {
					break;
				}

				LayoutPart layoutPart = child.getLayoutPart();
				BoxConstraints constraints = layoutPart.getBoxConstraints();

				int temp = 0;
				temp += constraints.getHeight().getValueAsInt(0);
				temp += constraints.getMarginBottom().getValueAsInt(0);
				temp += constraints.getMarginTop().getValueAsInt(0);

				height = Math.max(height, temp);

				temp = 0;
				temp += constraints.getWidth().getValueAsInt(0);
				temp += constraints.getMarginLeft().getValueAsInt(0);
				temp += constraints.getMarginRight().getValueAsInt(0);

				width = Math.max(width, temp);
			}

			int change = 0;

			if(updateHeight && height != element.getHeight()) {
				element.setConstraintHeight(GameUtil.getPixelSize(height));
				change++;
			}

			if(updateWidth && width != element.getWidth()) {
				element.setConstraintWidth(GameUtil.getPixelSize(width));
				change++;
			}

			if(change > 0) {
				ElementUtils.updateLayout(element.getParent());
			}

		} else if(isVertical() && updateHeight) {

			int height = 0;
			int width = 0;

			for(Element child : element.getArrayElements()) {

				if(child == null) {
					break;
				}

				LayoutPart layoutPart = child.getLayoutPart();
				BoxConstraints constraints = layoutPart.getBoxConstraints();

				height += constraints.getHeight().getValueAsInt(0);
				height += constraints.getMarginBottom().getValueAsInt(0);
				height += constraints.getMarginTop().getValueAsInt(0);

				int temp = 0;
				temp += constraints.getWidth().getValueAsInt(0);
				temp += constraints.getMarginLeft().getValueAsInt(0);
				temp += constraints.getMarginRight().getValueAsInt(0);

				width = Math.max(width, temp);
			}

			int change = 0;

			if(updateHeight && height != element.getHeight()) {
				element.setConstraintHeight(GameUtil.getPixelSize(height));
				change++;
			}

			if(updateWidth && width != element.getWidth()) {
				element.setConstraintWidth(GameUtil.getPixelSize(width));
				change++;
			}

			if(change > 0) {
				ElementUtils.updateLayout(element.getParent());
			}

		} else if(isHorizontal()) {

			int height = 0;
			int width = 0;

			for(Element child : element.getArrayElements()) {

				if(child == null) {
					break;
				}

				LayoutPart layoutPart = child.getLayoutPart();
				BoxConstraints constraints = layoutPart.getBoxConstraints();

				width += constraints.getWidth().getValueAsInt(0);
				width += constraints.getMarginLeft().getValueAsInt(0);
				width += constraints.getMarginRight().getValueAsInt(0);

				int temp = 0;
				temp += constraints.getHeight().getValueAsInt(0);
				temp += constraints.getMarginBottom().getValueAsInt(0);
				temp += constraints.getMarginTop().getValueAsInt(0);

				height = Math.max(height, temp);
			}

			int change = 0;

			if(updateHeight && height != element.getHeight()) {
				element.setConstraintHeight(GameUtil.getPixelSize(height));
				change++;
			}

			if(updateWidth && width != element.getWidth()) {
				element.setConstraintWidth(GameUtil.getPixelSize(width));
				change++;
			}

			if(change > 0) {
				ElementUtils.updateLayout(element.getParent());
			}
		}
	}
}
