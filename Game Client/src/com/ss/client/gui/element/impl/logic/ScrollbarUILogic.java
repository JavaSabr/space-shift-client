package com.ss.client.gui.element.impl.logic;

import de.lessvoid.nifty.controls.scrollbar.ScrollbarView;

/**
 * Реализация логики работы элемента скроллбара.
 * 
 * @author Ronn
 */
public final class ScrollbarUILogic {

	/** отображатель скрола */
	private ScrollbarView view;

	private final float min = 0.f;

	/** текущая позиция скрола */
	private float value;
	/** размер скролируемого элемента */
	private float worldMax;
	/** размер отображаемой части элемента */
	private float worldPageSize;
	/** размер шага при скролинге через кнопку */
	private float buttonStepSize;
	/** размер шага при клике на полоску скрола */
	private float pageStepSize;

	/** позиция старта начала движения скрола */
	private int moveTheHandleStartPosDelta;

	/** идет ли обработка скрола через движение */
	private boolean moveTheHandle;

	public void bindToView(final ScrollbarView view, final float value, final float worldMax, final float worldPageSize, final float buttonStepSize, final float pageStepSize) {
		this.view = view;
		this.value = value;
		this.worldMax = worldMax;
		this.worldPageSize = worldPageSize;
		this.buttonStepSize = buttonStepSize;
		this.pageStepSize = pageStepSize;
		this.moveTheHandle = false;

		ensureWorldPageSize();
		updateView();
		changeValue(getValue());
	}

	private int calcHandlePosition(final int viewSize, final int handleSize) {

		int viewMin = (int) Math.floor(worldToView(getValue(), getWorldMax() - getWorldPageSize(), viewSize - handleSize));

		if(viewMin + handleSize > viewSize) {
			viewMin = viewSize - handleSize;
		}

		return viewMin;
	}

	private int calcHandleSize(final int viewSize) {

		int minHandleSize = getView().getMinHandleSize();

		if(getWorldMax() == 0) {
			// special case: empty data and we can't divide by null anyway :)
			// we return the maximum size because when nothing is there the
			// scrollbar handle should be maximal.
			return viewSize;
		}

		int handleSize = (int) Math.floor(getWorldPageSize() * viewSize / getWorldMax());

		if(handleSize < minHandleSize) {
			return minHandleSize;
		}

		if(handleSize > viewSize) {
			return viewSize;
		}

		return handleSize;
	}

	/**
	 * Изменение текущей позиции.
	 * 
	 * @param newValue новоя позиция.
	 */
	private void changeValue(final float newValue) {

		float result = newValue;

		float max = getWorldMax() - getWorldPageSize();
		float min = getMin();

		if(result > max) {
			result = max;
		} else if(newValue < min) {
			result = min;
		}

		if(result != getValue()) {
			setValue(result);
			getView().valueChanged(result);
		}
	}

	/**
	 * Обновление размера отображаемой части элемента.
	 */
	private void ensureWorldPageSize() {
		if(worldPageSize > worldMax) {
			worldPageSize = worldMax;
		}
	}

	/**
	 * @return размер шага при скролинге через кнопку.
	 */
	public float getButtonStepSize() {
		return buttonStepSize;
	}

	/**
	 * @return минимальное значение позиции.
	 */
	public float getMin() {
		return min;
	}

	/**
	 * @return позиция старта начала движения скрола.
	 */
	public int getMoveTheHandleStartPosDelta() {
		return moveTheHandleStartPosDelta;
	}

	/**
	 * @return размер шага при клике на полоску скрола.
	 */
	public float getPageStepSize() {
		return pageStepSize;
	}

	/**
	 * @return текущая позиция скрола.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * @return отображатель скрола.
	 */
	public ScrollbarView getView() {
		return view;
	}

	/**
	 * @return размер скролируемого элемента.
	 */
	public float getWorldMax() {
		return worldMax;
	}

	/**
	 * @return размер отображаемой части элемента.
	 */
	public float getWorldPageSize() {
		return worldPageSize;
	}

	/**
	 * Проверка вхождения точки клика в рамки курсора скрола.
	 */
	private boolean hitsHandle(final int handlePosition, final int handleSize, final int viewValueClicked) {
		return viewValueClicked > handlePosition && viewValueClicked < (handlePosition + handleSize);
	}

	/**
	 * @return идет ли обработка скрола через движение.
	 */
	public boolean isMoveTheHandle() {
		return moveTheHandle;
	}

	/**
	 * Обработка клика по скроллбару.
	 * 
	 * @param viewValueClicked позиция точки клика.
	 */
	public void processClick(final int viewValueClicked) {

		int viewSize = getView().getAreaSize();
		int handleSize = calcHandleSize(viewSize);
		int handlePosition = calcHandlePosition(viewSize, handleSize);

		// если клик попался на сам указатель
		if(hitsHandle(handlePosition, handleSize, viewValueClicked)) {
			setMoveTheHandle(true);
			setMoveTheHandleStartPosDelta(viewValueClicked - handlePosition);
		} else {

			setMoveTheHandle(false);

			if(viewValueClicked < handlePosition && viewValueClicked > 0) {
				stepPageDown();
			} else if(viewValueClicked > (handlePosition + handleSize) && viewValueClicked < viewSize) {
				stepPageUp();
			}
		}
	}

	/**
	 * Обработка двигания позиции скрола.
	 * 
	 * @param viewValue новая позиция.
	 */
	public void processMove(final int viewValue) {

		if(!isMoveTheHandle()) {
			return;
		}

		int viewSize = getView().getAreaSize();

		if(viewValue < 0 || viewValue >= viewSize) {
			return;
		}

		int newViewPos = viewValue - getMoveTheHandleStartPosDelta();
		int handleSize = calcHandleSize(viewSize);

		float newWorldValue = viewToWorld(newViewPos, viewSize - handleSize, getWorldMax() - getWorldPageSize());

		changeValue(newWorldValue);
		updateView();
	}

	/**
	 * @param buttonStepSize размер шага при скролинге через кнопку.
	 */
	public void setButtonStepSize(final float buttonStepSize) {
		this.buttonStepSize = buttonStepSize;

		changeValue(getValue());
		updateView();
	}

	/**
	 * @param moveTheHandle идет ли обработка скрола через движение.
	 */
	public void setMoveTheHandle(boolean moveTheHandle) {
		this.moveTheHandle = moveTheHandle;
	}

	/**
	 * @param moveTheHandleStartPosDelta позиция старта начала движения скрола.
	 */
	public void setMoveTheHandleStartPosDelta(int moveTheHandleStartPosDelta) {
		this.moveTheHandleStartPosDelta = moveTheHandleStartPosDelta;
	}

	/**
	 * @param pageStepSize размер шага при клике на полоску скрола.
	 */
	public void setPageStepSize(final float pageStepSize) {
		this.pageStepSize = pageStepSize;
	}

	public void setup(final float value, final float worldMax, final float worldPageSize, final float buttonStepSize, final float pageStepSize) {
		this.value = value;
		this.worldMax = worldMax;
		this.worldPageSize = worldPageSize;
		this.buttonStepSize = buttonStepSize;
		this.pageStepSize = pageStepSize;

		ensureWorldPageSize();
		changeValue(value);
		updateView();
	}

	/**
	 * @param value текущая позиция скрола.
	 */
	public void setValue(float value) {
		this.value = value;
	}

	/**
	 * @param worldMax размер скролируемого элемента.
	 */
	public void setWorldMax(final float worldMax) {
		this.worldMax = worldMax;

		ensureWorldPageSize();
		changeValue(getValue());
		updateView();
	}

	/**
	 * @param worldPageSize размер отображаемой части элемента.
	 */
	public void setWorldPageSize(final float worldPageSize) {
		this.worldPageSize = worldPageSize;

		ensureWorldPageSize();
		changeValue(getValue());
		updateView();
	}

	/**
	 * Сделать шаг скролом вниз.
	 */
	public void stepDown() {
		changeValue(getValue() - getButtonStepSize());
		updateView();
	}

	/**
	 * Сделать шаг вниз.
	 */
	public void stepPageDown() {
		changeValue(getValue() - getPageStepSize());
		updateView();
	}

	/**
	 * Сделать шаг вверх.
	 */
	public void stepPageUp() {
		changeValue(getValue() + getPageStepSize());
		updateView();
	}

	/**
	 * Сделать шаг скролом вверх.
	 */
	public void stepUp() {
		changeValue(getValue() + getButtonStepSize());
		updateView();
	}

	/**
	 * Обновление позиции скрола.
	 * 
	 * @param value новая позиция скрола.
	 */
	public void updateValue(final float value) {
		changeValue(value);
		updateView();
	}

	/**
	 * обновление отображения скрола.
	 */
	public void updateView() {

		ScrollbarView view = getView();

		int viewSize = view.getAreaSize();
		int handleSize = calcHandleSize(viewSize);

		view.setHandle(calcHandlePosition(viewSize, handleSize), handleSize);
	}

	private float viewToWorld(final float viewValue, final float viewMaxValue, final float worldMaxValue) {
		return viewValue / viewMaxValue * worldMaxValue;
	}

	private float worldToView(final float worldValue, final float worldMaxValue, final float viewMaxValue) {

		if(getWorldMax() == 0.f) {
			return 0.f;
		}

		return Math.round(worldValue / worldMaxValue * viewMaxValue);
	}
}
