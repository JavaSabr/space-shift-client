package com.ss.client.gui.element.impl.list;

import com.ss.client.gui.element.ListElementUI;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.gui.element.list.ListElementUIFactory;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Базовая реализация фабрики элементов для итемов списка.
 * 
 * @author Ronn
 */
public class LabelListElementUIFactory<T> implements ListElementUIFactory<T> {

	public static final String ITEM_LABEL_ID = "#item_label";

	@Override
	public ListElementUI<T> build(T item, Element parent, Nifty nifty, Screen screen, int index) {

		LabelUIBuilder label = new LabelUIBuilder(ITEM_LABEL_ID);
		label.setText(item.toString());

		PanelUIBuilder builder = new PanelUIBuilder(ITEM_ID + index);
		builder.setController(ListElementUIImpl.class);
		builder.setVisibleToMouse(true);
		builder.setHeight("20px");
		builder.add(label);

		Element element = builder.build(nifty, screen, parent);

		@SuppressWarnings("unchecked")
		ListElementUI<T> control = element.getControl(ListElementUI.class);
		control.setItem(item);

		return control;
	}
}
