package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.MessageElementUI;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента сообщения.
 * 
 * @author Ronn
 */
public class MessageElementUIImpl extends LabelUIImpl implements MessageElementUI {

	/** контроллер UI */
	private Nifty nifty;
	/** элемент в котором содержится рендер текста */
	private Element textElement;

	/** отступ текста по ширине от краев */
	private int textPaddingWidth;
	/** отступ текста по высоте от краев */
	private int textPaddingHeight;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {

		setTextElement(element.findElementByName(TEXT_ELEMENT_ID));
		setTextPaddingHeight(attributes.getAsInteger(PROPERTY_TEXT_PADDING_HEIGHT));
		setTextPaddingWidth(attributes.getAsInteger(PROPERTY_TEXT_PADDING_WIDTH));
		setNifty(nifty);

		super.bind(nifty, screen, element, parameter, attributes);
	}

	@Override
	protected TextRenderer findTextRender(Element element) {
		return getTextElement().getRenderer(TextRenderer.class);
	}

	/**
	 * @return контроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return элемент в котором содержится рендер текста.
	 */
	public Element getTextElement() {
		return textElement;
	}

	@Override
	public int getTextPaddingHeight() {
		return textPaddingHeight;
	}

	@Override
	public int getTextPaddingWidth() {
		return textPaddingWidth;
	}

	/**
	 * @param nifty контроллер UI.
	 */
	public void setNifty(Nifty nifty) {
		this.nifty = nifty;
	}

	@Override
	public void setText(String text) {

		Element textElement = getTextElement();
		Element element = getElement();

		int parentWidth = element.getWidth();
		int textWidth = parentWidth - (getTextPaddingWidth() * 2);

		if(textElement.getWidth() != textWidth) {
			textElement.setConstraintWidth(GameUtil.getPixelSize(textWidth));
		}

		TextRenderer textRenderer = getTextRenderer();
		textRenderer.setText(text);
		textRenderer.setWidthConstraint(textElement, textElement.getConstraintWidth(), parentWidth, getNifty().getRenderEngine());

		int height = textRenderer.getTextHeight() + (getTextPaddingHeight() * 2);

		if(height == getHeight()) {
			return;
		}

		element.setConstraintHeight(GameUtil.getPixelSize(height));

		ElementUtils.updateLayout(element.getParent());
	}

	/**
	 * @param textElement элемент в котором содержится рендер текста.
	 */
	public void setTextElement(Element textElement) {
		this.textElement = textElement;
	}

	@Override
	public void setTextPaddingHeight(int textPaddingHeight) {
		this.textPaddingHeight = textPaddingHeight;
	}

	@Override
	public void setTextPaddingWidth(int textPaddingWidth) {
		this.textPaddingWidth = textPaddingWidth;
	}
}
