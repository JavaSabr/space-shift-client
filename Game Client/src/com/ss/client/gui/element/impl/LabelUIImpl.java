package com.ss.client.gui.element.impl;

import java.util.Properties;

import com.ss.client.gui.element.LabelUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.layout.align.VerticalAlign;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация элемента надписи.
 * 
 * @author Ronn
 */
public class LabelUIImpl extends AbstractElementUI implements LabelUI {

	/** рендер текста надписи */
	private TextRenderer textRenderer;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		final boolean wrap = attributes.getAsBoolean(PROPERTY_WRAP, false);

		TextRenderer textRenderer = findTextRender(element);
		textRenderer.setLineWrapping(wrap);
		setTextRenderer(textRenderer);

		String text = parameter.getProperty(PROPERTY_TEXT);

		if(text != null) {
			setText(text);
		}
	}

	protected TextRenderer findTextRender(Element element) {
		return element.getRenderer(TextRenderer.class);
	}

	@Override
	public Color getColor() {
		return getTextRenderer().getColor();
	}

	@Override
	public RenderFont getFont() {
		return getTextRenderer().getFont();
	}

	@Override
	public String getOriginalText() {
		return getTextRenderer().getOriginalText();
	}

	/**
	 * @return рендер текста надписи.
	 */
	protected TextRenderer getTextRenderer() {
		return textRenderer;
	}

	@Override
	public String getWrappedText() {
		return getTextRenderer().getWrappedText();
	}

	@Override
	public boolean isLineWrapping() {
		return getTextRenderer().isLineWrapping();
	}

	@Override
	public void setColor(Color color) {
		getTextRenderer().setColor(color);
	}

	@Override
	public void setFont(RenderFont fontParam) {
		getTextRenderer().setFont(fontParam);
	}

	@Override
	public void setText(String text) {
		getTextRenderer().setText(text);
	}

	@Override
	public void setTextHAlign(HorizontalAlign newTextHAlign) {
		getTextRenderer().setTextHAlign(newTextHAlign);
	}

	/**
	 * @param textRenderer рендер текста надписи.
	 */
	private void setTextRenderer(TextRenderer textRenderer) {
		this.textRenderer = textRenderer;
	}

	@Override
	public void setTextVAlign(VerticalAlign newTextVAlign) {
		getTextRenderer().setTextVAlign(newTextVAlign);
	}
}
