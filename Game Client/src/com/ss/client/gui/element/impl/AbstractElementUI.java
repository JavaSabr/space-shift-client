package com.ss.client.gui.element.impl;

import java.util.Properties;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.Game;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.ElementVisibleChangedEvent;
import com.ss.client.gui.element.event.impl.MouseOverEvent;
import com.ss.client.gui.element.event.impl.MouseReleaseEvent;
import com.ss.client.gui.element.event.impl.MouseWheelEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickMouseMoveEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickRepeatEvent;
import com.ss.client.gui.element.event.impl.SecondaryClickEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.util.ReflectionMethod;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.FocusGainedEvent;
import de.lessvoid.nifty.controls.FocusLostEvent;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.input.NiftyMouseInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Базовая реализация элемента UI Space Shift.
 * 
 * @author Ronn
 */
public abstract class AbstractElementUI implements ElementUI {

	protected static final Game GAME = Game.getInstance();

	/** таблица слушателей событий контрола */
	private final Table<ElementEventType, Array<ElementUIEventListener<?>>> listeners;

	/** локальные ивенты клика */
	private final ThreadLocal<PrimaryClickEvent<ElementUI>> localPrimaryClickEvent = new ThreadLocal<PrimaryClickEvent<ElementUI>>() {

		@Override
		protected PrimaryClickEvent<ElementUI> initialValue() {
			return new PrimaryClickEvent<>();
		}
	};

	/** локальные ивенты клика */
	private final ThreadLocal<PrimaryClickMouseMoveEvent<ElementUI>> localPrimaryClickMouseMoveEvent = new ThreadLocal<PrimaryClickMouseMoveEvent<ElementUI>>() {

		@Override
		protected PrimaryClickMouseMoveEvent<ElementUI> initialValue() {
			return new PrimaryClickMouseMoveEvent<>();
		}
	};

	/** локальные ивенты клика */
	private final ThreadLocal<PrimaryClickRepeatEvent<ElementUI>> localPrimaryClickRepeatEvent = new ThreadLocal<PrimaryClickRepeatEvent<ElementUI>>() {

		@Override
		protected PrimaryClickRepeatEvent<ElementUI> initialValue() {
			return new PrimaryClickRepeatEvent<>();
		}
	};

	/** локальные ивенты клика */
	private final ThreadLocal<SecondaryClickEvent<ElementUI>> localSecondaryClickEvent = new ThreadLocal<SecondaryClickEvent<ElementUI>>() {

		@Override
		protected SecondaryClickEvent<ElementUI> initialValue() {
			return new SecondaryClickEvent<>();
		}
	};

	/** локальные ивенты клика */
	private final ThreadLocal<MouseReleaseEvent<ElementUI>> localMouseReleaseEvent = new ThreadLocal<MouseReleaseEvent<ElementUI>>() {

		@Override
		protected MouseReleaseEvent<ElementUI> initialValue() {
			return new MouseReleaseEvent<>();
		}
	};

	/** событие о смене видимости элемента */
	private final ThreadLocal<ElementVisibleChangedEvent<ElementUI>> localVisibleChangedEvent = new ThreadLocal<ElementVisibleChangedEvent<ElementUI>>() {

		@Override
		protected ElementVisibleChangedEvent<ElementUI> initialValue() {
			return new ElementVisibleChangedEvent<ElementUI>();
		}
	};

	/** событие о вращении колеса мышки */
	private final ThreadLocal<MouseWheelEvent<ElementUI>> localMouseWheelEvent = new ThreadLocal<MouseWheelEvent<ElementUI>>() {

		@Override
		protected MouseWheelEvent<ElementUI> initialValue() {
			return new MouseWheelEvent<ElementUI>();
		}
	};

	/** событие о вхождение указателя мыщки в область элемента */
	private final ThreadLocal<MouseOverEvent<ElementUI>> localMouseOverEvent = new ThreadLocal<MouseOverEvent<ElementUI>>() {

		@Override
		protected MouseOverEvent<ElementUI> initialValue() {
			return new MouseOverEvent<ElementUI>();
		}
	};

	/** элемент контрола */
	private Element element;

	/** инициализирован ли контрол */
	private boolean bound;

	public AbstractElementUI() {
		this.listeners = Tables.newObjectTable();
	}

	@Override
	public void addElementListener(ElementEventType eventType, ElementUIEventListener<?> listener) {

		Table<ElementEventType, Array<ElementUIEventListener<?>>> listeners = getListeners();
		Array<ElementUIEventListener<?>> container = listeners.get(eventType);

		if(container == null) {
			container = Arrays.toArray(ElementUIEventListener.class, 3);
			listeners.put(eventType, container);
		}

		container.add(listener);
	}

	/**
	 * Привязка контрола к элементу.
	 * 
	 * @param element привязываемый элемент.
	 */
	protected void bind(final Element element) {
		this.element = element;
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		bind(element);
	}

	@Override
	public void disable() {
		element.disable();
	}

	@Override
	public void disableFocus() {
		element.disableFocus();
	}

	@ReflectionMethod
	public boolean elementMouseOver(final Element element, final NiftyMouseInputEvent inputEvent) {

		MouseOverEvent<ElementUI> event = localMouseOverEvent.get();
		event.setElement(this);

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementMouseRelease() {

		MouseReleaseEvent<ElementUI> event = localMouseReleaseEvent.get();
		event.setElement(this);

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementMouseWheel(final Element element, final NiftyMouseInputEvent inputEvent) {

		MouseWheelEvent<ElementUI> event = localMouseWheelEvent.get();
		event.setElement(this);
		event.setValue(inputEvent.getMouseWheel());

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementPrimaryClick(final int mouseX, final int mouseY) {

		PrimaryClickEvent<ElementUI> event = localPrimaryClickEvent.get();
		event.setElement(this);
		event.setMouseX(mouseX);
		event.setMouseY(mouseY);

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementPrimaryClickMouseMove(final int mouseX, final int mouseY) {

		PrimaryClickMouseMoveEvent<ElementUI> event = localPrimaryClickMouseMoveEvent.get();
		event.setElement(this);
		event.setMouseX(mouseX);
		event.setMouseY(mouseY);

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementPrimaryClickRepeat(final int mouseX, final int mouseY) {

		PrimaryClickRepeatEvent<ElementUI> event = localPrimaryClickRepeatEvent.get();
		event.setElement(this);
		event.setMouseX(mouseX);
		event.setMouseY(mouseY);

		notifyEvent(event);
		return true;
	}

	@ReflectionMethod
	public boolean elementSecondaryClick(final int mouseX, final int mouseY) {

		SecondaryClickEvent<ElementUI> event = localSecondaryClickEvent.get();
		event.setElement(this);
		event.setMouseX(mouseX);
		event.setMouseY(mouseY);

		notifyEvent(event);
		return true;
	}

	@Override
	public void enable() {
		element.enable();
	}

	@Override
	public Element getElement() {
		return element;
	}

	@Override
	public int getHeight() {
		return element.getHeight();
	}

	@Override
	public String getId() {
		return element.getId();
	}

	/**
	 * @return таблица слушателей.
	 */
	protected Table<ElementEventType, Array<ElementUIEventListener<?>>> getListeners() {
		return listeners;
	}

	@Override
	public String getStyle() {
		return element.getStyle();
	}

	@Override
	public int getWidth() {
		return element.getWidth();
	}

	@Override
	public boolean hasFocus() {

		Element element = getElement();

		if(element == null) {
			return false;
		}

		return element == element.getFocusHandler().getKeyboardFocusElement();
	}

	@Override
	public void init(final Properties parameter, final Attributes attributes) {
		this.bound = true;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public boolean isBound() {
		return bound;
	}

	@Override
	public boolean isEnabled() {
		return element.isEnabled();
	}

	@Override
	public boolean isVisible() {
		return getElement().isVisible();
	}

	@Override
	public void layoutCallback() {
	}

	@SuppressWarnings({
		"rawtypes",
		"unchecked"
	})
	protected void notifyEvent(ElementUIEvent<?> event) {

		Table<ElementEventType, Array<ElementUIEventListener<?>>> listeners = getListeners();
		Array<ElementUIEventListener<?>> container = listeners.get(event.getEventType());

		if(container == null) {
			return;
		}

		for(ElementUIEventListener listener : container.array()) {

			if(listener == null) {
				break;
			}

			listener.notifyEvent(event);
		}
	}

	@Override
	public void onFocus(final boolean focused) {

		Element element = getElement();

		if(element == null) {
			return;
		}

		Nifty nifty = element.getNifty();

		if(focused) {
			nifty.publishEvent(element.getId(), new FocusGainedEvent(this, this));
		} else {
			nifty.publishEvent(element.getId(), new FocusLostEvent(this, this));
		}
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void removeElementListener(ElementEventType eventType, ElementUIEventListener<?> listener) {

		Table<ElementEventType, Array<ElementUIEventListener<?>>> listeners = getListeners();
		Array<ElementUIEventListener<?>> container = listeners.get(eventType);

		if(container == null) {
			return;
		}

		container.fastRemove(listener);
	}

	@Override
	public void setEnabled(final boolean enabled) {
		if(enabled) {
			element.enable();
		} else {
			element.disable();
		}
	}

	@Override
	public void setFocus() {
		element.setFocus();
	}

	@Override
	public void setFocusable(final boolean focusable) {
		element.setFocusable(focusable);
	}

	@Override
	public void setHeight(final SizeValue height) {
		element.setConstraintHeight(height);
	}

	@Override
	public void setId(final String id) {
		element.setId(id);
	}

	@Override
	public void setStyle(final String style) {
		element.setStyle(element.getNifty().specialValuesReplace(style));
	}

	@Override
	public void setVisible(boolean visible) {

		if(visible != isVisible()) {

			getElement().setVisible(visible);

			ElementVisibleChangedEvent<ElementUI> event = localVisibleChangedEvent.get();
			event.setVisible(visible);

			notifyEvent(event);
		}
	}

	@Override
	public void setWidth(final SizeValue width) {
		element.setConstraintWidth(width);
	}
}
