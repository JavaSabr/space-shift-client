package com.ss.client.gui.element;

/**
 * Интерфейс для реализации элемента скролируемой панели.
 * 
 * @author Ronn
 */
public interface ScrollPanelUI extends PanelUI {

	/**
	 * Autoscrollmode
	 * 
	 * @author void
	 */
	public enum AutoScroll {
		TOP("top"),
		BOTTOM("bottom"),
		LEFT("left"),
		RIGHT("right"),
		TOP_LEFT("topLeft"),
		TOP_RIGHT("topRight"),
		BOTTOM_LEFT("bottomLeft"),
		BOTTOM_RIGHT("bottomRight"),
		OFF("off");

		public static AutoScroll parse(final String param) {
			for(AutoScroll auto : values()) {
				if(auto.param.compareTo(param) == 0) {
					return auto;
				}
			}
			return AutoScroll.OFF;
		}

		private final String param;

		private AutoScroll(final String param) {
			this.param = param;
		}

		public String getParam() {
			return param;
		}
	}
	/**
	 * VerticalAlign
	 * 
	 * @author void
	 */
	public enum VerticalAlign {
		top,
		center,
		bottom
	}
	public static final String VERTICAL_SCROLLBAR_ID = "#vertical-scrollbar";

	public static final String HORIZONTAL_SCROLLBAR_ID = "#horizontal-scrollbar";
	public static final String CONTAINER_ID = "#container";
	public static final String PROPERTY_AUTO_SCROLL = "autoScroll";
	public static final String PROPERTY_PAGE_SIZE_Y = "pageSizeY";
	public static final String PROPERTY_PAGE_SIZE_X = "pageSizeX";

	public static final String PROPERTY_STEP_SIZE_Y = "stepSizeY";

	public static final String PROPERTY_STEP_SIZE_X = "stepSizeX";

	/**
	 * Get auto scroll mode.
	 * 
	 * @return auto scroll mode
	 */
	public AutoScroll getAutoScroll();

	/**
	 * Get the horizontal position.
	 * 
	 * @return xPos
	 */
	public float getHorizontalPos();

	/**
	 * Get the vertical position.
	 * 
	 * @return yPos
	 */
	public float getVerticalPos();

	/**
	 * Set auto scroll mode.
	 * 
	 * @param auto auto scroll mode
	 */
	public void setAutoScroll(AutoScroll auto);

	/**
	 * Set horizontal position.
	 * 
	 * @param xPos new position
	 */
	public void setHorizontalPos(float xPos);

	/**
	 * Page size x
	 * 
	 * @param pageSizeX page size x
	 */
	public void setPageSizeX(float pageSizeX);

	/**
	 * Page size y
	 * 
	 * @param pageSizeY page sze y
	 */
	public void setPageSizeY(final float pageSizeY);

	/**
	 * Step size x
	 * 
	 * @return step size x
	 */
	public void setStepSizeX(float stepSizeX);

	/**
	 * Step size y
	 * 
	 * @param stepSizeY step size y
	 */
	public void setStepSizeY(float stepSizeY);

	/**
	 * Setup.
	 * 
	 * @param stepSizeX step size x
	 * @param stepSizeY step size y
	 * @param pageSizeX page size x
	 * @param pageSizeY page size y
	 * @param auto auto
	 */
	public void setUp(float stepSizeX, float stepSizeY, float pageSizeX, float pageSizeY, AutoScroll auto);

	/**
	 * Set vertical position.
	 * 
	 * @param yPos new position
	 */
	public void setVerticalPos(float yPos);

	/**
	 * Show element vertical position.
	 * 
	 * @param elemCount element index
	 */
	public void showElementVertical(int elemCount);
}
