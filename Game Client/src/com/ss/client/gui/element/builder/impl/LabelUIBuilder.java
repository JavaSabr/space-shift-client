package com.ss.client.gui.element.builder.impl;

import rlib.util.Strings;

import com.ss.client.gui.FontIds;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.impl.LabelUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация билдера надписи.
 * 
 * @author Ronn
 */
public class LabelUIBuilder extends AbstractElementUIBuilder<ElementBuilder> {

	public static final Color DEFAULT_SELECTION_COLOR = new Color("#696969");

	public static final String DEFAULT_FONT = "main_16.fnt";

	/** горизонтальное выравнивание */
	private Align textHAlign;
	/** вертикальное выравнивание */
	private VAlign textVAlign;

	/** текст надписи */
	private String text;
	/** шрифт надписи */
	private String font;

	/** цвет текста */
	private Color color;
	/** цвет выделенного текста */
	private Color selectedColor;

	/** переносить ли строки */
	private boolean wrap;

	public LabelUIBuilder(String id) {
		super(id);

		this.text = Strings.EMPTY;
		this.textHAlign = Align.Center;
		this.textVAlign = VAlign.Center;
		this.color = Color.WHITE;
		this.selectedColor = DEFAULT_SELECTION_COLOR;
		this.font = FontIds.MAIN_16;
	}

	/**
	 * @return цвет текста.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return шрифт надписи.
	 */
	public String getFont() {
		return font;
	}

	/**
	 * @return цвет выделенного текста.
	 */
	public Color getSelectedColor() {
		return selectedColor;
	}

	/**
	 * @return текст надписи.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return горизонтальное выравнивание.
	 */
	public Align getTextHAlign() {
		return textHAlign;
	}

	/**
	 * @return вертикальное выравнивание.
	 */
	public VAlign getTextVAlign() {
		return textVAlign;
	}

	/**
	 * @return переносить ли строки.
	 */
	public boolean isWrap() {
		return wrap;
	}

	@Override
	public ElementBuilder prepareBuilder() {

		TextBuilder text = new TextBuilder(getId());
		text.controller(LabelUIImpl.class.getName());
		text.selectionColor(getSelectedColor());
		text.textHAlign(getTextHAlign());
		text.textVAlign(getTextVAlign());
		text.color(getColor());
		text.font(getFont());
		text.set(LabelUI.PROPERTY_WRAP, String.valueOf(isWrap()));
		text.set(LabelUI.PROPERTY_TEXT, getText());

		return text;
	}

	/**
	 * @param color цвет текста.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @param font шрифт надписи.
	 */
	public void setFont(String font) {
		this.font = font;
	}

	/**
	 * @param selectedColor цвет выделенного текста.
	 */
	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	/**
	 * @param text текст надписи.
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @param textHAlign горизонтальное выравнивание.
	 */
	public void setTextHAlign(Align textHAlign) {
		this.textHAlign = textHAlign;
	}

	/**
	 * @param textVAlign вертикальное выравнивание.
	 */
	public void setTextVAlign(VAlign textVAlign) {
		this.textVAlign = textVAlign;
	}

	/**
	 * @param wrap переносить ли строки.
	 */
	public void setWrap(boolean wrap) {
		this.wrap = wrap;
	}
}
