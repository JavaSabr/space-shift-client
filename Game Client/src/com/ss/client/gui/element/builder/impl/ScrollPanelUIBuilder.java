package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.ScrollPanelUI;
import com.ss.client.gui.element.ScrollPanelUI.AutoScroll;
import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.impl.ScrollPanelUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Реализация конструктора скролируемой панели.
 * 
 * @author Ronn
 */
public class ScrollPanelUIBuilder extends PanelUIBuilder {

	public static final int DEFAULT_PAGE_SIZE_Y = 10;
	public static final int DEFAULT_PAGE_SIZE_X = 20;
	public static final int DEFAULT_STEP_SIZE_Y = 5;
	public static final int DEFAULT_STEP_SIZE_X = 10;

	/** панель для размещения скролируемого контента */
	private final PanelUIBuilder container;

	/** конструктор вертикального скроллбар */
	private ElementUIBuilder verticalScrollbar;
	/** конструктор горизонтального скроллбар */
	private ElementUIBuilder horizontalScrollbar;

	/** режим автоматического скролинга */
	private AutoScroll autoScroll;

	/** размер шага по X */
	private int stepSizeX;
	/** размер шага по Y */
	private int stepSizeY;
	/** размер шага при клике на полоску по X */
	private int pageSizeX;
	/** размер шага при клике на полоску по Y */
	private int pageSizeY;

	public ScrollPanelUIBuilder(String id) {
		super(id);

		this.verticalScrollbar = new VerticalScrollbarUIBuilder(ScrollPanelUI.VERTICAL_SCROLLBAR_ID);
		this.container = new PanelUIBuilder(ScrollPanelUI.CONTAINER_ID);
		this.container.setLayoutType(ChildLayoutType.Absolute);
		this.container.setChildClip(true);
		this.container.setWidth("*");
		this.stepSizeX = DEFAULT_STEP_SIZE_X;
		this.stepSizeY = DEFAULT_STEP_SIZE_Y;
		this.pageSizeX = DEFAULT_PAGE_SIZE_X;
		this.pageSizeY = DEFAULT_PAGE_SIZE_Y;
		this.autoScroll = AutoScroll.OFF;

		setLayoutType(ChildLayoutType.Vertical);
		setVisibleToMouse(true);
	}

	@Override
	public void add(ElementUIBuilder builder) {
		getContainer().add(builder);
	}

	/**
	 * @return режим автоматического скролинга.
	 */
	public AutoScroll getAutoScroll() {
		return autoScroll;
	}

	/**
	 * @return панель для размещения скролируемого контента.
	 */
	private PanelUIBuilder getContainer() {
		return container;
	}

	public ElementUIBuilder getHorizontalScrollbar() {
		return horizontalScrollbar;
	}

	/**
	 * @return размер шага при клике на полоску по X.
	 */
	public int getPageSizeX() {
		return pageSizeX;
	}

	/**
	 * @return размер шага при клике на полоску по Y.
	 */
	public int getPageSizeY() {
		return pageSizeY;
	}

	/**
	 * @return размер шага по X.
	 */
	public int getStepSizeX() {
		return stepSizeX;
	}

	/**
	 * @return размер шага по Y.
	 */
	public int getStepSizeY() {
		return stepSizeY;
	}

	public ElementUIBuilder getVerticalScrollbar() {
		return verticalScrollbar;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		ElementUIBuilder verticalScrollbar = getVerticalScrollbar();
		ElementUIBuilder horizontalScrollbar = getHorizontalScrollbar();

		PanelUIBuilder containerAndVScrollbar = new PanelUIBuilder("#container-and-vscrollbar");
		containerAndVScrollbar.setLayoutType(ChildLayoutType.Horizontal);
		containerAndVScrollbar.add(getContainer());
		containerAndVScrollbar.setHeight("*");

		if(verticalScrollbar != null) {
			containerAndVScrollbar.add(verticalScrollbar);
		}

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(ScrollPanelUIImpl.class.getName());
		builder.set(ScrollPanelUI.PROPERTY_STEP_SIZE_X, String.valueOf(getStepSizeX()));
		builder.set(ScrollPanelUI.PROPERTY_STEP_SIZE_Y, String.valueOf(getStepSizeY()));
		builder.set(ScrollPanelUI.PROPERTY_PAGE_SIZE_X, String.valueOf(getPageSizeX()));
		builder.set(ScrollPanelUI.PROPERTY_PAGE_SIZE_Y, String.valueOf(getPageSizeY()));
		builder.set(ScrollPanelUI.PROPERTY_AUTO_SCROLL, String.valueOf(getAutoScroll()));
		builder.childLayout(ChildLayoutType.Vertical);

		containerAndVScrollbar.buildTo(builder);

		if(horizontalScrollbar != null) {
			horizontalScrollbar.buildTo(builder);
		}

		return builder;
	}

	/**
	 * @param autoScroll режим автоматического скролинга.
	 */
	public void setAutoScroll(AutoScroll autoScroll) {
		this.autoScroll = autoScroll;
	}

	public void setHorizontalScrollbar(ElementUIBuilder horizontalScrollbar) {
		this.horizontalScrollbar = horizontalScrollbar;
	}

	/**
	 * @param pageSizeX размер шага при клике на полоску по X.
	 */
	public void setPageSizeX(int pageSizeX) {
		this.pageSizeX = pageSizeX;
	}

	/**
	 * @param pageSizeY размер шага при клике на полоску по Y.
	 */
	public void setPageSizeY(int pageSizeY) {
		this.pageSizeY = pageSizeY;
	}

	/**
	 * @param stepSizeX размер шага по X.
	 */
	public void setStepSizeX(int stepSizeX) {
		this.stepSizeX = stepSizeX;
	}

	/**
	 * @param stepSizeY размер шага по Y.
	 */
	public void setStepSizeY(int stepSizeY) {
		this.stepSizeY = stepSizeY;
	}

	public void setVerticalScrollbar(ElementUIBuilder verticalScrollbar) {
		this.verticalScrollbar = verticalScrollbar;
	}
}
