package com.ss.client.gui.element.builder.impl;

import rlib.util.Strings;

import com.ss.client.gui.element.impl.DroppableElementUIImpl;

import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Конструктор контейнера для перемещаемых элементов.
 * 
 * @author Ronn
 */
public class DroppableElementUIBuilder extends PanelUIBuilder {

	public DroppableElementUIBuilder(String id) {
		super(id);
	}

	@Override
	public PanelBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(DroppableElementUIImpl.class.getName());
		builder.style(Strings.EMPTY);

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		if(getBackgroundImage() != null) {
			builder.backgroundImage(getBackgroundImage());
		}

		return builder;
	}
}
