package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.ButtonUI;
import com.ss.client.gui.element.impl.ButtonUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация контрола UI кнопки.
 * 
 * @author Ronn
 */
public class ButtonUIBuilder extends LabelUIBuilder {

	/** фоновое изображение */
	private String backgroundImage;
	/** фоновый цвет кнопки */
	private Color backgroundColor;

	public ButtonUIBuilder(String id) {
		super(id);

		setVisibleToMouse(true);
	}

	/**
	 * @return фоновый цвет кнопки.
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return фоновое изображение.
	 */
	public String getBackgroundImage() {
		return backgroundImage;
	}

	@Override
	public ElementBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(ButtonUIImpl.class.getName());
		builder.childLayout(ChildLayoutType.Center);

		if(getBackgroundImage() != null) {
			builder.backgroundImage(getBackgroundImage());
		}

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		LabelUIBuilder labelBuilder = new LabelUIBuilder(ButtonUI.TEXT_ELEMENT_ID);
		labelBuilder.setColor(getColor());
		labelBuilder.setFont(getFont());
		labelBuilder.setText(getText());
		labelBuilder.setTextHAlign(getTextHAlign());
		labelBuilder.setTextVAlign(getTextVAlign());
		labelBuilder.buildTo(builder);

		return builder;
	}

	/**
	 * @param backgroundColor фоновый цвет кнопки.
	 */
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @param backgroundImage фоновое изображение.
	 */
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
}
