package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.impl.ImageUIImpl;

import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Конструктор элемента картинки.
 * 
 * @author Ronn
 */
public class ImageUIBuilder extends AbstractElementUIBuilder<ImageBuilder> {

	/** цвет фона */
	private Color backgroundColor;

	/** путь к изображению */
	private String path;
	/** режим изображения */
	private String imageMode;

	public ImageUIBuilder(String id) {
		super(id);
	}

	/**
	 * @return цвет фона.
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return режим изображения.
	 */
	public String getImageMode() {
		return imageMode;
	}

	/**
	 * @return путь к изображению.
	 */
	public String getPath() {
		return path;
	}

	@Override
	public ImageBuilder prepareBuilder() {

		ImageBuilder builder = new ImageBuilder(getId());
		builder.controller(ImageUIImpl.class.getName());

		if(getPath() != null) {
			builder.filename(getPath());
		}

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		if(getImageMode() != null) {
			builder.imageMode(getImageMode());
		}

		return builder;
	}

	/**
	 * @param backgroundColor цвет фона.
	 */
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @param imageMod режим изображения.
	 */
	public void setImageMode(String imageMod) {
		this.imageMode = imageMod;
	}

	/**
	 * @param path путь к изображению.
	 */
	public void setPath(String path) {
		this.path = path;
	}
}
