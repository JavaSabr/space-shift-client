package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.ListUI;
import com.ss.client.gui.element.impl.ListUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * @author Ronn
 */
public class ListUIBuilder extends AbstractElementUIBuilder<PanelBuilder> {

	private ScrollPanelUIBuilder scrollPanel;

	private Class<?> itemFactory;

	public ListUIBuilder(String id) {
		super(id);

		this.scrollPanel = new ScrollPanelUIBuilder("#scroll_panel");
	}

	public Class<?> getItemFactory() {
		return itemFactory;
	}

	/**
	 * @return
	 */
	public ScrollPanelUIBuilder getScrollPanel() {
		return scrollPanel;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(ListUIImpl.class.getName());

		Class<?> itemFactory = getItemFactory();
		if(itemFactory != null) {
			builder.set(ListUI.PROP_ITEM_FACTORY_CLASS, itemFactory.getName());
		}

		PanelUIBuilder content = new PanelUIBuilder("#content_panel");
		content.setLayoutType(ChildLayoutType.Vertical);

		ScrollPanelUIBuilder scrollPanel = getScrollPanel();
		scrollPanel.add(content);
		scrollPanel.buildTo(builder);

		return builder;
	}

	public void setItemFactory(Class<?> itemFactory) {
		this.itemFactory = itemFactory;
	}

	/**
	 * @param scrollPanel
	 */
	public void setScrollPanel(ScrollPanelUIBuilder scrollPanel) {
		this.scrollPanel = scrollPanel;
	}
}
