package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.ScrollbarUI;
import com.ss.client.gui.element.impl.VerticalScrollbarUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Реализация конструктора вертикального скроллбара.
 * 
 * @author Ronn
 */
public class VerticalScrollbarUIBuilder extends AbstractElementUIBuilder<PanelBuilder> {

	public static final String DEFAULT_BACKGROUND_WIDTH = "23px";
	public static final String DEFAULT_BACKGROUND_IMAGE = "general/scroll_bar/background.png";
	public static final String DEFAULT_POSITION_WIDTH = DEFAULT_BACKGROUND_WIDTH;
	public static final String DEFAULT_POSITION_IMAGE_MODE = "resize:2,20,2,2,2,20,2,20,2,20,2,2";
	public static final String DEFAULT_POSITION_IMAGE = "general/scroll_bar/button-position.png";
	public static final String DEFAULT_BUTTON_DOWN_IMAGE = "general/scroll_bar/button-down.png";
	public static final String DEFAULT_BUTTON_UP_IMAGE = "general/scroll_bar/button-up.png";
	public static final String DEFAULT_BUTTON_HEIGHT = DEFAULT_POSITION_WIDTH;
	public static final String DEFAULT_BUTTON_WIDTH = DEFAULT_BUTTON_HEIGHT;

	public static final int DEFAULT_BUTTON_STEP_SIZE = 10;
	public static final int DEFAULT_PAGE_STEP_SIZE = 20;

	/** ширина кнопок */
	private String buttonWidth;
	/** высота кнопок */
	private String buttonHeight;
	/** картинка кнопки вверх */
	private String buttonUpImage;
	/** картинка кнопки вниз */
	private String buttonDownImage;

	/** ширина позиции скрола */
	private String positionWidth;
	/** картинка позиции скрола */
	private String positionImage;
	/** режим растягивания картинка позиции скрола */
	private String positionImageMode;

	/** ширина полоски скрола */
	private String backgroundWidth;
	/** картинка полоски скрола */
	private String backgroundImage;

	/** размер шага скролинга при нажатии на кнопку */
	private int buttonStepSize;
	/** размер шага при клике на полоску */
	private int pageStepSize;

	public VerticalScrollbarUIBuilder(String id) {
		super(id);

		this.buttonWidth = DEFAULT_BUTTON_WIDTH;
		this.buttonHeight = DEFAULT_BUTTON_HEIGHT;
		this.buttonUpImage = DEFAULT_BUTTON_UP_IMAGE;
		this.buttonDownImage = DEFAULT_BUTTON_DOWN_IMAGE;
		this.positionImage = DEFAULT_POSITION_IMAGE;
		this.positionImageMode = DEFAULT_POSITION_IMAGE_MODE;
		this.positionWidth = DEFAULT_POSITION_WIDTH;
		this.backgroundImage = DEFAULT_BACKGROUND_IMAGE;
		this.backgroundWidth = DEFAULT_BACKGROUND_WIDTH;
		this.buttonStepSize = DEFAULT_BUTTON_STEP_SIZE;
		this.pageStepSize = DEFAULT_PAGE_STEP_SIZE;

		setVisibleToMouse(true);
		setLayoutType(ChildLayoutType.Vertical);
		setWidth(DEFAULT_BUTTON_HEIGHT);
	}

	/**
	 * @return картинка полоски скрола.
	 */
	public String getBackgroundImage() {
		return backgroundImage;
	}

	/**
	 * @return ширина полоски скрола.
	 */
	public String getBackgroundWidth() {
		return backgroundWidth;
	}

	/**
	 * @return картинка кнопки вниз.
	 */
	public String getButtonDownImage() {
		return buttonDownImage;
	}

	/**
	 * @return высота кнопок.
	 */
	public String getButtonHeight() {
		return buttonHeight;
	}

	/**
	 * @return размер шага скролинга при нажатии на кнопку.
	 */
	public int getButtonStepSize() {
		return buttonStepSize;
	}

	/**
	 * @return картинка кнопки вверх.
	 */
	public String getButtonUpImage() {
		return buttonUpImage;
	}

	/**
	 * @return ширина кнопок.
	 */
	public String getButtonWidth() {
		return buttonWidth;
	}

	/**
	 * @return размер шага при клике на полоску.
	 */
	public int getPageStepSize() {
		return pageStepSize;
	}

	/**
	 * @return картинка позиции скрола.
	 */
	public String getPositionImage() {
		return positionImage;
	}

	/**
	 * @return режим растягивания картинка позиции скрола.
	 */
	public String getPositionImageMode() {
		return positionImageMode;
	}

	/**
	 * @return ширина позиции скрола.
	 */
	public String getPositionWidth() {
		return positionWidth;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		ImageUIBuilder up = new ImageUIBuilder(ScrollbarUI.BUTTON_UP_ID);
		up.setPath(getButtonUpImage());
		up.setVisibleToMouse(true);
		up.setWidth(getButtonWidth());
		up.setHeight(getButtonHeight());

		ImageUIBuilder down = new ImageUIBuilder(ScrollbarUI.BUTTON_DOWN_ID);
		down.setPath(getButtonDownImage());
		down.setVisibleToMouse(true);
		down.setWidth(getButtonWidth());
		down.setHeight(getButtonHeight());

		ImageUIBuilder position = new ImageUIBuilder(ScrollbarUI.POSITION_ID);
		position.setVisibleToMouse(true);
		position.setPath(getPositionImage());
		position.setWidth(getPositionWidth());
		position.setHeight("*");

		if(getPositionImageMode() != null) {
			position.setImageMode(getPositionImageMode());
		}

		ImageUIBuilder background = new ImageUIBuilder(ScrollbarUI.BACKGROUND_ID);
		background.setVisibleToMouse(true);
		background.setPath(getBackgroundImage());
		background.setLayoutType(ChildLayoutType.Absolute);
		background.setWidth(getBackgroundWidth());
		background.setHeight("*");
		background.add(position);

		// ElementEffectUtils.addFocusEffect(up, "#parent",
		// EffectEventId.onClick);
		// ElementEffectUtils.addFocusEffect(down, "#parent",
		// EffectEventId.onClick);
		// ElementEffectUtils.addFocusEffect(position, "#parent#parent",
		// EffectEventId.onClick);
		// ElementEffectUtils.addFocusEffect(background, "#parent",
		// EffectEventId.onClick);

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(VerticalScrollbarUIImpl.class.getName());
		builder.set(ScrollbarUI.PROPERTY_BUTTON_STEP_SIZE, String.valueOf(getButtonStepSize()));
		builder.set(ScrollbarUI.PROPERTY_PAGE_STEP_SIZE, String.valueOf(getPageStepSize()));
		builder.focusable(true);

		up.buildTo(builder);
		background.buildTo(builder);
		down.buildTo(builder);

		return builder;
	}

	/**
	 * @param backgroundImage картинка полоски скрола.
	 */
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	/**
	 * @param backgroundWidth ширина полоски скрола.
	 */
	public void setBackgroundWidth(String backgroundWidth) {
		this.backgroundWidth = backgroundWidth;
	}

	/**
	 * @param buttonDownImage картинка кнопки вниз.
	 */
	public void setButtonDownImage(String buttonDownImage) {
		this.buttonDownImage = buttonDownImage;
	}

	/**
	 * @param buttonHeight высота кнопок.
	 */
	public void setButtonHeight(String buttonHeight) {
		this.buttonHeight = buttonHeight;
	}

	/**
	 * @param buttonStepSize размер шага скролинга при нажатии на кнопку.
	 */
	public void setButtonStepSize(int buttonStepSize) {
		this.buttonStepSize = buttonStepSize;
	}

	/**
	 * @param buttonUpImage картинка кнопки вверх.
	 */
	public void setButtonUpImage(String buttonUpImage) {
		this.buttonUpImage = buttonUpImage;
	}

	/**
	 * @param buttonWidth ширина кнопок.
	 */
	public void setButtonWidth(String buttonWidth) {
		this.buttonWidth = buttonWidth;
	}

	/**
	 * @param pageStepSize размер шага при клике на полоску.
	 */
	public void setPageStepSize(int pageStepSize) {
		this.pageStepSize = pageStepSize;
	}

	/**
	 * @param positionImage картинка позиции скрола.
	 */
	public void setPositionImage(String positionImage) {
		this.positionImage = positionImage;
	}

	/**
	 * @param positionImageMode режим растягивания картинка позиции скрола.
	 */
	public void setPositionImageMode(String positionImageMode) {
		this.positionImageMode = positionImageMode;
	}

	/**
	 * @param positionWidth ширина позиции скрола.
	 */
	public void setPositionWidth(String positionWidth) {
		this.positionWidth = positionWidth;
	}
}
