package com.ss.client.gui.element.builder.impl;

import rlib.util.Strings;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;

import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Реадлизация конструктора по построению перемещаемого элемента.
 * 
 * @author Ronn
 */
public class DraggableElementUIBuilder extends PanelUIBuilder {

	/** может ли элемент размещаться в контейнере */
	private boolean doppable;
	/** возвращается ли элемент на изначальную позицию после перетаскивания */
	private boolean revertable;

	public DraggableElementUIBuilder(String id) {
		super(id);

		this.doppable = false;
		this.revertable = false;

		setVisibleToMouse(true);
	}

	/**
	 * @return может ли элемент размещаться в контейнере.
	 */
	public boolean isDoppable() {
		return doppable;
	}

	/**
	 * @return возвращается ли элемент на изначальную позицию после
	 * перетаскивания.
	 */
	public boolean isRevertable() {
		return revertable;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(DraggableElementUIImpl.class.getName());
		builder.set(DraggableElementUI.PROPERTY_DROPPABLE, String.valueOf(isDoppable()));
		builder.set(DraggableElementUI.PROPERTY_REVERTABLE, String.valueOf(isRevertable()));
		builder.style(Strings.EMPTY);

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		if(getBackgroundImage() != null) {
			builder.backgroundImage(getBackgroundImage());
		}

		return builder;
	}

	/**
	 * @param doppable может ли элемент размещаться в контейнере.
	 */
	public void setDoppable(boolean doppable) {
		this.doppable = doppable;
	}

	/**
	 * @param revertable возвращается ли элемент на изначальную позицию после
	 * перетаскивания.
	 */
	public void setRevertable(boolean revertable) {
		this.revertable = revertable;
	}
}
