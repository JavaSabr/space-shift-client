package com.ss.client.gui.element.list;

import com.ss.client.gui.element.ListElementUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Интерфейс для реализации фабрики констуирования элемента для итема списка.
 * 
 * @author Ronn
 */
public interface ListElementUIFactory<T> {

	public static final String ITEM_ID = "#item_";

	/**
	 * Конструирование нового элемента для казанного итема.
	 * 
	 * @param item итем списка.
	 * @param parent родительский элемент.
	 * @param nifty контроллер UI.
	 * @param screen экран UI.
	 * @param index индекс итема в списке.
	 * @return новый элемент.
	 */
	public ListElementUI<T> build(T item, Element parent, Nifty nifty, Screen screen, int index);
}
