package com.ss.client.gui.element;

/**
 * Интерфейс для реализации контрола элемента списка.
 * 
 * @author Ronn
 */
public interface ListElementUI<T> extends ElementUI {

	public static final String SELECT_EFFECT_KEY = "select";

	/**
	 * @return отображаемый итем.
	 */
	public T getItem();

	/**
	 * @return выделенный ли элемент.
	 */
	public boolean isSelected();

	/**
	 * @param item отображаемый итем.
	 */
	public void setItem(T item);

	/**
	 * @param selected выделенный ли элемент.
	 */
	public void setSelected(boolean selected);
}
