package com.ss.client.gui.element;

/**
 * Интерфейс для реализации элемента скроллбара.
 * 
 * @author Ronn
 */
public interface ScrollbarUI extends ElementUI {

	public static final String PROPERTY_PAGE_STEP_SIZE = "pageStepSize";
	public static final String PROPERTY_BUTTON_STEP_SIZE = "buttonStepSize";
	public static final String PROPERTY_INITIAL = "initial";
	public static final String PROPERTY_WORLD_PAGE_SIZE = "worldPageSize";
	public static final String PROPERTY_WORLD_MAX = "worldMax";

	public static final String POSITION_ID = "#position";
	public static final String BACKGROUND_ID = "#background";
	public static final String BUTTON_DOWN_ID = "#button_down";
	public static final String BUTTON_UP_ID = "#button_up";

	/**
	 * Get the current button step size.
	 * 
	 * @return step size
	 */
	public float getButtonStepSize();

	/**
	 * Get the current page size.
	 * 
	 * @return page size
	 */
	public float getPageStepSize();

	/**
	 * Get the current value of the scrollbar.
	 * 
	 * @return
	 */
	public float getValue();

	/**
	 * Get the current maximum of the scrollbar.
	 * 
	 * @return current world max
	 */
	public float getWorldMax();

	/**
	 * Get the current page size
	 * 
	 * @return current page size
	 */
	public float getWorldPageSize();

	/**
	 * Set the button step size to a new value.
	 * 
	 * @param stepSize step size
	 */
	public void setButtonStepSize(float stepSize);

	/**
	 * Set a new page size for page up/down and clicks on the background.
	 * 
	 * @param stepSize new step size
	 */
	public void setPageStepSize(float stepSize);

	/**
	 * Setup all parameters of this Scrollbar.
	 * 
	 * @param value the initial value
	 * @param worldMax the maximum value
	 * @param worldPageSize the page size
	 * @param buttonStepSize the step size for button clicks
	 * @param pageStepSize the step size for page up/down or clicks on the
	 * scrollbar background
	 */
	public void setup(float value, float worldMax, float worldPageSize, float buttonStepSize, float pageStepSize);

	/**
	 * Change the value of the scrollbar.
	 * 
	 * @param value the new value
	 */
	public void setValue(float value);

	/**
	 * Set the new maximum of the scrollbar.
	 * 
	 * @param worldMax new maximum
	 */
	public void setWorldMax(float worldMax);

	/**
	 * Set the new page size
	 * 
	 * @param worldPageSize new page size
	 */
	public void setWorldPageSize(float worldPageSize);
}
