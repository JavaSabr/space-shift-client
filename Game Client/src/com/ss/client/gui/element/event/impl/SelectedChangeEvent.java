package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о изменении выбранных объектов.
 * 
 * @author Ronn
 */
public class SelectedChangeEvent<T extends ElementUI, V> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	private V object;

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	public V getObject() {
		return object;
	}

	public void setObject(V object) {
		this.object = object;
	}
}
