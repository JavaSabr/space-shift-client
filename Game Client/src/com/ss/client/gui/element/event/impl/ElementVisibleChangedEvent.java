package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Событие об изменения статуса видимости элемента.
 * 
 * @author Ronn
 */
public class ElementVisibleChangedEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType ELEMENT_VISIBLE_CHANGE = new ElementEventType() {
	};

	/** видим ли элемент */
	private boolean visible;

	@Override
	public ElementEventType getEventType() {
		return ELEMENT_VISIBLE_CHANGE;
	}

	/**
	 * @return видим ли элемент.
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible видим ли элемент.
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
