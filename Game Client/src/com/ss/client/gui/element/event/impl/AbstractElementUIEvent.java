package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;

/**
 * Базовая реализация события элемента UI.
 * 
 * @author Ronn
 */
public abstract class AbstractElementUIEvent<T extends ElementUI> implements ElementUIEvent<T> {

	/** контрол испустивший событие */
	private T element;

	@Override
	public T getElement() {
		return element;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setElement(Object element) {
		this.element = (T) element;
	}
}
