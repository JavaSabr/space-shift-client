package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о старте перемещения элемента.
 * 
 * @author Ronn
 */
public class DraggableStartEvent extends AbstractElementUIEvent<DraggableElementUI> {

	public static final ElementEventType DRAGGABLE_START_EVENT = new ElementEventType() {
	};

	/** исходный контейнер */
	private DroppableElementUI source;

	@Override
	public ElementEventType getEventType() {
		return DRAGGABLE_START_EVENT;
	}

	/**
	 * @return исходный контейнер.
	 */
	public DroppableElementUI getSource() {
		return source;
	}

	/**
	 * @param source исходный контейнер.
	 */
	public void setSource(DroppableElementUI source) {
		this.source = source;
	}
}
