package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

import de.lessvoid.nifty.input.NiftyInputEvent;

/**
 * Реализация события о вводе с клавиатуры.
 * 
 * @author Ronn
 */
public class KeyInputEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	/** тип ввода */
	private NiftyInputEvent inputEvent;

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return тип ввода.
	 */
	public NiftyInputEvent getInputEvent() {
		return inputEvent;
	}

	/**
	 * @param inputEvent тип ввода.
	 */
	public void setInputEvent(NiftyInputEvent inputEvent) {
		this.inputEvent = inputEvent;
	}
}
