package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о отпускании нажатия кнопкой мыши.
 * 
 * @author Ronn
 */
public class MouseReleaseEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}
}
