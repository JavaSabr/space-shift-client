package com.ss.client.gui.element;

import de.lessvoid.nifty.render.NiftyImage;

/**
 * Интерфейс для реализации элемента картинки UI.
 * 
 * @author Ronn
 */
public interface ImageUI extends ElementUI {

	/**
	 * @return текущее изображениею
	 */
	public NiftyImage getImage();

	/**
	 * @param image новое изображение элемента.
	 */
	public void setImage(NiftyImage image);

	/**
	 * @param path путь к новому изображению элемента.
	 */
	public void setImage(String path);
}
