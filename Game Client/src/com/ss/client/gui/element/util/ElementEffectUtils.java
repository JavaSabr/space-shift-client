package com.ss.client.gui.element.util;

import com.ss.client.gui.element.builder.ElementUIBuilder;

import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.effects.EffectEventId;

/**
 * Набор вспомогательных методов по добавлению эффектов.
 * 
 * @author Ronn
 */
public class ElementEffectUtils {

	public static void addFocusEffect(ElementUIBuilder builder, String targetId, EffectEventId effectType) {

		EffectBuilder effectBuilder = new EffectBuilder("focus");
		effectBuilder.effectParameter("targetElement", targetId);

		builder.addEffect(effectType, effectBuilder);
	}

	public static void addImagePulsateEffect(ElementUIBuilder builder, String path, int period, boolean post) {

		EffectBuilder effectBuilder = new EffectBuilder("imageOverlayPulsate");
		effectBuilder.post(post);
		effectBuilder.timeType("infinite");
		effectBuilder.effectParameter("period", String.valueOf(period));
		effectBuilder.effectParameter("filename", path);
		effectBuilder.effectParameter("pulsateType", "rectangle");

		builder.addEffect(EffectEventId.onCustom, effectBuilder);
	}
}
