package com.ss.client.gui.controller;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.gui.builder.SpaceImageBuilder;
import com.ss.client.gui.builder.game.GameUIBuilder;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ModuleType;
import com.ss.client.model.module.system.ModuleSlot;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.table.LangTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.SkillTemplate;
import com.ss.client.util.Initializable;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.window.WindowControl;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Контролер панели корабля.
 * 
 * @author Ronn
 */
public class ShipPanelController extends WindowControl implements Initializable {

	private static final Logger log = Loggers.getLogger(ShipPanelController.class);

	private static final Game GAME = Game.getInstance();
	private static final Nifty NIFTY = GAME.getNifty();
	private static final Network NETWORK = Network.getInstance();
	private static final LangTable LANG_TABLE = LangTable.getInstance();

	private static ShipPanelController instance;

	public static ShipPanelController getInstance() {
		return instance;
	}

	private final FuncElement<Element> REMOVE_PANEL = new FuncElement<Element>() {

		@Override
		public void apply(final Element element) {
			element.markForMove(hidePanel);
			panelPool.add(element);
		}
	};

	private final FuncElement<Element> REMOVE_LABEL = new FuncElement<Element>() {

		@Override
		public void apply(final Element element) {
			element.markForMove(hidePanel);
			labelPool.add(element);
		}
	};

	private final FuncElement<Element> REMOVE_CELL = new FuncElement<Element>() {

		@Override
		public void apply(final Element element) {
			element.markForMove(hidePanel);
			cellPool.add(element);
		}
	};

	/** пулы элементов */
	private final Array<Element> panelPool;
	private final Array<Element> labelPool;
	private final Array<Element> cellPool;

	/** списка использованных элементов */
	private final Array<Element> labels;
	private final Array<Element> panels;
	private final Array<Element> cells;

	/** кран интерфейса */
	private Screen screen;

	/** панель для размещения схемы модулей */
	private Element modulePanel;
	/** панель для размещения характеристик корабля */
	private Element statPanel;
	/** скрытая панель контейнер */
	private Element hidePanel;

	/** корабль игрока */
	private PlayerShip ship;

	/** инициализирован ли контролер */
	private boolean initialized;

	public ShipPanelController() {
		this.panelPool = Arrays.toArray(Element.class);
		this.labelPool = Arrays.toArray(Element.class);
		this.cellPool = Arrays.toArray(Element.class);
		this.labels = Arrays.toArray(Element.class);
		this.panels = Arrays.toArray(Element.class);
		this.cells = Arrays.toArray(Element.class);

		instance = this;
	}

	private Array<Element> getCells() {
		return cells;
	}

	private Array<Element> getLabels() {
		return labels;
	}

	public Element getModulePanel() {
		return modulePanel;
	}

	private Element getNewCell(final Element parent) {

		Element cell = cellPool.pop();

		if(cell == null) {

			final PanelBuilder cellBuilder = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), GameUIBuilder.DEFAULT_PANEL_STYLE, 52, 52);
			cellBuilder.childLayout(ChildLayoutType.Center);

			cell = cellBuilder.build(NIFTY, screen, hidePanel);
		}

		cell.markForMove(parent);

		return cell;
	}

	private Element getNewLabel(final String text, final Element parent) {

		Element label = labelPool.pop();

		if(label == null) {
			final LabelBuilder labelBuilder = ElementFactory.getLabelBuilder(ElementId.getNextElementId(), text);
			label = labelBuilder.build(NIFTY, screen, hidePanel);
		} else {
			final Label control = label.getNiftyControl(Label.class);
			control.setText(text);
		}

		label.markForMove(parent);

		return label;
	}

	private Element getNewPanel(final Element parent) {

		Element panel = panelPool.pop();

		if(panel == null) {
			final PanelBuilder cellPanelBuilder = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, "100%", "52px");
			panel = cellPanelBuilder.build(NIFTY, screen, hidePanel);
		}

		panel.markForMove(parent);

		return panel;
	}

	private Array<Element> getPanels() {
		return panels;
	}

	public PlayerShip getShip() {
		return ship;
	}

	/**
	 * Есть ли слоты указанного типа.
	 * 
	 * @param type тип искомых слотов.
	 * @param slots доступные слоты.
	 * @return есть ли слоты такого типа.
	 */
	private boolean hasType(final ModuleType type, final ModuleSlot[] slots) {

		for(final ModuleSlot slot : slots) {
			if(slot.getType() == type) {
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public void onStartScreen() {

		if(!isInitialized()) {

			screen = NIFTY.getScreen(ElementId.GAME_SCREEN.getId());

			modulePanel = screen.findElementByName(ElementId.GAME_SHIP_PANEL_MODULES_PANEL.getId());
			statPanel = screen.findElementByName(ElementId.GAME_SHIP_PANEL_STATS_PANEL.getId());
			hidePanel = screen.findElementByName(ElementId.GAME_SHIP_PANEL_HIDE.getId());

			getElement().hide();
			setInitialized(true);
		}

		super.onStartScreen();
	}

	private void rebuildSchema(final PlayerShip ship) {

		final Array<Element> labels = getLabels();
		labels.apply(REMOVE_LABEL);
		labels.clear();

		final Array<Element> cells = getCells();
		cells.apply(REMOVE_CELL);
		cells.clear();

		final Array<Element> panels = getPanels();
		panels.apply(REMOVE_PANEL);
		panels.clear();

		final Element panel = getModulePanel();

		final ModuleSystem moduleSystem = ship.getModuleSystem();

		final ModuleSlot[] slots = moduleSystem.getSlots();
		final ModuleType[] types = ModuleType.getValues();

		for(final ModuleType type : types) {

			if(!hasType(type, slots)) {
				continue;
			}

			final Element label = getNewLabel(LANG_TABLE.getText(type.getTypeName()), panel);
			labels.add(label);

			final Element cellPanel = getNewPanel(panel);
			panels.add(cellPanel);

			for(final ModuleSlot slot : slots) {

				if(slot.getType() != type)
					continue;

				final Element cell = getNewCell(cellPanel);
				cells.add(cell);
				slot.setCell(cell);
			}
		}
	}

	public void setInitialized(final boolean initialized) {
		this.initialized = initialized;
	}

	public void setShip(final PlayerShip ship) {
		this.ship = ship;
	}

	/**
	 * Обновление схемы и характристик.
	 * 
	 * @param ship корвбль игрока.
	 * @param changeTemplate изменен ли шаблон корабля.
	 * @param changeModules изменены ли вставленные модули.
	 */
	public void update(final PlayerShip ship, final boolean changeTemplate, final boolean changeModules) {

		final PlayerShip current = getShip();

		if(current == null) {
			rebuildSchema(ship);
			updateCells(ship);
		} else {

			if(changeTemplate) {
				rebuildSchema(ship);
			}

			if(changeModules) {
				updateCells(ship);
			}
		}

		setShip(ship);

		hidePanel.hide();
	}

	private void updateCells(final PlayerShip ship) {

		final Array<Element> cells = getCells();

		for(final Element cell : cells.array()) {

			if(cell == null) {
				break;
			}

			ElementUtils.removeChilds(cell);
		}

		final ModuleSystem moduleSystem = ship.getModuleSystem();

		final ModuleSlot[] slots = moduleSystem.getSlots();

		for(final ModuleSlot slot : slots) {

			final Module module = slot.getModule();

			if(module == null) {
				continue;
			}

			final ModuleTemplate template = module.getTemplate();

			String text = null;

			// строим описание модуля
			{
				final SkillTemplate[] skills = template.getSkills();

				final StringBuilder builder = new StringBuilder();

				for(final SkillTemplate skill : skills)
					builder.append(skill.getDescription()).append("\n\n");

				if(builder.length() > 2)
					builder.replace(builder.length() - 2, builder.length(), "");

				text = builder.toString();
			}

			try {

				final SpaceImageBuilder builder = new SpaceImageBuilder(ElementId.getNextElementId());
				builder.filename(module.getIcon());

				ElementFactory.addHintTo(builder, text);

				builder.build(NIFTY, screen, slot.getCell());
			} catch(final Exception e) {
				log.warning(e);
			}
		}
	}
}
