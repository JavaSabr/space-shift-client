package com.ss.client.gui.controller.camera;

import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.util.LocalObjects;

/**
 * Реализация контролера работы камеры.
 * 
 * @author Ronn
 */
public final class CameraController implements AnalogListener, ActionListener {

	public static final String CAMERA_CONTROL_ROTATE_DOWN = "camera_control_rotate_down";
	public static final String CAMERA_CONTROL_ROTATE_UP = "camera_control_rotate_up";
	public static final String CAMERA_CONTROL_ROTATE_RIGHT = "camera_control_rotate_right";
	public static final String CAMERA_CONTROL_ROTATE_LEFT = "camera_control_rotate_left";

	public static final String CAMERA_CONTROL_ROTATE_ACTIVATE = "camera_control_rotate_activate";

	public static final String CAMERA_CONTROL_ZOOM_IN = "camera_control_zoom_in";
	public static final String CAMERA_CONTROL_ZOOM_OUT = "camera_control_zoom_out";

	public static final String[] MAPPING = {

		CAMERA_CONTROL_ZOOM_IN,
		CAMERA_CONTROL_ZOOM_OUT,

		CAMERA_CONTROL_ROTATE_DOWN,
		CAMERA_CONTROL_ROTATE_UP,
		CAMERA_CONTROL_ROTATE_RIGHT,
		CAMERA_CONTROL_ROTATE_LEFT,

		CAMERA_CONTROL_ROTATE_ACTIVATE
	};

	private static final Game GAME = Game.getInstance();

	private static final CameraController instance = new CameraController();

	public static CameraController getInstance() {
		return instance;
	}

	/** камера игрока */
	private final Camera camera;

	/** задача по плавному централизировании камеры */
	private final RotationTask task;

	/** матрица для рассчета разворота */
	private final Matrix3f rotateMatrix;

	/** последний разворот камеры */
	private final Quaternion cameraRotate;

	/** вектора для расчета разворота */
	private final Vector3f vectorUp;
	private final Vector3f vectorLeft;
	private final Vector3f vectorDir;

	/** вектор направления относительно корабля */
	private final Vector3f vectorShipDir;
	/** вектор предыдущей позиции корбаля */
	private final Vector3f vectorShipPos;
	/** вектор позиции камеры */
	private final Vector3f vectorPos;

	/** менеджер ввода в игре */
	private InputManager inputManager;
	/** корабль для просмотра */
	private SpaceShip ship;

	/** режим интерфейса */
	private InputMode inputMode;

	/** скорость зумирования */
	private final float zoomSpeed;
	/** ограничение зума камеры */
	private final float zoomMax;
	private final float zoomMin;

	/** скорость вращения камеры */
	private final float rotationSpeed;

	/** модификатор дистанции */
	private float distanceMod;

	/** активирован ли контролер */
	private boolean enabled;
	/** активна ли вазможность вращения камеры */
	private boolean rotationActive;

	public CameraController() {
		this.camera = GAME.getCamera();
		this.zoomSpeed = 3F;
		this.zoomMax = 35F;
		this.zoomMin = 75F;
		this.rotationSpeed = 1F;
		this.rotateMatrix = new Matrix3f();
		this.vectorDir = new Vector3f();
		this.vectorLeft = new Vector3f();
		this.vectorUp = new Vector3f();
		this.vectorShipDir = new Vector3f();
		this.vectorShipPos = new Vector3f();
		this.vectorPos = new Vector3f();
		this.task = new RotationTask(this);
		this.cameraRotate = new Quaternion();
		this.distanceMod = 1F;
	}

	/**
	 * Разворот камеры.
	 */
	private void cameraRotate(float value, Vector3f axis) {

		Camera camera = getCamera();

		Matrix3f mat = getRotateMatrix();
		mat.fromAngleNormalAxis(getRotationSpeed() * value, axis);

		Quaternion current = camera.getRotation();

		Vector3f up = current.getRotationColumn(1, getVectorUp());
		Vector3f left = current.getRotationColumn(0, getVectorLeft());
		Vector3f dir = current.getRotationColumn(2, getVectorDir());

		mat.mult(up, up);
		mat.mult(left, left);
		mat.mult(dir, dir);

		Quaternion quaternion = new Quaternion();
		quaternion.fromAxes(left, up, dir);
		quaternion.normalizeLocal();

		camera.setAxes(quaternion);
	}

	/**
	 * Произведения зума камерой.
	 * 
	 * @param value значение изменения степени зума.
	 */
	private void cameraZoom(float value) {

		GameUIController controller = GameUIController.getInstance();

		if(controller.getInputMode() != InputMode.CONTROL_MODE) {
			return;
		}

		value *= 0.2F;

		float currentMod = getDistanceMod();
		float newMod = Math.max(currentMod + value, 1F);

		setDistanceMod(newMod);

		if(Float.compare(currentMod, newMod) != 0) {
			updatePosition(getSpaceShip(), true);
		}
	}

	/**
	 * @return камера игрока.
	 */
	private Camera getCamera() {
		return camera;
	}

	/**
	 * @return последний разворот камеры.
	 */
	private Quaternion getCameraRotate() {
		return cameraRotate;
	}

	/**
	 * @return модификатор дистанции.
	 */
	public float getDistanceMod() {
		return distanceMod;
	}

	/**
	 * @return менеджер ввода.
	 */
	public InputManager getInputManager() {
		return inputManager;
	}

	/**
	 * @return режим интерфейса.
	 */
	public InputMode getInputMode() {
		return inputMode;
	}

	/**
	 * @return матрица для рассчета разворота.
	 */
	private Matrix3f getRotateMatrix() {
		return rotateMatrix;
	}

	/**
	 * @return скорость вращения.
	 */
	private float getRotationSpeed() {
		return rotationSpeed;
	}

	/**
	 * @return корабль игрока.
	 */
	public SpaceShip getSpaceShip() {
		return ship;
	}

	/**
	 * @return вектор прямо.
	 */
	private Vector3f getVectorDir() {
		return vectorDir;
	}

	/**
	 * @return вектор плево.
	 */
	private Vector3f getVectorLeft() {
		return vectorLeft;
	}

	/**
	 * @return вектор позиции камеры.
	 */
	private Vector3f getVectorPos() {
		return vectorPos;
	}

	/**
	 * @return вектор направления относительно корабля.
	 */
	private Vector3f getVectorShipDir() {
		return vectorShipDir;
	}

	/**
	 * @return вектор предыдущей позиции корбаля.
	 */
	private Vector3f getVectorShipPos() {
		return vectorShipPos;
	}

	/**
	 * @return вектор вверх.
	 */
	private Vector3f getVectorUp() {
		return vectorUp;
	}

	/**
	 * @return максимальный зум.
	 */
	private float getZoomMax() {
		return zoomMax;
	}

	/**
	 * @return минимальный зум.
	 */
	private float getZoomMin() {
		return zoomMin;
	}

	/**
	 * @return скорость зума.
	 */
	private float getZoomSpeed() {
		return zoomSpeed;
	}

	/**
	 * Инициализация прослушивания кнопок.
	 */
	public void initControls() {

		inputManager = GAME.getInputManager();

		inputManager.addMapping(CAMERA_CONTROL_ZOOM_IN, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));
		inputManager.addMapping(CAMERA_CONTROL_ZOOM_OUT, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));

		inputManager.addMapping(CAMERA_CONTROL_ROTATE_LEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
		inputManager.addMapping(CAMERA_CONTROL_ROTATE_RIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
		inputManager.addMapping(CAMERA_CONTROL_ROTATE_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		inputManager.addMapping(CAMERA_CONTROL_ROTATE_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true));

		inputManager.addMapping(CAMERA_CONTROL_ROTATE_ACTIVATE, new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE));

		inputManager.addListener(this, MAPPING);
	}

	/**
	 * @return активирован ли контролер.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @return активна ли возможность вращения камеры.
	 */
	public boolean isRotationActive() {
		return rotationActive;
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {

		if(!isEnabled()) {
			return;
		}

		if(name == CAMERA_CONTROL_ROTATE_ACTIVATE) {
			setRotationActive(isPressed);
			return;
		}
	}

	@Override
	public void onAnalog(String name, float value, float tpf) {

		if(!isEnabled()) {
			return;
		}

		if(name == CAMERA_CONTROL_ZOOM_IN) {
			cameraZoom(value);
			return;
		} else if(name == CAMERA_CONTROL_ZOOM_OUT) {
			cameraZoom(-value);
			return;
		}

		SpaceShip ship = getSpaceShip();

		if(ship == null || !isRotationActive()) {
			return;
		}

		Quaternion rotation = ship.getRotation();
		Vector3f vector = getVectorShipDir();

		if(name == CAMERA_CONTROL_ROTATE_LEFT) {
			cameraRotate(value, rotation.getRotationColumn(1, vector));
		} else if(name == CAMERA_CONTROL_ROTATE_RIGHT) {
			cameraRotate(-value, rotation.getRotationColumn(1, vector));
		} else if(name == CAMERA_CONTROL_ROTATE_UP) {
			cameraRotate(-value, rotation.getRotationColumn(0, vector));
		} else if(name == CAMERA_CONTROL_ROTATE_DOWN) {
			cameraRotate(value, rotation.getRotationColumn(0, vector));
		}
	}

	/**
	 * @param visible отображать ли курсор.
	 */
	public void setCursorVisible(boolean visible) {
		inputManager.setCursorVisible(visible);
	}

	/**
	 * @param distanceMod модификатор дистанции.
	 */
	public void setDistanceMod(float distanceMod) {
		this.distanceMod = distanceMod;
	}

	/**
	 * @param enabled активирован ли контролер.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @param inputMode режим интерфейса.
	 */
	public void setInputMode(InputMode inputMode) {
		this.inputMode = inputMode;

		if(inputMode == InputMode.INTERFACE_MODE) {
			getInputManager().setCursorVisible(true);
		} else if(inputMode == InputMode.CONTROL_MODE) {
			getInputManager().setCursorVisible(false);
		}
	}

	/**
	 * @param rotationActive активна ли возможность вращения камеры.
	 */
	private void setRotationActive(boolean rotationActive) {
		this.rotationActive = rotationActive;

		InputMode inputMode = getInputMode();

		if(inputMode == InputMode.INTERFACE_MODE) {
			getInputManager().setCursorVisible(!rotationActive);
		}
	}

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setSpaceShip(SpaceShip ship) {
		this.ship = ship;
		this.task.setSpaceShip(ship);
	}

	/**
	 * Обновление камеры.
	 */
	public void update() {

		SpaceShip ship = getSpaceShip();

		if(ship == null) {
			return;
		}

		updatePosition(ship, false);
	}

	/**
	 * Обновление положение камеры.
	 */
	public void update(LocalObjects local, long currentTime) {

		if(isEnabled()) {
			task.update(local, currentTime);
		}
	}

	/**
	 * Обновление позиции камеры относительно корабля.
	 */
	private void updatePosition(SpaceShip ship, boolean force) {

		SpaceShipView view = ship.getView();

		Vector3f position = getVectorShipPos();
		Quaternion rotation = getCameraRotate();

		Camera camera = getCamera();

		Vector3f shipPosition = view.getLocation();
		Quaternion cameraRotation = camera.getRotation();

		if(!force && position.equals(shipPosition) && cameraRotation.equals(rotation)) {
			return;
		}

		try {

			float mod = -12F;
			float speed = ship.getFlyCurrentSpeed();

			if(speed > 0) {
				mod -= speed / 30;
			}

			int offset = ship.getSizeY();
			offset *= getDistanceMod();

			Vector3f pos = camera.getDirection(getVectorPos());
			pos.multLocal(offset * mod);
			pos.addLocal(view.getLocation());

			float firstX = pos.getX();
			float firstY = pos.getY();
			float firstZ = pos.getZ();

			pos = camera.getUp(pos);
			pos.multLocal(offset * 4F);
			pos.addLocal(firstX, firstY, firstZ);

			camera.setLocation(pos);

		} finally {
			position.set(view.getLocation());
			rotation.set(camera.getRotation());
		}
	}
}
