package com.ss.client.gui.controller.game;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.ss.client.Game;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.controller.camera.CameraController;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.position.ElementPosition;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.DraggableStopEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.events.WindowMinimizedEvent;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.KeySetting;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.GameState;
import com.ss.client.network.Network;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.RequestHangarDialog;
import com.ss.client.settings.InputSetting;
import com.ss.client.util.Initializable;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Controller;
import de.lessvoid.nifty.controls.Draggable;
import de.lessvoid.nifty.controls.DraggableDragCanceledEvent;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.DefaultScreenController;
import de.lessvoid.nifty.screen.Screen;

/**
 * Модель управление кораблем игрока.
 * 
 * @author Ronn
 */
public class GameUIController extends DefaultScreenController implements Initializable {

	private static final Game GAME = Game.getInstance();
	private static final Network NETWORK = Network.getInstance();

	private static final GameUIController instance = new GameUIController();

	public static GameUIController getInstance() {
		return instance;
	}

	/** слушатель перемещения компонентов игрового UI */
	private final ElementUIEventListener<DraggableElementUI> elementDraggedListener = new ElementUIEventListener<DraggableElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<DraggableElementUI> event) {
			DraggableElementUI draggable = event.getElement();
			getInterfacePosition().changePosition(draggable.getElement());
		}
	};

	/** таблица слушателей событий */
	private final Table<GameUIEventType, Array<GameUIEventListener>> listenerTable;

	/** список компонентов игрового UI */
	private final Array<GameUIComponent> components;

	/** контролер позиций элементов интерфейса */
	private final ElementPosition interfacePosition;

	/** экран игрового УИ */
	private Screen screen;

	/** управляемый корабль */
	private PlayerShip playerShip;
	/** игровая стадия */
	@SuppressWarnings("unused")
	private GameState gameState;

	/** текущий режим ввода */
	private InputMode inputMode;

	/** контролер работы камеры */
	private CameraController cameraController;

	/** окно квестового диалога */
	private Element questDialogWindow;
	/** индикатор нахождения в ангаре */
	private Element hangarIndicator;

	/** текущий диалог с ангаром */
	private volatile Element hangarDialog;

	/** инициализирован ли контролер */
	private boolean initialized;

	/**
	 * Прослушчик переключателей.
	 */
	private final ActionListener actionInputListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {

			if(!keyPressed)
				return;

			if(name == KeySetting.SWITCH_CONTROL_MODE.name()) {
				enabled(inputMode == InputMode.INTERFACE_MODE);
				return;
			}

			if(inputMode == InputMode.INTERFACE_MODE) {

				if(name == KeySetting.OPEN_DIALOG.name()) {
					openHangarDialog();
				}

			} else {

				final KeySetting key = KeySetting.valueOf(name);
			}
		}
	};

	/** ----------------- */

	public GameUIController() {
		this.components = Arrays.toArraySet(GameUIComponent.class);
		this.interfacePosition = new ElementPosition(this);
		this.listenerTable = Tables.newObjectTable();
	}

	/**
	 * Активация контролера.
	 */
	public void activate(final GameState gameState) {
		this.gameState = gameState;

		InputManager inputManager = GAME.getInputManager();

		addKeys(inputManager);
		enabled(false);
		setPlayerShip(gameState.getPlayerShip());
		registerInput(inputManager);
	}

	/**
	 * Активаця клавишь управления.
	 * 
	 * @param inputManager менеджер по прослушке клавишь ввода.
	 */
	public void addKeys(final InputManager inputManager) {

		final KeySetting[] settings = KeySetting.values();

		for(final KeySetting setting : settings)
			if(setting.isKeyboard())
				inputManager.addMapping(setting.name(), new KeyTrigger(setting.getKey()));

		inputManager.addMapping(KeySetting.FLY_LEFT.name(), new MouseAxisTrigger(MouseInput.AXIS_X, true));
		inputManager.addMapping(KeySetting.FLY_RIGHT.name(), new MouseAxisTrigger(MouseInput.AXIS_X, false));
		inputManager.addMapping(KeySetting.FLY_UP.name(), new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		inputManager.addMapping(KeySetting.FLY_DOWN.name(), new MouseAxisTrigger(MouseInput.AXIS_Y, true));

		inputManager.addMapping(KeySetting.FAST_MENU_7.name(), new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		inputManager.addMapping(KeySetting.FAST_MENU_8.name(), new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));

		inputManager.addListener(actionInputListener, KeySetting.SWITCH_CONTROL_MODE.name());
		inputManager.addListener(actionInputListener, KeySetting.SWITCH_SELECTOR_INDICATOR.name());
		inputManager.addListener(actionInputListener, KeySetting.OPEN_DIALOG.name());
		inputManager.addListener(actionInputListener, KeySetting.ACTIVATE_INDICATOR.name());

		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_1.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_2.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_3.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_4.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_5.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_6.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_7.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_8.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_9.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_10.name());
		inputManager.addListener(actionInputListener, KeySetting.FAST_MENU_11.name());
	}

	@Override
	public void bind(Nifty nifty, Screen screen) {
		super.bind(nifty, screen);

		this.screen = screen;

		hangarIndicator = screen.findElementByName(ElementId.GAME_WINDOW_ANGAR_INDICATOR.getId());
		hangarIndicator.hide();

		cameraController = CameraController.getInstance();
		cameraController.setEnabled(true);
		cameraController.initControls();

		interfacePosition.applyDefault(screen);

		Array<GameUIComponent> components = getComponents();

		for(Element child : screen.getLayerElements()) {

			GameUIComponent component = child.getControl(GameUIComponent.class);

			if(component != null) {
				components.add(component);
			}

			for(Element subChild : child.getFastElements()) {

				component = subChild.getControl(GameUIComponent.class);

				if(component != null) {
					components.add(component);
				}
			}
		}

		for(GameUIComponent component : components) {

			component.register(this);

			if(component instanceof DraggableElementUI) {
				((DraggableElementUI) component).addElementListener(DraggableStopEvent.DRAGGABLE_STOP_EVENT, getElementDraggedListener());
			}
		}

		setInitialized(true);
	}

	@NiftyEventSubscriber(pattern = "game_.*")
	public void changeElementMinimized(final String id, final WindowMinimizedEvent event) {
		interfacePosition.changeMinimized(event.getElement(), event.isMinimized());
	}

	@NiftyEventSubscriber(pattern = "game_.*")
	public void changeElementPosition(final String id, final DraggableDragCanceledEvent event) {
		final Draggable draggable = event.getDraggable();
		interfacePosition.changePosition(draggable.getElement());
	}

	/**
	 * Закрытие диалога с ангаром.
	 */
	public void closeHangarDialog() {
		GAME.syncLock();
		try {

			if(hangarDialog == null) {
				return;
			}

			hangarDialog.hide();
			hangarDialog = null;

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * @param enabled включен ли режим управления.
	 */
	private void enabled(final boolean enabled) {
		GAME.syncLock();
		try {

			if(enabled) {
				cameraController.setInputMode(InputMode.CONTROL_MODE);
				setInputMode(InputMode.CONTROL_MODE);
			} else {
				cameraController.setInputMode(InputMode.INTERFACE_MODE);
				setInputMode(InputMode.INTERFACE_MODE);
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Поиск контролера.
	 * 
	 * @param id ид контролера.
	 * @param type класс контролера.
	 * @return искомый контролер.
	 */
	public <T extends Controller> T findController(String id, Class<T> type) {
		return getScreen().findControl(id, type);
	}

	/**
	 * @return список компонентов игрового UI.
	 */
	public Array<GameUIComponent> getComponents() {
		return components;
	}

	public ElementUIEventListener<DraggableElementUI> getElementDraggedListener() {
		return elementDraggedListener;
	}

	/**
	 * @return активный диалог с ангаром.
	 */
	public Element getHangarDialog() {
		return hangarDialog;
	}

	/**
	 * @return индикатор взаимодействия с ангаром.
	 */
	public Element getHangarIndicator() {
		return hangarIndicator;
	}

	/**
	 * @return inputMode
	 */
	public final InputMode getInputMode() {
		return inputMode;
	}

	/**
	 * @return контролер позиции интерфейса.
	 */
	public ElementPosition getInterfacePosition() {
		return interfacePosition;
	}

	/**
	 * @return таблица слушателей событий.
	 */
	private Table<GameUIEventType, Array<GameUIEventListener>> getListenerTable() {
		return listenerTable;
	}

	/**
	 * @return playerShip
	 */
	public final PlayerShip getPlayerShip() {
		return playerShip;
	}

	private Element getQuestDialogWindow() {
		return questDialogWindow;
	}

	/**
	 * @return экран игрового УИ.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * Спрятать индикатор ангара.
	 */
	public void hideHangarIndicator() {
		GAME.syncLock();
		try {

			if(!hangarIndicator.isVisible())
				return;

			hangarIndicator.hide();
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Скрыть квестовый диалог.
	 */
	public void hideQuestDialog() {
		GAME.syncLock();
		try {

			final Element questDialogWindow = getQuestDialogWindow();

			if(!questDialogWindow.isVisible()) {
				return;
			}

			final Element hangarDialog = getHangarDialog();

			if(hangarDialog == questDialogWindow) {
				setHangarDialog(null);
			}

			questDialogWindow.hide();

		} finally {
			GAME.syncUnlock();
		}
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * отправка всем события.
	 * 
	 * @param event события игрового UI.
	 */
	public void notifyEvent(GameUIEvent event) {

		Table<GameUIEventType, Array<GameUIEventListener>> listenerTable = getListenerTable();
		Array<GameUIEventListener> listeners = listenerTable.get(event.getEventType());

		if(listeners == null) {
			return;
		}

		for(GameUIEventListener listener : listeners.array()) {

			if(listener == null) {
				break;
			}

			listener.notifyEvent(event);
		}
	}

	/**
	 * Обработка открытия диалога с ангаром.
	 */
	public void openHangarDialog() {

		final NetServer server = NETWORK.getGameServer();

		if(server == null) {
			return;
		}

		server.sendPacket(RequestHangarDialog.getInstance());
	}

	/**
	 * Регистрация прослушки действий ввода пользователя.
	 * 
	 * @param inputManager менеджер ввода.
	 */
	public void registerInput(InputManager inputManager) {

		InputSetting[] settings = InputSetting.values();

		for(InputSetting setting : settings) {

			Trigger[] triggers = setting.getTriggers();

			if(triggers == null || triggers.length < 1) {
				continue;
			}

			inputManager.addMapping(setting.name(), triggers);
		}

		for(GameUIComponent component : getComponents()) {
			component.register(inputManager);
		}
	}

	/**
	 * Регистрация слушателя событий.
	 * 
	 * @param eventType тип события.
	 * @param listener слушатель событий.
	 */
	public void registerListener(GameUIEventType eventType, GameUIEventListener listener) {

		Table<GameUIEventType, Array<GameUIEventListener>> listenerTable = getListenerTable();
		Array<GameUIEventListener> listeners = listenerTable.get(eventType);

		if(listeners == null) {
			listeners = Arrays.toArray(GameUIEventListener.class);
			listenerTable.put(eventType, listeners);
		}

		listeners.add(listener);
	}

	/**
	 * Удаление с прослушки кнопок.
	 * 
	 * @param inputManager менеджер прослушки кнопок.
	 */
	public void removeKeys(final InputManager inputManager) {
		inputManager.deleteMapping(KeySetting.SWITCH_CONTROL_MODE.name());
	}

	/**
	 * @param hangarDialog активный диалог с ангаром.
	 */
	public void setHangarDialog(final Element hangarDialog) {
		this.hangarDialog = hangarDialog;
	}

	public void setInitialized(final boolean initialized) {
		this.initialized = initialized;
	}

	/**
	 * @param inputMode режим управления.
	 */
	public void setInputMode(final InputMode inputMode) {
		this.inputMode = inputMode;

		for(GameUIComponent component : getComponents()) {
			component.setInputMode(inputMode);
		}
	}

	/**
	 * Установка в контрол корабля игрока.
	 * 
	 * @param playerShip корабль игрока.
	 */
	private void setPlayerShip(final PlayerShip playerShip) {
		this.playerShip = playerShip;

		// labelShipName.setText(playerShip.getName());
		// labelShipLevel.setText(LANG_TABLE.getText("GAME_LABEL_SHIP_LEVEL_STATUS")
		// + " : " + playerShip.getLevel());
		// labelShipCp.setText("CP " + playerShip.getCurrentCp() + "/" +
		// playerShip.getMaxCp());
		// labelShipEp.setText("EP " + playerShip.getCurrentEp() + "/" +
		// playerShip.getMaxEp());

		cameraController.setSpaceShip(playerShip);

		Array<GameUIComponent> components = getComponents();

		for(GameUIComponent component : components) {
			component.setPlayerShip(playerShip);
		}
	}

	/**
	 * Отобразить индикатор ангара.
	 */
	public void showHangarIndicator() {
		GAME.syncLock();
		try {

			Element hangarIndicator = getHangarIndicator();

			if(hangarIndicator.isVisible()) {
				return;
			}

			hangarIndicator.show();

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Отобразить квестовый диалог.
	 */
	public void showQuestDialog() {
		GAME.syncLock();
		try {

			if(questDialogWindow.isVisible()) {
				return;
			}

			if(hangarDialog != null) {
				hangarDialog.hide();
			}

			questDialogWindow.show();

			setHangarDialog(questDialogWindow);

		} finally {
			GAME.syncUnlock();
		}
	}
}
