package com.ss.client.gui.controller.game.window.quest;

import java.util.Properties;

import com.ss.client.gui.element.impl.list.ListElementUIImpl;
import com.ss.client.gui.model.quest.QuestState;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * @author Ronn
 */
public class QuestElementController extends ListElementUIImpl<QuestState> {

	public static final String QUEST_NAME_ID = "#quest_name";

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		Element textElement = element.findElementByName(QUEST_NAME_ID);

		TextRenderer textRenderer = textElement.getRenderer(TextRenderer.class);
		textRenderer.setWidthConstraint(textElement, textElement.getConstraintWidth(), textElement.getWidth() - 4, nifty.getRenderEngine());

		int height = textRenderer.getTextHeight() + 4;

		if(height == getHeight()) {
			return;
		}

		element.setConstraintHeight(GameUtil.getPixelSize(height));
	}
}
