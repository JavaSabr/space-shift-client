package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEvent;

/**
 * Базовая реализация события игрового UI.
 * 
 * @author Ronn
 */
public abstract class AbstractGameUIEvent implements GameUIEvent {

}
