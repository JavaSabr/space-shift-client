package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;

/**
 * Событие о обновлении счетчика задания.
 * 
 * @author Ronn
 */
public class UpdateQuestCounterEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateQuestCounterEvent> LOCAL_EVENT = new ThreadLocal<UpdateQuestCounterEvent>() {

		@Override
		protected UpdateQuestCounterEvent initialValue() {
			return new UpdateQuestCounterEvent();
		}
	};

	public static final UpdateQuestCounterEvent get(int questId, int objectId, int counterId, int value) {

		UpdateQuestCounterEvent event = LOCAL_EVENT.get();
		event.counterId = counterId;
		event.objectId = objectId;
		event.value = value;
		event.questId = questId;

		return event;
	}

	/** ид задания */
	private int questId;
	/** уникальный ид задания */
	private int objectId;
	/** ид счетчика */
	private int counterId;
	/** текущее значение */
	private int value;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return ид задания.
	 */
	public int getQuestId() {
		return questId;
	}

	/**
	 * @return уникальный ид задания.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return ид счетчика.
	 */
	public int getCounterId() {
		return counterId;
	}

	/**
	 * @return текущее значение.
	 */
	public int getValue() {
		return value;
	}
}
