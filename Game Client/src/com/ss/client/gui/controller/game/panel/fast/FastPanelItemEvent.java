package com.ss.client.gui.controller.game.panel.fast;

import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.GameUIEventType;

/**
 * Интерфейс для реализации события связанного с элементом на панели.
 * 
 * @author Ronn
 */
public interface FastPanelItemEvent extends GameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};
}
