package com.ss.client.gui.controller.game.quest;

import java.util.Properties;

import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.impl.DynamicPanelUIImpl;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера контейнера элементов задания.
 * 
 * @author Ronn
 */
public class QuestStructContainerController extends DynamicPanelUIImpl {

	public static final String QUEST_CONTAINER_TITLE_ID = "#quest_container_title";

	/** титул контейнера */
	private LabelUI title;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.title = element.findControl(QUEST_CONTAINER_TITLE_ID, LabelUI.class);
	}

	/**
	 * @return титул контейнера.
	 */
	private LabelUI getTitle() {
		return title;
	}

	/**
	 * Установка нового титула контейнера.
	 * 
	 * @param title титул контейнера.
	 */
	public void setTitle(String title) {
		getTitle().setText(title);
	}
}
