package com.ss.client.gui.controller.game.panel.chat;

import com.ss.client.gui.element.builder.impl.ButtonUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder;

/**
 * Конструктор таба чата.
 * 
 * @author Ronn
 */
public class ChatTabElementUIBuilder extends ButtonUIBuilder {

	/** тип таба чата */
	private ChatTabType tabType;

	public ChatTabElementUIBuilder(String id) {
		super(id);
	}

	/**
	 * @return тип таба чата.
	 */
	public ChatTabType getTabType() {
		return tabType;
	}

	@Override
	public ElementBuilder prepareBuilder() {

		ElementBuilder builder = super.prepareBuilder();
		builder.set(ChatTabElementUI.PROPERTY_TAB_TYPE, getTabType().name());

		return builder;
	}

	/**
	 * @param tabType тип таба чата.
	 */
	public void setTabType(ChatTabType tabType) {
		this.tabType = tabType;
	}
}
