package com.ss.client.gui.controller.game.hud;

import java.util.Properties;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;

import com.jme3.input.InputManager;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.element.impl.PanelUIImpl;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.Network;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Базовая реализация HUD компонента.
 * 
 * @author Ronn
 */
public abstract class AbstractHudComponent extends PanelUIImpl implements HudComponent, UpdateUIElement {

	protected static final Logger LOGGER = Loggers.getLogger(HudComponent.class);

	protected static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	protected static final UpdateUIManager UPDATE_UI_MANAGER = UpdateUIManager.getInstance();
	protected static final Network NETWORK = Network.getInstance();
	protected static final Game GAME = Game.getInstance();
	protected static final Camera CAMERA = GAME.getCamera();

	/** контролер HUD компонентов */
	protected HudController controller;
	/** менеджер ввода */
	protected InputManager inputManager;
	/** режим ввода */
	protected InputMode inputMode;
	/** корабль игрока */
	protected PlayerShip playerShip;

	/** контролер UI */
	protected Nifty nifty;
	/** текущий экран */
	protected Screen screen;
	/** базовая панель */
	protected Element basePanel;

	@Override
	public void bind(Array<UpdateUIElement> container) {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);
		this.basePanel = element;
		this.nifty = nifty;
		this.screen = screen;
	}

	@Override
	public void finish() {
		setPlayerShip(null);
		setController(null);
		setInputManager(null);
	}

	/**
	 * @return базовая панель.
	 */
	public Element getBasePanel() {
		return basePanel;
	}

	/**
	 * @return контролер HUD компонентов.
	 */
	public HudController getController() {
		return controller;
	}

	/**
	 * @return менеджер ввода.
	 */
	public InputManager getInputManager() {
		return inputManager;
	}

	/**
	 * @return режим ввода.
	 */
	public InputMode getInputMode() {
		return inputMode;
	}

	/**
	 * @return контролер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	@Override
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return текущий экран.
	 */
	public Screen getScreen() {
		return screen;
	}

	@Override
	public void register(HudController controller) {
		setController(controller);
	}

	@Override
	public void register(InputManager inputManager) {
		setInputManager(inputManager);
	}

	/**
	 * @param controller контролер HUD компонентов.
	 */
	public void setController(HudController controller) {
		this.controller = controller;
	}

	/**
	 * @param inputManager менеджер ввода.
	 */
	public void setInputManager(InputManager inputManager) {
		this.inputManager = inputManager;
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		this.inputMode = inputMode;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {
		return false;
	}

}
