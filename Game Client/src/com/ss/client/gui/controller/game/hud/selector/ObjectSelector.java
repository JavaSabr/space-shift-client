package com.ss.client.gui.controller.game.hud.selector;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.controls.NiftyControl;

/**
 * Интерфейс для реализации селектораобъекта.
 * 
 * @author Ronn
 */
public interface ObjectSelector extends NiftyControl {

	public static final String NAME_LABEL_ID = "#name_label";
	public static final String DIST_LABEL_ID = "#dist_label";
	public static final String INFO_PANEL_ID = "#info_panel";
	public static final String SELECTOR_ICON_ID = "#selector_icon";

	public static final String SELECTOR_ICON = "game/hud/selector/default.png";

	/**
	 * Активировать.
	 */
	public void acivate();

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip();

	/**
	 * @return объект селектора.
	 */
	public SpaceObject getTarget();

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(PlayerShip playerShip);

	/**
	 * @param target селекнутый объект.
	 */
	public void setTarget(SpaceObject target);

	/**
	 * Запуск работы селектора.
	 */
	public void start();

	/**
	 * Обновление селектора.
	 */
	public void update(long currentTime);
}
