package com.ss.client.gui.controller.game.hud;

import java.util.Properties;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.event.impl.ObjectChangeFriendStatusEvent;
import com.ss.client.gui.controller.game.event.impl.SelectedObjectEvent;
import com.ss.client.gui.controller.game.hud.event.HudEvent;
import com.ss.client.gui.controller.game.hud.event.HudEventListener;
import com.ss.client.gui.controller.game.hud.indicator.FriendStatusChangedEvent;
import com.ss.client.gui.controller.game.hud.selector.SelectedEvent;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Базовый контроллер HUD.
 * 
 * @author Ronn
 */
public class HudController extends AbstractController implements GameUIComponent {

	public static final String HUD_LAYER_ID = "#hud_layer";

	private final Table<Class<? extends HudEvent>, Array<HudEventListener<HudEvent>>> tableListeners;

	/** слушатель событий игрового UI */
	private final GameUIEventListener eventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			processEvent(event);
		}
	};

	/** список компонентов HUD */
	private final Array<HudComponent> components;

	public HudController() {
		this.components = Arrays.toArray(HudComponent.class);
		this.tableListeners = Tables.newObjectTable();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {

		Array<HudComponent> components = getComponents();

		for(Element child : element.getElements()) {

			HudComponent component = child.getControl(HudComponent.class);

			if(component != null) {
				components.add(component);
				component.register(this);
			}
		}
	}

	@Override
	public void finish() {
		for(HudComponent component : getComponents()) {
			component.finish();
		}
	}

	/**
	 * @return список компонентов HUD.
	 */
	public Array<HudComponent> getComponents() {
		return components;
	}

	/**
	 * @return слушатель событий игрового UI.
	 */
	public GameUIEventListener getEventListener() {
		return eventListener;
	}

	/**
	 * @return таюдица слушателей.
	 */
	public Table<Class<? extends HudEvent>, Array<HudEventListener<HudEvent>>> getTableListeners() {
		return tableListeners;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	/**
	 * Уведомление слушателей об HUD событии.
	 * 
	 * @param event HUD событие.
	 */
	public void notify(HudEvent event) {

		Table<Class<? extends HudEvent>, Array<HudEventListener<HudEvent>>> tableListeners = getTableListeners();
		Array<HudEventListener<HudEvent>> listeners = tableListeners.get(event.getClass());

		if(listeners == null) {
			return;
		}

		for(HudEventListener<HudEvent> listener : listeners.array()) {
			listener.notify(event);
		}
	}

	@Override
	public void onStartScreen() {
	}

	/**
	 * Обработка события.
	 */
	private void processEvent(GameUIEvent gameUIEvent) {

		GameUIEventType eventType = gameUIEvent.getEventType();

		if(eventType == ObjectChangeFriendStatusEvent.EVENT_TYPE) {

			FriendStatusChangedEvent event = FriendStatusChangedEvent.get();
			event.setObject(((ObjectChangeFriendStatusEvent) gameUIEvent).getObject());

			notify(event);

		} else if(eventType == SelectedObjectEvent.EVENT_TYPE) {

			SelectedObjectEvent event = (SelectedObjectEvent) gameUIEvent;

			SelectedEvent selectEvent = SelectedEvent.get();
			selectEvent.setObject(event.getObject());

			notify(selectEvent);
		}
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(ObjectChangeFriendStatusEvent.EVENT_TYPE, getEventListener());
		controller.registerListener(SelectedObjectEvent.EVENT_TYPE, getEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
		for(HudComponent component : getComponents()) {
			component.register(inputManager);
		}
	}

	/**
	 * Регистрация слушателя на прослушку определенных HUD событий.
	 * 
	 * @param eventClass класс событий.
	 * @param listener слушатель событий.
	 */
	@SuppressWarnings("unchecked")
	public <T extends HudEvent> void registerListener(Class<T> eventClass, HudEventListener<T> listener) {

		Table<Class<? extends HudEvent>, Array<HudEventListener<HudEvent>>> tableListeners = getTableListeners();
		Array<HudEventListener<HudEvent>> listeners = tableListeners.get(eventClass);

		if(listeners == null) {
			listeners = Arrays.toArray(HudEventListener.class);
			tableListeners.put(eventClass, listeners);
		}

		if(!listeners.contains(listener)) {
			listeners.add((HudEventListener<HudEvent>) listener);
			listeners.trimToSize();
		}
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		for(HudComponent component : getComponents()) {
			component.setInputMode(inputMode);
		}
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		for(HudComponent component : getComponents()) {
			component.setPlayerShip(playerShip);
		}
	}
}
