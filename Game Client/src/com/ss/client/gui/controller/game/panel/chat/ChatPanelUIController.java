package com.ss.client.gui.controller.game.panel.chat;

import java.util.Properties;

import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.impl.ChatMessageEvent;
import com.ss.client.gui.element.ButtonUI;
import com.ss.client.gui.element.ImageUI;
import com.ss.client.gui.element.TextFieldUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.KeyInputEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.MessageType;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.SayMessage;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера панели с чатом.
 * 
 * @author Ronn
 */
public class ChatPanelUIController extends DraggableElementUIImpl implements GameUIComponent {

	private static final Network NETWORK = Network.getInstance();

	public static final String PANEL_ID = "#chat_panel";
	public static final String SEND_BUTTON_ID = "#send_button";
	public static final String TEXT_FIELD_ID = "#text_field";
	public static final String TAB_PANEL_ID = "#tab_panel";
	public static final String MESSAGE_PANEL_ID = "#message_panel";
	public static final String CONTENT_ID = "#content";
	public static final String INPUT_PANEL_ID = "#input_panel";
	public static final String SPLIT_PANEL_ID = "#split_panel";

	public static final String SEND_BUTTON_IMAGE = "ui/game/chat_panel/send_button.png";
	public static final String INPUT_WINDOW_IMAGE = "ui/game/chat_panel/input_window.png";
	public static final String PANEL_LINE_IMAGE = "ui/game/chat_panel/line.png";
	public static final String BACKGROUND_IMAGE = "ui/game/chat_panel/background.png";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static final String SEND_BUTTON_NAME = LANG_TABLE.getText("@interface:chatSendButtonName@");

	/** слушатель нажатия на кнопку отправки */
	private final ElementUIEventListener<ButtonUI> sendButtonEventListener = new ElementUIEventListener<ButtonUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ButtonUI> event) {
			sendMessage();
		}
	};

	/** слушатель выбора табов */
	private final ElementUIEventListener<ChatTabElementUI> selectTabEventListener = new ElementUIEventListener<ChatTabElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ChatTabElementUI> event) {
			PrimaryClickEvent<ChatTabElementUI> clickEvent = (PrimaryClickEvent<ChatTabElementUI>) event;
			select(clickEvent.getElement());
		}
	};

	/** слушатель сообщений чата */
	private final GameUIEventListener messageUIEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			ChatMessageEvent messageEvent = (ChatMessageEvent) event;
			addMessage(messageEvent.getMessage());
		}
	};

	/** слушатель ввода с текстовое поле */
	private final ElementUIEventListener<TextFieldUI> inputEventListener = new ElementUIEventListener<TextFieldUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<TextFieldUI> event) {

			KeyInputEvent<TextFieldUI> inputEvent = (KeyInputEvent<TextFieldUI>) event;

			switch(inputEvent.getInputEvent()) {
				case SubmitText: {
					sendMessage();
					break;
				}
				default: {
					break;
				}
			}
		}
	};

	/** список табов чата */
	private final Array<ChatTabElementUI> tabs;

	/** поле для ввода сообщения */
	private TextFieldUI textField;

	/** кнопка отправки */
	private ImageUI sendButton;

	/** текущий таб */
	private ChatTabElementUI currentTab;

	public ChatPanelUIController() {
		this.tabs = Arrays.toArray(ChatTabElementUI.class);
	}

	/**
	 * Добавления сообщения в чат.
	 * 
	 * @param message сообщение в чат.
	 */
	private void addMessage(ChatMessage message) {
		for(ChatTabElementUI tab : getTabs()) {
			tab.addMessage(message);
		}
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.sendButton = element.findControl(SEND_BUTTON_ID, ImageUI.class);
		this.sendButton.addElementListener(PrimaryClickEvent.EVENT_TYPE, getSendButtonEventListener());
		this.textField = element.findControl(TEXT_FIELD_ID, TextFieldUI.class);
		this.textField.addElementListener(KeyInputEvent.EVENT_TYPE, getInputEventListener());

		Element messagePanel = element.findElementByName(MESSAGE_PANEL_ID);
		Element panel = element.findElementByName(TAB_PANEL_ID);

		for(Element tab : panel.getFastElements()) {

			ChatTabElementUI control = tab.getControl(ChatTabElementUI.class);

			if(control != null) {
				control.addElementListener(PrimaryClickEvent.EVENT_TYPE, getSelectTabEventListener());
				control.init(this, messagePanel);
				tabs.add(control);
			}
		}
	}

	@Override
	public void finish() {
	}

	/**
	 * @return текущий тип таба.
	 */
	public ChatTabElementUI getCurrentTab() {
		return currentTab;
	}

	/**
	 * @return слушатель ввода с текстовое поле.
	 */
	public ElementUIEventListener<TextFieldUI> getInputEventListener() {
		return inputEventListener;
	}

	/**
	 * @return слушатель сообщений чата.
	 */
	public GameUIEventListener getMessageUIEventListener() {
		return messageUIEventListener;
	}

	/**
	 * @return слушатель выбора табов.
	 */
	public ElementUIEventListener<ChatTabElementUI> getSelectTabEventListener() {
		return selectTabEventListener;
	}

	/**
	 * @return слушатель нажатия на кнопку отправки.
	 */
	public ElementUIEventListener<ButtonUI> getSendButtonEventListener() {
		return sendButtonEventListener;
	}

	/**
	 * @return список табов чата.
	 */
	public Array<ChatTabElementUI> getTabs() {
		return tabs;
	}

	/**
	 * @return поле для ввода сообщения.
	 */
	public TextFieldUI getTextField() {
		return textField;
	}

	@Override
	public void onStartScreen() {
		select(getTabs().first());
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(ChatMessageEvent.EVENT_TYPE, getMessageUIEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
	}

	/**
	 * Выбор таба.
	 * 
	 * @param selected контрол таба.
	 */
	private void select(ChatTabElementUI selected) {

		for(ChatTabElementUI tab : getTabs()) {

			tab.setVisibleMessages(tab == selected);

			if(tab == selected) {
				setCurrentTab(tab);
			}
		}
	}

	/**
	 * Отправка сообщения.
	 */
	private void sendMessage() {

		final TextFieldUI textField = getTextField();
		final String text = textField.getDisplayedText();

		if(text.isEmpty()) {
			return;
		}

		ChatTabElementUI currentTab = getCurrentTab();
		ChatTabType tabType = currentTab.getTabType();

		NETWORK.sendPacketToGameServer(SayMessage.getInstance(tabType.getMessageType(), text));

		textField.setText(Strings.EMPTY);
	}

	/**
	 * @param currentTab текущий таб.
	 */
	public void setCurrentTab(ChatTabElementUI currentTab) {
		this.currentTab = currentTab;

		ChatTabType tabType = currentTab.getTabType();
		MessageType messageType = tabType.getMessageType();

		getTextField().setColor(messageType.getColor());
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		getTextField().disableFocus();
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);

		if(visible) {
			select(getCurrentTab());
		}
	}
}
