package com.ss.client.gui.controller.game.event;

/**
 * Интерфейс для реализации события игрового UI.
 * 
 * @author Ronn
 */
public interface GameUIEvent {

	/**
	 * @return тип события.
	 */
	public GameUIEventType getEventType();
}
