package com.ss.client.gui.controller.game.hud.selector;

import java.util.Properties;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.element.ImageUI;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.table.LangTable;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Базовая реализации индикатора выбора объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractObjectSelector<T extends SpaceObject> extends AbstractController implements ObjectSelector {

	protected static enum Direction {

		NORMAL {

			@Override
			protected NiftyImage getImage() {
				return getNormalImage();
			}
		},
		UP {

			@Override
			protected NiftyImage getImage() {
				return getUpImage();
			}
		},
		UP_LEFT {

			@Override
			protected NiftyImage getImage() {
				return getUpLeftImage();
			}
		},
		UP_RIGHT {

			@Override
			protected NiftyImage getImage() {
				return getUpRightImage();
			}
		},
		DOWN {

			@Override
			protected NiftyImage getImage() {
				return getDownImage();
			}
		},
		DOWN_LEFT {

			@Override
			protected NiftyImage getImage() {
				return getDownLeftImage();
			}
		},
		DOWN_RIGHT {

			@Override
			protected NiftyImage getImage() {
				return getDownRightImage();
			}
		},
		LEFT {

			@Override
			protected NiftyImage getImage() {
				return getLeftImage();
			}
		},
		RIGHT {

			@Override
			protected NiftyImage getImage() {
				return getRightImage();
			}
		},
		REFLECTION {

			@Override
			protected NiftyImage getImage() {
				return getUpImage();
			}
		};

		protected NiftyImage getImage() {
			return null;
		}
	}
	protected static final LangTable LANG_TABLE = LangTable.getInstance();
	protected static final Game GAME = Game.getInstance();
	protected static final Camera CAMERA = GAME.getCamera();
	protected static final Nifty NIFTY = GAME.getNifty();

	protected static final NiftyRenderEngine RENDER_ENGINE = NIFTY.getRenderEngine();

	public static final String DISTANCE_LABEL_NAME = LANG_TABLE.getText("@interface:selectorObjectDistance@");

	/** нормальное изображение селектора */
	private static NiftyImage normalImage;

	/** картинка стрелки вверх */
	private static NiftyImage upImage;

	/** картинка стрелки вверх-влево */
	private static NiftyImage upLeftImage;

	/** картинка стрелки вверх-вправо */
	private static NiftyImage upRightImage;

	/** картинка стрелки вниз */
	private static NiftyImage downImage;

	/** картинка стрелки вниз-влево */
	private static NiftyImage downLeftImage;

	/** картинка стрелки вниз-вправо */
	private static NiftyImage downRightImage;

	/** картинка стрелки влево */
	private static NiftyImage leftImage;

	/** картинка стрелки вправо */
	private static NiftyImage rightImage;

	public static NiftyImage getDownImage() {

		if(downImage == null) {
			downImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/down.png", true);
		}

		return downImage;
	}

	public static NiftyImage getDownLeftImage() {

		if(downLeftImage == null) {
			downLeftImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/down-left.png", true);
		}

		return downLeftImage;
	}

	public static NiftyImage getDownRightImage() {

		if(downRightImage == null) {
			downRightImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/down-right.png", true);
		}

		return downRightImage;
	}

	public static NiftyImage getLeftImage() {

		if(leftImage == null) {
			leftImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/left.png", true);
		}

		return leftImage;
	}

	public static NiftyImage getNormalImage() {

		if(normalImage == null) {
			normalImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/default.png", true);
		}

		return normalImage;
	}

	public static NiftyImage getRightImage() {

		if(rightImage == null) {
			rightImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/right.png", true);
		}

		return rightImage;
	}

	public static NiftyImage getUpImage() {

		if(upImage == null) {
			upImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/up.png", true);
		}

		return upImage;
	}

	public static NiftyImage getUpLeftImage() {

		if(upLeftImage == null) {
			upLeftImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/up-left.png", true);
		}

		return upLeftImage;
	}

	public static NiftyImage getUpRightImage() {

		if(upRightImage == null) {
			upRightImage = RENDER_ENGINE.createImage(NIFTY.getCurrentScreen(), "game/hud/selector/up-right.png", true);
		}

		return upRightImage;
	}

	/** позиция индикатора на экране */
	private final Vector3f position;

	/** панель с информацией о цели */
	private Element infoPanel;

	/** надпись с названием объекта */
	private LabelUI nameLabel;
	/** надпись с дистанцией */
	private LabelUI distance;
	/** изображение селектора */
	private ImageUI image;

	/** корабль игрока */
	private PlayerShip playerShip;

	/** выбранный объект */
	private T target;

	/** направление цели относительно игрока */
	private Direction direction;

	public AbstractObjectSelector() {
		this.position = new Vector3f();
	}

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties parameter, final Attributes controlDefinitionAttributes) {
		bind(element);
		this.nameLabel = element.findNiftyControl(NAME_LABEL_ID, LabelUI.class);
		this.distance = element.findNiftyControl(DIST_LABEL_ID, LabelUI.class);
		this.image = element.findNiftyControl(SELECTOR_ICON_ID, ImageUI.class);
		this.infoPanel = element.findElementByName(INFO_PANEL_ID);
	}

	/**
	 * @return направление цели относительно игрока.
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * @return надпись с дистанцией.
	 */
	public LabelUI getDistance() {
		return distance;
	}

	/**
	 * @return изображение селектора.
	 */
	public ImageUI getImage() {
		return image;
	}

	/**
	 * @return панель с информацией о цели.
	 */
	public Element getInfoPanel() {
		return infoPanel;
	}

	/**
	 * @return надпись с названием объекта.
	 */
	public LabelUI getNameLabel() {
		return nameLabel;
	}

	@Override
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return позиция индикатора на экране.
	 */
	protected Vector3f getPosition() {
		return position;
	}

	@Override
	public T getTarget() {
		return target;
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void onStartScreen() {
	}

	/**
	 * @param direction направление цели относительно игрока.
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setTarget(final SpaceObject target) {
		this.target = (T) target;

		if(target != null) {
			getNameLabel().setText(target.getName());
		}
	}

	@Override
	public void start() {
	}

	@Override
	public void update(long currentTime) {

		final T target = getTarget();
		final PlayerShip playerShip = getPlayerShip();

		if(playerShip == null || target == null) {
			return;
		}

		SpaceObjectView view = target.getView();

		final Vector3f position = getPosition();
		final Vector3f location = view.getLocation();

		Element element = getElement();

		CAMERA.getScreenCoordinates(location, position);

		int maxHeight = CAMERA.getHeight() - element.getHeight();
		int maxWidth = CAMERA.getWidth() - element.getWidth();

		Direction direction = Direction.NORMAL;

		if(position.getZ() >= 1) {

			direction = Direction.UP;
			position.setY(maxHeight);
			position.setX(maxWidth / 2);

		} else {

			if(position.getY() < 0) {

				direction = Direction.DOWN;

				if(position.getX() < 0) {
					direction = Direction.DOWN_LEFT;
					position.setX(0);
				} else if(position.getX() > maxWidth) {
					direction = Direction.DOWN_RIGHT;
					position.setX(maxWidth);
				} else {
					position.setX(maxWidth / 2);
				}

				position.setY(0);

			} else if(position.getY() > maxHeight) {

				direction = Direction.UP;

				if(position.getX() < 0) {
					direction = Direction.UP_LEFT;
					position.setX(0);
				} else if(position.getX() > maxWidth) {
					direction = Direction.UP_RIGHT;
					position.setX(maxWidth);
				} else {
					position.setX(maxWidth / 2);
				}

				position.setY(maxHeight);
			} else if(position.getX() < 0) {
				direction = Direction.LEFT;
				position.setY(maxHeight / 2);
				position.setX(0);
			} else if(position.getX() > maxWidth) {
				direction = Direction.RIGHT;
				position.setY(maxHeight / 2);
				position.setX(maxWidth);
			}
		}

		final int x = (int) (position.getX() - element.getWidth() / 2);
		final int y = (int) (CAMERA.getHeight() - position.getY() - element.getHeight() / 2);

		element.setConstraintX(GameUtil.getPixelSize(x));
		element.setConstraintY(GameUtil.getPixelSize(y));

		ElementUtils.updateLayout(element.getParent());

		if(!element.isVisible()) {
			element.show();
		}

		updateDirection(direction);

		element = getInfoPanel();

		if((getDirection() == Direction.NORMAL) != element.isVisible()) {
			element.setVisible(getDirection() == Direction.NORMAL);
		}

		String text = DISTANCE_LABEL_NAME.replace("%distance%", String.valueOf((int) playerShip.distanceTo(target)));
		getDistance().setText(text);
	}

	/**
	 * Обновление режима селектора.
	 */
	public void updateDirection(Direction direction) {

		Direction current = getDirection();
		try {
			if(current != direction) {
				getImage().setImage(direction.getImage());
			}

		} finally {
			setDirection(direction);
		}
	}
}
