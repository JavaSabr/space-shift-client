package com.ss.client.gui.controller.game.item;

import java.util.Properties;

import com.ss.client.gui.builder.game.factory.ItemElementUIFactory;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.ImageUI;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestItemPanelUpdate;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * @author Ronn
 */
public class ItemElementUIController extends DraggableElementUIImpl implements FastPanelItem {

	public static final String ID = "#item_";
	public static final String LABEL_COUNT_ID = "#label_count";
	public static final String IMAGE_ID = "#image";

	private static final Network NETWORK = Network.getInstance();

	/** предмет элемента */
	private SpaceItem item;

	/** надпись с кол-вом предметов */
	private LabelUI countLabel;
	/** изображение предмета */
	private ImageUI image;

	/** позиция на панели */
	private int index;

	@Override
	public void activate() {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.countLabel = element.findControl(LABEL_COUNT_ID, LabelUI.class);
		this.image = element.findControl(IMAGE_ID, ImageUI.class);
	}

	@Override
	public DraggableElementUI createFrom(Element parent, Screen screen, Nifty nifty) {
		ItemElementUIController control = ItemElementUIFactory.build(getItem(), parent, screen, nifty);
		control.setIndex(getIndex());
		return control;
	}

	/**
	 * @return надпись с кол-вом предметов.
	 */
	public LabelUI getCountLabel() {
		return countLabel;
	}

	/**
	 * @return изображение предмета.
	 */
	public NiftyImage getImage() {
		return image.getImage();
	}

	@Override
	public int getIndex() {
		return index;
	}

	/**
	 * @return предмет элемента.
	 */
	public SpaceItem getItem() {
		return item;
	}

	@Override
	public boolean matches(FastPanelItem item) {

		if(item.getClass() != getClass()) {
			return false;
		}

		ItemElementUIController controller = (ItemElementUIController) item;
		return getItem().getObjectId() == controller.getItem().getObjectId();
	}

	@Override
	public boolean processEvent(FastPanelItemEvent event) {

		SpaceItem item = getItem();

		if(event.getClass() == UpdateItemEvent.class) {

			UpdateItemEvent itemEvent = (UpdateItemEvent) event;

			if(item.getObjectId() == itemEvent.getObjectId()) {
				updateCount(itemEvent.getItemCount());
			}
		}

		return false;
	}

	@Override
	public void remove() {

		DroppableElementUI droppable = getDroppable();
		droppable.setDraggable(null);

		getElement().markForRemoval();
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param item предмет элемента.
	 */
	public void setItem(SpaceItem item) {
		this.item = item;
	}

	/**
	 * Обновление отображаемого кол-ва предметов.
	 * 
	 * @param count актуальное кол-во.
	 */
	public void updateCount(long count) {
		getCountLabel().setText(String.valueOf(count));
	}

	@Override
	public void updateToServer(int index) {

		SpaceItem item = getItem();

		if(item == null) {
			return;
		}

		NETWORK.sendPacketToGameServer(RequestItemPanelUpdate.getInstance(item.getObjectId(), getIndex(), index));
		setIndex(index);
	}
}
