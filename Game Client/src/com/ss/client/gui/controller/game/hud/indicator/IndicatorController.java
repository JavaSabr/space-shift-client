package com.ss.client.gui.controller.game.hud.indicator;

import java.util.Properties;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.gui.builder.game.IndicatorUIBuilder;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.ImageUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.impl.AbstractElementUI;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Контролер индикатора объекта.
 * 
 * @author Ronn
 */
public final class IndicatorController extends AbstractElementUI {

	public static final String LABEL_DISTANCE_ID = "#label_distance";

	private final ElementUIEventListener<ElementUI> imageEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {

			IndicatorController indicator = IndicatorController.this;

			event.setElement(indicator);
			indicator.notifyEvent(event);
		}
	};

	/** позиция на экране индикатора */
	private final Vector3f position;

	/** изображение индикатора */
	private ImageUI image;

	/** корабль игрока */
	private PlayerShip playerShip;
	/** индицируемый объект */
	private SpaceObject object;

	/** оригинальная ширина */
	private int originalWidth;
	/** оригинальная высота */
	private int originalHeight;

	/** является ли индекатор выбранным */
	private boolean selected;

	public IndicatorController() {
		this.position = new Vector3f();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {
		super.bind(element);

		element.hide();

		this.image = element.findNiftyControl(IndicatorsController.IMAGE_ID, ImageUI.class);
		this.image.addElementListener(PrimaryClickEvent.EVENT_TYPE, imageEventListener);

		Element imageElement = image.getElement();

		this.originalWidth = imageElement.getWidth();
		this.originalHeight = imageElement.getHeight();
	}

	/**
	 * @return изображение индикатора.
	 */
	public ImageUI getImage() {
		return image;
	}

	/**
	 * @return элемент иконки индикатора.
	 */
	public Element getImageElement() {
		return getImage().getElement();
	}

	/**
	 * @return индицируемый объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @return оригинальная высота.
	 */
	public int getOriginalHeight() {
		return originalHeight;
	}

	/**
	 * @return оригинальная ширина.
	 */
	public int getOriginalWidth() {
		return originalWidth;
	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return позиция на экране индикатора.
	 */
	public Vector3f getPosition() {
		return position;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	/**
	 * @return является ли индекатор выбранным.
	 */
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void onStartScreen() {
	}

	/**
	 * Включение режима выбора объекта.
	 */
	public void select() {
		setSelected(true);
	}

	/**
	 * @param object индицируемый объект.
	 */
	public void setObject(SpaceObject object) {
		this.object = object;
	}

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	/**
	 * @param selected является ли индекатор выбранным.
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "ObjectIndicator object = " + object;
	}

	/**
	 * Выключение режима выбора объекта.
	 */
	public void unselect() {
		setSelected(false);
	}

	/**
	 * @return изменилось ли положение.
	 */
	public boolean update(Camera camera) {

		PlayerShip playerShip = getPlayerShip();
		SpaceObject object = getObject();

		if(object == null || playerShip == null) {
			return true;
		}

		SpaceObjectView view = object.getView();
		Vector3f location = view.getLocation();

		Element element = getElement();

		if(!object.isVisible()) {

			if(isVisible()) {
				setVisible(false);
			}

			return false;
		}

		final Vector3f position = camera.getScreenCoordinates(location, getPosition());

		final int x = (int) (position.getX() - element.getWidth() / 2);
		final int y = (int) (camera.getHeight() - position.getY() - element.getHeight() / 2);

		int maxHeight = camera.getHeight() - element.getHeight();
		int maxWidth = camera.getWidth() - element.getWidth();

		boolean isBottom = y < 0;
		boolean isTop = y > maxHeight;
		boolean isLeft = x < 0;
		boolean isRight = x > maxWidth;

		boolean visible = !isBottom && !isLeft && !isTop && !isRight && position.getZ() < 1F;

		if(visible != isVisible()) {
			setVisible(visible);
		}

		if(!isVisible()) {
			return false;
		}

		boolean changePosition = false;

		if(x != element.getX() || y != element.getY()) {
			element.setConstraintX(GameUtil.getPixelSize(x));
			element.setConstraintY(GameUtil.getPixelSize(y));
			changePosition = true;
		}

		int originalWidth = getOriginalWidth();
		int originalHeight = getOriginalHeight();

		element = getImageElement();

		if(isSelected() && element.getWidth() != originalWidth) {
			element.setConstraintWidth(GameUtil.getPixelSize(originalWidth));
			element.setConstraintHeight(GameUtil.getPixelSize(originalHeight));
		} else if(!isSelected() && element.getWidth() == originalWidth) {
			element.setConstraintWidth(GameUtil.getPixelSize(originalWidth / 2));
			element.setConstraintHeight(GameUtil.getPixelSize(originalHeight / 2));
		}

		return changePosition;
	}

	/**
	 * Обновление иконки объекта.
	 */
	public void updateIcon() {

		SpaceObject object = getObject();

		if(object == null) {
			return;
		}

		String imagePath = IndicatorUIBuilder.buildImagePath(object.getFriendStatus());
		getImage().setImage(imagePath);
	}
}
