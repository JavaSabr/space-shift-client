package com.ss.client.gui.controller.game.item;

import com.ss.client.gui.builder.game.factory.ItemElementUIFactory;
import com.ss.client.gui.element.effect.impl.HintEffectUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Реализация эффекта подсказки для предмета.
 * 
 * @author Ronn
 */
public class ItemDescriptionEffect extends HintEffectUI {

	public static final String DESCRIPTION_ID = "#description";
	public static final String NAME_ID = "#name";

	@Override
	protected Element build(Nifty nifty, Screen screen, Element parent, Element activator, EffectProperties properties) {
		return ItemElementUIFactory.build(nifty, screen, parent, activator, properties);
	}
}