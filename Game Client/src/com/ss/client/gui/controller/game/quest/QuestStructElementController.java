package com.ss.client.gui.controller.game.quest;

import com.ss.client.gui.element.PanelUI;

/**
 * Интерфейс для реализации контроллера структурного элемента задания.
 * 
 * @author Ronn
 */
public interface QuestStructElementController extends PanelUI {

	/**
	 * Актуализация отображаемых данных.
	 */
	public void refresh();
}
