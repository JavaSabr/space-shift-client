package com.ss.client.gui.controller.game.skill;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.event.impl.AbstractGameUIEvent;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;

/**
 * Событие о статусе отката умения.
 * 
 * @author Ronn
 */
public class SkillElementReuseEvent extends AbstractGameUIEvent implements FastPanelItemEvent {

	private static final ThreadLocal<SkillElementReuseEvent> LOCAL_EVENT = new ThreadLocal<SkillElementReuseEvent>() {

		@Override
		protected SkillElementReuseEvent initialValue() {
			return new SkillElementReuseEvent();
		}
	};

	public static final SkillElementReuseEvent get(int objectId, int templateId, int reuseTime, long startReuseTime) {

		SkillElementReuseEvent event = LOCAL_EVENT.get();
		event.objectId = objectId;
		event.reuseTime = reuseTime;
		event.startReuseTime = startReuseTime;
		event.templateId = templateId;

		return event;
	}

	/** дата старта отката */
	private long startReuseTime;

	/** уникальный ид умения */
	private int objectId;
	/** ид шаблона умения */
	private int templateId;
	/** время отката умения */
	private int reuseTime;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return уникальный ид умения.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return время отката умения.
	 */
	public int getReuseTime() {
		return reuseTime;
	}

	/**
	 * @return дата старта отката.
	 */
	public long getStartReuseTime() {
		return startReuseTime;
	}

	/**
	 * @return ид шаблона умения.
	 */
	public int getTemplateId() {
		return templateId;
	}
}
