package com.ss.client.gui.controller.game.hud.selector;

import com.ss.client.gui.controller.game.hud.event.HudEvent;
import com.ss.client.model.SpaceObject;

/**
 * Событие о выборе объекта.
 * 
 * @author Ronn
 */
public class SelectedEvent implements HudEvent {

	private static final ThreadLocal<SelectedEvent> LOCAL_EVENT = new ThreadLocal<SelectedEvent>() {

		@Override
		protected SelectedEvent initialValue() {
			return new SelectedEvent();
		};
	};

	/**
	 * @return локальный ивент.
	 */
	public static final SelectedEvent get() {
		return LOCAL_EVENT.get();
	}

	/** выбранный объект */
	private SpaceObject object;

	/**
	 * @return выбранный объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @param object выбранный объект.
	 */
	public void setObject(SpaceObject object) {
		this.object = object;
	}
}
