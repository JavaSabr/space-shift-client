package com.ss.client.gui.controller.game.window.storage;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Информация о статусе ячейки.
 * 
 * @author Ronn
 */
public class CellInfo implements Foldable {

	private static final FoldablePool<CellInfo> pool = Pools.newConcurrentFoldablePool(CellInfo.class);

	public static CellInfo newInstance() {

		CellInfo info = pool.take();

		if(info == null) {
			info = new CellInfo();
		}

		return info;
	}

	/** уникальный ид предмета */
	private long objectId;
	/** кол-во предметов */
	private long itemCount;

	/** ид шаблона предмета */
	private int templateId;
	/** индекс ячейки */
	private int index;

	@Override
	public void finalyze() {
		objectId = 0;
	}

	public void fold() {
		pool.put(this);
	}

	/**
	 * @return индекс ячейки.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return кол-во предметов.
	 */
	public long getItemCount() {
		return itemCount;
	}

	/**
	 * @return уникальный ид предмета.
	 */
	public long getObjectId() {
		return objectId;
	}

	/**
	 * @return ид шаблона предмета.
	 */
	public int getTemplateId() {
		return templateId;
	}

	@Override
	public void reinit() {
	}

	public void set(CellInfo info) {
		setIndex(info.getIndex());
		setItemCount(info.getItemCount());
		setObjectId(info.getObjectId());
		setTemplateId(info.getTemplateId());
	}

	/**
	 * @param index индекс ячейки.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param itemCount
	 */
	public void setItemCount(long itemCount) {
		this.itemCount = itemCount;
	}

	/**
	 * @param objectId уникальный ид предмета.
	 */
	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param templateId ид шаблона предмета.
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}
}