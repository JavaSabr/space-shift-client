package com.ss.client.gui.controller.game.hud.indicator;

import com.ss.client.gui.controller.game.hud.event.HudEvent;
import com.ss.client.model.SpaceObject;

/**
 * Событие о выборе индикатора.
 * 
 * @author Ronn
 */
public class SelectIndicatorEvent implements HudEvent {

	private static final ThreadLocal<SelectIndicatorEvent> LOCAL_EVENT = new ThreadLocal<SelectIndicatorEvent>() {

		@Override
		protected SelectIndicatorEvent initialValue() {
			return new SelectIndicatorEvent();
		};
	};

	/**
	 * @return локальный ивент.
	 */
	public static final SelectIndicatorEvent get() {
		return LOCAL_EVENT.get();
	}

	/** выбранный объект */
	private SpaceObject object;

	/**
	 * @return выбранный объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @param object выбранный объект.
	 */
	public void setObject(SpaceObject object) {
		this.object = object;
	}
}
