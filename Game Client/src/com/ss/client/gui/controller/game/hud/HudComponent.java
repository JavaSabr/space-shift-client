package com.ss.client.gui.controller.game.hud;

import com.jme3.input.InputManager;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.player.PlayerShip;

/**
 * Интерфейс для реализации компонента HUD.
 * 
 * @author Ronn
 */
public interface HudComponent extends ElementUI {

	/**
	 * Завершение работы компонента.
	 */
	public void finish();

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip();

	/**
	 * Регистрация контроллера у компонента.
	 * 
	 * @param controller контроллер компонентов.
	 */
	public void register(HudController controller);

	/**
	 * Регистрация прослушки ввода.
	 * 
	 * @param inputManager менеджер ввода.
	 */
	public void register(final InputManager inputManager);

	/**
	 * Уведомление о смене режима ввода.
	 * 
	 * @param inputMode новый режим ввода.
	 */
	public void setInputMode(InputMode inputMode);

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(PlayerShip playerShip);
}
