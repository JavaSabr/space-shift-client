package com.ss.client.gui.controller.game.event.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.model.SkillPanelInfo;

/**
 * Событие о обновлении позиций умений на панели.
 * 
 * @author Ronn
 */
public class UpdateSkillPanelEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateSkillPanelEvent> LOCAL_EVENT = new ThreadLocal<UpdateSkillPanelEvent>() {

		@Override
		protected UpdateSkillPanelEvent initialValue() {
			return new UpdateSkillPanelEvent();
		}
	};

	public static final UpdateSkillPanelEvent get(Array<SkillPanelInfo> skillPanelInfo) {

		UpdateSkillPanelEvent event = LOCAL_EVENT.get();
		event.skillPanelInfo.clear();
		event.skillPanelInfo.addAll(skillPanelInfo);

		return event;
	}

	/** список размещений умений на панели */
	private final Array<SkillPanelInfo> skillPanelInfo;

	public UpdateSkillPanelEvent() {
		this.skillPanelInfo = Arrays.toArray(SkillPanelInfo.class);
	}

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return список размещений умений на панели.
	 */
	public Array<SkillPanelInfo> getSkillPanelInfo() {
		return skillPanelInfo;
	}
}
