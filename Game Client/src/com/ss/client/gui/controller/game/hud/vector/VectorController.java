package com.ss.client.gui.controller.game.hud.vector;

import java.util.Properties;

import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.GameConfig;
import com.ss.client.gui.controller.camera.CameraController;
import com.ss.client.gui.controller.game.hud.AbstractHudComponent;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.KeySetting;
import com.ss.client.gui.model.ScreenSize;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.packet.client.RequestShipRotate;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Модель управления вектором полета.
 * 
 * @author Ronn
 */
public class VectorController extends AbstractHudComponent implements ActionListener, AnalogListener {

	public static final String BASE_PANEL_ID = "#vector_panel";

	public static final String VECTOR_ID = "#vector";
	public static final String VECTOR_ICON_ID = "#vector_icon";
	public static final String VECTOR_ICON = "game/hud/vector.png";
	public static final String VECTOR_CENTER_ID = "#center";
	public static final String VECTOR_CENTER_ICON_ID = "#center_icon";
	public static final String VECTOR_CENTER_ICON = "game/hud/orientation.png";

	public static final String VECTOR_CENTER_WIDTH = "46px";
	public static final String VECTOR_CENTER_HEIGHT = "46px";

	/** период отправки пакета 300 мс */
	private static final int SENDER_PERIOD = 300;

	/** целевое направление */
	private final Quaternion targetRotation;
	/** отправленное направление */
	private final Quaternion sendRotation;
	/** промежуточный рассчет целевого направления */
	private final Quaternion bufferRotation;

	/** матрица для рассчета */
	private final Matrix3f matrix;

	/** центр экрана */
	private final Vector2f screenCenter;
	/** текущая позиция вектора */
	private final Vector2f currentPos;
	/** новая позиция вектора */
	private final Vector2f updatePos;
	/** предыдущая позиция вектора на экране */
	private final Vector2f lastPos;

	/** индикатор направления корабля */
	private Element indicatorElement;
	/** центральный элемент */
	private Element centerElement;

	/** время последнего изменения */
	private long lastSendTime;

	/** модификатор скорости разворота */
	private float modiff;

	public VectorController() {
		this.matrix = new Matrix3f();
		this.targetRotation = new Quaternion();
		this.sendRotation = new Quaternion();
		this.bufferRotation = new Quaternion();
		this.screenCenter = new Vector2f(CAMERA.getWidth() / 2, CAMERA.getHeight() / 2);
		this.currentPos = new Vector2f();
		this.updatePos = new Vector2f();
		this.lastPos = new Vector2f();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {
		super.bind(nifty, screen, element, parameter, controlDefinitionAttributes);

		this.indicatorElement = element.findElementByName(VECTOR_ID);
		this.centerElement = element.findElementByName(VECTOR_CENTER_ID);

		int resultX = (int) (CAMERA.getWidth() / 2 - indicatorElement.getWidth() * 0.5F);
		int resultY = (int) (CAMERA.getHeight() / 2 - indicatorElement.getHeight() * 0.5F);

		ElementUtils.moveElement(indicatorElement, resultX, resultY);

		UPDATE_UI_MANAGER.addElement(this);
	}

	/**
	 * Поворот вектора движения.
	 * 
	 * @param value сила поворота.
	 * @param axis вектор поворота.
	 * @param up вектор вверх.
	 * @param left вектор вбок.
	 * @param direction вектор направления.
	 */
	protected void changeFly(final float value, final Vector3f axis, final Vector3f up, final Vector3f left, final Vector3f direction, Vector3f temp, Vector3f result) {

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(new Exception("not found player ship."));
			return;
		}

		Matrix3f matrix = getMatrix();
		matrix.fromAngleNormalAxis(1 * value, axis);
		matrix.mult(up, up);
		matrix.mult(left, left);
		matrix.mult(direction, direction);

		Quaternion buffer = getBufferRotation();
		buffer.fromAxes(left, up, direction);
		buffer.normalizeLocal();
		buffer.getRotationColumn(2, temp);

		temp.multLocal(300);
		temp.addLocal(playerShip.getLocation());

		final Camera camera = GAME.getCamera();
		camera.getScreenCoordinates(temp, result);

		ScreenSize size = GameConfig.SCREEN_SIZE;

		if(result.getX() < 0 || result.getY() < 0 || result.getX() > size.getWidth() || result.getY() > size.getHeight()) {
			return;
		}

		setTargetRotation(buffer);
	}

	/**
	 * @return буферный разворот.
	 */
	public Quaternion getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return центральный элементю
	 */
	public Element getCenterElement() {
		return centerElement;
	}

	/**
	 * @return текущая позиция вектора.
	 */
	public Vector2f getCurrentPos() {
		return currentPos;
	}

	/**
	 * @return индикатор направления.
	 */
	public Element getIndicatorElement() {
		return indicatorElement;
	}

	/**
	 * @return предыдущая позиция вектора на экране.
	 */
	public Vector2f getLastPos() {
		return lastPos;
	}

	/**
	 * @return lastSend время последней отправки.
	 */
	public final long getLastSendTime() {
		return lastSendTime;
	}

	/**
	 * @return матрица для рассчета.
	 */
	public Matrix3f getMatrix() {
		return matrix;
	}

	/**
	 * @return модификатор скорости разворота.
	 */
	public float getModiff() {
		return modiff;
	}

	/**
	 * Получение текущей позиции элемента.
	 * 
	 * @param element интреcуемый элемент.
	 * @param store вектор для хранения результата.
	 * @return вектор положения элемента.
	 */
	private Vector2f getPositionFor(Element element, Vector2f store) {

		store.setX(element.getX() + element.getWidth() * 0.5F);
		store.setY(CAMERA.getHeight() - element.getY() - element.getHeight() / 2);

		return store;
	}

	/**
	 * @return центр экрана.
	 */
	public Vector2f getScreenCenter() {
		return screenCenter;
	}

	/**
	 * @return отправленный разворот.
	 */
	public Quaternion getSendRotation() {
		return sendRotation;
	}

	/**
	 * @return целевое направление.
	 */
	public Quaternion getTargetRotation() {
		return targetRotation;
	}

	/**
	 * @return новая позиция вектора.
	 */
	public Vector2f getUpdatePos() {
		return updatePos;
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	@Override
	public void onAction(final String name, final boolean isPressed, final float tpf) {

		CameraController controller = CameraController.getInstance();

		if(getInputMode() == InputMode.INTERFACE_MODE || controller.isRotationActive()) {
			return;
		}

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip == null || !playerShip.isActiveEngine()) {
			return;
		}

		updateToServer(playerShip.getRotation(), getTargetRotation(), !isPressed);
	}

	@Override
	public void onAnalog(final String name, final float value, final float tpf) {

		CameraController controller = CameraController.getInstance();

		if(getInputMode() == InputMode.INTERFACE_MODE || controller.isRotationActive()) {
			return;
		}

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return;
		}

		LocalObjects local = LocalObjects.get();
		Element indicator = getIndicatorElement();

		int x = indicator.getX();
		int y = indicator.getY();
		int added = Math.max((int) (value * 1000F), 1);

		if(name == KeySetting.FLY_UP.name()) {
			y = Math.min(Math.max(y - added, 0), CAMERA.getHeight());
		} else if(name == KeySetting.FLY_DOWN.name()) {
			y = Math.min(Math.max(y + added, 0), CAMERA.getHeight());
		}

		if(name == KeySetting.FLY_LEFT.name()) {
			x = Math.min(Math.max(x - added, 0), CAMERA.getWidth());
		} else if(name == KeySetting.FLY_RIGHT.name()) {
			x = Math.min(Math.max(x + added, 0), CAMERA.getWidth());
		}

		Camera camera = GAME.getCamera();

		Vector2f screenCenter = getScreenCenter();
		Vector2f currentPos = getPositionFor(indicator, getCurrentPos());

		float maxDistance = Math.min(camera.getWidth(), camera.getHeight()) / 2F;

		setModiff(Math.max(Math.min(currentPos.distance(screenCenter) / maxDistance, 1F), 0F));

		if(x == indicator.getX() && y == indicator.getY()) {
			return;
		}

		ElementUtils.moveElement(indicator, x, y);

		getPositionFor(indicator, playerShip.getVectorRotation());
		updateRotation(local);
	}

	@Override
	public void register(InputManager inputManager) {
		super.register(inputManager);

		inputManager.addListener(this, KeySetting.FLY_DOWN.name());
		inputManager.addListener(this, KeySetting.FLY_UP.name());
		inputManager.addListener(this, KeySetting.FLY_LEFT.name());
		inputManager.addListener(this, KeySetting.FLY_RIGHT.name());
	}

	/**
	 * Отправка на сервер целевой разворот.
	 * 
	 * @param curent текущий разворот.
	 * @param target целевой разворот.
	 * @param modiff модификатор скорости разворота.
	 */
	public void requestRotation(final Quaternion curent, final Quaternion target, float modiff) {
		getSendRotation().set(target);
		NETWORK.sendPacketToGameServer(RequestShipRotate.getInstance(curent, target, Game.getCurrentTime(), modiff));
	}

	/**
	 * @param lastPos предыдущая позиция вектора на экране.
	 */
	public void setLastPos(Vector2f lastPos) {
		this.lastPos.set(lastPos);
	}

	/**
	 * @param lastSendTime время последней отправки.
	 */
	public final void setLastSendTime(final long lastSendTime) {
		this.lastSendTime = lastSendTime;
	}

	/**
	 * @param modiff модификатор скорости разворота.
	 */
	public void setModiff(float modiff) {
		this.modiff = modiff;
	}

	/**
	 * Устанавливаем корабль-владелец, от которого будет идти расчет вектора
	 * направления.
	 * 
	 * @param playerShip владелец направления.
	 */
	@Override
	public void setPlayerShip(final PlayerShip playerShip) {
		super.setPlayerShip(playerShip);

		if(playerShip != null) {
			setTargetRotation(playerShip.getRotation());
		}
	}

	/**
	 * @param rotation целевое направление.
	 */
	public void setTargetRotation(Quaternion rotation) {
		this.targetRotation.set(rotation);
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {
		updateRotation(local);
		return false;
	}

	/**
	 * Обновление разворота.
	 */
	public void updateRotation(LocalObjects local) {

		Element indicator = getIndicatorElement();
		PlayerShip playerShip = getPlayerShip();

		if(indicator == null || playerShip == null || !playerShip.isActiveEngine()) {
			return;
		}

		Quaternion currentRotation = playerShip.getRotation();

		Vector2f currentPos = getPositionFor(indicator, getCurrentPos());
		Vector2f lastPos = getLastPos();

		if(getModiff() < 0.01F) {
			updateToServer(currentRotation, currentRotation, false);
		} else if(!lastPos.equals(currentPos)) {

			Quaternion targetRotation = GameUtil.getTargetRotation(playerShip, CAMERA, local, (int) currentPos.getX(), (int) currentPos.getY());
			setTargetRotation(targetRotation);

			updateToServer(currentRotation, targetRotation, false);
			setLastPos(currentPos);
		}
	}

	/**
	 * Обновление целевого разворота на сервере.
	 */
	public void updateToServer(final Quaternion current, final Quaternion target, boolean force) {

		Quaternion sendRotation = getSendRotation();

		if(sendRotation.equals(target)) {
			return;
		}

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return;
		}

		final long currentTime = System.currentTimeMillis();

		if(force || currentTime - getLastSendTime() > SENDER_PERIOD) {

			float modiff = getModiff();

			requestRotation(current, target, modiff);
			setLastSendTime(currentTime);

			playerShip.rotate(target, modiff);
		}
	}
}
