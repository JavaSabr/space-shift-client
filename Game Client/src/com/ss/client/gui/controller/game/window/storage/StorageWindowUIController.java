package com.ss.client.gui.controller.game.window.storage;

import java.util.Comparator;
import java.util.Properties;

import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.ss.client.gui.builder.game.factory.ItemElementUIFactory;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateFastPanelCellEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateItemPanelEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateStorageCellEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateStorageEvent;
import com.ss.client.gui.controller.game.item.ItemElementUIController;
import com.ss.client.gui.controller.game.item.UpdateItemEvent;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;
import com.ss.client.gui.element.ButtonUI;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.DroppableDropedEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.filter.DroppableDropFilter;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.ItemPanelInfo;
import com.ss.client.model.item.ItemUtils;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestItemPanel;
import com.ss.client.network.packet.client.RequestMoveStorageItem;
import com.ss.client.network.packet.client.RequestStorageInfo;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера окна хранилища.
 * 
 * @author Ronn
 */
public class StorageWindowUIController extends DraggableElementUIImpl implements DroppableDropFilter, GameUIComponent {

	private static final Network NETWORK = Network.getInstance();

	public static final String CLOSE_BUTTON_IMAGE = "ui/game/storage_window/close_button.png";
	public static final String WINDOW_CELL_IMAGE = "ui/game/storage_window/cell.png";
	public static final String WINDOW_BACKGROUND_IMAGE = "ui/game/storage_window/background.png";
	public static final String WINDOW_LINE_IMAGE = "ui/game/storage_window/line.png";
	public static final String SORT_BUTTON_IMAGE = "ui/game/storage_window/sort_button.png";
	public static final String DESTROY_AREA_IMAGE = "ui/game/storage_window/destroy_area.png";

	public static final String DESTROYED_LABEL_ID = "#destroyed_label";
	public static final String LINE_ID = "#line";
	public static final String WINDOW_CONTENT_ID = "#content";
	public static final String WINDOW_ID = "#storage_window";
	public static final String WINDOW_STORAGE_COLUMN_ID = "#column_";
	public static final String WINDOW_HEADER_ID = "#header";
	public static final String WINDOW_BUTTON_CLOSE_ID = "#close";
	public static final String WINDOW_TITLE_ID = "#title";
	public static final String WINDOW_ITEM_TABLE_PANEL_ID = "#item_table_panel";
	public static final String WINDOW_INFO_PANEL_ID = "#info_panel";
	public static final String WINDOW_SORT_BUTTON_ID = "#sort_button";
	public static final String WINDOW_DESTROYED_PANEL_ID = "#destroyed_panel";
	public static final String WINDOW_CELL_ID = "#cell_";

	public static final Color TITLE_TEXT_COLOR = Color.WHITE;

	private static final Comparator<Element> CELL_COMPARATOR = new ArrayComparator<Element>() {

		@Override
		protected int compareImpl(Element first, Element second) {

			String firstId = first.getId();
			String secondId = second.getId();

			int firstValue = Integer.parseInt(firstId.replace(WINDOW_CELL_ID, Strings.EMPTY));
			int secondValue = Integer.parseInt(secondId.replace(WINDOW_CELL_ID, Strings.EMPTY));

			return Integer.compare(firstValue, secondValue);
		}
	};

	/** слушатель обновлений хранилища */
	private final GameUIEventListener updateStorageEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			UpdateStorageEvent storageEvent = (UpdateStorageEvent) event;
			updateStorage(storageEvent.getContainer());
		}
	};

	/** слушатель обновлений ячеяк хранилища */
	private final GameUIEventListener updateStorageCellEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			UpdateStorageCellEvent cellEvent = (UpdateStorageCellEvent) event;
			update(cellEvent.getInfo());
		}
	};

	/** слушатель смены ячейки предмета */
	private final ElementUIEventListener<ItemCellUIController> cellDropEventListener = new ElementUIEventListener<ItemCellUIController>() {

		@Override
		public void notifyEvent(ElementUIEvent<ItemCellUIController> event) {
			DroppableDropedEvent<ItemCellUIController> dropedEvent = (DroppableDropedEvent<ItemCellUIController>) event;
			processMove((ItemCellUIController) dropedEvent.getSource(), (ItemElementUIController) dropedEvent.getDraggable(), dropedEvent.getElement());
		}
	};

	/** слушатель нажатий на сортировку предметов */
	private final ElementUIEventListener<ButtonUI> sortItemsEventListener = new ElementUIEventListener<ButtonUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ButtonUI> event) {
			sortItems();
		}
	};

	/** обработчик уничтожения предметов */
	private final DroppableDropFilter destroyItemHandler = new DroppableDropFilter() {

		@Override
		public boolean accept(DroppableElementUI source, DraggableElementUI draggable, DroppableElementUI target) {

			if(draggable instanceof ItemElementUIController) {
				requestDestroyItems((ItemElementUIController) draggable);
			}

			return false;
		}
	};

	/** слушатель событий игрового УИ */
	private final GameUIEventListener eventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			processEvent((UpdateItemPanelEvent) event);
		}
	};

	/** слушатель событий элементов панели быстрого доступа */
	private final GameUIEventListener fastPanelItemEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			// TODO Auto-generated method stub

		}
	};

	/** слушатель нажатий на кнопку закрытия */
	private final ElementUIEventListener<ElementUI> closeEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {
			setVisible(false);
		}
	};

	/** контролер UI */
	private Nifty nifty;
	/** текущий экран UI */
	private Screen screen;

	/** список ячеяк хранилища */
	private ItemCellUIController[] cells;

	public StorageWindowUIController() {
	}

	@Override
	public boolean accept(DroppableElementUI source, DraggableElementUI draggable, DroppableElementUI target) {

		if(!(source instanceof ItemCellUIController)) {
			return false;
		}

		DraggableElementUI current = target.getDraggable();

		if(current != null) {
			source.drop(current, null, false);
		}

		return true;
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		// подготовка списка ячеяк
		{
			final Array<Element> cells = Arrays.toArray(Element.class);

			// формируем список всех ячейках
			ElementUtils.addElements(cells, element, WINDOW_CELL_ID);

			cells.sort(CELL_COMPARATOR);

			Array<ItemCellUIController> controls = Arrays.toArray(ItemCellUIController.class);

			for(Element cell : cells) {

				ItemCellUIController control = cell.getControl(ItemCellUIController.class);
				control.addElementListener(DroppableDropedEvent.EVENT_TYPE, getCellDropEventListener());
				control.addFilter(this);
				controls.add(control);
			}

			this.cells = controls.toArray(new ItemCellUIController[controls.size()]);
		}

		DroppableElementUI destroyItems = element.findControl(WINDOW_DESTROYED_PANEL_ID, DroppableElementUI.class);
		destroyItems.addFilter(getDestroyItemHandler());

		ButtonUI sortItems = element.findControl(WINDOW_SORT_BUTTON_ID, ButtonUI.class);
		sortItems.removeElementListener(PrimaryClickEvent.EVENT_TYPE, getSortItemsEventListener());

		ElementUI close = element.findControl(WINDOW_BUTTON_CLOSE_ID, ElementUI.class);
		close.addElementListener(PrimaryClickEvent.EVENT_TYPE, getCloseEventListener());

		this.nifty = nifty;
		this.screen = screen;
	}

	/**
	 * @return слушатель нажатий на кнопку закрытия.
	 */
	public ElementUIEventListener<ElementUI> getCloseEventListener() {
		return closeEventListener;
	}

	/**
	 * Очистка ячейки.
	 * 
	 * @param cell очищаемая ячейка хранилища.
	 */
	public void clear(ItemCellUIController cell) {

		DraggableElementUI draggable = cell.getDraggable();
		cell.setDraggable(null);

		if(draggable == null) {
			return;
		}

		Element element = draggable.getElement();
		element.markForRemoval();
	}

	/**
	 * Поиск соотвествующего предмета.
	 */
	public FastPanelItem findItem(ItemPanelInfo info) {

		for(ItemCellUIController cell : getCells()) {

			ItemElementUIController controller = (ItemElementUIController) cell.getDraggable();

			if(controller == null) {
				continue;
			}

			SpaceItem item = controller.getItem();

			if(item.getObjectId() != info.getObjectId()) {
				continue;
			}

			return controller;
		}

		return null;
	}

	@Override
	public void finish() {
	}

	/**
	 * Получение ячейки указанного индекса.
	 * 
	 * @param index индекс ячейки.
	 * @return ячейка.
	 */
	public ItemCellUIController getCell(int index) {
		return getCells()[index];
	}

	/**
	 * @return слушатель смены ячейки предмета.
	 */
	public ElementUIEventListener<ItemCellUIController> getCellDropEventListener() {
		return cellDropEventListener;
	}

	/**
	 * @return список ячеяк хранилища.
	 */
	public ItemCellUIController[] getCells() {
		return cells;
	}

	/**
	 * @return обработчик уничтожения предметов.
	 */
	public DroppableDropFilter getDestroyItemHandler() {
		return destroyItemHandler;
	}

	/**
	 * @return слушатель событий игрового УИ.
	 */
	public GameUIEventListener getEventListener() {
		return eventListener;
	}

	/**
	 * @return слушатель событий элементов панели быстрого доступа.
	 */
	public GameUIEventListener getFastPanelItemEventListener() {
		return fastPanelItemEventListener;
	}

	/**
	 * @return контролер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return текущий экран UI.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return слушатель нажатий на сортировку предметов.
	 */
	public ElementUIEventListener<ButtonUI> getSortItemsEventListener() {
		return sortItemsEventListener;
	}

	/**
	 * @return слушатель обновлений ячеяк хранилища.
	 */
	public GameUIEventListener getUpdateStorageCellEventListener() {
		return updateStorageCellEventListener;
	}

	/**
	 * @return слушатель обновлений хранилища.
	 */
	public GameUIEventListener getUpdateStorageEventListener() {
		return updateStorageEventListener;
	}

	/**
	 * Определение индекса ячейки.
	 * 
	 * @param cell ячейка.
	 * @return индекс ячейки.
	 */
	public int indexOf(ItemCellUIController cell) {
		return Arrays.indexOf(getCells(), cell);
	}

	@Override
	public void onStartScreen() {
		super.onStartScreen();

		NETWORK.sendPacketToGameServer(RequestStorageInfo.getInstance());
		NETWORK.sendPacketToGameServer(RequestItemPanel.getInstance());
	}

	private void processEvent(UpdateItemPanelEvent itemPanelEvent) {

		Array<ItemPanelInfo> itemPanelInfo = itemPanelEvent.getItemPanelInfo();

		if(itemPanelInfo.isEmpty()) {
			return;
		}

		UpdateFastPanelCellEvent event = UpdateFastPanelCellEvent.get();
		GameUIController controller = GameUIController.getInstance();

		for(ItemPanelInfo info : itemPanelInfo.array()) {

			if(info == null) {
				break;
			}

			FastPanelItem item = findItem(info);

			if(item == null) {
				continue;
			}

			event.setItem(item);
			event.setOrder(info.getOrder());

			controller.notifyEvent(event);
		}
	}

	/**
	 * Обработка перемещения предмета в другую ячейку.
	 * 
	 * @param source исходная ячейка.
	 * @param element перемещаемый предмет.
	 * @param target целевая ячейка.
	 */
	public void processMove(ItemCellUIController source, ItemElementUIController element, ItemCellUIController target) {

		int startIndex = indexOf(source);
		int endIndex = indexOf(target);

		if(startIndex == -1 || endIndex == -1) {
			return;
		}

		NETWORK.sendPacketToGameServer(RequestMoveStorageItem.getInstance(startIndex, endIndex));
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(UpdateStorageEvent.EVENT_TYPE, getUpdateStorageEventListener());
		controller.registerListener(UpdateStorageCellEvent.EVENT_TYPE, getUpdateStorageCellEventListener());
		controller.registerListener(FastPanelItemEvent.EVENT_TYPE, getFastPanelItemEventListener());
		controller.registerListener(UpdateItemPanelEvent.EVENT_TYPE, getEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
	}

	private void requestDestroyItems(ItemElementUIController itemElement) {
		// TODO
	}

	@Override
	public void setInputMode(InputMode inputMode) {

		if(inputMode == InputMode.CONTROL_MODE) {
			setVisible(false);
		}
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	private void sortItems() {
		// TODO
	}

	/**
	 * Обновление ячейки хранилища.
	 * 
	 * @param info информация о состоянии ячейки.
	 */
	public void update(CellInfo info) {

		int index = info.getIndex();

		ItemCellUIController cell = getCell(index);
		ItemElementUIController itemElement = (ItemElementUIController) cell.getDraggable();

		if(itemElement != null) {

			SpaceItem item = itemElement.getItem();

			if(item != null) {

				UpdateItemEvent itemEvent = UpdateItemEvent.get(item.getObjectId(), info.getItemCount());

				GameUIController controller = GameUIController.getInstance();
				controller.notifyEvent(itemEvent);

				if(item.getObjectId() == info.getObjectId()) {

					long itemCount = info.getItemCount();

					if(item.getItemCount() != itemCount) {
						item.setItemCount(itemCount);
						itemElement.updateCount(itemCount);
					}

					return;
				}
			}

			clear(cell);
		}

		// если ячейка пуста
		if(info.getObjectId() == 0) {
			return;
		}

		SpaceItem item = ItemUtils.createItem(info.getTemplateId(), info.getObjectId(), info.getItemCount());

		if(item == null) {
			return;
		}

		ItemElementUIController element = ItemElementUIFactory.build(item, cell.getElement(), getScreen(), getNifty());
		element.setDroppable(cell);

		cell.setDraggable(element);
	}

	/**
	 * Полное обновление хранилища.
	 * 
	 * @param container контейнер информации об ячеяк.
	 */
	public void updateStorage(Array<CellInfo> container) {

		ItemCellUIController[] cells = getCells();

		for(ItemCellUIController cell : cells) {
			clear(cell);
		}

		for(CellInfo info : container.array()) {

			if(info == null) {
				break;
			}

			update(info);
		}
	}
}
