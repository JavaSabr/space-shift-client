package com.ss.client.gui.controller.game.hud.indicator;

import com.ss.client.gui.controller.game.hud.event.HudEvent;
import com.ss.client.model.SpaceObject;

/**
 * Событие о выборе смене статус дрожелюбьности объекта.
 * 
 * @author Ronn
 */
public class FriendStatusChangedEvent implements HudEvent {

	private static final ThreadLocal<FriendStatusChangedEvent> LOCAL_EVENT = new ThreadLocal<FriendStatusChangedEvent>() {

		@Override
		protected FriendStatusChangedEvent initialValue() {
			return new FriendStatusChangedEvent();
		};
	};

	/**
	 * @return локальный ивент.
	 */
	public static final FriendStatusChangedEvent get() {
		return LOCAL_EVENT.get();
	}

	/** выбранный объект */
	private SpaceObject object;

	/**
	 * @return выбранный объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @param object выбранный объект.
	 */
	public void setObject(SpaceObject object) {
		this.object = object;
	}
}
