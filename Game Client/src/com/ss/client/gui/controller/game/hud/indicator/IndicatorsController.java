package com.ss.client.gui.controller.game.hud.indicator;

import static com.ss.client.gui.builder.game.IndicatorUIBuilder.buildIndicator;

import java.util.concurrent.atomic.AtomicInteger;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.LongKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.controller.game.hud.AbstractHudComponent;
import com.ss.client.gui.controller.game.hud.HudController;
import com.ss.client.gui.controller.game.hud.event.HudEventListener;
import com.ss.client.gui.controller.game.hud.selector.SelectedEvent;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.listeners.AddRemoveObjectListener;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.elements.Element;

/**
 * Реализация контролера индикаторов объектов вокруг корабля игрока.
 * 
 * @author Ronn
 */
public class IndicatorsController extends AbstractHudComponent implements AddRemoveObjectListener {

	public static final String SELECT_INDICATOR_METHOD_NAME = "selectIndicator(%param%)";

	public static final String BASE_PANEL_ID = "#indicator_panel";
	public static final String OBJECT_INDICATOR_ID = "#object_indicator_";
	public static final String IMAGE_ID = "#image";

	public static final String INDICATOR_WIDTH = "81px";
	public static final String INDICATOR_HEIGHT = "81px";

	private static final Game GAME = Game.getInstance();
	private static final Camera CAMERA = GAME.getCamera();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** слушатель выбора объекта */
	private final HudEventListener<SelectedEvent> selectObjectListener = new HudEventListener<SelectedEvent>() {

		@Override
		public void notify(SelectedEvent event) {
			doSelect(event.getObject());
		}
	};

	/** слушатель смены статуса дружелюбности объекта */
	private final HudEventListener<FriendStatusChangedEvent> friendStatusChangedListener = new HudEventListener<FriendStatusChangedEvent>() {

		@Override
		public void notify(FriendStatusChangedEvent event) {
			updateFriendStatus(event.getObject());
		}
	};

	/** слушатель событий индикатора */
	private final ElementUIEventListener<IndicatorController> indicatorEventListener = new ElementUIEventListener<IndicatorController>() {

		@Override
		public void notifyEvent(ElementUIEvent<IndicatorController> event) {

			if(event.getEventType() == PrimaryClickEvent.EVENT_TYPE) {
				selectIndicator(event.getElement());
			}
		}
	};

	/** таблица индикаторов объектв */
	private final Table<LongKey, IndicatorController> indicatorTable;

	/** список индикаторов на обновление */
	private final Array<IndicatorController> updated;

	/** индекс следующего индикатора */
	private final AtomicInteger indicatorIndex;

	public IndicatorsController() {
		this.indicatorTable = Tables.newLongTable();
		this.indicatorIndex = new AtomicInteger();
		this.updated = Arrays.toArray(IndicatorController.class);
	}

	@Override
	public void addObject(SpaceObject object) {

		if(object == getPlayerShip() || !object.isNeedIndicator()) {
			return;
		}

		Table<LongKey, IndicatorController> indicatorTable = getIndicatorTable();

		if(indicatorTable.containsKey(object.getObjectId())) {
			return;
		}

		AtomicInteger indicatorIndex = getIndicatorIndex();

		final Element element = buildIndicator(getNifty(), getScreen(), getBasePanel(), object, indicatorIndex.incrementAndGet());
		element.hide();

		IndicatorController indicator = element.getNiftyControl(IndicatorController.class);
		indicator.addElementListener(PrimaryClickEvent.EVENT_TYPE, getIndicatorEventListener());
		indicator.setPlayerShip(getPlayerShip());
		indicator.setObject(object);

		indicatorTable.put(object.getObjectId(), indicator);
	}

	/**
	 * Обработка выделения индикатора выделенного объекта.
	 * 
	 * @param object выделенный объект.
	 */
	private void doSelect(SpaceObject object) {

		Table<LongKey, IndicatorController> indicators = getIndicatorTable();

		for(IndicatorController indicator : indicators) {
			if(indicator.isSelected()) {
				indicator.unselect();
			}
		}

		IndicatorController indicator = indicators.get(object.getObjectId());

		if(indicator == null) {
			return;
		}

		if(!indicator.isSelected()) {
			indicator.select();
		}
	}

	/**
	 * @return слушатель смены статуса дружелюбности объекта.
	 */
	public HudEventListener<FriendStatusChangedEvent> getFriendStatusChangedListener() {
		return friendStatusChangedListener;
	}

	/**
	 * @return слушатель событий индикатора.
	 */
	private ElementUIEventListener<IndicatorController> getIndicatorEventListener() {
		return indicatorEventListener;
	}

	/**
	 * @return индекс следующего индикатора.
	 */
	public AtomicInteger getIndicatorIndex() {
		return indicatorIndex;
	}

	/**
	 * @return таблица индикатров для объектов.
	 */
	public Table<LongKey, IndicatorController> getIndicatorTable() {
		return indicatorTable;
	}

	/**
	 * @return слушатель выбора объекта.
	 */
	public HudEventListener<SelectedEvent> getSelectObjectListener() {
		return selectObjectListener;
	}

	/**
	 * @return список индикаторов на обновление.
	 */
	public Array<IndicatorController> getUpdated() {
		return updated.clear();
	}

	@Override
	public boolean isComplexElement() {
		return true;
	}

	@Override
	public void onStartScreen() {

		UpdateUIManager manager = UpdateUIManager.getInstance();
		manager.addElement(this);

		SPACE_LOCATION.addListener(this);
	}

	@Override
	public void register(HudController controller) {
		super.register(controller);
		controller.registerListener(SelectedEvent.class, getSelectObjectListener());
		controller.registerListener(FriendStatusChangedEvent.class, getFriendStatusChangedListener());
	}

	@Override
	public void removeObject(SpaceObject object) {

		if(object == getPlayerShip() || !object.isNeedIndicator()) {
			return;
		}

		Table<LongKey, IndicatorController> indicatorTable = getIndicatorTable();

		if(!indicatorTable.containsKey(object.getObjectId())) {
			return;
		}

		IndicatorController indicator = indicatorTable.get(object.getObjectId());
		indicator.removeElementListener(PrimaryClickEvent.EVENT_TYPE, getIndicatorEventListener());

		Element element = indicator.getElement();
		element.markForRemoval();

		indicatorTable.remove(object.getObjectId());
	}

	/**
	 * Обработка нажатия на индикатор.
	 * 
	 * @param indicator нажатый индикатор.
	 */
	public void selectIndicator(IndicatorController indicator) {

		if(indicator == null) {
			return;
		}

		SpaceObject object = indicator.getObject();

		if(object == null) {
			return;
		}

		SelectIndicatorEvent indicatorEvent = SelectIndicatorEvent.get();
		indicatorEvent.setObject(object);

		getController().notify(indicatorEvent);
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		Table<LongKey, IndicatorController> indicatorTable = getIndicatorTable();

		if(indicatorTable.isEmpty()) {
			return false;
		}

		Array<IndicatorController> indicators = indicatorTable.values(getUpdated());

		Camera camera = CAMERA;

		int counter = 0;

		for(IndicatorController indicator : indicators.array()) {

			if(indicator == null) {
				break;
			}

			if(indicator.update(camera)) {
				counter++;
			}
		}

		if(counter > 0) {
			ElementUtils.updateLayout(getBasePanel());
		}

		return super.update(local, currentTime);
	}

	/**
	 * Обновление статуса дружелюбности объекта.
	 * 
	 * @param object обновляемый объект.
	 */
	private void updateFriendStatus(SpaceObject object) {

		Table<LongKey, IndicatorController> indicators = getIndicatorTable();

		IndicatorController indicator = indicators.get(object.getObjectId());

		if(indicator == null) {
			return;
		}

		indicator.updateIcon();
	}
}
