package com.ss.client.gui.controller.game.event.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.model.ItemPanelInfo;

/**
 * Событие о обновлении позиций предметов на панели.
 * 
 * @author Ronn
 */
public class UpdateItemPanelEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateItemPanelEvent> LOCAL_EVENT = new ThreadLocal<UpdateItemPanelEvent>() {

		@Override
		protected UpdateItemPanelEvent initialValue() {
			return new UpdateItemPanelEvent();
		}
	};

	public static final UpdateItemPanelEvent get(Array<ItemPanelInfo> itemPanelInfo) {

		UpdateItemPanelEvent event = LOCAL_EVENT.get();
		event.itemPanelInfo.clear();
		event.itemPanelInfo.addAll(itemPanelInfo);

		return event;
	}

	/** список размещений умений на панели */
	private final Array<ItemPanelInfo> itemPanelInfo;

	public UpdateItemPanelEvent() {
		this.itemPanelInfo = Arrays.toArray(ItemPanelInfo.class);
	}

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return список размещений умений на панели.
	 */
	public Array<ItemPanelInfo> getItemPanelInfo() {
		return itemPanelInfo;
	}
}
