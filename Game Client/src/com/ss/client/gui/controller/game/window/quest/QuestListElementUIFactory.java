package com.ss.client.gui.controller.game.window.quest;

import com.ss.client.gui.element.ListElementUI;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.gui.element.effect.builder.EffectTimeType;
import com.ss.client.gui.element.effect.builder.impl.SimpleColorBarUIEffectBuilder;
import com.ss.client.gui.element.list.ListElementUIFactory;
import com.ss.client.gui.model.quest.QuestState;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация фабрики элементов заданий в списке заданий.
 * 
 * @author Ronn
 */
public class QuestListElementUIFactory implements ListElementUIFactory<QuestState> {

	private static final SimpleColorBarUIEffectBuilder SELECT_COLOR_EFFECT = new SimpleColorBarUIEffectBuilder();

	static {
		SELECT_COLOR_EFFECT.setColor(Color.WHITE);
		SELECT_COLOR_EFFECT.setCustomKey(ListElementUI.SELECT_EFFECT_KEY);
		SELECT_COLOR_EFFECT.setNeverStopRendering(true);
		SELECT_COLOR_EFFECT.setPost(true);
		SELECT_COLOR_EFFECT.setTimeType(EffectTimeType.INFINITE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ListElementUI<QuestState> build(QuestState quest, Element parent, Nifty nifty, Screen screen, int index) {

		LabelUIBuilder name = new LabelUIBuilder(QuestElementController.QUEST_NAME_ID);
		name.setWrap(true);
		name.setText(quest.getName());

		PanelUIBuilder builder = new PanelUIBuilder(ListElementUIFactory.ITEM_ID + index);
		builder.setController(QuestElementController.class);
		builder.setVisibleToMouse(true);
		builder.setHeight("20px");
		builder.add(name);
		builder.setBackgroundColor(Color.randomColor());

		SELECT_COLOR_EFFECT.addTo(builder, EffectEventId.onCustom);

		Element element = builder.build(nifty, screen, parent);

		ListElementUI<QuestState> control = element.getControl(ListElementUI.class);
		control.setItem(quest);

		return control;
	}
}
