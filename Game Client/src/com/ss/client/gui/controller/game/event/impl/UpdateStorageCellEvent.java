package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.window.storage.CellInfo;

/**
 * Реализация события обновления ячейки хранилища корабля.
 * 
 * @author Ronn
 */
public class UpdateStorageCellEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateStorageCellEvent> LOCAL_EVENT = new ThreadLocal<UpdateStorageCellEvent>() {

		@Override
		protected UpdateStorageCellEvent initialValue() {
			return new UpdateStorageCellEvent();
		}
	};

	public static UpdateStorageCellEvent get(CellInfo info) {

		UpdateStorageCellEvent event = LOCAL_EVENT.get();
		event.info = info;

		return event;
	}

	/** информация о ячейках */
	private CellInfo info;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return информация о ячейках.
	 */
	public CellInfo getInfo() {
		return info;
	}
}
