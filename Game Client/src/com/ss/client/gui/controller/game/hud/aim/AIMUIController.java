package com.ss.client.gui.controller.game.hud.aim;

import java.util.Properties;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.gui.controller.game.hud.AbstractHudComponent;
import com.ss.client.gui.controller.game.hud.selector.SelectorType;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.ship.player.PlayerShipView;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера прицельной системы.
 * 
 * @author Ronn
 */
public class AIMUIController extends AbstractHudComponent {

	public static final String AIM_ICON = "game/hud/aim.png";

	public static final String BASE_PANEL_ID = "#aim_panel";

	public static final String AIM_ID = "#aim";
	public static final String AIM_ICON_ID = "#aim_icon";

	public static final String AIM_HEIGHT = "26px";
	public static final String AIM_WIDTH = "40px";

	/** позиция прицела */
	private final Vector3f position;
	/** вектор направления корабял */
	private final Vector3f direction;

	/** элемент прицела */
	private Element aim;

	public AIMUIController() {
		this.position = new Vector3f();
		this.direction = new Vector3f();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);
		this.aim = element.findElementByName(AIM_ID);

		UPDATE_UI_MANAGER.addElement(this);
	}

	/**
	 * @return элемент прицела.
	 */
	public Element getAim() {
		return aim;
	}

	/**
	 * @return вектор направления корабял.
	 */
	public Vector3f getDirection() {
		return direction;
	}

	/**
	 * @return позиция прицела.
	 */
	public Vector3f getPosition() {
		return position;
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		PlayerShipView playerView = playerShip.getView();

		if(playerView == null) {
			return true;
		}

		Element aim = getAim();

		SpaceObject target = playerShip.getTarget();
		SelectorType selectorType = target == null ? SelectorType.NONE : target.getSelectorType();

		if(target == null || !selectorType.isNeedAIM()) {

			if(aim.isVisible()) {
				aim.hide();
			}

			return false;
		}

		SpaceObjectView targetView = target.getView();

		if(targetView == null) {

			if(aim.isVisible()) {
				aim.hide();
			}

			return false;
		}

		Quaternion rotation = playerView.getRotation();

		Vector3f myLoc = playerView.getLocation();
		Vector3f targetLoc = targetView.getLocation();
		Vector3f direction = rotation.getRotationColumn(2, getDirection());

		float distance = targetLoc.distance(myLoc);

		Vector3f aimPosition = local.getNextVector();
		aimPosition.set(direction);
		aimPosition.multLocal(distance);
		aimPosition.addLocal(myLoc);

		final Vector3f position = getPosition();

		CAMERA.getScreenCoordinates(aimPosition, position);

		int maxHeight = CAMERA.getHeight() - aim.getHeight();
		int maxWidth = CAMERA.getWidth() - aim.getWidth();

		boolean isBottom = position.getY() < 0;
		boolean isTop = position.getY() > maxHeight;
		boolean isLeft = position.getX() < 0;
		boolean isRight = position.getX() > maxWidth;

		boolean visible = !isBottom && !isLeft && !isTop && !isRight && position.getZ() < 1;

		if(visible != aim.isVisible()) {
			aim.setVisible(visible);
		}

		if(!aim.isVisible()) {
			return false;
		}

		final int x = (int) (position.getX() - aim.getWidth() / 2);
		final int y = (int) (CAMERA.getHeight() - position.getY() - aim.getHeight() / 2);

		if(aim.getX() != x || aim.getY() != y) {

			aim.setConstraintX(GameUtil.getPixelSize(x));
			aim.setConstraintY(GameUtil.getPixelSize(y));

			ElementUtils.updateLayout(getElement());
		}

		return false;
	}
}
