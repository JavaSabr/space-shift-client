package com.ss.client.gui.controller.game.quest.reward;

import com.ss.client.gui.controller.game.quest.QuestStructElementController;
import com.ss.client.gui.model.quest.reward.QuestReward;

/**
 * Интерфейс для реализации контроллера элемента отображающего награду за
 * задание.
 * 
 * @author Ronn
 */
public interface QuestRewardElementController extends QuestStructElementController {

	public static final String ID = "#quest_reward_element_";
	public static final String QUEST_REWARD_ICON_ID = "#quest_reward_icon";
	public static final String QUEST_REWARD_NAME_ID = "#quest_reward_name";

	/**
	 * @return отображаемая награда за задание.
	 */
	public QuestReward getQuestReward();

	/**
	 * @param reward отображаемая награда за задание.
	 */
	public void setQuestReward(QuestReward reward);
}
