package com.ss.client.gui.controller.game.hud.event;

/**
 * Интерфейс для реализации слушателя HUD событий.
 * 
 * @author Ronn
 */
public interface HudEventListener<T extends HudEvent> {

	/**
	 * Уведомление о HUD событии.
	 * 
	 * @param event HUD событие.
	 */
	public void notify(T event);
}
