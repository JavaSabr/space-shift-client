package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;

/**
 * Игровое событие о запуске отката объекта.
 * 
 * @author Ronn
 */
public class ObjectReuseEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<ObjectReuseEvent> LOCAL_EVENT = new ThreadLocal<ObjectReuseEvent>() {

		@Override
		protected ObjectReuseEvent initialValue() {
			return new ObjectReuseEvent();
		}
	};

	public static final ObjectReuseEvent get(int objectId, int templateId, int reuseTime, int objectType, long startReuseTime) {

		ObjectReuseEvent event = LOCAL_EVENT.get();
		event.objectId = objectId;
		event.reuseTime = reuseTime;
		event.startReuseTime = startReuseTime;
		event.templateId = templateId;
		event.objectType = objectType;

		return event;
	}

	/** дата старта отката */
	private long startReuseTime;

	/** уникальный ид скила */
	private int objectId;
	/** ид шаблона скила */
	private int templateId;
	/** время отката скила */
	private int reuseTime;
	/** тип откатываемого объекта */
	private int objectType;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return уникальный ид скила.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return тип откатываемого объекта.
	 */
	public int getObjectType() {
		return objectType;
	}

	/**
	 * @return время отката скила.
	 */
	public int getReuseTime() {
		return reuseTime;
	}

	/**
	 * @return дата старта отката.
	 */
	public long getStartReuseTime() {
		return startReuseTime;
	}

	/**
	 * @return ид шаблона скила.
	 */
	public int getTemplateId() {
		return templateId;
	}
}
