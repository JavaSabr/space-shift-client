package com.ss.client.gui.controller.game.event.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.model.skills.Skill;

/**
 * Событие о обновлении списка умений корабля игрока.
 * 
 * @author Ronn
 */
public class UpdateSkillListEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateSkillListEvent> LOCAL_EVENT = new ThreadLocal<UpdateSkillListEvent>() {

		@Override
		protected UpdateSkillListEvent initialValue() {
			return new UpdateSkillListEvent();
		}
	};

	public static final UpdateSkillListEvent get(Array<Skill> skills) {

		UpdateSkillListEvent event = LOCAL_EVENT.get();
		event.update(skills);

		return event;
	}

	/** список умений */
	private final Array<Skill> skills;

	public UpdateSkillListEvent() {
		this.skills = Arrays.toArray(Skill.class);
	}

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return список умений.
	 */
	public Array<Skill> getSkills() {
		return skills;
	}

	private void update(Array<Skill> skills) {
		this.skills.clear();
		this.skills.addAll(skills);
	}
}
