package com.ss.client.gui.controller.game.panel.fast;

import com.ss.client.gui.element.DraggableElementUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Интерфейс для реализации размещаемого на панели элемента.
 * 
 * @author Ronn
 */
public interface FastPanelItem extends DraggableElementUI {

	public static final int OBJECT_TYPE_SKILL = 1;

	/**
	 * Активация элемента.
	 */
	public void activate();

	/**
	 * @return получение клона элемента.
	 */
	public DraggableElementUI createFrom(Element parent, Screen screen, Nifty nifty);

	/**
	 * @return индекс элемента.
	 */
	public int getIndex();

	/**
	 * Является ли указанный элемент таким же.
	 */
	public boolean matches(FastPanelItem item);

	/**
	 * Обработка события элементом.
	 * 
	 * @return стоит ли завершить перебор на этом.
	 */
	public boolean processEvent(FastPanelItemEvent event);

	/**
	 * Удаление элемента.
	 */
	public void remove();

	/**
	 * @param index индекс элемента.
	 */
	public void setIndex(int index);

	/**
	 * Обновить позицию на панели на сервере.
	 */
	public void updateToServer(int index);
}
