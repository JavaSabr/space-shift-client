package com.ss.client.gui.controller.game.window.quest;

import java.util.Properties;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateQuestCounterEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateQuestListEvent;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.ListUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.SelectedChangeEvent;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.quest.QuestCounter;
import com.ss.client.gui.model.quest.QuestState;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestQuestList;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера журнала заданий.
 * 
 * @author Ronn
 */
public class QuestWindowUIController extends DraggableElementUIImpl implements GameUIComponent {

	public static final String WINDOW_ID = "#quest_window";
	public static final String QUEST_DETAILS_ID = "#quest_details";
	public static final String QUEST_LIST_ID = "#quest_list";
	public static final String CLOSE_ID = "#close";
	public static final String HEADER_ID = "#header";
	public static final String TITLE_ID = "#title";

	private static final Network NETWORK = Network.getInstance();

	/** слушатель событий по обновлению списка заданий */
	private final GameUIEventListener questListListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			processEvent((UpdateQuestListEvent) event);
		}
	};

	/** слушатель выбор заданияд ля просмотра */
	private final ElementUIEventListener<ElementUI> selectedQuestEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		@SuppressWarnings("unchecked")
		public void notifyEvent(ElementUIEvent<ElementUI> event) {
			processEvent((SelectedChangeEvent<ElementUI, QuestState>) event);
		}
	};

	/** слушатель событий по обновлению счетчиков заданий */
	private final GameUIEventListener updateCounterEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			processEvent((UpdateQuestCounterEvent) event);
		}
	};

	/** массив всех заданий */
	private final Array<QuestState> quests;

	/** список заданий */
	private ListUI<QuestState> questList;
	/** контролер отображения информации о задании */
	private QuestDetailsElementController details;

	public QuestWindowUIController() {
		this.quests = Arrays.toArray(QuestState.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.questList = element.findControl(QUEST_LIST_ID, ListUI.class);
		this.questList.addElementListener(SelectedChangeEvent.EVENT_TYPE, getSelectedQuestEventListener());
		this.details = element.findControl(QUEST_DETAILS_ID, QuestDetailsElementController.class);
	}

	@Override
	public void onStartScreen() {
		super.onStartScreen();
		NETWORK.sendPacketToGameServer(RequestQuestList.getInstance());
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
	}

	/**
	 * @return список заданий.
	 */
	public ListUI<QuestState> getQuestList() {
		return questList;
	}

	/**
	 * @return слушатель событий по обновлению списка заданий.
	 */
	public GameUIEventListener getQuestListListener() {
		return questListListener;
	}

	/**
	 * @return слушатель выбор заданияд ля просмотра.
	 */
	public ElementUIEventListener<ElementUI> getSelectedQuestEventListener() {
		return selectedQuestEventListener;
	}

	/**
	 * @return слушатель событий по обновлению счетчиков заданий.
	 */
	public GameUIEventListener getUpdateCounterEventListener() {
		return updateCounterEventListener;
	}

	/**
	 * Обрабготка события по запросу на отображения задания.
	 * 
	 * @param event событие выбора задания.
	 */
	protected void processEvent(SelectedChangeEvent<ElementUI, QuestState> event) {
		QuestState quest = event.getObject();
		details.showQuest(quest);
	}

	/**
	 * Обработка события перезагрузки списка заданий.
	 * 
	 * @param event событие игрового UI.
	 */
	private void processEvent(UpdateQuestListEvent event) {

		ListUI<QuestState> questList = getQuestList();
		questList.clear();

		Array<QuestState> quests = getQuests();
		quests.clear();
		quests.addAll(event.getContainer());

		for(QuestState quest : event.getContainer()) {
			questList.addItem(quest);
		}
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(UpdateQuestListEvent.EVENT_TYPE, getQuestListListener());
		controller.registerListener(UpdateQuestCounterEvent.EVENT_TYPE, getUpdateCounterEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setInputMode(InputMode inputMode) {

		if(inputMode == InputMode.CONTROL_MODE) {
			setVisible(false);
		}
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setVisible(boolean visible) {

		if(!visible) {
			getQuestList().setSelected(null);
		}

		super.setVisible(visible);

		if(visible) {
			details.updateVisible();
		}
	}

	private void processEvent(UpdateQuestCounterEvent event) {

		Array<QuestState> quests = getQuests();

		int questId = event.getQuestId();
		int objectId = event.getObjectId();

		for(QuestState state : quests.array()) {

			if(state == null) {
				break;
			}

			if(state.getId() != questId) {
				continue;
			}

			if(state.getObjectId() != objectId) {
				continue;
			}

			QuestCounter counter = state.findCounter(event.getCounterId());

			if(counter != null) {
				counter.setValue(event.getValue());
			}

			QuestDetailsElementController details = getDetails();

			if(state == details.getQuest()) {
				details.update();
			}
		}
	}

	/**
	 * @return массив всех заданий.
	 */
	public Array<QuestState> getQuests() {
		return quests;
	}

	/**
	 * @return контролер отображения информации о задании.
	 */
	public QuestDetailsElementController getDetails() {
		return details;
	}
}
