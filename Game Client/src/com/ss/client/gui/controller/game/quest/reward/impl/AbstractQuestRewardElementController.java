package com.ss.client.gui.controller.game.quest.reward.impl;

import java.util.Properties;

import com.ss.client.gui.controller.game.quest.reward.QuestRewardElementController;
import com.ss.client.gui.element.ImageUI;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.impl.PanelUIImpl;
import com.ss.client.gui.model.quest.reward.QuestReward;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Базовая реализация контроллера элемента отображения награды.
 * 
 * @author Ronn
 */
public abstract class AbstractQuestRewardElementController extends PanelUIImpl implements QuestRewardElementController {

	/** награда за задание */
	private QuestReward reward;

	/** надпись о награде */
	private LabelUI name;
	/** иконка награды */
	private ImageUI icon;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.name = element.findControl(QUEST_REWARD_NAME_ID, LabelUI.class);
		this.icon = element.findControl(QUEST_REWARD_ICON_ID, ImageUI.class);
	}

	/**
	 * @return иконка награды.
	 */
	public ImageUI getIcon() {
		return icon;
	}

	/**
	 * @return надпись о награде.
	 */
	public LabelUI getName() {
		return name;
	}

	@Override
	public QuestReward getQuestReward() {
		return reward;
	}

	@Override
	public void setQuestReward(QuestReward reward) {
		this.reward = reward;
		refresh();
	}
}
