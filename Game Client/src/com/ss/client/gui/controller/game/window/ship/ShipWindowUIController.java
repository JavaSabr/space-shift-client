package com.ss.client.gui.controller.game.window.ship;

import java.util.Comparator;
import java.util.Properties;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.jme3.math.Quaternion;
import com.ss.client.Game;
import com.ss.client.gui.builder.game.ShipWindowUIBuilder;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSlot;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.table.LangTable;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контрола для панели корабля.
 * 
 * @author Ronn
 */
public class ShipWindowUIController extends DraggableElementUIImpl implements GameUIComponent, UpdateUIElement {

	private static final Logger LOGGER = Loggers.getLogger(ShipWindowUIController.class);

	public static final String STATS_PANEL_ID = "#stats_panel";
	public static final String STATS_TITLE_ID = "#stats_title";
	public static final String MODULE_CELL_ID = "#cell_";
	public static final String MODULES_ROW_ID = "#row_";
	public static final String MODULES_PANEL_ID = "#modules_panel";
	public static final String MODULES_TITLE_ID = "#modules_title";
	public static final String CONTENT_ID = "#content";
	public static final String INFO_PANEL_ID = "#info_panel";
	public static final String SHIP_NAME_ID = "#ship_name";
	public static final String SHIP_VIEW_ID = "#ship_view";
	public static final String WINDOW_ID = "#ship_window";
	public static final String HEADER_ID = "#header";
	public static final String TITLE_ID = "#title";
	public static final String CLOSE_ID = "#close";
	public static final String STATS_FLY_ACCEL_VALUE_ID = "#fly_accel_value";
	public static final String STATS_FLY_ACCEL_NAME_ID = "#fly_accel_name";
	public static final String STATS_FLY_SPEED_VALUE_ID = "#fly_speed_value";
	public static final String STATS_FLY_SPEED_NAME_ID = "#fly_speed_name";
	public static final String STATS_ENERGY_GEN_VALUE_ID = "#energy_gen_value";
	public static final String STATS_ENERGY_GEN_NAME_ID = "#energy_gen_name";
	public static final String STATS_ENERGY_VALUE_ID = "#energy_value";
	public static final String STATS_ENERGY_NAME_ID = "#energy_name";
	public static final String STATS_STRENGTH_VALUE_ID = "#strength_value";
	public static final String STATS_STRENGTH_NAME_ID = "#strength_name";
	public static final String STATS_EXP_VALUE_ID = "#exp_value";
	public static final String STATS_EXP_NAME_ID = "#exp_name";
	public static final String STATS_LEVEL_VALUE_ID = "#level_value";
	public static final String STATS_LEVEL_NAME_ID = "#level_name";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static final String WINDOW_TITLE = LANG_TABLE.getText(ShipWindowTextIds.WINDOW_TITLE);
	public static final String SHIP_STATS_TITLE = LANG_TABLE.getText(ShipWindowTextIds.STATS_TITLE);
	public static final String SHIP_MODULES_TILTLE = LANG_TABLE.getText(ShipWindowTextIds.MODULES_TITLE);
	public static final String SHIP_NAME_LABEL = LANG_TABLE.getText(ShipWindowTextIds.SHIP_NAME_LABEL);

	public static final String[][] STAT_ROWS = {
		{
			STATS_LEVEL_NAME_ID,
			STATS_LEVEL_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_LEVEL),
			"1"
		},
		{
			STATS_EXP_NAME_ID,
			STATS_EXP_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_EXP),
			"0"
		},
		{
			STATS_STRENGTH_NAME_ID,
			STATS_STRENGTH_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_STRENGTH),
			"0/0"
		},
		{
			STATS_ENERGY_NAME_ID,
			STATS_ENERGY_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_ENERGY),
			"0/0",
		},
		{
			STATS_ENERGY_GEN_NAME_ID,
			STATS_ENERGY_GEN_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_ENERGY_GEN),
			"0",
		},
		{
			STATS_FLY_SPEED_NAME_ID,
			STATS_FLY_SPEED_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_FLY_SPEED),
			"0",
		},
		{
			STATS_FLY_ACCEL_NAME_ID,
			STATS_FLY_ACCEL_VALUE_ID,
			LANG_TABLE.getText(ShipWindowTextIds.STATS_FLY_ACCEL),
			"0",
		},
	};

	private static final UpdateUIManager UPDATE_UI_MANAGER = UpdateUIManager.getInstance();
	private static final Game GAME = Game.getInstance();

	private static final Quaternion ROTATION = new Quaternion(0.17475824F, -0.0029526774F, 0.007872019F, 0.9845765F);

	private static final Comparator<Element> CELL_COMPARATOR = new ArrayComparator<Element>() {

		@Override
		protected int compareImpl(Element first, Element second) {

			String firstId = first.getId();
			String secondId = second.getId();

			int firstValue = Integer.parseInt(firstId.replace(MODULE_CELL_ID, Strings.EMPTY));
			int secondValue = Integer.parseInt(secondId.replace(MODULE_CELL_ID, Strings.EMPTY));

			return Integer.compare(firstValue, secondValue);
		}
	};

	/** контроллер UI */
	private Nifty nifty;
	/** UI экран этого элемнта */
	private Screen screen;

	/** корабль игрока */
	private PlayerShip playerShip;

	/** название корабля */
	private LabelUI shipNameLabel;

	/** уровень корабля */
	private LabelUI statLevelLabel;
	/** кол-во полученного опыта */
	private LabelUI statExpLabel;
	/** прочность корабля */
	private LabelUI statStrengthLabel;
	/** уровень энергии корабля */
	private LabelUI statEnergyLabel;
	/** скорость генерации энергии */
	private LabelUI statEnergyGenLabel;
	/** максимальная скорость корабля */
	private LabelUI statFlySpeedLabel;
	/** ускорение корабля */
	private LabelUI statFlyAccelLabel;

	/** список ячеяк для размещения модулей */
	private Element[] moduleCells;

	public ShipWindowUIController() {
	}

	@Override
	public void bind(Array<UpdateUIElement> container) {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.nifty = nifty;
		this.screen = screen;
		this.shipNameLabel = element.findControl(SHIP_NAME_ID, LabelUI.class);
		this.statLevelLabel = element.findControl(STATS_LEVEL_VALUE_ID, LabelUI.class);
		this.statExpLabel = element.findControl(STATS_EXP_VALUE_ID, LabelUI.class);
		this.statStrengthLabel = element.findControl(STATS_STRENGTH_VALUE_ID, LabelUI.class);
		this.statEnergyLabel = element.findControl(STATS_ENERGY_VALUE_ID, LabelUI.class);
		this.statEnergyGenLabel = element.findControl(STATS_ENERGY_GEN_VALUE_ID, LabelUI.class);
		this.statFlySpeedLabel = element.findControl(STATS_FLY_SPEED_VALUE_ID, LabelUI.class);
		this.statFlyAccelLabel = element.findControl(STATS_FLY_ACCEL_VALUE_ID, LabelUI.class);

		// подготовка списка ячеяк
		{
			final Array<Element> cells = Arrays.toArray(Element.class);

			// формируем список всех ячейках
			ElementUtils.addElements(cells, element, MODULE_CELL_ID);

			cells.sort(CELL_COMPARATOR);

			this.moduleCells = cells.toArray(new Element[cells.size()]);
		}
	}

	@Override
	public void finish() {
	}

	/**
	 * @return список ячеяк для размещения модулей.
	 */
	public Element[] getModuleCells() {
		return moduleCells;
	}

	/**
	 * @return контроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return UI экран этого элемнта.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return название корабля.
	 */
	public LabelUI getShipNameLabel() {
		return shipNameLabel;
	}

	/**
	 * @return скорость генерации энергии.
	 */
	public LabelUI getStatEnergyGenLabel() {
		return statEnergyGenLabel;
	}

	/**
	 * @return уровень энергии корабля.
	 */
	public LabelUI getStatEnergyLabel() {
		return statEnergyLabel;
	}

	/**
	 * @return кол-во полученного опыта.
	 */
	public LabelUI getStatExpLabel() {
		return statExpLabel;
	}

	/**
	 * @return ускорение корабля.
	 */
	public LabelUI getStatFlyAccelLabel() {
		return statFlyAccelLabel;
	}

	/**
	 * @return максимальная скорость корабля.
	 */
	public LabelUI getStatFlySpeedLabel() {
		return statFlySpeedLabel;
	}

	/**
	 * @return уровень корабля.
	 */
	public LabelUI getStatLevelLabel() {
		return statLevelLabel;
	}

	/**
	 * @return прочность корабля.
	 */
	public LabelUI getStatStrengthLabel() {
		return statStrengthLabel;
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	@Override
	public void onStartScreen() {
		super.onStartScreen();
		UPDATE_UI_MANAGER.addElement(this);
	}

	@Override
	public void register(GameUIController controller) {
	}

	@Override
	public void register(InputManager inputManager) {
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		if(inputMode == InputMode.CONTROL_MODE) {
			setVisible(false);
		}
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;

		getShipNameLabel().setText(SHIP_NAME_LABEL.replace("%ship_name%", "\"" + playerShip.getName() + "\""));
		updateStats();
		updateModules();
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		if(!isVisible()) {
			return false;
		}

		updateStats();

		return false;
	}

	/**
	 * Обновление набора модулей.
	 */
	private void updateModules() {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(new Exception("not found player ship."));
		}

		Element[] cells = getModuleCells();

		for(Element cell : cells) {
			ElementUtils.removeChilds(cell);
		}

		ModuleSystem moduleSystem = playerShip.getModuleSystem();
		moduleSystem.lock();
		try {

			ModuleSlot[] slots = moduleSystem.getSlots();

			for(int i = 0, g = 0, length = slots.length, max = cells.length; i < length && g < max; i++) {

				Module module = slots[i].getModule();

				if(module == null) {
					continue;
				}

				ShipWindowUIBuilder.buildModuleElement(module, cells[g++], getScreen(), getNifty(), g - 1);
			}

		} finally {
			moduleSystem.unlock();
		}
	}

	/**
	 * Обовление характеристик корабля.
	 */
	private void updateStats() {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(new Exception("not found player ship."));
		}

		getStatLevelLabel().setText(String.valueOf(playerShip.getLevel()));
		getStatExpLabel().setText(String.valueOf(playerShip.getExp()));
		getStatStrengthLabel().setText(playerShip.getCurrentStrength() + "/" + playerShip.getMaxStrength());
		getStatEnergyLabel().setText(playerShip.getCurrentEnergy() + "/" + playerShip.getMaxEnergy());
		getStatEnergyGenLabel().setText(String.valueOf(playerShip.getEnergyRegen()));
		getStatFlySpeedLabel().setText(String.valueOf((int) playerShip.getFlyMaxSpeed()));
		getStatFlyAccelLabel().setText(String.valueOf(Math.max((int) playerShip.getFlyAccel(), 0)));
	}
}
