package com.ss.client.gui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.ss.client.gui.builder.HangarBuilder;
import com.ss.client.gui.builder.LoginBuilder;
import com.ss.client.gui.builder.game.GameUIBuilder;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.screen.Screen;

/**
 * Перечисление наборов интерфейсов.
 * 
 * @author Ronn
 */
public enum InterfaceId {
	LOGIN_MENU(LoginBuilder.class),
	HANGAR(HangarBuilder.class),
	GAME(GameUIBuilder.class);

	private static final Logger log = Loggers.getLogger(InterfaceId.class);

	/** контруктор билдера экрана */
	private Constructor<? extends ScreenBuilder> constructor;

	private InterfaceId(final Class<? extends ScreenBuilder> type) {
		try {
			this.constructor = type.getConstructor();
		} catch(NoSuchMethodException | SecurityException e) {
			Loggers.warning(this, e);
		}
	}

	/**
	 * Создание экрана.
	 */
	public Screen newInstance(final Nifty nifty) {
		try {
			return constructor.newInstance().build(nifty);
		} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.warning(e);
		}

		return null;
	}
}
