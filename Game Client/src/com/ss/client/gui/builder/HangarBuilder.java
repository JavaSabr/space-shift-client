package com.ss.client.gui.builder;

import rlib.util.Strings;

import com.ss.client.gui.ElementId;
import com.ss.client.gui.controller.HangarController;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.dropdown.builder.DropDownBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;

/**
 * Конструктор ангара выбора карабля.
 * 
 * @author Ronn
 */
public class HangarBuilder extends ScreenBuilder {

	private static final String METHOD_WARNING_CANCEL = "warningCancel()";
	private static final String METHOD_SELECTED_SHIP = "selectedShip()";
	private static final String METHOD_CREATE_SHIP = "createShip()";
	private static final String METHOD_DELETE_SHIP = "deleteShip()";
	private static final String METHOD_EXIT = "exit()";
	private static final String METHOD_SELECTED_SHIP_ENTER = "selectedShipEnter()";
	private static final String METHOD_SELECTED_SHIP_CANCEL = "selectedShipCancel()";
	private static final String METHOD_DELETE_SHIP_DELETE = "deleteShipDelete()";
	private static final String METHOD_DELETE_SHIP_CANCEL = "deleteShipCancel()";
	private static final String METHOD_CREATE_SHIP_CREATE = "createShipCreate()";
	private static final String METHOD_CREATE_SHIP_CANCEL = "createShipCancel()";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	private static final String INTERFACE_HANGAR_BUTTON_WARNING = LANG_TABLE.getText("@interface:hangarButtonWarning@");
	private static final String INTERFACE_HANGAR_BUTTON_EXIT = LANG_TABLE.getText("@interface:hangarButtonExit@");
	private static final String INTERFACE_HANGAR_BUTTON_ENTER = LANG_TABLE.getText("@interface:hangarButtonEnter@");
	private static final String INTERFACE_HANGAR_LABEL_SHIP_ENTER = LANG_TABLE.getText("@interface:hangarLabelShipEnter@");
	private static final String INTERFACE_HANGAR_BUTTON_DELETE = LANG_TABLE.getText("@interface:hangarButtonDelete@");
	private static final String INTERFACE_HANGAR_BUTTON_CANCEL = LANG_TABLE.getText("@interface:hangarButtonCancel@");
	private static final String INTERFACE_HANGAR_LABEL_SHIP_DELETE = LANG_TABLE.getText("@interface:hangarLabelShipDelete@");
	private static final String INTERFACE_HANGAR_BUTTON_CREATE = LANG_TABLE.getText("@interface:hangarButtonCreate@");
	private static final String INTERFACE_HANGAR_LABEL_SHIP_NAME = LANG_TABLE.getText("@interface:hangarLabelShipName@");

	public HangarBuilder() {
		super(ElementId.HANGAR_SCREEN.getId());

		// добавляем прослушивателя действий
		controller(HangarController.getInstance());

		buildHangarInfo();
		buildHangarBar();
		buildCreatePanel();
		buildEnterPanel();
		buildDeletePanel();
		buildWarningPanel();
	}

	/**
	 * Формируем панель создания корабля.
	 */
	private void buildCreatePanel() {
		LayerBuilder layer = new LayerBuilder(SpaceObjectView.NAME_OBJECT_LAYER);
		layer.childLayoutAbsolute();
		layer.height("100%");
		layer.width("100%");
		layer(layer);

		layer = new LayerBuilder(ElementId.HANGAR_CREATE_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "300px", "140px");
		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 8));
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_HANGAR_LABEL_SHIP_NAME, "100%", "30px"));

		final TextFieldBuilder textField = ElementFactory.getTextFieldBuilder(ElementId.HANGAR_CREATE_FIELD_NAME);
		textField.alignCenter();

		panel.control(textField);

		final PanelBuilder bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, Strings.EMPTY, "83%", "80px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;

		ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_CANCEL);
		button.valignCenter();
		button.interactOnClick(METHOD_CREATE_SHIP_CANCEL);
		panel.control(button);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 51, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_CREATE);
		button.valignCenter();
		button.interactOnClick(METHOD_CREATE_SHIP_CREATE);

		panel.control(button);
	}

	/**
	 * Формирование панели удаления корабля.
	 */
	private void buildDeletePanel() {
		final LayerBuilder layer = new LayerBuilder(ElementId.HANGAR_DELETE_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, Strings.EMPTY, "100%", "120px");
		panel.valignTop();

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 60));

		PanelBuilder bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "557px", "80px");
		bufferPanel.alignCenter();
		panel.panel(bufferPanel);

		panel = bufferPanel;
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 8));
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_HANGAR_LABEL_SHIP_DELETE, "80%", null));

		bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, Strings.EMPTY, "100%", "30px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 12, 0));

		ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_CANCEL);
		button.valignCenter();
		button.interactOnClick(METHOD_DELETE_SHIP_CANCEL);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		final DropDownBuilder dropDown = ElementFactory.getDropDownBuilder(ElementId.HANGAR_DELETE_DROP_DOWN_SELECTED, "57%", null);
		dropDown.valignCenter();

		panel.control(dropDown);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_DELETE);
		button.valignCenter();
		button.interactOnClick(METHOD_DELETE_SHIP_DELETE);

		panel.control(button);
	}

	/**
	 * Формирование панели для входа кораблем.
	 */
	private void buildEnterPanel() {
		final LayerBuilder layer = new LayerBuilder(ElementId.HANGAR_ENTER_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, Strings.EMPTY, "100%", "120px");
		panel.valignTop();

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 60));

		PanelBuilder bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "557px", "80px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 8));
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_HANGAR_LABEL_SHIP_ENTER, "80%", null));

		bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, Strings.EMPTY, "100%", "30px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 12, 0));

		ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_CANCEL);
		button.valignCenter();
		button.interactOnClick(METHOD_SELECTED_SHIP_CANCEL);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		final DropDownBuilder dropDown = ElementFactory.getDropDownBuilder(ElementId.HANGAR_ENTER_DROP_DOWN_SELECTED, "57%", null);
		dropDown.valignCenter();

		panel.control(dropDown);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		button = ElementFactory.getButtonBuild(ElementId.HANGAR_ENTER_BUTTON_ENTER, INTERFACE_HANGAR_BUTTON_ENTER);
		button.valignCenter();
		button.interactOnClick(METHOD_SELECTED_SHIP_ENTER);

		panel.control(button);
	}

	private void buildHangarBar() {
		final LayerBuilder layer = new LayerBuilder(ElementId.getNextElementId());
		layer.childLayoutCenter();
		layer(layer);

		final PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, "100%", "50px");
		panel.valignBottom();
		panel.paddingBottom("10px");
		panel.paddingTop("10px");
		panel.marginBottom("200px");

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_EXIT);
		button.valignCenter();
		button.interactOnClick(METHOD_EXIT);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_DELETE);
		button.valignCenter();
		button.interactOnClick(METHOD_DELETE_SHIP);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_CREATE);
		button.valignCenter();
		button.interactOnClick(METHOD_CREATE_SHIP);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 8, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_ENTER);
		button.valignCenter();
		button.interactOnClick(METHOD_SELECTED_SHIP);

		panel.control(button);
	}

	/**
	 * Формирование информации ангара.
	 */
	private void buildHangarInfo() {
		final LayerBuilder layer = new LayerBuilder(ElementId.getNextElementId());
		layer.childLayoutCenter();
		layer(layer);

		final PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Center, "100%", "40px");
		panel.valignTop();

		layer.panel(panel);

		final LabelBuilder label = ElementFactory.getLabelBuilder(ElementId.HANGAR_SERVER_LABEL_INFO, Strings.EMPTY, "50%", "30px");
		label.alignRight();
		label.valignCenter();
		label.textHAlignRight();

		panel.control(label);
	}

	/**
	 * Формирование панели сообщений.
	 */
	private void buildWarningPanel() {
		final LayerBuilder layer = new LayerBuilder(ElementId.HANGAR_WARNING_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		final PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "440px", "90px");
		panel.alignCenter();
		panel.valignCenter();

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 10));
		panel.control(ElementFactory.getLabelBuilder(ElementId.HANGAR_WARNING_LABEL.getId(), Strings.EMPTY, "90%", "30px"));

		final ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_HANGAR_BUTTON_WARNING);
		button.alignCenter();
		button.interactOnClick(METHOD_WARNING_CANCEL);

		panel.control(button);
	}
}
