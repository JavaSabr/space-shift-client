package com.ss.client.gui.builder;

import rlib.util.Strings;

import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.GameConfig;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.ImageId;
import com.ss.client.gui.StyleIds;
import com.ss.client.gui.control.DropDownControl;
import com.ss.client.gui.controller.LoginController;
import com.ss.client.gui.model.ScreenSize;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.checkbox.builder.CheckboxBuilder;
import de.lessvoid.nifty.controls.dropdown.builder.DropDownBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.slider.builder.SliderBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;

/**
 * Конструктор меню авторизации.
 * 
 * @author Ronn
 */
public class LoginBuilder extends ScreenBuilder {

	public static final int DEFAULT_AUTH_BUTTON_PANEL_HEIGHT = 33;
	public static final int DEFAULT_AUTH_BUTTON_PANEL_WIDTH = 240;
	public static final int DEFAULT_AUTH_TEXT_FIELD_HEIGHT = 24;
	public static final int DEFAULT_AUTH_TEXT_FIELD_WIDTH = 153;
	public static final int DEFAULT_AUTH_LABEL_WIDTH = 100;

	public static final int DEFAULT_BUTTON_LOGIN_MARGIN = 1;
	public static final int DEFAULT_BUTTON_LOGIN_HEIGHT = 28;
	public static final int DEFAULT_BUTTON_ABOUT_WIDTH = 47;
	public static final int DEFAULT_BUTTON_SETTING_WIDTH = 33;
	public static final int DEFAULT_BUTTON_AYTH_WIDTH = 152;

	private static final LangTable LANG_TABLE = LangTable.getInstance();
	private static final Game GAME = Game.getInstance();

	private static final String INTERFACE_LOGIN_BACKGROUND_LABEL = LANG_TABLE.getText("@interface:loginBackgroundLabel@");
	private static final String INTERFACE_LOGIN_LABEL_LOGIN = LANG_TABLE.getText("@interface:loginLabelLogin@");
	private static final String INTERFACE_LOGIN_LABEL_PASSWORD = LANG_TABLE.getText("@interface:loginLabelPassword@");
	private static final String INTERFACE_LOGIN_LABEL_SERVER_LIST = LANG_TABLE.getText("@interface:loginLabelServerList@");
	private static final String INTERFACE_LOGIN_LABEL_EMAIL = LANG_TABLE.getText("@interface:loginLabelEmail@");
	private static final String INTERFACE_LOGIN_BUTTON_APPLY = LANG_TABLE.getText("@interface:loginButtonApply@");
	private static final String INTERFACE_LOGIN_BUTTON_CANCEL = LANG_TABLE.getText("@interface:loginButtonCancel@");
	private static final String INTERFACE_LOGIN_LABEL_EFFECT = LANG_TABLE.getText("@interface:loginLabelEffect@");
	private static final String INTERFACE_LOGIN_LABEL_MUSIC = LANG_TABLE.getText("@interface:loginLabelMusic@");
	private static final String INTERFACE_LOGIN_LABEL_SCREEN = LANG_TABLE.getText("@interface:loginLabelScreen@");
	private static final String INTERFACE_LOGIN_LABEL_LANG = LANG_TABLE.getText("@interface:loginLabelLang@");
	private static final String INTERFACE_LOGIN_BUTTON_REGISTER = LANG_TABLE.getText("@interface:loginButtonRegister@");
	private static final String INTERFACE_LOGIN_BUTTON_AUTH = LANG_TABLE.getText("@interface:loginButtonAuth@");
	private static final String INTERFACE_LOGIN_BUTTON_WARNING = LANG_TABLE.getText("@interface:loginButtonWarning@");

	private static final String METHOD_WARNING_CANCEL = "warningCancel()";
	private static final String METHOD_REGISTER_APPLY = "registerApply()";
	private static final String METHOD_REGISTER_CANCEL = "registerCancel()";
	private static final String METHOD_CONFIG_APPLY = "configApply()";
	private static final String METHOD_CONFIG_CANCEL = "configCancel()";
	private static final String METHOD_CONFIG = "config()";
	private static final String METHOD_REGISTER = "register()";
	private static final String METHOD_AUTH = "auth()";

	public LoginBuilder() {
		super(ElementId.LOGIN_SCREEN.getId());

		// добавляем прослушивателя действий
		controller(LoginController.getInstance());

		// формируем фон
		buildBackgroundImage();

		// создаем слой для размещения первичных элементов
		final SpaceLayerBuilder layer = new SpaceLayerBuilder(ElementId.LOGIN_AUTH_LAYER);
		layer.childLayout(ChildLayoutType.AbsoluteInside);
		layer(layer);

		// реализуем первичные элементы
		buildAuthPanel(layer);

		buildConfigPanel();
		buildRegisterPanel();
		buildWarningPanel();
	}

	/**
	 * Формирование панели авторизации.
	 */
	private void buildAuthPanel(final SpaceLayerBuilder layer) {

		Camera camera = GAME.getCamera();

		final PanelBuilder panel = ElementFactory.getPanelBuilder(LoginController.AUTH_PARENT_PANEL_ID, ChildLayoutType.Vertical, Strings.EMPTY, "100%", "100%");

		final PanelBuilder authPanel = ElementFactory.getPanelBuilder(LoginController.AUTH_PANEL_ID, ChildLayoutType.Vertical, Strings.EMPTY, "100%", "85px");
		authPanel.align(Align.Center);

		final PanelBuilder loginPanel = ElementFactory.getPanelBuilder(LoginController.AUTH_LOGIN_PANEL, ChildLayoutType.Horizontal, Strings.EMPTY, "100%", "27px");

		int labelWidth = DEFAULT_AUTH_LABEL_WIDTH;
		int textFieldWidth = DEFAULT_AUTH_TEXT_FIELD_WIDTH;
		int textFieldHeight = DEFAULT_AUTH_TEXT_FIELD_HEIGHT;

		LabelBuilder label = ElementFactory.getLabelBuilder(LoginController.AUTH_LOGIN_LABEL_NAME_ID, INTERFACE_LOGIN_LABEL_LOGIN + ":");
		label.textHAlign(Align.Right);
		label.valign(VAlign.Center);
		label.width(labelWidth + "px");
		label.style(StyleIds.LOGIN_PAGE_LABEL);

		TextFieldBuilder textField = ElementFactory.getTextFieldBuilder(LoginController.AUTH_LOGIN_FIELD_NAME_ID);
		textField.valign(VAlign.Center);
		textField.marginLeft("5px");
		textField.marginRight("9px");
		textField.width(textFieldWidth + "px");
		textField.height(textFieldHeight + "px");
		textField.style(StyleIds.GENERAL_TEXT_FIELD);

		LabelBuilder registerLabel = ElementFactory.getLabelBuilder(LoginController.AUTH_LINK_REGISTER_ID, INTERFACE_LOGIN_BUTTON_REGISTER);
		registerLabel.textHAlign(Align.Left);
		registerLabel.valign(VAlign.Center);
		registerLabel.width("100px");
		registerLabel.interactOnClick(METHOD_REGISTER);
		registerLabel.style(StyleIds.LOGIN_PAGE_LABEL_LINK);

		loginPanel.panel(ElementFactory.getPanelBuilder(LoginController.BUFFER_PANEL_ID, Strings.EMPTY, ((camera.getWidth() / 2) - 180), 35));
		loginPanel.control(label);
		loginPanel.control(textField);
		loginPanel.control(registerLabel);

		final PanelBuilder passwordPanel = ElementFactory.getPanelBuilder(LoginController.AUTH_PASSWORD_PANEL_ID, ChildLayoutType.Horizontal, Strings.EMPTY, "100%", "27px");

		label = ElementFactory.getLabelBuilder(LoginController.AUTH_LOGIN_LABEL_PASSWORD_ID, INTERFACE_LOGIN_LABEL_PASSWORD + ":");
		label.textHAlign(Align.Right);
		label.valign(VAlign.Center);
		label.width(labelWidth + "px");
		label.style(StyleIds.LOGIN_PAGE_LABEL);

		textField = ElementFactory.getTextFieldBuilder(LoginController.AUTH_LOGIN_FIELD_PASSWORD_ID);
		textField.valign(VAlign.Center);
		textField.marginLeft("5px");
		textField.marginRight("9px");
		textField.width(textFieldWidth + "px");
		textField.height(textFieldHeight + "px");
		textField.style(StyleIds.GENERAL_TEXT_FIELD);
		textField.passwordChar('*');

		CheckboxBuilder checkbox = new CheckboxBuilder("sfwef");
		checkbox.marginRight("5px");
		checkbox.width("17px");
		checkbox.height("17px");
		checkbox.style(StyleIds.GENERAL_CHECK_BOX);
		checkbox.valign(VAlign.Center);

		LabelBuilder savePasswordLabel = ElementFactory.getLabelBuilder(LoginController.AUTH_LINK_REGISTER_ID, "Сохранить пароль");
		savePasswordLabel.textHAlign(Align.Left);
		savePasswordLabel.valign(VAlign.Center);
		savePasswordLabel.width("200px");
		savePasswordLabel.interactOnClick(METHOD_REGISTER);
		savePasswordLabel.style(StyleIds.LOGIN_PAGE_LABEL);

		passwordPanel.panel(ElementFactory.getPanelBuilder(LoginController.BUFFER_PANEL_ID, Strings.EMPTY, ((camera.getWidth() / 2) - 180), 35));
		passwordPanel.control(label);
		passwordPanel.control(textField);
		passwordPanel.control(checkbox);
		passwordPanel.control(savePasswordLabel);

		final PanelBuilder serverListPanel = ElementFactory.getPanelBuilder(LoginController.AUTH_PASSWORD_PANEL_ID, ChildLayoutType.Horizontal, Strings.EMPTY, "100%", "27px");

		label = ElementFactory.getLabelBuilder(LoginController.AUTH_LOGIN_LABEL_SERVER_LIST_ID, INTERFACE_LOGIN_LABEL_SERVER_LIST + ":");
		label.textHAlign(Align.Right);
		label.valign(VAlign.Center);
		label.width(labelWidth + "px");
		label.style(StyleIds.LOGIN_PAGE_LABEL);

		DropDownBuilder dropDown = ElementFactory.getDropDownBuilder(LoginController.AUTH_LOGIN_DROP_DOWN_SERVER_LIST_ID, textFieldWidth + "px", textFieldHeight + "px");
		dropDown.valign(VAlign.Center);
		dropDown.marginLeft("5px");
		dropDown.marginRight("9px");
		dropDown.set(DropDownControl.ATTRIBUTE_VERTICAL_SCROLL, DropDownControl.SCROLL_OPTIONAL);
		dropDown.set(DropDownControl.ATTRIBUTE_HORIZONTAL_SCROLL, DropDownControl.SCROLL_OPTIONAL);
		dropDown.style(StyleIds.GENERAL_DROP_DOWN);

		serverListPanel.panel(ElementFactory.getPanelBuilder(LoginController.BUFFER_PANEL_ID, Strings.EMPTY, ((camera.getWidth() / 2) - 180), 35));
		serverListPanel.control(label);
		serverListPanel.control(dropDown);

		authPanel.panel(loginPanel);
		authPanel.panel(passwordPanel);
		authPanel.panel(serverListPanel);

		panel.panel(ElementFactory.getPanelBuilder(LoginController.BUFFER_PANEL_ID, Strings.EMPTY, 60, (int) (camera.getHeight() * 0.7)));
		panel.panel(authPanel);
		panel.panel(buildButtonPanel());

		layer.panel(panel);
	}

	/**
	 * Формирование фонового изображения.
	 */
	private void buildBackgroundImage() {

		final LayerBuilder layer = new LayerBuilder(LoginController.BACKGROUND_LAYER_ID);
		layer.childLayoutAbsoluteInside();
		layer(layer);

		final ScreenSize screenSize = GameConfig.SCREEN_SIZE;

		SpaceImageBuilder image = new SpaceImageBuilder(LoginController.BACKGROUND_IMAGE_ID);
		image.filename(ImageId.getLoginBackground());
		image.width(screenSize.getWidth() + "px");
		image.height(screenSize.getHeight() + "px");

		layer.image(image);

		int x = screenSize.getWidth() / 2 - 430;
		int y = screenSize.getHeight() / 2 - 120;

		image = new SpaceImageBuilder(LoginController.BACKGROUND_LOGO_ID);
		image.filename(ImageId.LOGIN_PAGE_LOGO);
		image.width("349px");
		image.height("67px");
		image.x(x + "px");
		image.y(y + "px");

		LabelBuilder label = ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_BACKGROUND_LABEL);
		label.align(Align.Center);
		label.style(StyleIds.LOGIN_PAGE_LABEL);

		PanelBuilder labelPanel = ElementFactory.getPanelBuilder(LoginController.BACKGROUND_LABEL_PANEL_ID, ChildLayoutType.Vertical, Strings.EMPTY, "100%", "100%");
		labelPanel.panel(ElementFactory.getEmptyPanel(1, screenSize.getHeight() - 30));
		labelPanel.control(label);

		layer.image(image);
		layer.panel(labelPanel);
	}

	/**
	 * @return построение панели с главными кнопками логина.
	 */
	private PanelBuilder buildButtonPanel() {

		Camera camera = GAME.getCamera();

		int authButtonWidth = DEFAULT_BUTTON_AYTH_WIDTH;
		int settingButtonWidth = DEFAULT_BUTTON_SETTING_WIDTH;
		int aboutButtonWidth = DEFAULT_BUTTON_ABOUT_WIDTH;

		int buttonHeight = DEFAULT_BUTTON_LOGIN_HEIGHT;
		int buttonMargin = DEFAULT_BUTTON_LOGIN_MARGIN;

		int offset = authButtonWidth + settingButtonWidth + aboutButtonWidth + buttonMargin * 2;
		offset = camera.getWidth() / 2 - offset / 2;

		int buttonPanelWidth = DEFAULT_AUTH_BUTTON_PANEL_WIDTH;
		int buttonPanelHeight = DEFAULT_AUTH_BUTTON_PANEL_HEIGHT;

		final PanelBuilder buttonPanel = ElementFactory.getPanelBuilder(LoginController.AUTH_BUTTON_PANEL_ID, ChildLayoutType.Horizontal, Strings.EMPTY, buttonPanelWidth + "px", buttonPanelHeight
				+ "px");
		buttonPanel.backgroundImage(ImageId.LOGIN_PAGE_BUTTON_PANEL.getPath());

		ButtonBuilder button = ElementFactory.getButtonBuild(LoginController.AUTH_BUTTON_LOGIN_ID, INTERFACE_LOGIN_BUTTON_AUTH);
		button.width(authButtonWidth + "px");
		button.height(buttonHeight + "px");
		button.marginRight(buttonMargin + "px");
		button.marginTop("1px");
		button.valign(VAlign.Center);
		button.style(StyleIds.LOGIN_PAGE_BUTTON_AUTH);
		button.interactOnClick(METHOD_AUTH);

		buttonPanel.control(button);

		button = ElementFactory.getButtonBuild(LoginController.AUTH_BUTTON_SETTINGS_ID, Strings.EMPTY);
		button.width(settingButtonWidth + "px");
		button.height(buttonHeight + "px");
		button.marginRight(buttonMargin + "px");
		button.marginTop("1px");
		button.valign(VAlign.Center);
		button.style(StyleIds.LOGIN_PAGE_BUTTON_SETTING);
		button.interactOnClick(METHOD_CONFIG);

		buttonPanel.control(button);

		button = ElementFactory.getButtonBuild(LoginController.AUTH_BUTTON_ABOUT_ID, Strings.EMPTY);
		button.width(aboutButtonWidth + "px");
		button.height(buttonHeight + "px");
		button.marginTop("1px");
		button.valign(VAlign.Center);
		button.style(StyleIds.LOGIN_PAGE_BUTTON_ABOUT);
		// button.interactOnClick(METHOD_AUTH);

		buttonPanel.control(button);

		final PanelBuilder buttonContainer = ElementFactory.getPanelBuilder(LoginController.BUFFER_PANEL_ID, ChildLayoutType.Horizontal, Strings.EMPTY, "100%", buttonPanelHeight + "px");
		buttonContainer.panel(ElementFactory.getEmptyPanel(offset, 1));
		buttonContainer.panel(buttonPanel);

		return buttonContainer;
	}

	/**
	 * Формирование панели настроек.
	 */
	private void buildConfigPanel() {

		final LayerBuilder layer = new LayerBuilder(ElementId.LOGIN_CONFIG_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "300px", "260px");
		panel.alignCenter();
		panel.valignCenter();

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 10));
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_LANG));

		DropDownBuilder dropDown = ElementFactory.getDropDownBuilder(ElementId.LOGIN_CONFIG_DROP_DOWN_LANG, "90%", null);
		dropDown.alignCenter();

		panel.control(dropDown);
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_SCREEN));

		dropDown = ElementFactory.getDropDownBuilder(ElementId.LOGIN_CONFIG_DROP_DOWN_SCREEN, "90%", null);
		dropDown.alignCenter();

		panel.control(dropDown);
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_MUSIC));

		SliderBuilder slider = new SliderBuilder(ElementId.LOGIN_CONFIG_SLIDER_MUSIC.getId(), false);
		slider.width("90%");
		slider.alignCenter();

		panel.control(slider);
		panel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_EFFECT));

		slider = new SliderBuilder(ElementId.LOGIN_CONFIG_SLIDER_EFFECT.getId(), false);
		slider.width("90%");
		slider.alignCenter();

		panel.control(slider);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 10));

		final PanelBuilder bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, Strings.EMPTY, "83%", "40px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;

		ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_LOGIN_BUTTON_CANCEL);
		button.valignCenter();
		button.interactOnClick(METHOD_CONFIG_CANCEL);

		panel.control(button);
		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 50, 0));

		button = ElementFactory.getButtonBuild(ElementId.getNextElementId(), INTERFACE_LOGIN_BUTTON_APPLY);
		button.valignCenter();
		button.interactOnClick(METHOD_CONFIG_APPLY);

		panel.control(button);
	}

	/**
	 * Формирование панели регистрации.
	 */
	private void buildRegisterPanel() {
		final LayerBuilder layer = new LayerBuilder(ElementId.LOGIN_REGISTER_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "300px", "265px");
		panel.alignCenter();
		panel.valignCenter();

		layer.panel(panel);

		PanelBuilder bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, Strings.EMPTY, "100%", "190px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);

		bufferPanel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 10));
		bufferPanel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_LOGIN, 150, 30));

		TextFieldBuilder textField = ElementFactory.getTextFieldBuilder(ElementId.LOGIN_REGISTER_FIELD_LOGIN);
		textField.alignCenter();

		bufferPanel.control(textField);
		bufferPanel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_PASSWORD, 150, 30));

		textField = ElementFactory.getTextFieldBuilder(ElementId.LOGIN_REGISTER_FIELD_PASSWORD);
		textField.alignCenter();
		textField.passwordChar('*');

		bufferPanel.control(textField);
		bufferPanel.control(ElementFactory.getLabelBuilder(ElementId.getNextElementId(), INTERFACE_LOGIN_LABEL_EMAIL, 150, 30));

		textField = ElementFactory.getTextFieldBuilder(ElementId.LOGIN_REGISTER_FIELD_EMAIL);
		textField.alignCenter();

		bufferPanel.control(textField);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 15));

		bufferPanel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Horizontal, Strings.EMPTY, "80%", "40px");
		bufferPanel.alignCenter();

		panel.panel(bufferPanel);
		panel = bufferPanel;

		ButtonBuilder button = new ButtonBuilder(ElementId.LOGIN_REGISTER_BUTTON_CANCEL.getId(), INTERFACE_LOGIN_BUTTON_CANCEL);
		button.valignCenter();
		button.interactOnClick(METHOD_REGISTER_CANCEL);
		button.height("30px");

		bufferPanel.control(button);

		bufferPanel = new PanelBuilder(ElementId.getNextElementId());
		bufferPanel.width("40px");

		panel.panel(bufferPanel);

		button = new ButtonBuilder(ElementId.LOGIN_REGISTER_BUTTON_REGISTER.getId(), INTERFACE_LOGIN_BUTTON_REGISTER);
		button.valignCenter();
		button.interactOnClick(METHOD_REGISTER_APPLY);
		button.height("30px");

		panel.control(button);
	}

	/**
	 * Формируем панель для отображения ошибок.
	 */
	private void buildWarningPanel() {
		final LayerBuilder layer = new LayerBuilder(ElementId.LOGIN_WARNING_LAYER.getId());
		layer.childLayoutCenter();
		layer(layer);

		final PanelBuilder panel = ElementFactory.getPanelBuilder(ElementId.getNextElementId(), ChildLayoutType.Vertical, "440px", "90px");
		panel.alignCenter();
		panel.valignCenter();

		layer.panel(panel);

		panel.panel(ElementFactory.getPanelBuilder(ElementId.getNextElementId(), Strings.EMPTY, 0, 10));
		panel.control(ElementFactory.getLabelBuilder(ElementId.LOGIN_WARNING_LABEL, Strings.EMPTY, "90%", "30px"));

		final ButtonBuilder button = ElementFactory.getButtonBuild(ElementId.LOGIN_WARNING_BUTTON, INTERFACE_LOGIN_BUTTON_WARNING);
		button.alignCenter();
		button.interactOnClick(METHOD_WARNING_CANCEL);

		panel.control(button);
	}
}
