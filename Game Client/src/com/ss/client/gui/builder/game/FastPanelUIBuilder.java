package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.panel.fast.FastPanelUIController;
import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DroppableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;

/**
 * Конструктор панели быстрого доступа.
 * 
 * @author Ronn
 */
public class FastPanelUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		ImageUIBuilder background = new ImageUIBuilder(FastPanelUIController.BACKGROUND_ID);
		background.setPath(FastPanelUIController.BACKGROUND_IMAGE);
		background.setAlign(Align.Right);
		background.setWidth("161px");
		background.setHeight("46px");

		DraggableElementUIBuilder fastPanel = new DraggableElementUIBuilder(FastPanelUIController.PANEL_ID);
		fastPanel.setController(FastPanelUIController.class);
		fastPanel.setHeight("50px");
		fastPanel.setWidth("380px");
		fastPanel.add(background);
		fastPanel.add(buildContent());
		fastPanel.buildTo(layer);
	}

	private static ElementUIBuilder buildCell(String keyText, String cellIcon, int order) {

		DroppableElementUIBuilder container = new DroppableElementUIBuilder(FastPanelUIController.CELL_CONTAINER_ID);
		container.setLayoutType(ChildLayoutType.Center);
		container.setWidth("34px");
		container.setHeight("34px");

		PanelUIBuilder cell = new PanelUIBuilder(FastPanelUIController.CELL_PANEL_ID + order);
		cell.setBackgroundImage(FastPanelUIController.CELL_BACKGROUND_IMAGE);
		cell.setLayoutType(ChildLayoutType.Center);
		cell.setMarginLeft("2px");
		cell.setHeight("34px");
		cell.setWidth("34px");

		if(cellIcon != null) {

			ImageUIBuilder image = new ImageUIBuilder(FastPanelUIController.CELL_KEY_ICON_ID);
			image.setWidth("30px");
			image.setHeight("28px");
			image.setPath(cellIcon);

			cell.add(image);
		}

		cell.add(container);

		if(keyText != null) {

			LabelUIBuilder keyLabel = new LabelUIBuilder(FastPanelUIController.CELL_KEY_LABEL_ID);
			keyLabel.setColor(FastPanelUIController.TEXT_COLOR);
			keyLabel.setTextHAlign(Align.Left);
			keyLabel.setTextVAlign(VAlign.Top);
			keyLabel.setVisibleToMouse(false);
			keyLabel.setMarginLeft("3px");
			keyLabel.setWidth("100%");
			keyLabel.setHeight("100%");
			keyLabel.setText(keyText);

			cell.add(keyLabel);
		}

		return cell;
	}

	private static PanelUIBuilder buildContent() {

		PanelUIBuilder cellPanel = new PanelUIBuilder(FastPanelUIController.CELL_PANEL_ID);
		cellPanel.setLayoutType(ChildLayoutType.Horizontal);

		ImageUIBuilder split = new ImageUIBuilder(FastPanelUIController.SPLIT_CELLS_ID);
		split.setPath(FastPanelUIController.SPLIT_IMAGE);
		split.setHeight("15px");
		split.setWidth("3px");
		split.setMargin("0,2,0,4");

		cellPanel.add(buildCell(null, null, 0));
		cellPanel.add(buildCell(null, null, 1));
		cellPanel.add(split);

		for(int i = 2, length = 10; i < length; i++) {
			cellPanel.add(buildCell(String.valueOf(i - 1), null, i));
		}

		return cellPanel;
	}
}
