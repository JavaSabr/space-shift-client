package com.ss.client.gui.builder.game;

import static com.ss.client.gui.controller.game.hud.indicator.IndicatorsController.BASE_PANEL_ID;
import static com.ss.client.gui.controller.game.hud.indicator.IndicatorsController.IMAGE_ID;
import static com.ss.client.gui.controller.game.hud.indicator.IndicatorsController.OBJECT_INDICATOR_ID;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.hud.indicator.IndicatorController;
import com.ss.client.gui.controller.game.hud.indicator.IndicatorsController;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.model.FriendStatus;
import com.ss.client.model.SpaceObject;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Реализация конструктора индикаторов объектов.
 * 
 * @author Ronn
 */
public class IndicatorUIBuilder {

	private static final Table<FriendStatus, String> PATH_CACHE = Tables.newObjectTable();

	public static void build(SpaceLayerBuilder layer) {
		PanelUIBuilder builder = new PanelUIBuilder(BASE_PANEL_ID);
		builder.setController(IndicatorsController.class);
		builder.setLayoutType(ChildLayoutType.AbsoluteInside);
		builder.buildTo(layer);
	}

	/**
	 * Построение пути к иконке индикатора.
	 */
	public static synchronized String buildImagePath(FriendStatus friendStatus) {

		String result = PATH_CACHE.get(friendStatus);

		if(result == null) {
			result = "game/hud/indicator/indicator_" + friendStatus + ".png";
			PATH_CACHE.put(friendStatus, result);
		}

		return result;
	}

	/**
	 * Создание нового элемента индикатора.
	 * 
	 * @return элемент индикатора.
	 */
	public static Element buildIndicator(Nifty nifty, Screen screen, Element parent, SpaceObject object, int index) {

		ImageUIBuilder image = new ImageUIBuilder(IMAGE_ID);
		image.setPath(buildImagePath(object.getFriendStatus()));
		image.setWidth(IndicatorsController.INDICATOR_WIDTH);
		image.setHeight(IndicatorsController.INDICATOR_HEIGHT);

		PanelUIBuilder builder = new PanelUIBuilder(OBJECT_INDICATOR_ID + index);
		builder.setController(IndicatorController.class);
		builder.setWidth(IndicatorsController.INDICATOR_WIDTH);
		builder.setHeight(IndicatorsController.INDICATOR_HEIGHT);
		builder.setVisibleToMouse(true);
		builder.add(image);

		return builder.build(nifty, screen, parent);
	}
}
