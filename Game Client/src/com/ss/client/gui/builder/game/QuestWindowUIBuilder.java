package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.quest.QuestStructContainerController;
import com.ss.client.gui.controller.game.window.quest.QuestDetailsElementController;
import com.ss.client.gui.controller.game.window.quest.QuestListElementUIFactory;
import com.ss.client.gui.controller.game.window.quest.QuestWindowTextIds;
import com.ss.client.gui.controller.game.window.quest.QuestWindowUIController;
import com.ss.client.gui.controller.game.window.ship.ShipWindowUIController;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.ListUIBuilder;
import com.ss.client.gui.element.builder.impl.MessageElementUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.tools.Color;

/**
 * Конструктор журнала задач.
 * 
 * @author Ronn
 */
public class QuestWindowUIBuilder {

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static void build(final SpaceLayerBuilder layer) {

		DraggableElementUIBuilder builder = new DraggableElementUIBuilder(QuestWindowUIController.WINDOW_ID);
		builder.setController(QuestWindowUIController.class);
		builder.setLayoutType(ChildLayoutType.Vertical);
		builder.setBackgroundColor(Color.randomColor());
		builder.setWidth("700px");
		builder.setHeight("500px");
		builder.add(buildHeader());
		builder.add(buildContent());
		builder.setVisible(false);
		builder.buildTo(layer);
	}

	/**
	 * Конструирование контейнер панели для элементов задания.
	 */
	protected static PanelUIBuilder buildContainer(String containerId, String text) {

		PanelUIBuilder container = new PanelUIBuilder(containerId);
		container.setController(QuestStructContainerController.class);
		container.setBackgroundColor(Color.randomColor());
		container.setLayoutType(ChildLayoutType.Vertical);
		container.setWidth("420px");
		container.setHeight("30px");
		container.setMargin("5,0,5,0");
		container.setVisible(false);

		LabelUIBuilder title = new LabelUIBuilder(QuestStructContainerController.QUEST_CONTAINER_TITLE_ID);
		title.setHeight("20px");
		title.setText(text);

		container.add(title);
		return container;
	}

	private static PanelUIBuilder buildContent() {

		ListUIBuilder questList = new ListUIBuilder(QuestWindowUIController.QUEST_LIST_ID);
		questList.setItemFactory(QuestListElementUIFactory.class);
		questList.setWidth("210px");

		PanelUIBuilder content = new PanelUIBuilder(ShipWindowUIController.CONTENT_ID);
		content.setBackgroundColor(Color.randomColor());
		content.setLayoutType(ChildLayoutType.Horizontal);
		content.setHeight("*");
		content.add(questList);
		content.add(buildQuestDetails());

		return content;
	}

	private static PanelUIBuilder buildHeader() {

		LabelUIBuilder close = new LabelUIBuilder(QuestWindowUIController.CLOSE_ID);
		close.setWidth("90px");
		close.setText("Закрыть");
		close.setAlign(Align.Right);

		LabelUIBuilder title = new LabelUIBuilder(QuestWindowUIController.TITLE_ID);
		title.setLayoutType(ChildLayoutType.Center);
		title.setText(LANG_TABLE.getText(QuestWindowTextIds.WINDOW_TITLE));
		title.add(close);

		PanelUIBuilder header = new PanelUIBuilder(QuestWindowUIController.HEADER_ID);
		header.setBackgroundColor(Color.randomColor());
		header.setHeight("30px");
		header.add(title);
		return header;
	}

	/**
	 * Построение панели с информацией о задании.
	 */
	protected static PanelUIBuilder buildQuestDetails() {

		LabelUIBuilder questStatus = new LabelUIBuilder(QuestDetailsElementController.QUEST_STATUS_ID);
		questStatus.setHeight("20px");
		questStatus.setTextHAlign(Align.Right);

		LabelUIBuilder questName = new LabelUIBuilder(QuestDetailsElementController.QUEST_NAME_ID);
		questName.setLayoutType(ChildLayoutType.Center);
		questName.setHeight("20px");
		questName.setTextHAlign(Align.Left);
		questName.setWidth("420px");
		questName.setMargin("5,0,5,0");
		questName.add(questStatus);

		MessageElementUIBuilder description = new MessageElementUIBuilder(QuestDetailsElementController.QUEST_DESCRIPTION_ID);
		description.setHeight("20px");
		description.setTextHAlign(Align.Left);
		description.setMargin("5,0,5,0");
		description.setWidth("410px");
		description.setBackgroundColor(Color.randomColor());

		PanelUIBuilder descriptionContainer = buildContainer(QuestDetailsElementController.QUEST_DESCRIPTION_CONTAINER_ID, LANG_TABLE.getText(QuestWindowTextIds.DESCRIPTION_CONTAINER_TITLE));
		descriptionContainer.add(description);

		PanelUIBuilder detail = new PanelUIBuilder(QuestWindowUIController.QUEST_DETAILS_ID);
		detail.setController(QuestDetailsElementController.class);
		detail.setLayoutType(ChildLayoutType.Vertical);
		detail.setWidth("*");
		detail.setLayoutInvisibleElements(false);
		detail.add(questName);
		detail.add(descriptionContainer);
		detail.add(buildContainer(QuestDetailsElementController.QUEST_REWARD_CONTAINER_ID, LANG_TABLE.getText(QuestWindowTextIds.REWARDS_CONTAINER_TITLE)));
		detail.add(buildContainer(QuestDetailsElementController.QUEST_COUNTER_CONTAINER_ID, LANG_TABLE.getText(QuestWindowTextIds.COUNTERS_CONTAINER_TITLE)));
		detail.add(buildContainer(QuestDetailsElementController.QUEST_BUTTON_CONTAINER_ID, LANG_TABLE.getText(QuestWindowTextIds.BUTTONS_CONTAINER_TITLE)));

		return detail;
	}
}
