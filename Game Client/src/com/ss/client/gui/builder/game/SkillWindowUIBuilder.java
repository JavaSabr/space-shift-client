package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.window.skills.SkillCellUIController;
import com.ss.client.gui.controller.game.window.skills.SkillWindowUIController;
import com.ss.client.gui.controller.game.window.skills.SkillWindowTextIds;
import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DroppableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;

/**
 * Реализация конструктора окна умений.
 * 
 * @author Ronn
 */
public class SkillWindowUIBuilder {

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static void build(final SpaceLayerBuilder layer) {

		ImageUIBuilder line = new ImageUIBuilder(SkillWindowUIController.WINDOW_LINE_ID);
		line.setPath(SkillWindowUIController.WINDOW_LINE_IMAGE);
		line.setWidth("236px");
		line.setHeight("1px");
		line.setMarginLeft("-27px");
		line.setMarginTop("4px");
		line.setMarginBottom("12px");

		DraggableElementUIBuilder window = new DraggableElementUIBuilder(SkillWindowUIController.WINDOW_ID);
		window.setBackgroundImage(SkillWindowUIController.WINDOW_BACKGROUND_IMAGE);
		window.setController(SkillWindowUIController.class);
		window.setLayoutType(ChildLayoutType.Vertical);
		window.setWidth("357px");
		window.setHeight("198px");
		window.add(buildHeader());
		window.add(line);
		window.add(buildContent());
		window.setVisible(false);
		window.buildTo(layer);
	}

	private static ElementUIBuilder buildCell(int order) {

		DroppableElementUIBuilder container = new DroppableElementUIBuilder(SkillWindowUIController.WINDOW_CELL_ID + order);
		container.setController(SkillCellUIController.class);
		container.setBackgroundImage(SkillWindowUIController.WINDOW_CELL_IMAGE);
		container.setLayoutType(ChildLayoutType.Center);
		container.setWidth("34px");
		container.setHeight("34px");
		container.setMargin("0,2,0,2");

		return container;
	}

	/**
	 * @return панель с ячейками.
	 */
	private static PanelUIBuilder buildContent() {

		PanelUIBuilder contentPanel = new PanelUIBuilder(SkillWindowUIController.WINDOW_CONTENT_ID);
		contentPanel.setLayoutType(ChildLayoutType.Vertical);
		contentPanel.setWidth("320px");
		contentPanel.setHeight("150px");

		for(int i = 0, length = 3; i < length; i++) {

			PanelUIBuilder row = new PanelUIBuilder(SkillWindowUIController.WINDOW_CELL_ROW_ID + i);
			row.setLayoutType(ChildLayoutType.Horizontal);
			row.setMarginLeft("4px");
			row.setHeight("38px");
			row.setWidth("304px");

			for(int g = 0, count = 8; g < count; g++) {
				row.add(buildCell((count * i) + g));
			}

			contentPanel.add(row);
		}

		return contentPanel;
	}

	/**
	 * @return шапка окна.
	 */
	private static PanelUIBuilder buildHeader() {

		LabelUIBuilder title = new LabelUIBuilder(SkillWindowUIController.WINDOW_TITLE_ID);
		title.setText(LANG_TABLE.getText(SkillWindowTextIds.WINDOW_TITLE));
		title.setTextHAlign(Align.Left);
		title.setWidth("150px");

		ImageUIBuilder close = new ImageUIBuilder(SkillWindowUIController.WINDOW_BUTTON_CLOSE_ID);
		close.setPath(SkillWindowUIController.WINDOW_CLOSE_BUTTON_IMAGE);
		close.setVisibleToMouse(true);
		close.setMarginLeft("135px");
		close.setWidth("25px");
		close.setHeight("25px");

		PanelUIBuilder header = new PanelUIBuilder(SkillWindowUIController.WINDOW_HEADER_ID);
		header.setLayoutType(ChildLayoutType.Horizontal);
		header.setWidth("290px");
		header.setHeight("25px");
		header.setMarginTop("15px");
		header.add(title);
		header.add(close);

		return header;
	}
}
