package com.ss.client.gui.builder.game.factory;

import com.ss.client.gui.FontIds;
import com.ss.client.gui.controller.game.item.ItemDescriptionBuilder;
import com.ss.client.gui.controller.game.item.ItemDescriptionEffect;
import com.ss.client.gui.controller.game.item.ItemElementUIController;
import com.ss.client.gui.element.DynamicPanelUI;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DynamicPanelUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.MessageElementUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.template.item.ItemTemplate;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Фабрика элементов UI предметов.
 * 
 * @author Ronn
 */
public class ItemElementUIFactory {

	private static final ItemDescriptionBuilder DESCRIPTION_BUILDER = new ItemDescriptionBuilder();

	public static ItemElementUIController build(SpaceItem item, Element parent, Screen screen, Nifty nifty) {

		ItemTemplate template = item.getTemplate();

		ImageUIBuilder image = new ImageUIBuilder(ItemElementUIController.IMAGE_ID);
		image.setPath(template.getIcon());

		LabelUIBuilder count = new LabelUIBuilder(ItemElementUIController.LABEL_COUNT_ID);
		count.setTextHAlign(Align.Right);
		count.setTextVAlign(VAlign.Bottom);
		count.setValign(VAlign.Bottom);
		count.setFont(FontIds.MAIN_14);
		count.setHeight("32px");
		count.setWrap(true);

		DraggableElementUIBuilder builder = new DraggableElementUIBuilder(ItemElementUIController.ID + item.getObjectId());
		builder.setLayoutType(ChildLayoutType.Center);
		builder.setController(ItemElementUIController.class);
		builder.setDoppable(true);
		builder.setWidth("32px");
		builder.setHeight("32px");
		builder.add(image);
		builder.add(count);

		DESCRIPTION_BUILDER.addTo(builder, EffectEventId.onHover);

		Element result = builder.build(nifty, screen, parent);

		ItemElementUIController controller = result.getControl(ItemElementUIController.class);
		controller.updateCount(item.getItemCount());
		controller.setItem(item);
		return controller;
	}

	public static Element build(Nifty nifty, Screen screen, Element parent, Element activator, EffectProperties properties) {

		ItemElementUIController controller = activator.getControl(ItemElementUIController.class);

		SpaceItem item = controller.getItem();
		ItemTemplate template = item.getTemplate();

		MessageElementUIBuilder name = new MessageElementUIBuilder(ItemDescriptionEffect.NAME_ID);
		name.setText(template.getName() + "(" + item.getItemCount() + ")");
		name.setMarginBottom("2px");
		name.setMarginTop("2px");
		name.setWidth("200px");

		MessageElementUIBuilder description = new MessageElementUIBuilder(ItemDescriptionEffect.DESCRIPTION_ID);
		description.setText(template.getDescription());
		description.setMarginBottom("2px");
		description.setMarginTop("2px");
		description.setWidth("200px");

		PanelUIBuilder builder = new DynamicPanelUIBuilder(activator.getId());
		builder.setBackgroundColor(Color.randomColor());
		builder.setLayoutType(ChildLayoutType.Vertical);
		builder.setWidth("220px");
		builder.setHeight("20px");
		builder.setVisible(false);
		builder.add(name);
		builder.add(description);

		Element element = builder.build(nifty, screen, parent);

		DynamicPanelUI panel = element.getControl(DynamicPanelUI.class);
		panel.updateSize(true, false);

		return element;
	}
}
