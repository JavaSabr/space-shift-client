package com.ss.client.gui.builder.game;

import java.util.concurrent.atomic.AtomicInteger;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.window.storage.ItemCellUIController;
import com.ss.client.gui.controller.game.window.storage.StorageWindowTextIds;
import com.ss.client.gui.controller.game.window.storage.StorageWindowUIController;
import com.ss.client.gui.element.builder.impl.ButtonUIBuilder;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DroppableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;

/**
 * Реализация конструктора окна хранилища.
 * 
 * @author Ronn
 */
public class StorageWindowUIBuilder {

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static void build(final SpaceLayerBuilder layer) {

		PanelUIBuilder contentPanel = new PanelUIBuilder(StorageWindowUIController.WINDOW_CONTENT_ID);
		contentPanel.setLayoutType(ChildLayoutType.Horizontal);
		contentPanel.setWidth("294px");
		contentPanel.setHeight("230px");
		contentPanel.add(buildItemTable());

		ImageUIBuilder line = new ImageUIBuilder(StorageWindowUIController.LINE_ID);
		line.setPath(StorageWindowUIController.WINDOW_LINE_IMAGE);
		line.setWidth("236px");
		line.setHeight("1px");
		line.setMarginTop("2px");
		line.setMarginBottom("2px");
		line.setMarginLeft("-27px");

		DraggableElementUIBuilder window = new DraggableElementUIBuilder(StorageWindowUIController.WINDOW_ID);
		window.setBackgroundImage(StorageWindowUIController.WINDOW_BACKGROUND_IMAGE);
		window.setController(StorageWindowUIController.class);
		window.setLayoutType(ChildLayoutType.Vertical);
		window.setWidth("357px");
		window.setHeight("339px");
		window.add(buildHeader());
		window.add(line);
		window.add(contentPanel);
		window.setVisible(false);
		window.buildTo(layer);
	}

	private static DroppableElementUIBuilder buildCell(AtomicInteger orderCounter) {

		DroppableElementUIBuilder cell = new DroppableElementUIBuilder(StorageWindowUIController.WINDOW_CELL_ID + orderCounter.incrementAndGet());
		cell.setBackgroundImage(StorageWindowUIController.WINDOW_CELL_IMAGE);
		cell.setController(ItemCellUIController.class);
		cell.setMargin("0,1,0,1");
		cell.setWidth("34px");
		cell.setHeight("34px");

		return cell;
	}

	private static PanelUIBuilder buildHeader() {

		LabelUIBuilder title = new LabelUIBuilder(StorageWindowUIController.WINDOW_TITLE_ID);
		title.setText(LANG_TABLE.getText(StorageWindowTextIds.WINDOW_TITLE));
		title.setTextHAlign(Align.Left);
		title.setWidth("200px");

		ImageUIBuilder close = new ImageUIBuilder(StorageWindowUIController.WINDOW_BUTTON_CLOSE_ID);
		close.setPath(StorageWindowUIController.CLOSE_BUTTON_IMAGE);
		close.setVisibleToMouse(true);
		close.setMarginLeft("85px");
		close.setWidth("25px");
		close.setHeight("25px");

		PanelUIBuilder header = new PanelUIBuilder(StorageWindowUIController.WINDOW_HEADER_ID);
		header.setLayoutType(ChildLayoutType.Horizontal);
		header.setWidth("290px");
		header.setHeight("30px");
		header.setMarginTop("12px");
		header.add(title);
		header.add(close);
		return header;
	}

	private static PanelUIBuilder buildItemTable() {

		LabelUIBuilder destroyedLabel = new LabelUIBuilder(StorageWindowUIController.DESTROYED_LABEL_ID);
		destroyedLabel.setText(LANG_TABLE.getText(StorageWindowTextIds.WINDOW_DESTROY_AREA));

		DroppableElementUIBuilder destroyedPanel = new DroppableElementUIBuilder(StorageWindowUIController.WINDOW_DESTROYED_PANEL_ID);
		destroyedPanel.setBackgroundImage(StorageWindowUIController.DESTROY_AREA_IMAGE);
		destroyedPanel.setWidth("106px");
		destroyedPanel.setHeight("27px");
		destroyedPanel.setMarginLeft("6px");
		destroyedPanel.add(destroyedLabel);

		ButtonUIBuilder sortButton = new ButtonUIBuilder(StorageWindowUIController.WINDOW_SORT_BUTTON_ID);
		sortButton.setBackgroundImage(StorageWindowUIController.SORT_BUTTON_IMAGE);
		sortButton.setMarginLeft("150px");
		sortButton.setWidth("27px");
		sortButton.setHeight("27px");

		PanelUIBuilder infoPanel = new PanelUIBuilder(StorageWindowUIController.WINDOW_INFO_PANEL_ID);
		infoPanel.setLayoutType(ChildLayoutType.Horizontal);
		infoPanel.setMarginTop("4px");
		infoPanel.setHeight("36px");
		infoPanel.add(sortButton);
		infoPanel.add(destroyedPanel);

		PanelUIBuilder itemTablePanel = new PanelUIBuilder(StorageWindowUIController.WINDOW_ITEM_TABLE_PANEL_ID);
		itemTablePanel.setLayoutType(ChildLayoutType.Vertical);
		itemTablePanel.setWidth("*");
		itemTablePanel.setMarginTop("10px");

		fillTable(itemTablePanel);

		itemTablePanel.add(infoPanel);
		return itemTablePanel;
	}

	private static void fillTable(PanelUIBuilder itemTablePanel) {

		AtomicInteger orderCounter = new AtomicInteger(0);

		for(int i = 0, rowCount = 6; i < rowCount; i++) {

			PanelUIBuilder rowPanel = new PanelUIBuilder(StorageWindowUIController.WINDOW_STORAGE_COLUMN_ID + i);
			rowPanel.setLayoutType(ChildLayoutType.Horizontal);
			rowPanel.setMarginTop("1px");
			rowPanel.setHeight("36px");

			for(int g = 0, columnCount = 8; g < columnCount; g++) {
				rowPanel.add(buildCell(orderCounter));
			}

			itemTablePanel.add(rowPanel);
		}
	}
}
