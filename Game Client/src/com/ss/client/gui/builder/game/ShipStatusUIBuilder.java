package com.ss.client.gui.builder.game;

import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.CONTROL_PANEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.CONTROL_WIDTH;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENERGY_ICON_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENERGY_INDICATOR_COLOR;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENERGY_INDICATOR_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENERGY_LABEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENGINE_CONTROL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ENGINE_LABEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ICON_PANEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.INDICATOR_PANEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.INDICATOR_WIDTH;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.SHIELD_CONTROL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.SHIELD_LABEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.SHIELD_PANEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.STATE_PANEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.STRENGTH_ICON_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.STRENGTH_INDICATOR_COLOR;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.STRENGTH_INDICATOR_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.STRENGTH_LABEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.WEAPON_CONTROL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.WEAPON_LABEL_ID;
import static com.ss.client.gui.controller.game.panel.status.ShipStatusController.WEAPON_PANEL_ID;
import static de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType.Vertical;
import rlib.util.Strings;

import com.ss.client.GameConfig;
import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.panel.status.ShipStatusController;
import com.ss.client.gui.element.builder.impl.HorizontalRegulatorUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.gui.model.ScreenSize;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Реализация билдера панели статуса корабля.
 * 
 * @author Ronn
 */
public class ShipStatusUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		ScreenSize screenSize = GameConfig.SCREEN_SIZE;

		int height = 130;
		int width = 300;

		PanelUIBuilder bufferPanel = new PanelUIBuilder(ShipStatusController.BUFFER_PANEL_ID);
		bufferPanel.setWidth("1px");
		bufferPanel.setHeight((screenSize.getHeight() - height) + "px");

		HorizontalRegulatorUIBuilder weaponRegulator = new HorizontalRegulatorUIBuilder(WEAPON_CONTROL_ID);
		weaponRegulator.setButtonSubImage(ShipStatusController.IMAGE_REGULATOR_SUB);
		weaponRegulator.setButtonAddImage(ShipStatusController.IMAGE_REGULATOR_ADD);
		weaponRegulator.setBasePanelWidth(ShipStatusController.REGULATOR_PANEL_WIDTH);
		weaponRegulator.setBasePanelHeight(ShipStatusController.REGULATOR_PANEL_HEIGHT);
		weaponRegulator.setBasePanelColor(ShipStatusController.REGULATOR_BASE_COLOR);
		weaponRegulator.setValuePanelColor(ShipStatusController.REGULATOR_VALUE_COLOR);
		weaponRegulator.setWidth(CONTROL_WIDTH + "px");
		weaponRegulator.setHeight(ShipStatusController.REGULATOR_HEIGHT);
		weaponRegulator.setButtonSize(ShipStatusController.REGULATOR_BUTTON_SIZE);
		weaponRegulator.setMinimum(0);
		weaponRegulator.setMaximum(1F);
		weaponRegulator.setStep(0.1F);

		ImageUIBuilder weaponImage = new ImageUIBuilder(WEAPON_PANEL_ID);
		weaponImage.setPath(ShipStatusController.REGULATOR_WEAPON_IMAGE);
		weaponImage.setWidth("23px");
		weaponImage.setHeight("23px");

		PanelUIBuilder weaponPanel = new PanelUIBuilder(WEAPON_LABEL_ID);
		weaponPanel.setLayoutType(ChildLayoutType.Horizontal);
		weaponPanel.setWidth(ShipStatusController.ENERGY_CONTROL_WIDTH);
		weaponPanel.setHeight(ShipStatusController.ENERGY_CONTROL_HEIGHT);
		weaponPanel.add(weaponRegulator);
		weaponPanel.add(weaponImage);

		HorizontalRegulatorUIBuilder shieldRegulator = new HorizontalRegulatorUIBuilder(SHIELD_CONTROL_ID);
		shieldRegulator.setButtonSubImage(ShipStatusController.IMAGE_REGULATOR_SUB);
		shieldRegulator.setButtonAddImage(ShipStatusController.IMAGE_REGULATOR_ADD);
		shieldRegulator.setBasePanelWidth(ShipStatusController.REGULATOR_PANEL_WIDTH);
		shieldRegulator.setBasePanelHeight(ShipStatusController.REGULATOR_PANEL_HEIGHT);
		shieldRegulator.setBasePanelColor(ShipStatusController.REGULATOR_BASE_COLOR);
		shieldRegulator.setValuePanelColor(ShipStatusController.REGULATOR_VALUE_COLOR);
		shieldRegulator.setWidth(CONTROL_WIDTH + "px");
		shieldRegulator.setHeight(ShipStatusController.REGULATOR_HEIGHT);
		shieldRegulator.setButtonSize(ShipStatusController.REGULATOR_BUTTON_SIZE);
		shieldRegulator.setMinimum(0);
		shieldRegulator.setMaximum(1F);
		shieldRegulator.setStep(0.1F);

		ImageUIBuilder shieldImage = new ImageUIBuilder(SHIELD_LABEL_ID);
		shieldImage.setPath(ShipStatusController.REGULATOR_SHIELD_IMAGE);
		shieldImage.setWidth("23px");
		shieldImage.setHeight("23px");

		PanelUIBuilder shieldPanel = new PanelUIBuilder(SHIELD_PANEL_ID);
		shieldPanel.setLayoutType(ChildLayoutType.Horizontal);
		shieldPanel.setWidth(ShipStatusController.ENERGY_CONTROL_WIDTH);
		shieldPanel.setHeight(ShipStatusController.ENERGY_CONTROL_HEIGHT);
		shieldPanel.add(shieldRegulator);
		shieldPanel.add(shieldImage);

		HorizontalRegulatorUIBuilder engineRegulator = new HorizontalRegulatorUIBuilder(ENGINE_CONTROL_ID);
		engineRegulator.setButtonSubImage(ShipStatusController.IMAGE_REGULATOR_SUB);
		engineRegulator.setButtonAddImage(ShipStatusController.IMAGE_REGULATOR_ADD);
		engineRegulator.setBasePanelWidth(ShipStatusController.REGULATOR_PANEL_WIDTH);
		engineRegulator.setBasePanelHeight(ShipStatusController.REGULATOR_PANEL_HEIGHT);
		engineRegulator.setBasePanelColor(ShipStatusController.REGULATOR_BASE_COLOR);
		engineRegulator.setValuePanelColor(ShipStatusController.REGULATOR_VALUE_COLOR);
		engineRegulator.setWidth(CONTROL_WIDTH + "px");
		engineRegulator.setHeight(ShipStatusController.REGULATOR_HEIGHT);
		engineRegulator.setButtonSize(ShipStatusController.REGULATOR_BUTTON_SIZE);
		engineRegulator.setMinimum(0);
		engineRegulator.setMaximum(1F);
		engineRegulator.setStep(0.1F);

		ImageUIBuilder engineImage = new ImageUIBuilder(ENGINE_LABEL_ID);
		engineImage.setPath(ShipStatusController.REGULATOR_ENGINE_IMAGE);
		engineImage.setWidth("23px");
		engineImage.setHeight("23px");

		PanelUIBuilder enginePanel = new PanelUIBuilder(SHIELD_PANEL_ID);
		enginePanel.setLayoutType(ChildLayoutType.Horizontal);
		enginePanel.setWidth(ShipStatusController.ENERGY_CONTROL_WIDTH);
		enginePanel.setHeight(ShipStatusController.ENERGY_CONTROL_HEIGHT);
		enginePanel.add(engineRegulator);
		enginePanel.add(engineImage);

		PanelUIBuilder controlPanel = new PanelUIBuilder(CONTROL_PANEL_ID);
		controlPanel.setLayoutType(ChildLayoutType.Vertical);
		controlPanel.setWidth("155px");
		controlPanel.setHeight("74px");
		controlPanel.setAlign(Align.Right);
		controlPanel.add(weaponPanel);
		controlPanel.add(shieldPanel);
		controlPanel.add(enginePanel);

		ImageUIBuilder energyIcon = new ImageUIBuilder(ENERGY_ICON_ID);
		energyIcon.setPath(ShipStatusController.SHIP_STATUS_ENERGY_IMAGE);
		energyIcon.setMarginTop("2px");
		energyIcon.setWidth("23px");
		energyIcon.setHeight("23px");

		ImageUIBuilder strengthIcon = new ImageUIBuilder(STRENGTH_ICON_ID);
		strengthIcon.setPath(ShipStatusController.SHIP_STATUS_STRENGTH_IMAGE);
		strengthIcon.setMarginTop("2px");
		strengthIcon.setWidth("23px");
		strengthIcon.setHeight("23px");

		PanelUIBuilder iconPanel = new PanelUIBuilder(ICON_PANEL_ID);
		iconPanel.setLayoutType(ChildLayoutType.Vertical);
		iconPanel.setWidth("25px");
		iconPanel.setHeight("80px");
		iconPanel.setValign(VAlign.Top);
		iconPanel.add(energyIcon);
		iconPanel.add(strengthIcon);

		LabelUIBuilder energyLabel = new LabelUIBuilder(ENERGY_LABEL_ID);
		energyLabel.setHeight(ShipStatusController.REGULATOR_HEIGHT);
		energyLabel.setWidth("100%");
		energyLabel.setText("0");
		energyLabel.setTextHAlign(Align.Center);
		energyLabel.setTextVAlign(VAlign.Center);
		energyLabel.setAlign(Align.Center);
		energyLabel.setValign(VAlign.Center);

		PanelUIBuilder energyIndicator = new PanelUIBuilder(ENERGY_INDICATOR_ID);
		energyIndicator.setBackgroundColor(ENERGY_INDICATOR_COLOR);
		energyIndicator.setAlign(Align.Left);
		energyIndicator.setHeight("12px");
		energyIndicator.setWidth("100%");

		PanelUIBuilder energyIndicatorContainer = new PanelUIBuilder("#energy_indicator_container");
		energyIndicatorContainer.setLayoutType(ChildLayoutType.Center);
		energyIndicatorContainer.setMarginTop("8px");
		energyIndicatorContainer.setHeight("12px");
		energyIndicatorContainer.setWidth("100%");
		energyIndicatorContainer.add(energyIndicator);
		energyIndicatorContainer.add(energyLabel);

		LabelUIBuilder strengthLabel = new LabelUIBuilder(STRENGTH_LABEL_ID);
		strengthLabel.setHeight(ShipStatusController.REGULATOR_HEIGHT);
		strengthLabel.setWidth("100%");
		strengthLabel.setText("0");
		strengthLabel.setTextHAlign(Align.Center);
		strengthLabel.setTextVAlign(VAlign.Center);
		strengthLabel.setAlign(Align.Center);
		strengthLabel.setValign(VAlign.Center);

		PanelUIBuilder strengthIndicator = new PanelUIBuilder(STRENGTH_INDICATOR_ID);
		strengthIndicator.setBackgroundColor(STRENGTH_INDICATOR_COLOR);
		strengthIndicator.setAlign(Align.Left);
		strengthIndicator.setHeight("12px");
		strengthIndicator.setWidth("100%");

		PanelUIBuilder strengthIndicatorContainer = new PanelUIBuilder("#strength_indicator_container");
		strengthIndicatorContainer.setLayoutType(ChildLayoutType.Center);
		strengthIndicatorContainer.setMarginTop("12px");
		strengthIndicatorContainer.setHeight("12px");
		strengthIndicatorContainer.setWidth("100%");
		strengthIndicatorContainer.add(strengthIndicator);
		strengthIndicatorContainer.add(strengthLabel);

		PanelUIBuilder indicatorPanel = new PanelUIBuilder(INDICATOR_PANEL_ID);
		indicatorPanel.setLayoutType(ChildLayoutType.Vertical);
		indicatorPanel.setValign(VAlign.Top);
		indicatorPanel.setWidth(INDICATOR_WIDTH + "px");
		indicatorPanel.setHeight("30px");
		indicatorPanel.setMarginRight("1px");
		indicatorPanel.add(energyIndicatorContainer);
		indicatorPanel.add(strengthIndicatorContainer);

		PanelUIBuilder statePanel = new PanelUIBuilder(STATE_PANEL_ID);
		statePanel.setLayoutType(ChildLayoutType.Horizontal);
		statePanel.setAlign(Align.Right);
		statePanel.setWidth(width + "px");
		statePanel.setHeight(height + "px");
		statePanel.add(indicatorPanel);
		statePanel.add(iconPanel);

		PanelBuilder basePanel = ElementFactory.getPanelBuilder(ID, Vertical, Strings.EMPTY, "100%", "100%");
		basePanel.controller(ShipStatusController.class.getName());

		bufferPanel.buildTo(basePanel);
		controlPanel.buildTo(basePanel);
		statePanel.buildTo(basePanel);

		layer.panel(basePanel);
	}
}
