package com.ss.client.gui.builder.game;

import java.util.concurrent.atomic.AtomicInteger;

import com.ss.client.gui.FontIds;
import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.window.ship.ShipWindowUIController;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.model.module.Module;
import com.ss.client.template.ModuleTemplate;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Конструктор главной панели.
 * 
 * @author Ronn
 */
public class ShipWindowUIBuilder {

	public static final String MODULE_ICON_ID = "#icon";
	public static final String MODULE_ELEMENT_ID = "#module_element_";

	public static void build(final SpaceLayerBuilder layer) {

		DraggableElementUIBuilder builder = new DraggableElementUIBuilder(ShipWindowUIController.WINDOW_ID);
		builder.setController(ShipWindowUIController.class);
		builder.setLayoutType(ChildLayoutType.Vertical);
		builder.setBackgroundColor(Color.randomColor());
		builder.setWidth("410px");
		builder.setHeight("310px");
		builder.add(buildHeader());
		builder.add(buildContent());
		builder.setVisible(false);
		builder.buildTo(layer);
	}

	private static PanelUIBuilder buildContent() {

		LabelUIBuilder shipName = new LabelUIBuilder(ShipWindowUIController.SHIP_NAME_ID);
		shipName.setHeight("20px");

		PanelUIBuilder infoPanel = new PanelUIBuilder(ShipWindowUIController.INFO_PANEL_ID);
		infoPanel.setBackgroundColor(Color.randomColor());
		infoPanel.setLayoutType(ChildLayoutType.Vertical);
		infoPanel.add(shipName);
		infoPanel.add(buildStatsPanel());
		infoPanel.add(buildModulesPanel());

		PanelUIBuilder content = new PanelUIBuilder(ShipWindowUIController.CONTENT_ID);
		content.setBackgroundColor(Color.randomColor());
		content.setLayoutType(ChildLayoutType.Horizontal);
		content.add(infoPanel);
		content.setHeight("*");
		return content;
	}

	private static PanelUIBuilder buildHeader() {

		LabelUIBuilder close = new LabelUIBuilder(ShipWindowUIController.CLOSE_ID);
		close.setWidth("90px");
		close.setText("Закрыть");
		close.setAlign(Align.Right);

		LabelUIBuilder title = new LabelUIBuilder(ShipWindowUIController.TITLE_ID);
		title.setLayoutType(ChildLayoutType.Center);
		title.setText(ShipWindowUIController.WINDOW_TITLE);
		title.add(close);

		PanelUIBuilder header = new PanelUIBuilder(ShipWindowUIController.HEADER_ID);
		header.setBackgroundColor(Color.randomColor());
		header.setHeight("30px");
		header.add(title);
		return header;
	}

	private static void buildModuleCells(AtomicInteger counter, PanelUIBuilder row) {

		for(int g = 0, count = 11; g < count; g++) {
			PanelUIBuilder cell = new PanelUIBuilder(ShipWindowUIController.MODULE_CELL_ID + counter.incrementAndGet());
			cell.setBackgroundColor(Color.randomColor());
			cell.setWidth("36px");
			cell.setHeight("36px");
			cell.setMarginLeft("1px");
			row.add(cell);
		}
	}

	public static Element buildModuleElement(Module module, Element parent, Screen screen, Nifty nifty, int order) {

		ModuleTemplate template = module.getTemplate();

		ImageUIBuilder icon = new ImageUIBuilder(MODULE_ICON_ID);
		icon.setPath(template.getIcon());

		PanelUIBuilder builder = new PanelUIBuilder(MODULE_ELEMENT_ID + order);
		builder.setWidth("32px");
		builder.setHeight("32px");
		builder.add(icon);

		return builder.build(nifty, screen, parent);
	}

	private static void buildModuleRows(PanelUIBuilder modulesPanel) {

		AtomicInteger counter = new AtomicInteger();

		for(int i = 0, length = 2; i < length; i++) {

			PanelUIBuilder row = new PanelUIBuilder(ShipWindowUIController.MODULES_ROW_ID + i);
			row.setBackgroundColor(Color.randomColor());
			row.setLayoutType(ChildLayoutType.Horizontal);
			row.setMarginTop("1px");
			row.setHeight("36px");

			buildModuleCells(counter, row);

			modulesPanel.add(row);
		}
	}

	private static PanelUIBuilder buildModulesPanel() {

		LabelUIBuilder modulesTitle = new LabelUIBuilder(ShipWindowUIController.MODULES_TITLE_ID);
		modulesTitle.setText(ShipWindowUIController.SHIP_MODULES_TILTLE);
		modulesTitle.setHeight("30px");

		PanelUIBuilder modulesPanel = new PanelUIBuilder(ShipWindowUIController.MODULES_PANEL_ID);
		modulesPanel.setBackgroundColor(Color.randomColor());
		modulesPanel.setLayoutType(ChildLayoutType.Vertical);
		modulesPanel.setHeight("120px");
		modulesPanel.add(modulesTitle);

		buildModuleRows(modulesPanel);

		return modulesPanel;
	}

	private static LabelUIBuilder buildStatRow(String nameId, String valueId, String nameText, String valueText) {

		LabelUIBuilder labelValue = new LabelUIBuilder(valueId);
		labelValue.setAlign(Align.Right);
		labelValue.setTextHAlign(Align.Right);
		labelValue.setText(valueText);
		labelValue.setHeight("16px");
		labelValue.setFont(FontIds.MAIN_14);

		LabelUIBuilder labelName = new LabelUIBuilder(nameId);
		labelName.setAlign(Align.Center);
		labelName.setWidth("90%");
		labelName.setTextHAlign(Align.Left);
		labelName.setText(nameText);
		labelName.setHeight("16px");
		labelName.add(labelValue);
		labelName.setFont(FontIds.MAIN_14);

		return labelName;
	}

	private static PanelUIBuilder buildStatsPanel() {

		LabelUIBuilder statsTitle = new LabelUIBuilder(ShipWindowUIController.STATS_TITLE_ID);
		statsTitle.setText(ShipWindowUIController.SHIP_STATS_TITLE);
		statsTitle.setHeight("30px");

		PanelUIBuilder statsPanel = new PanelUIBuilder(ShipWindowUIController.STATS_PANEL_ID);
		statsPanel.setBackgroundColor(Color.randomColor());
		statsPanel.setLayoutType(ChildLayoutType.Vertical);
		statsPanel.add(statsTitle);

		for(String[] values : ShipWindowUIController.STAT_ROWS) {
			statsPanel.add(buildStatRow(values[0], values[1], values[2], values[3]));
		}

		statsPanel.setHeight("150px");

		return statsPanel;
	}
}
