package com.ss.client.gui.builder.game;

import static de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType.AbsoluteInside;
import static de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType.Center;
import rlib.util.Strings;

import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.hud.vector.VectorController;

import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;

/**
 * Реализация билдера селекторов.
 * 
 * @author Ronn
 */
public class VectorUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		ImageBuilder image = new ImageBuilder(VectorController.VECTOR_ICON_ID);
		image.filename(VectorController.VECTOR_ICON);

		PanelBuilder vector = ElementFactory.getPanelBuilder(VectorController.VECTOR_ID, Center, Strings.EMPTY, "50px", "50px");
		vector.image(image);

		image = new ImageBuilder(VectorController.VECTOR_CENTER_ICON_ID);
		image.filename(VectorController.VECTOR_CENTER_ICON);

		PanelBuilder center = ElementFactory.getPanelBuilder(VectorController.VECTOR_CENTER_ID, Center, Strings.EMPTY, VectorController.VECTOR_CENTER_WIDTH, VectorController.VECTOR_CENTER_HEIGHT);
		center.image(image);

		final PanelBuilder panel = ElementFactory.getPanelBuilder(VectorController.BASE_PANEL_ID, AbsoluteInside, Strings.EMPTY, "100%", "100%");
		panel.controller(VectorController.class.getName());
		panel.panel(vector);

		layer.panel(panel);
	}
}
