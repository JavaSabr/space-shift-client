package com.ss.client.gui.builder.game.factory;

import java.util.concurrent.atomic.AtomicInteger;

import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.gui.controller.game.panel.radar.RadarObjectUIController;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.model.FriendStatus;
import com.ss.client.model.SpaceObject;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Фабрика элементов объектов на радаре.
 * 
 * @author Ronn
 */
public class RadarElementUIFactory {

	private static final Table<FriendStatus, String> UNSELECTED_PATH_CACHE = Tables.newObjectTable();
	private static final Table<FriendStatus, String> SELECTED_PATH_CACHE = Tables.newObjectTable();

	public static RadarObjectUIController build(SpaceObject object, AtomicInteger indicatorIndex, Element parent, Screen screen, Nifty nifty, boolean isPlayer) {

		ImageUIBuilder builder = new ImageUIBuilder(RadarObjectUIController.OBJECT_ID + indicatorIndex.incrementAndGet());
		builder.setController(RadarObjectUIController.class);
		builder.setVisible(false);

		if(isPlayer) {
			builder.setWidth("9px");
			builder.setHeight("9px");
			builder.setPath("ui/game/radar_panel/player.png");
		} else {
			builder.setWidth("12px");
			builder.setHeight("12px");
			builder.setPath(getPath(object.getFriendStatus(), false));
		}

		Element element = builder.build(nifty, screen, parent);

		RadarObjectUIController controller = element.getControl(RadarObjectUIController.class);
		controller.setObject(object);
		return controller;
	}

	/**
	 * Построение пути к иконке индикатора.
	 */
	public static String getPath(FriendStatus friendStatus, boolean selected) {

		String result = null;

		if(selected) {

			result = SELECTED_PATH_CACHE.get(friendStatus);

			if(result == null) {
				result = "ui/game/radar_panel/select__" + friendStatus + ".png";
				SELECTED_PATH_CACHE.put(friendStatus, result);
			}

		} else {

			result = UNSELECTED_PATH_CACHE.get(friendStatus);

			if(result == null) {
				result = "ui/game/radar_panel/unselect__" + friendStatus + ".png";
				UNSELECTED_PATH_CACHE.put(friendStatus, result);
			}
		}

		return result;
	}
}
