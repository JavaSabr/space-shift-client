package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.hud.aim.AIMUIController;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;

/**
 * Реализация конструктора прицела.
 * 
 * @author Ronn
 */
public class AIMUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		ImageUIBuilder image = new ImageUIBuilder(AIMUIController.AIM_ICON_ID);
		image.setPath(AIMUIController.AIM_ICON);

		PanelUIBuilder panel = new PanelUIBuilder(AIMUIController.AIM_ID);
		panel.setWidth(AIMUIController.AIM_WIDTH);
		panel.setHeight(AIMUIController.AIM_HEIGHT);
		panel.add(image);

		PanelUIBuilder builder = new PanelUIBuilder(AIMUIController.BASE_PANEL_ID);
		builder.setLayoutType(ChildLayoutType.AbsoluteInside);
		builder.setController(AIMUIController.class);
		builder.add(panel);
		builder.buildTo(layer);
	}
}
