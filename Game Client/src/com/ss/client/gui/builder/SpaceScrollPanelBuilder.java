package com.ss.client.gui.builder;

import com.ss.client.gui.ElementId;
import com.ss.client.gui.control.ScrollPanelControl;

import de.lessvoid.nifty.controls.ScrollPanel.AutoScroll;

/**
 * @author Ronn
 */
public class SpaceScrollPanelBuilder extends SpaceControlBuilder {

	public SpaceScrollPanelBuilder(final ElementId elementId, final String name) {
		super(elementId, name);
	}

	public SpaceScrollPanelBuilder(final String id, final String name) {
		super(id, name);
	}

	public void setAutoScroll(final AutoScroll scroll) {
		set(ScrollPanelControl.AUTO_SCROLL, scroll.getParam());
	}

	public void setVisibleHScrollbar(final boolean value) {
		set(ScrollPanelControl.VISIBLE_H_SCROLLBAR, String.valueOf(value));
	}

	public void setVisibleVScrollbar(final boolean value) {
		set(ScrollPanelControl.VISIBLE_V_SCROLLBAR, String.valueOf(value));
	}
}
