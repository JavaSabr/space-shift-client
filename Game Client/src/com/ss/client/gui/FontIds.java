package com.ss.client.gui;

/**
 * Описание ид шрифтов.
 * 
 * @author Ronn
 */
public interface FontIds {

	public static final String MAIN_14 = "main_14.fnt";
	public static final String MAIN_16 = "main_16.fnt";
	public static final String MAIN_16_ITALIC = "main_16_italic.fnt";
	public static final String MAIN_18 = "main_18.fnt";
	public static final String MAIN_18_ITALIC = "main_18_italic.fnt";
	public static final String MAIN_20 = "main_20.fnt";
	public static final String MAIN_22 = "main_22.fnt";
	public static final String MAIN_24 = "main_24.fnt";

}