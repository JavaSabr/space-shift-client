package com.ss.client.gui.control;

import java.util.Properties;

import org.bushe.swing.event.EventTopicSubscriber;

import rlib.util.array.Array;

import com.ss.client.util.ReflectionClass;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.controls.ScrollPanel;
import de.lessvoid.nifty.controls.ScrollPanelChangedEvent;
import de.lessvoid.nifty.controls.Scrollbar;
import de.lessvoid.nifty.controls.ScrollbarChangedEvent;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.input.NiftyMouseInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

@ReflectionClass
public class ScrollPanelControl extends AbstractController implements ScrollPanel {

	private class HorizontalEventTopicSubscriber implements EventTopicSubscriber<ScrollbarChangedEvent> {

		private final ScrollPanel scrollPanel;

		public HorizontalEventTopicSubscriber(final ScrollPanel scrollPanel) {
			this.scrollPanel = scrollPanel;
		}

		@Override
		public void onEvent(final String id, final ScrollbarChangedEvent event) {
			final Element childRootElement = getChildRootElement();

			if(childRootElement == null)
				return;

			final Array<Element> elements = childRootElement.getFastElements();

			if(elements.isEmpty())
				return;

			final Element scrollElement = elements.get(0);

			if(scrollElement != null) {
				scrollElement.setConstraintX(new SizeValue(-(int) event.getValue() + "px"));
				updateWorldHorizontal();
				scrollElement.getParent().layoutElements();

				float yPos = 0.f;

				final Scrollbar vertical = getVerticalScrollbar();

				if(vertical != null && isVisibleVScrollbar())
					yPos = vertical.getValue();

				nifty.publishEvent(getElement().getId(), new ScrollPanelChangedEvent(scrollPanel, event.getValue(), yPos));
			}
		}
	}

	private class VerticalEventTopicSubscriber implements EventTopicSubscriber<ScrollbarChangedEvent> {

		private final ScrollPanel scrollPanel;

		public VerticalEventTopicSubscriber(final ScrollPanel scrollPanel) {
			this.scrollPanel = scrollPanel;
		}

		@Override
		public void onEvent(final String id, final ScrollbarChangedEvent event) {
			final Element childRootElement = getChildRootElement();

			if(childRootElement == null)
				return;

			final Array<Element> elements = childRootElement.getFastElements();

			if(elements.isEmpty())
				return;

			final Element scrollElement = elements.get(0);

			if(scrollElement != null) {
				scrollElement.setConstraintY(new SizeValue(-(int) event.getValue() + "px"));
				updateWorldVertical();
				scrollElement.getParent().layoutElements();

				float xPos = 0.f;

				final Scrollbar horizontal = getHorizontalScrollbar();

				if(horizontal != null && isVisibleHScrollbar())
					xPos = horizontal.getValue();

				nifty.publishEvent(getElement().getId(), new ScrollPanelChangedEvent(scrollPanel, xPos, event.getValue()));
			}
		}
	}

	private static final String CHILD_ROOT_ID = "childRootId";
	public static final String AUTO_SCROLL = "autoScroll";

	public static final String VISIBLE_H_SCROLLBAR = "visibleHScrollbar";
	public static final String VISIBLE_V_SCROLLBAR = "visibleVScrollbar";

	private static final String NIFTY_INTERNAL_VERTICAL_SCROLLBAR = "#space-internal-vertical-scrollbar";
	private static final String NIFTY_INTERNAL_HORIZONTAL_SCROLLBAR = "#space-internal-horizontal-scrollbar";

	private EventTopicSubscriber<ScrollbarChangedEvent> horizontalScrollbarSubscriber;
	private EventTopicSubscriber<ScrollbarChangedEvent> verticalScrollbarSubscriber;

	private Nifty nifty;

	private Screen screen;
	private Element childRootElement;

	private Scrollbar horizontalScrollbar;

	private Scrollbar verticalScrollbar;
	private AutoScroll autoScroll;
	private float stepSizeX;
	private float stepSizeY;

	private float pageSizeX;
	private float pageSizeY;

	private boolean visibleVScrollbar;

	private boolean visibleHScrollbar;

	@Override
	public void bind(final Nifty niftyParam, final Screen screenParam, final Element elementParam, final Properties parameter, final Attributes controlDefinitionAttributes) {
		super.bind(elementParam);
		nifty = niftyParam;
		screen = screenParam;

		visibleVScrollbar = new Boolean(parameter.getProperty(VISIBLE_V_SCROLLBAR, "true"));
		visibleHScrollbar = new Boolean(parameter.getProperty(VISIBLE_H_SCROLLBAR, "true"));

		childRootElement = getElement().findElementByName(controlDefinitionAttributes.get(CHILD_ROOT_ID));

		stepSizeX = new Float(parameter.getProperty("stepSizeX", "1.0"));
		stepSizeY = new Float(parameter.getProperty("stepSizeY", "1.0"));
		pageSizeX = new Float(parameter.getProperty("pageSizeX", "1.0"));
		pageSizeY = new Float(parameter.getProperty("pageSizeY", "1.0"));

		autoScroll = AutoScroll.parse(parameter.getProperty(AUTO_SCROLL, "off"));

		horizontalScrollbarSubscriber = new HorizontalEventTopicSubscriber(this);
		verticalScrollbarSubscriber = new VerticalEventTopicSubscriber(this);
	}

	@Override
	public AutoScroll getAutoScroll() {
		return autoScroll;
	}

	private Element getChildRootElement() {
		return childRootElement;
	}

	@Override
	public float getHorizontalPos() {
		final Scrollbar scrollbar = getHorizontalScrollbar();

		if(scrollbar != null && isVisibleHScrollbar())
			return scrollbar.getValue();

		return 0.f;
	}

	public Scrollbar getHorizontalScrollbar() {
		if(horizontalScrollbar == null && isVisibleHScrollbar())
			horizontalScrollbar = getElement().findNiftyControl(NIFTY_INTERNAL_HORIZONTAL_SCROLLBAR, Scrollbar.class);

		return horizontalScrollbar;
	}

	@Override
	public float getVerticalPos() {
		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null && isVisibleVScrollbar())
			return scrollbar.getValue();

		return 0.f;
	}

	public Scrollbar getVerticalScrollbar() {
		if(verticalScrollbar == null && isVisibleVScrollbar())
			verticalScrollbar = getElement().findNiftyControl(NIFTY_INTERNAL_VERTICAL_SCROLLBAR, Scrollbar.class);

		return verticalScrollbar;
	}

	@Override
	public void init(final Properties parameter, final Attributes controlDefinitionAttributes) {
		initializeScrollPanel();
		initializeScrollbars();
		subscribeHorizontalScrollbar();
		subscribeVerticalScrollbar();

		super.init(parameter, controlDefinitionAttributes);
	}

	private void initializeScrollbars() {
		final Element childRootElement = getChildRootElement();

		if(childRootElement != null) {
			final Array<Element> elements = childRootElement.getFastElements();

			if(elements.isEmpty())
				return;

			final Element scrollElement = elements.get(0);

			if(scrollElement != null) {
				final Scrollbar horizontal = getHorizontalScrollbar();

				if(horizontal != null) {
					horizontal.setWorldMax(scrollElement.getWidth());
					updateWorldHorizontal();
					horizontal.setWorldPageSize(horizontal.getWidth());
					horizontal.setValue(0.0f);
					horizontal.setButtonStepSize(stepSizeX);
					horizontal.setPageStepSize(pageSizeX);
				}

				final Scrollbar vertical = getVerticalScrollbar();

				if(vertical != null) {
					vertical.setWorldMax(scrollElement.getHeight());
					updateWorldVertical();
					vertical.setWorldPageSize(vertical.getHeight());
					vertical.setValue(0.0f);
					vertical.setButtonStepSize(stepSizeY);
					vertical.setPageStepSize(pageSizeY);
				}

				scrollElement.setConstraintX(new SizeValue("0px"));
				scrollElement.setConstraintY(new SizeValue("0px"));
			}

			scrollElement.getParent().layoutElements();
		}
	}

	private void initializeScrollPanel() {
		if(!isVisibleVScrollbar()) {
			final Element vertical = getElement().findElementByName(NIFTY_INTERNAL_VERTICAL_SCROLLBAR);

			if(vertical != null)
				nifty.removeElement(screen, vertical);
		}

		if(!isVisibleHScrollbar()) {
			final Element horizontal = getElement().findElementByName(NIFTY_INTERNAL_HORIZONTAL_SCROLLBAR);

			if(horizontal != null)
				nifty.removeElement(screen, horizontal);
		}

		nifty.executeEndOfFrameElementActions();
		screen.layoutLayers();
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	public boolean isVisibleHScrollbar() {
		return visibleHScrollbar;
	}

	public boolean isVisibleVScrollbar() {
		return visibleVScrollbar;
	}

	@Override
	public void layoutCallback() {
		final Element childRootElement = getChildRootElement();

		if(childRootElement != null) {
			final Array<Element> elements = childRootElement.getFastElements();

			if(elements.isEmpty())
				return;

			final Element scrollElement = elements.get(0);

			final Scrollbar horizontalScrollbar = getHorizontalScrollbar();

			if(horizontalScrollbar != null) {
				horizontalScrollbar.setWorldMax(scrollElement.getWidth());
				horizontalScrollbar.setWorldPageSize(horizontalScrollbar.getWidth());
				updateWorldHorizontal();
			}

			final Scrollbar verticalScrollbar = getVerticalScrollbar();

			if(verticalScrollbar != null) {
				verticalScrollbar.setWorldMax(scrollElement.getHeight());
				verticalScrollbar.setWorldPageSize(verticalScrollbar.getHeight());
				updateWorldVertical();
			}
		}
	}

	public void mouseWheel(final Element element, final NiftyMouseInputEvent inputEvent) {
		final int mouseWheel = inputEvent.getMouseWheel();

		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null) {
			final float currentValue = scrollbar.getValue();

			if(mouseWheel < 0)
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
			else if(mouseWheel > 0)
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
		}
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void setAutoScroll(final AutoScroll auto) {
		this.autoScroll = auto;

		updateWorldHorizontal();
		updateWorldVertical();
	}

	@Override
	public void setHorizontalPos(final float xPos) {
		final Scrollbar scrollbar = getHorizontalScrollbar();

		if(scrollbar != null && isVisibleHScrollbar())
			scrollbar.setValue(xPos);
	}

	@Override
	public void setPageSizeX(final float pageSizeX) {
		this.pageSizeX = pageSizeX;

		final Scrollbar scrollbar = getHorizontalScrollbar();

		if(scrollbar != null)
			scrollbar.setPageStepSize(pageSizeX);
	}

	@Override
	public void setPageSizeY(final float pageSizeY) {
		this.pageSizeY = pageSizeY;

		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null)
			scrollbar.setPageStepSize(pageSizeY);
	}

	@Override
	public void setStepSizeX(final float stepSizeX) {
		this.stepSizeX = stepSizeX;

		final Scrollbar scrollbar = getHorizontalScrollbar();

		if(scrollbar != null)
			scrollbar.setButtonStepSize(stepSizeX);
	}

	@Override
	public void setStepSizeY(final float stepSizeY) {
		this.stepSizeY = stepSizeY;

		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null)
			scrollbar.setButtonStepSize(stepSizeY);
	}

	@Override
	public void setUp(final float stepSizeX, final float stepSizeY, final float pageSizeX, final float pageSizeY, final AutoScroll auto) {
		this.stepSizeX = stepSizeX;
		this.stepSizeY = stepSizeY;
		this.pageSizeX = pageSizeX;
		this.pageSizeY = pageSizeY;
		this.autoScroll = auto;

		initializeScrollbars();
	}

	@Override
	public void setVerticalPos(final float yPos) {
		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null && isVisibleVScrollbar())
			scrollbar.setValue(yPos);
	}

	@Override
	public void showElementVertical(final int elemCount) {
		showElementVertical(elemCount, VerticalAlign.center);
	}

	private void showElementVertical(final int elemCount, final VerticalAlign valign) {
		float newPos;

		switch(valign) {
			case top:
				newPos = stepSizeY * elemCount;
				break;
			case center:
				newPos = stepSizeY * elemCount - getElement().getHeight() / 2;
				break;
			case bottom:
				newPos = stepSizeY * elemCount - getElement().getHeight();
				break;
			default:
				newPos = 0;
		}

		setVerticalPos(newPos);
	}

	private void subscribeHorizontalScrollbar() {
		final Element scrollbar = getElement().findElementByName(NIFTY_INTERNAL_HORIZONTAL_SCROLLBAR);

		if(scrollbar != null)
			nifty.subscribe(screen, scrollbar.getId(), ScrollbarChangedEvent.class, horizontalScrollbarSubscriber);
	}

	private void subscribeVerticalScrollbar() {
		final Element scrollbar = getElement().findElementByName(NIFTY_INTERNAL_VERTICAL_SCROLLBAR);

		if(scrollbar != null)
			nifty.subscribe(screen, scrollbar.getId(), ScrollbarChangedEvent.class, verticalScrollbarSubscriber);
	}

	private void updateWorldHorizontal() {
		final Scrollbar horizontal = getHorizontalScrollbar();

		if(horizontal != null) {
			final AutoScroll autoScroll = getAutoScroll();

			if(autoScroll == AutoScroll.RIGHT || autoScroll == AutoScroll.BOTTOM_RIGHT || autoScroll == AutoScroll.TOP_RIGHT)
				horizontal.setValue(horizontal.getWorldMax());
			else if(autoScroll == AutoScroll.LEFT || autoScroll == AutoScroll.BOTTOM_LEFT || autoScroll == AutoScroll.TOP_LEFT)
				horizontal.setValue(0);
		}
	}

	private void updateWorldVertical() {
		final Scrollbar vertical = getVerticalScrollbar();

		if(vertical != null) {
			final AutoScroll autoScroll = getAutoScroll();

			if(autoScroll == AutoScroll.BOTTOM || autoScroll == AutoScroll.BOTTOM_LEFT || autoScroll == AutoScroll.BOTTOM_RIGHT)
				vertical.setValue(vertical.getWorldMax());
			else if(autoScroll == AutoScroll.TOP || autoScroll == AutoScroll.TOP_LEFT || autoScroll == AutoScroll.TOP_RIGHT)
				vertical.setValue(0);
		}
	}
}
