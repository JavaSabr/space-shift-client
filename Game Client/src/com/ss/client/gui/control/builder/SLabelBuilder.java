package com.ss.client.gui.control.builder;

import de.lessvoid.nifty.builder.ControlBuilder;

/**
 * Реализация строителя надписи.
 * 
 * @author Ronn
 */
public class SLabelBuilder extends ControlBuilder {

	public SLabelBuilder(String id) {
		super(id, "s_label");
	}
}
