package com.ss.client.gui.control;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.bushe.swing.event.EventTopicSubscriber;

import com.ss.client.util.ReflectionClass;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyIdCreator;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.ListBoxSelectionChangedEvent;
import de.lessvoid.nifty.controls.Scrollbar;
import de.lessvoid.nifty.controls.ScrollbarChangedEvent;
import de.lessvoid.nifty.controls.dynamic.CustomControlCreator;
import de.lessvoid.nifty.controls.listbox.ListBoxItemController;
import de.lessvoid.nifty.controls.listbox.ListBoxPanel;
import de.lessvoid.nifty.controls.listbox.ListBoxView;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.events.ElementShowEvent;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.input.NiftyMouseInputEvent;
import de.lessvoid.nifty.loaderv2.types.ControlType;
import de.lessvoid.nifty.loaderv2.types.ElementType;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * @author Ronn
 */
@ReflectionClass
public final class ListBoxControl<T> extends AbstractController implements ListBox<T>, ListBoxView<T> {

	private enum ScrollbarMode {
		off,
		on,
		optional
	}

	private static final Logger log = Loggers.getLogger(ListBoxControl.class);

	private final EventTopicSubscriber<ScrollbarChangedEvent> verticalScrollbarSubscriber = new EventTopicSubscriber<ScrollbarChangedEvent>() {

		@Override
		public void onEvent(final String id, final ScrollbarChangedEvent event) {
			getListBoxImpl().updateView((int) (event.getValue() / labelTemplateHeight));
		}
	};

	private final EventTopicSubscriber<ScrollbarChangedEvent> horizontalScrollbarSubscriber = new EventTopicSubscriber<ScrollbarChangedEvent>() {

		@Override
		public void onEvent(final String id, final ScrollbarChangedEvent event) {
			final Element childRootElement = getChildRootElement();

			if(childRootElement != null) {
				childRootElement.setConstraintX(new SizeValue(-(int) event.getValue() + "px"));
				childRootElement.getParent().layoutElements();
			}
		}
	};

	private final EventTopicSubscriber<ElementShowEvent> listBoxControlShowEventSubscriber = new EventTopicSubscriber<ElementShowEvent>() {

		@Override
		public void onEvent(final String id, final ElementShowEvent event) {
			getListBoxImpl().updateView();
		}
	};

	private final ListBoxImpl<T> listBoxImpl;

	private ListBoxViewConverter<T> viewConverter;

	private Element[] labelElements;
	private Nifty nifty;

	private Screen screen;
	private ScrollbarMode verticalScrollbar;

	private ScrollbarMode horizontalScrollbar;
	private Element verticalScrollbarTemplate;
	private Element scrollElement;
	private Element horizontalScrollbarTemplate;
	private Element childRootElement;
	private Element labelTemplateElement;
	private Element listBoxPanelElement;

	private Element bottomRightTemplate;

	private Properties parameter;
	private int labelTemplateHeight;
	private int displayItems;
	private int lastMaxWidth;

	private int applyWidthConstraintsLastWidth;

	public ListBoxControl() {
		this.listBoxImpl = new ListBoxImpl<T>(this);
		this.applyWidthConstraintsLastWidth = -1;
	}

	@Override
	public void addAllItems(final List<T> itemsToAdd) {
		listBoxImpl.addAllItems(itemsToAdd);
	}

	@Override
	public void addItem(final T newItem) {
		listBoxImpl.addItem(newItem);
	}

	private void applyIdPrefixToElementType(final String prefix, final ElementType type) {
		type.getAttributes().set("id", prefix + type.getAttributes().get("id"));

		for(final ElementType child : type.getElements())
			applyIdPrefixToElementType(prefix, child);
	}

	private void applyWidthConstraints(final int width) {
		if(applyWidthConstraintsLastWidth == width)
			return;

		applyWidthConstraintsLastWidth = width;
		final SizeValue newWidthSizeValue = new SizeValue(width + "px");
		for(final Element element : labelElements)
			if(element != null)
				element.setConstraintWidth(newWidthSizeValue);
		childRootElement.setConstraintWidth(newWidthSizeValue);
		getElement().layoutElements();
	}

	@Override
	public void bind(final Nifty niftyParam, final Screen screenParam, final Element elementParam, final Properties parameterParam, final Attributes controlDefinitionAttributes) {
		super.bind(elementParam);

		nifty = niftyParam;
		screen = screenParam;
		parameter = parameterParam;
		viewConverter = createViewConverter(parameter.getProperty("viewConverterClass", ListBoxViewConverterSimple.class.getName()));
		verticalScrollbar = getScrollbarMode("vertical");
		verticalScrollbarTemplate = getElement().findElementByName("#vertical-scrollbar");
		horizontalScrollbar = getScrollbarMode("horizontal");
		horizontalScrollbarTemplate = getElement().findElementByName("#horizontal-scrollbar-parent");
		bottomRightTemplate = getElement().findElementByName("#bottom-right");
		displayItems = new Integer(parameter.getProperty("displayItems", "2"));
		scrollElement = getElement().findElementByName("#scrollpanel");
		applyWidthConstraintsLastWidth = -1;

		childRootElement = getElement().findElementByName("#child-root");

		if(!childRootElement.getElements().isEmpty())
			labelTemplateElement = childRootElement.getElements().get(0);

		labelElements = new Element[displayItems];
		listBoxPanelElement = getElement().findElementByName("#panel");

		initSelectionMode(listBoxImpl, parameter.getProperty("selectionMode", "Single"), parameter.getProperty("forceSelection", "false"));
		connectListBoxAndListBoxPanel();
		listBoxImpl.bindToView(this, displayItems);
		lastMaxWidth = childRootElement.getWidth();
		ensureVerticalScrollbar();
		initLabelTemplateData();
		createLabels();
		initializeScrollPanel(screen);
		setElementHeight();
		initializeScrollElementHeight();
		listBoxImpl.updateView(0);
		initializeHorizontalScrollbar();
		initializeVerticalScrollbar(screen, labelTemplateHeight, 0);
	}

	@Override
	public void changeSelectionMode(final SelectionMode listBoxSelectionMode, final boolean forceSelection) {
		listBoxImpl.changeSelectionMode(listBoxSelectionMode, forceSelection);
	}

	@Override
	public void clear() {
		listBoxImpl.clear();
	}

	@SuppressWarnings("unchecked")
	private void connectListBoxAndListBoxPanel() {
		final ListBoxPanel<T> listBoxPanel = listBoxPanelElement.getControl(ListBoxPanel.class);
		// listBoxPanel.setListBox(listBoxImpl);
	}

	@SuppressWarnings("unchecked")
	private void createLabels() {
		if(labelTemplateElement == null)
			return;
		for(final Element e : childRootElement.getElements())
			nifty.removeElement(screen, e);
		for(int i = 0; i < displayItems; i++) {
			final ElementType templateType = labelTemplateElement.getElementType().copy();
			templateType.getAttributes().set("id", NiftyIdCreator.generate());
			labelElements[i] = nifty.createElementFromType(screen, childRootElement, templateType);

			// connect it to this listbox
			final ListBoxItemController<T> listBoxItemController = labelElements[i].getControl(ListBoxItemController.class);
			if(listBoxItemController != null) {
				// listBoxItemController.setListBox(listBoxImpl);
			}
		}
	}

	@SuppressWarnings({
		"unchecked",
		"rawtypes"
	})
	private ListBoxViewConverter<T> createViewConverter(final String className) {
		try {
			return (ListBoxViewConverter<T>) Class.forName(className).newInstance();
		} catch(final Exception e) {
			log.warning("Unable to instantiate given class [" + className + "] with error: " + e.getMessage(), e);
			return new ListBoxViewConverterSimple();
		}
	}

	@Override
	public void deselectItem(final T item) {
		listBoxImpl.deselectItem(item);
	}

	@Override
	public void deselectItemByIndex(final int itemIndex) {
		listBoxImpl.deselectItemByIndex(itemIndex);
	}

	@Override
	public void display(final List<T> visibleItems, final int focusElement, final List<Integer> selectedElements) {
		ensureWidthConstraints();

		final Element[] labelElements = getLabelElements();
		final Element element = getElement();

		for(int i = 0; i < visibleItems.size(); i++) {
			final T item = visibleItems.get(i);

			final Element label = labelElements[i];

			if(label != null) {
				label.setVisible(item != null && element.isVisible());

				if(item != null) {
					displayElement(i, item);
					setListBoxItemIndex(i);
					handleElementFocus(i, focusElement);
					handleElementSelection(i, item, selectedElements);
				}
			}
		}
	}

	private void displayElement(final int index, final T item) {
		viewConverter.display(labelElements[index], item);
	}

	// ListBoxView Interface implementation

	private void ensureVerticalScrollbar() {
		if(displayItems == 1)
			verticalScrollbar = ScrollbarMode.off;
	}

	public void ensureWidthConstraints() {
		applyWidthConstraints(Math.max(lastMaxWidth, listBoxPanelElement.getWidth()));
	}

	private int findHorizontalScrollbarHeight() {
		int horizontalScrollbarElementHeight = 0;
		final Element horizontalScrollbarElement = getElement().findElementByName("#horizontal-scrollbar");
		if(horizontalScrollbarElement != null)
			horizontalScrollbarElementHeight = horizontalScrollbarElement.getHeight();
		return horizontalScrollbarElementHeight;
	}

	private Element getChildRootElement() {
		return childRootElement;
	}

	@Override
	public int getDisplayItemCount() {
		return displayItems;
	}

	@Override
	public T getFocusItem() {
		return listBoxImpl.getFocusItem();
	}

	@Override
	public int getFocusItemIndex() {
		return listBoxImpl.getFocusItemIndex();
	}

	private Scrollbar getHorizontalScrollbar() {
		return getScrollbar("#horizontal-scrollbar");
	}

	@Override
	public List<T> getItems() {
		return listBoxImpl.getItems();
	}

	private Element[] getLabelElements() {
		return labelElements;
	}

	public ListBoxImpl<T> getListBoxImpl() {
		return listBoxImpl;
	}

	// ListBox Interface Implementation

	private Scrollbar getScrollbar(final String id) {
		return getElement().findNiftyControl(id, Scrollbar.class);
	}

	private ScrollbarMode getScrollbarMode(final String key) {
		try {
			return ScrollbarMode.valueOf(parameter.getProperty(key, ScrollbarMode.on.toString()));
		} catch(final Exception e) {
			log.warning("unknown scrollbar mode [" + key + "] falling back to 'on'");
			return ScrollbarMode.on;
		}
	}

	@Override
	public List<Integer> getSelectedIndices() {
		return listBoxImpl.getSelectedIndices();
	}

	@Override
	public List<T> getSelection() {
		return listBoxImpl.getSelection();
	}

	private Scrollbar getVerticalScrollbar() {
		return getScrollbar("#vertical-scrollbar");
	}

	private ScrollbarMode getVerticalScrollbarMode() {
		return verticalScrollbar;
	}

	public ListBoxViewConverter<T> getViewConverter() {
		return viewConverter;
	}

	@Override
	public int getWidth(final T item) {
		return viewConverter.getWidth(labelElements[0], item);
	}

	@SuppressWarnings("unchecked")
	private void handleElementFocus(final int index, final int focusElement) {
		final ListBoxPanel<T> listBoxPanel = listBoxPanelElement.getControl(ListBoxPanel.class);
		if(listBoxPanel.hasFocus()) {
			if(focusElement == index)
				labelElements[index].startEffect(EffectEventId.onCustom, null, "focus");
			else
				labelElements[index].resetSingleEffect(EffectEventId.onCustom, "focus");
		} else
			labelElements[index].resetSingleEffect(EffectEventId.onCustom, "focus");
	}

	private void handleElementSelection(final int index, final T item, final List<Integer> selectedElements) {
		if(item != null && selectedElements.contains(index))
			labelElements[index].startEffect(EffectEventId.onCustom, null, "select");
		else
			labelElements[index].resetSingleEffect(EffectEventId.onCustom, "select");
	}

	@Override
	public void init(final Properties parameter, final Attributes controlDefinitionAttributes) {
		listBoxImpl.updateViewTotalCount();
		listBoxImpl.updateViewScroll();

		subscribeHorizontalScrollbar();
		subscribeVerticalScrollbar();

		nifty.subscribe(screen, getId(), ElementShowEvent.class, listBoxControlShowEventSubscriber);

		super.init(parameter, controlDefinitionAttributes);
	}

	private void initializeHorizontalScrollbar() {
		final Scrollbar horizontalS = getHorizontalScrollbar();
		if(horizontalS != null && horizontalS.isBound()) {
			horizontalS.setWorldMax(lastMaxWidth);
			horizontalS.setWorldPageSize(listBoxPanelElement.getWidth());
		}
	}

	private void initializeScrollElementHeight() {
		scrollElement.setConstraintHeight(new SizeValue(displayItems * labelTemplateHeight + "px"));
	}

	private void initializeScrollPanel(final Screen screen) {
		if(horizontalScrollbar == ScrollbarMode.off || horizontalScrollbar == ScrollbarMode.optional) {
			final Element horizontal = getElement().findElementByName("#horizontal-scrollbar-parent");
			if(horizontal != null)
				nifty.removeElement(screen, horizontal);
		}
		if(verticalScrollbar == ScrollbarMode.off || verticalScrollbar == ScrollbarMode.optional) {
			final Element vertical = getElement().findElementByName("#vertical-scrollbar");
			if(vertical != null)
				nifty.removeElement(screen, vertical);
		}

		childRootElement.setConstraintX(new SizeValue("0px"));
		childRootElement.setConstraintY(new SizeValue("0px"));

		updateBottomRightElement();
		nifty.executeEndOfFrameElementActions();
		screen.layoutLayers();
	}

	private void initializeVerticalScrollbar(final Screen screen, final float labelTemplateHeight, final int itemCount) {
		final Scrollbar verticalS = getVerticalScrollbar();
		if(verticalS != null && verticalS.isBound()) {
			verticalS.setWorldMax(itemCount * labelTemplateHeight);
			verticalS.setWorldPageSize(displayItems * labelTemplateHeight);
			verticalS.setButtonStepSize(labelTemplateHeight);
		}
	}

	private void initLabelTemplateData() {
		if(labelTemplateElement == null)
			return;
		labelTemplateElement.getParent().layoutElements();
		labelTemplateHeight = labelTemplateElement.getHeight();
		nifty.removeElement(screen, labelTemplateElement);
	}

	private void initSelectionMode(final ListBoxImpl<T> listBoxImpl, final String selectionMode, final String forceSelection) {
		SelectionMode listBoxSelectionMode = SelectionMode.Single;
		try {
			listBoxSelectionMode = SelectionMode.valueOf(selectionMode);
		} catch(final RuntimeException e) {
			log.warning("Unsupported value for selectionMode [" + selectionMode + "]. Fall back to using single selection mode.");
		}

		listBoxImpl.changeSelectionMode(listBoxSelectionMode, "true".equalsIgnoreCase(forceSelection));
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void insertItem(final T item, final int index) {
		listBoxImpl.insertItem(item, index);
	}

	@Override
	public int itemCount() {
		return listBoxImpl.itemCount();
	}

	@Override
	public void layoutCallback() {
		if(listBoxPanelElement == null)
			return;
		ensureWidthConstraints();
		initializeHorizontalScrollbar();
	}

	public void mouseWheel(final Element element, final NiftyMouseInputEvent inputEvent) {
		final int mouseWheel = inputEvent.getMouseWheel();

		final Scrollbar scrollbar = getVerticalScrollbar();

		if(scrollbar != null) {
			final float currentValue = scrollbar.getValue();

			if(mouseWheel < 0)
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
			else if(mouseWheel > 0)
				scrollbar.setValue(currentValue - scrollbar.getButtonStepSize() * mouseWheel);
		}
	}

	@Override
	public void onFocus(final boolean getFocus) {
		super.onFocus(getFocus);
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void publish(final ListBoxSelectionChangedEvent<T> event) {
		if(getElement().getId() != null)
			nifty.publishEvent(getElement().getId(), event);
	}

	@Override
	public void refresh() {
		listBoxImpl.updateView();
	}

	@Override
	public void removeAllItems(final List<T> itemsToRemove) {
		listBoxImpl.removeAllItems(itemsToRemove);
	}

	@Override
	public void removeItem(final T item) {
		listBoxImpl.removeItem(item);
	}

	@Override
	public void removeItemByIndex(final int itemIndex) {
		listBoxImpl.removeItemByIndex(itemIndex);
	}

	@Override
	public void scrollTo(final int newPosition) {
		final Scrollbar verticalS = getVerticalScrollbar();
		if(verticalS != null)
			verticalS.setValue(newPosition * labelTemplateHeight);
	}

	// internals

	@Override
	public void selectItem(final T item) {
		listBoxImpl.selectItem(item);
	}

	@Override
	public void selectItemByIndex(final int selectionIndex) {
		listBoxImpl.selectItemByIndex(selectionIndex);
	}

	@Override
	public void selectNext() {
		listBoxImpl.selectNext();
	}

	@Override
	public void selectPrevious() {
		listBoxImpl.selectPrevious();
	}

	private void setElementHeight() {
		getElement().setConstraintHeight(new SizeValue(displayItems * labelTemplateHeight + findHorizontalScrollbarHeight() + "px"));
		screen.layoutLayers();
	}

	@Override
	public void setFocus() {
		childRootElement.setFocus();
	}

	@Override
	public void setFocusItem(final T item) {
		listBoxImpl.setFocusItem(item);
	}

	@Override
	public void setFocusItemByIndex(final int itemIndex) {
		listBoxImpl.setFocusItemByIndex(itemIndex);
	}

	@SuppressWarnings("unchecked")
	private void setListBoxItemIndex(final int itemIndex) {
		final ListBoxItemController<T> listBoxItemController = labelElements[itemIndex].getControl(ListBoxItemController.class);
		if(listBoxItemController != null)
			listBoxItemController.setItemIndex(itemIndex);
	}

	@Override
	public void setListBoxViewConverter(final ListBoxViewConverter<T> viewConverter) {
		this.viewConverter = viewConverter;
	}

	@Override
	public void showItem(final T item) {
		listBoxImpl.showItem(item);
	}

	@Override
	public void showItemByIndex(final int itemIndex) {
		listBoxImpl.showItemByIndex(itemIndex);
	}

	@Override
	public void sortAllItems() {
		listBoxImpl.sortItems(null);
	}

	@Override
	public void sortAllItems(final Comparator<T> comperator) {
		listBoxImpl.sortItems(comperator);
	}

	private void subscribeHorizontalScrollbar() {
		final Element scrollbar = getElement().findElementByName("#horizontal-scrollbar");

		if(scrollbar != null)
			nifty.subscribe(screen, scrollbar.getId(), ScrollbarChangedEvent.class, horizontalScrollbarSubscriber);
	}

	private void subscribeVerticalScrollbar() {
		final Element scrollbar = getElement().findElementByName("#vertical-scrollbar");

		if(scrollbar != null)
			nifty.subscribe(screen, scrollbar.getId(), ScrollbarChangedEvent.class, verticalScrollbarSubscriber);
	}

	private void unsubscribeHorizontalScrollbar() {
		final Element scrollbar = getElement().findElementByName("#horizontal-scrollbar");
		if(scrollbar != null)
			nifty.unsubscribe(scrollbar.getId(), horizontalScrollbarSubscriber);
	}

	private void unsubscribeVerticalScrollbar() {
		final Element scrollbar = getElement().findElementByName("#vertical-scrollbar");

		if(scrollbar != null)
			nifty.unsubscribe(scrollbar.getId(), verticalScrollbarSubscriber);
	}

	private void updateBottomRightElement() {
		final Element horizontal = getElement().findElementByName("#horizontal-scrollbar-parent");
		final Element vertical = getElement().findElementByName("#vertical-scrollbar");
		final Element bottomRight = getElement().findElementByName("#bottom-right");
		if(horizontal != null)
			if(vertical == null) {
				if(bottomRight != null) {
					nifty.removeElement(screen, bottomRight);
					nifty.executeEndOfFrameElementActions();
					initializeHorizontalScrollbar();
				}
			} else if(bottomRight == null) {
				nifty.createElementFromType(screen, horizontal, bottomRightTemplate.getElementType());
				initializeHorizontalScrollbar();
			}
	}

	@Override
	public void updateTotalCount(final int newCount) {
		final ScrollbarMode verticalScrollbar = getVerticalScrollbarMode();

		if(verticalScrollbar == ScrollbarMode.optional) {
			final Element vertical = getElement().findElementByName("#vertical-scrollbar");

			if(newCount > displayItems) {
				if(vertical == null) {
					final ElementType templateType = verticalScrollbarTemplate.getElementType().copy();
					final CustomControlCreator create = new CustomControlCreator((ControlType) templateType);
					final Element e = create.create(nifty, screen, scrollElement);
					if(e.getHeight() < 23 * 2) { // ugly
						nifty.removeElement(screen, e);
						return;
					}
					subscribeVerticalScrollbar();
					ensureWidthConstraints();

					updateBottomRightElement();
					nifty.executeEndOfFrameElementActions();
					screen.layoutLayers();
				}
			} else if(newCount <= displayItems)
				if(vertical != null) {
					unsubscribeVerticalScrollbar();
					nifty.removeElement(screen, vertical);
					nifty.executeEndOfFrameElementActions();
					ensureWidthConstraints();

					updateBottomRightElement();
					nifty.executeEndOfFrameElementActions();
					screen.layoutLayers();
				}
		}

		initializeVerticalScrollbar(screen, labelTemplateHeight, newCount);
	}

	@Override
	public void updateTotalWidth(final int newWidth) {
		this.lastMaxWidth = newWidth;
		if(horizontalScrollbar == ScrollbarMode.optional) {
			final Element horizontal = getElement().findElementByName("#horizontal-scrollbar-parent");
			if(newWidth > listBoxPanelElement.getWidth()) {
				if(horizontal == null) {
					final ElementType type = horizontalScrollbarTemplate.getElementType().copy();
					applyIdPrefixToElementType(getElement().getId(), type);
					nifty.createElementFromType(screen, getElement(), type);

					updateBottomRightElement();
					nifty.executeEndOfFrameElementActions();
					screen.layoutLayers();

					setElementHeight();
					subscribeHorizontalScrollbar();
				}
			} else if(newWidth <= listBoxPanelElement.getWidth())
				if(horizontal != null) {
					unsubscribeHorizontalScrollbar();
					nifty.removeElement(screen, horizontal);
					nifty.executeEndOfFrameElementActions();
					setElementHeight();
				}
		}
		initializeHorizontalScrollbar();
		ensureWidthConstraints();
	}
}
