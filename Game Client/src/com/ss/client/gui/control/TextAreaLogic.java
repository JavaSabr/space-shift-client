package com.ss.client.gui.control;

import com.ss.client.util.ReflectionClass;

import de.lessvoid.nifty.Clipboard;
import de.lessvoid.nifty.controls.textfield.TextFieldView;

/**
 * Реализация логики работы TextArea элемента.
 * 
 * @author Ronn
 */
@ReflectionClass
public class TextAreaLogic {

	/** контейнер текста */
	private StringBuilder text;
	/** буфер обмена */
	private final Clipboard clipboard;
	/** отображатель текста */
	private final TextFieldView view;

	/** текущая позиция курсоа */
	private int cursorPosition;
	/** стартовая позиция курсора */
	private int selectionStart;
	/** конечная позиция курсора */
	private int selectionEnd;
	/** максимальная длинна */
	private int maxLength;
	/** стартовая позиция выделения */
	private int selectionStartIndex;

	/** флаг выделенности текста */
	private boolean selecting;

	/**
	 * Создание обработчика области ввода.
	 * 
	 * @param text начальный текст.
	 * @param clipboard буфер обмена.
	 * @param textFieldView отображатель текста.
	 */
	public TextAreaLogic(final String text, final Clipboard clipboard, final TextFieldView textFieldView) {
		this.selectionStart = -1;
		this.selectionEnd = -1;
		this.maxLength = -1;
		this.view = textFieldView;

		initText(text);

		this.clipboard = clipboard;
		this.maxLength = -1;
	}

	/**
	 * Удаление символов.
	 */
	public void backspace() {
		// получаем старый текст
		final String old = text.toString();

		// если есть выделение
		if(hasSelection()) {
			// удаляем выделенную область
			text.delete(selectionStart, selectionEnd);
			// обновляем позицию курсора
			cursorPosition = selectionStart;
			// сбрасываем выделение
			resetSelection();
		}
		// если курсор не в начале
		else if(cursorPosition > 0) {
			// удаляем предыдущий символ
			text.delete(cursorPosition - 1, cursorPosition);
			// смещаем курсор назад
			cursorPosition--;
		}

		// уведомляем об обновлении текста
		notifyTextChange(old);
	}

	/**
	 * Смена внутреннего текста.
	 * 
	 * @param newText новый текст.
	 */
	private void changeText(final String newText) {
		// создаем новый билдер
		text = new StringBuilder();

		// если текст есть
		if(newText != null)
			// вносим его в билдер
			text.append(newText);

		// если текущая позиция курсора больше текста
		if(cursorPosition > text.length())
			// возвращаем назад ег
			cursorPosition = 0;

		setSelectionStart(-1);
		setSelectionEnd(-1);
		setSelecting(false);
		setSelectionStartIndex(-1);
	}

	/**
	 * Копирование текста в буффер обмена.
	 * 
	 * @param passwordChar текущий символ заменяющий текст.
	 */
	public void copy(final Character passwordChar) {
		// получаем выделенный текст
		final String selectedText = modifyWithPasswordChar(getSelectedText(), passwordChar);

		// если такой текст есть
		if(selectedText != null)
			// вносим в буффер обмена
			clipboard.put(selectedText);
	}

	/**
	 * Переместить курсор вниз.
	 */
	public void cursorDown() {
		// получаем строкудо курсора
		final String q = text.substring(cursorPosition);

		// если нет строк ниже
		if(q.indexOf('n') == -1)
			// ставим позицию курсора на последний символ
			cursorPosition = text.length();
		else {
			// определяем новое положение курсора
			final int next = cursorPosition + q.indexOf('n') + 1;

			// определяем положение окончания ниже строки
			int rowEnd = next + text.substring(next).indexOf('n');

			if(rowEnd < next)
				rowEnd = text.length();

			final int offset = cursorPosition - (text.substring(0, cursorPosition).lastIndexOf('n') + 1);
			if(offset > rowEnd - next)
				cursorPosition = rowEnd;
			else
				cursorPosition = next + offset;
		}

		// если активирован режим выделения
		if(isSelecting())
			// выделяем в соответствии с курсором
			selectionFromCursorPosition();
		else
			// иначе сбрасываем выделение
			resetSelection();
	}

	/**
	 * Move cursor left.
	 */
	public void cursorLeft() {
		cursorPosition--;
		if(cursorPosition < 0)
			cursorPosition = 0;

		// если активирован режим выделения
		if(isSelecting())
			// выделяем в соответствии с курсором
			selectionFromCursorPosition();
		else
			// иначе сбрасываем выделение
			resetSelection();
	}

	/**
	 * Move cursor right.
	 */
	public void cursorRight() {
		cursorPosition++;
		if(cursorPosition > text.length())
			cursorPosition = text.length();

		// если активирован режим выделения
		if(isSelecting())
			// выделяем в соответствии с курсором
			selectionFromCursorPosition();
		else
			// иначе сбрасываем выделение
			resetSelection();
	}

	/**
	 * Move cursor up.
	 */
	public void cursorUp() {
		// Check if there is a previous row
		final String q = text.substring(0, cursorPosition);
		if(q.indexOf('n') == -1)
			cursorPosition = 0;
		else {
			final int prev = q.lastIndexOf('n') + 1;
			final int prev2 = text.substring(0, prev - 1).lastIndexOf('n') + 1;

			final int offset = cursorPosition - prev;
			if(offset > prev - 1 - prev2)
				cursorPosition = prev - 1;
			else
				cursorPosition = prev2 + offset;
		}

		// если активирован режим выделения
		if(isSelecting())
			// выделяем в соответствии с курсором
			selectionFromCursorPosition();
		else
			// иначе сбрасываем выделение
			resetSelection();
	}

	/**
	 * Вырезка текста с занесением в буффер обмена.
	 * 
	 * @param passwordChar текущий символ заменяющий текст.
	 */
	public void cut(final Character passwordChar) {
		// получаем выделенный текст
		final String selectedText = getSelectedText();

		// если такого нет, выходим
		if(selectedText == null)
			return;

		// вносим в буффер вырезанный текст
		clipboard.put(modifyWithPasswordChar(selectedText, passwordChar));

		// удаляем из текстовой области
		delete();
	}

	/**
	 * Delete the character at the cursor position.
	 */
	public void delete() {
		// получаем старый текст
		final String old = text.toString();

		// если нету выделения
		if(!hasSelection())
			// просто удаляем симво после курсора
			text.delete(cursorPosition, cursorPosition + 1);
		else {
			// удаляем выделенный фрагмент
			text.delete(selectionStart, selectionEnd);
			// обновляем текущую позицию
			cursorPosition = selectionStart;
			// сбрасываем выделение
			resetSelection();
		}

		// уведомляем об изменении текста
		notifyTextChange(old);
	}

	/**
	 * Завершение выделения.
	 */
	public void endSelecting() {
		setSelecting(false);
	}

	/**
	 * @return текущее положение курсора.
	 */
	public int getCursorPosition() {
		return cursorPosition;
	}

	/**
	 * @return максимальная длинна текста.
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @return выделенный текст.
	 */
	public String getSelectedText() {
		// если ничего не выделенно, возвращаем пустоту
		if(!hasSelection())
			return null;

		// копируем нужный нам текст
		return text.substring(selectionStart, selectionEnd);
	}

	/**
	 * @return коечный индекс выделения.
	 */
	public int getSelectionEnd() {
		return selectionEnd;
	}

	/**
	 * @return начальный индекс выделения.
	 */
	public int getSelectionStart() {
		return selectionStart;
	}

	/**
	 * @return текущий текст в поле.
	 */
	public String getText() {
		return text.toString();
	}

	/**
	 * @return есть ли выделенный фрагмент.
	 */
	public boolean hasSelection() {
		return selectionStart != -1 && selectionEnd != -1;
	}

	/**
	 * Инициализация начального текста.
	 * 
	 * @param newText новый текст.
	 */
	private void initText(final String newText) {
		text = new StringBuilder();

		if(newText != null)
			text.append(newText);

		cursorPosition = 0;
		selectionStart = -1;
		selectionEnd = -1;
		selecting = false;
		selectionStartIndex = -1;
	}

	/**
	 * init instance wit the given text.
	 * 
	 * @param newText new text
	 */
	public void initWithText(final String newText) {
		changeText(newText);

		if(newText != null && newText.length() > 0)
			view.textChangeEvent(newText);
	}

	/**
	 * Вставка нового символа в текстовое поле.
	 * 
	 * @param c новый символ.
	 */
	public void insert(final char c) {
		// получаем старый текст
		final String old = text.toString();

		// если сейчас есть выделенная область
		if(hasSelection()) {
			// удаляем из нее текст
			text.delete(selectionStart, selectionEnd);
			// обновляем позицию курсора
			cursorPosition = selectionStart;
			// сбрасываем выделение
			resetSelection();
		}

		// если есть место для вставки
		if(maxLength == -1 || text.length() < maxLength) {
			// вставляем символ
			text.insert(cursorPosition, c);
			// смещаем позицию
			cursorPosition++;
		}

		// уведомляем о обновлении текста
		notifyTextChange(old);
	}

	/**
	 * @return активировано ли выделение.
	 */
	public boolean isSelecting() {
		return selecting;
	}

	/**
	 * TODO
	 * 
	 * @param selectedText
	 * @param passwordChar
	 * @return
	 */
	private String modifyWithPasswordChar(final String selectedText, final Character passwordChar) {
		if(passwordChar == null)
			return selectedText;
		if(selectedText == null)
			return null;
		final String result = selectedText;
		return result.replaceAll(".", new String(new char[] { passwordChar
		}));
	}

	/**
	 * Уведомление о изменении текста.
	 * 
	 * @param old старый текст.
	 */
	private void notifyTextChange(final String old) {
		// получаем текущий текст
		final String current = text.toString();

		// если он эквивалентен новому, то выходим
		if(old.equals(current))
			return;

		// уведомляем текстовую область
		view.textChangeEvent(current);
	}

	/**
	 * Вставка текста из буффера обмена.
	 */
	public void paste() {
		// получаем текущие данные из буфера
		final String clipboardText = clipboard.get();

		// если они есть
		if(clipboardText != null)
			// вносим их в текстовую область
			for(int i = 0; i < clipboardText.length(); i++)
				insert(clipboardText.charAt(i));
	}

	/**
	 * Сбрасывание выделения.
	 */
	public void resetSelection() {
		setSelectionStart(-1);
		setSelectionEnd(-1);
		setSelecting(false);
	}

	/**
	 * Выделить фрагмент по положению курсора.
	 */
	private void selectionFromCursorPosition() {
		// если текущее положение равняется началу выделения
		if(cursorPosition == selectionStartIndex)
			// сбрасываем выделение
			resetSelection();
		// если курсор расположен после начального индекса выделения
		else if(cursorPosition > selectionStartIndex) {
			// начальный индекс так и остается
			setSelectionStart(selectionStartIndex);
			// конечный становится текущим
			setSelectionEnd(getCursorPosition());
		}
		// если курсор расположен перед началом выделения
		else {
			// то началом выделения становится текущее положение курсора
			setSelectionStart(getCursorPosition());
			// а конечное, началом выделения
			setSelectionEnd(selectionStartIndex);
		}
	}

	/**
	 * Обновление позиции курсора.
	 * 
	 * @param index новое положение.
	 */
	public void setCursorPosition(final int index) {
		if(index < 0)
			cursorPosition = 0;
		else if(index > text.length())
			cursorPosition = text.length();
		else
			cursorPosition = index;

		if(isSelecting())
			selectionFromCursorPosition();
	}

	/**
	 * Установка максимального размера текста.
	 * 
	 * @param limit максимальная длинна.
	 */
	public void setMaxLength(final int limit) {
		// вносим новый лимит
		setMaxLength(limit);

		// если она указана
		if(getMaxLength() != -1)
			// если текущий превышает ее
			if(text.length() > limit) {
				// отодвигаем курсор на конец лимита
				setCursorPosition(limit);
				// запускаем выделение
				startSelecting();
				// ставим курсор на конец текущего текста
				setCursorPosition(text.length());
				// удаляем текст
				delete();
			}
	}

	/**
	 * @param selecting флаг выделения.
	 */
	public void setSelecting(final boolean selecting) {
		this.selecting = selecting;
	}

	/**
	 * @param selectionEnd индекс завершения выделения.
	 */
	public void setSelectionEnd(final int selectionEnd) {
		this.selectionEnd = selectionEnd;
	}

	/**
	 * @param selectionStart индекс начала выделения.
	 */
	public void setSelectionStart(final int selectionStart) {
		this.selectionStart = selectionStart;
	}

	/**
	 * @param selectionStartIndex начальный индекс выделения.
	 */
	public void setSelectionStartIndex(final int selectionStartIndex) {
		this.selectionStartIndex = selectionStartIndex;
	}

	/**
	 * Запуск выделения.
	 */
	public void startSelecting() {
		// ставим флаг выделения
		setSelecting(true);
		// ставим неачало выделения текущую позицию курсора
		setSelectionStartIndex(getCursorPosition());
	}

	/**
	 * Перемещению курсора к первому символу в строке.
	 */
	public void toFirstPosition() {
		// определяем позици ю первого символа в строке
		cursorPosition = text.substring(0, cursorPosition).lastIndexOf("n") + 1;

		// если активировано выделение
		if(isSelecting())
			// выделяем область с учетом нового положения корусора
			selectionFromCursorPosition();
		// если был уже выделен фрагмент
		else if(hasSelection())
			// сбрасываем выделение
			resetSelection();
	}

	/**
	 * Перемещение курсора к последнему символу в строке.
	 */
	public void toLastPosition() {
		// определяем новую позицию
		final int curPos = text.substring(cursorPosition).indexOf("n");

		// применяем ее
		cursorPosition = curPos > -1 ? curPos + cursorPosition : text.length();

		// если активировано выделение
		if(isSelecting())
			// выделяем область с учетом нового положения корусора
			selectionFromCursorPosition();
		// если был уже выделен фрагмент
		else if(hasSelection())
			// сбрасываем выделение
			resetSelection();
	}
}