package com.ss.client.gui.control.impl;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.bushe.swing.event.EventTopicSubscriber;

import com.ss.client.gui.control.SButton;
import com.ss.client.gui.control.event.ButtonClickedEvent;
import com.ss.client.gui.element.impl.AbstractElementUI;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;
import com.ss.client.util.ReflectionClass;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.FocusHandler;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.layout.align.VerticalAlign;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контрола кнопки.
 * 
 * @author Ronn
 */
@ReflectionClass
public final class SButtonImpl extends AbstractElementUI implements SButton {

	private static final Set<NiftyInputEvent> HANDLED_INPUT_EVENT = new HashSet<>();

	static {
		HANDLED_INPUT_EVENT.add(NiftyInputEvent.NextInputElement);
		HANDLED_INPUT_EVENT.add(NiftyInputEvent.PrevInputElement);
		HANDLED_INPUT_EVENT.add(NiftyInputEvent.Activate);
	}

	/** контролер UI */
	private Nifty nifty;
	/** экран элемента */
	private Screen screen;

	/** обработчик фокуса на элементе */
	private FocusHandler focusHandler;

	/** элемент для отображеният екста кнопки */
	private Element textElement;
	/** рендер текста на кнопке */
	private TextRenderer textRenderer;

	@Override
	public void activate() {
		buttonClick();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.nifty = nifty;
		this.screen = screen;
		this.textElement = element.findElementByName("#text");
		this.textRenderer = element.getRenderer(TextRenderer.class);
		this.focusHandler = screen.getFocusHandler();
	}

	private void buttonClick() {
		getElement().onClick();
	}

	/**
	 * @return обработчик фокуса на элементею
	 */
	protected FocusHandler getFocusHandler() {
		return focusHandler;
	}

	@Override
	public RenderFont getFont() {
		return getTextRenderer().getFont();
	}

	/**
	 * @return контролер UI.
	 */
	protected Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return экран элемента.
	 */
	protected Screen getScreen() {
		return screen;
	}

	@Override
	public String getText() {
		return getTextRenderer().getOriginalText();
	}

	@Override
	public Color getTextColor() {
		return getTextRenderer().getColor();
	}

	/**
	 * @return элемент для отображеният екста кнопкию
	 */
	protected Element getTextElement() {
		return textElement;
	}

	@Override
	public HorizontalAlign getTextHAlign() {
		return getTextRenderer().getTextHAlign();
	}

	@Override
	public int getTextHeight() {
		return getTextRenderer().getTextHeight();
	}

	protected TextRenderer getTextRenderer() {
		return textRenderer;
	}

	@Override
	public VerticalAlign getTextVAlign() {
		return getTextRenderer().getTextVAlign();
	}

	@Override
	public int getTextWidth() {
		return getTextRenderer().getTextWidth();
	}

	@Override
	public void init(final Properties parameter, final Attributes controlDefinitionAttributes) {

		EventTopicSubscriber<NiftyMousePrimaryClickedEvent> subscriber = new EventTopicSubscriber<NiftyMousePrimaryClickedEvent>() {

			@Override
			public void onEvent(final String topic, final NiftyMousePrimaryClickedEvent data) {
				getNifty().publishEvent(topic, ButtonClickedEvent.get(SButtonImpl.this));
			}
		};

		getNifty().subscribe(getScreen(), getElement().getId(), NiftyMousePrimaryClickedEvent.class, subscriber);

		super.init(parameter, controlDefinitionAttributes);
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {

		FocusHandler focusHandler = getFocusHandler();
		Element element = getElement();

		if(focusHandler != null) {

			if(inputEvent == NiftyInputEvent.NextInputElement) {
				focusHandler.getNext(element).setFocus();
			} else if(inputEvent == NiftyInputEvent.PrevInputElement) {
				focusHandler.getPrev(element).setFocus();
			} else if(inputEvent == NiftyInputEvent.MoveCursorDown) {

				Element nextElement = focusHandler.getNext(element);

				if(ElementUtils.equalsParent(element, nextElement)) {
					nextElement.setFocus();
					return true;
				}

			} else if(inputEvent == NiftyInputEvent.MoveCursorUp) {

				Element prevElement = focusHandler.getPrev(element);

				if(ElementUtils.equalsParent(element, prevElement)) {
					prevElement.setFocus();
					return true;
				}
			}
		}

		if(inputEvent == NiftyInputEvent.Activate) {
			buttonClick();
		}

		return HANDLED_INPUT_EVENT.contains(inputEvent);
	}

	@Override
	public void setFont(final RenderFont fontParam) {
		getTextRenderer().setFont(fontParam);
	}

	@Override
	public void setText(final String text) {

		TextRenderer textRenderer = getTextRenderer();
		textRenderer.setText(text);

		if(!textRenderer.isLineWrapping()) {
			getTextElement().setConstraintWidth(GameUtil.getPixelSize(textRenderer.getTextWidth()));
		}
	}

	@Override
	public void setTextColor(final Color newColor) {
		getTextRenderer().setColor(newColor);
	}

	@Override
	public void setTextHAlign(final HorizontalAlign newTextHAlign) {
		getTextRenderer().setTextHAlign(newTextHAlign);
	}

	@Override
	public void setTextVAlign(final VerticalAlign newTextVAlign) {
		getTextRenderer().setTextVAlign(newTextVAlign);
	}
}
