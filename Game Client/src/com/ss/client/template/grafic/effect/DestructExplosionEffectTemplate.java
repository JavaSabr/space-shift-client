package com.ss.client.template.grafic.effect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.ss.client.model.effect.ExplosionEffect;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.network.ServerPacket;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Шаблон эффекта взрыва разрушения.
 * 
 * @author Ronn
 */
public class DestructExplosionEffectTemplate extends ExplosionEffectTemplate {

	/**
	 * Информация о взрыве во время серии взрывов.
	 * 
	 * @author Ronn
	 */
	public final static class ExplosionInfo {

		/** радиус разброса */
		private final int radius;

		/** задержка */
		private final int delay;

		/** маштаб взрыва */
		private final float scale;

		private ExplosionInfo(int radius, int delay, float scale) {
			this.radius = radius;
			this.delay = delay;
			this.scale = scale;
		}

		/**
		 * @return задержка.
		 */
		public final int getDelay() {
			return delay;
		}

		/**
		 * @return радиус разброса.
		 */
		public final int getRadius() {
			return radius;
		}

		/**
		 * @return маштаб взрыва.
		 */
		public final float getScale() {
			return scale;
		}
	}

	/** пул использованных графических эффектов */
	private final FoldablePool<ExplosionEffect> explosionPool = Pools.newConcurrentFoldablePool(ExplosionEffect.class);

	/** серия взрывов */
	private final ExplosionInfo[] explosions;

	public DestructExplosionEffectTemplate(ServerPacket packet) {
		super(packet);

		int count = packet.readByte();

		this.explosions = new ExplosionInfo[count];

		for(int i = 0; i < count; i++) {
			explosions[i] = new ExplosionInfo(packet.readInt(), packet.readInt(), packet.readFloat());
		}
	}

	@Override
	public GraficEffectType getEffectType() {
		return GraficEffectType.DESTRUCT_EXPLOSION;
	}

	/**
	 * @return список взрывов.
	 */
	public ExplosionInfo[] getExplosions() {
		return explosions;
	}

	@Override
	public void put(GraficEffect effect) {

		if(effect instanceof ExplosionEffect) {
			explosionPool.put((ExplosionEffect) effect);
		} else {
			super.put(effect);
		}
	}

	public ExplosionEffect takeExplosion() {

		ExplosionEffect instance = explosionPool.take();

		if(instance == null) {
			try {

				GraficEffectType effectType = GraficEffectType.EXPLOSION;

				Class<? extends GraficEffect> instanceClass = effectType.getInstanceClass();
				Constructor<? extends GraficEffect> constructor = instanceClass.getConstructor(getClass());

				instance = (ExplosionEffect) constructor.newInstance(this);

			} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				LOGGER.warning(e);
			}
		}

		return instance;
	}
}
