package com.ss.client.template.grafic.effect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.network.ServerPacket;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Базовая реализация шаблона графического эффекта.
 * 
 * @author Ronn
 */
public abstract class GraficEffectTemplate {

	protected static final Logger LOGGER = Loggers.getLogger(GraficEffectTemplate.class);

	/** пул использованных графических эффектов */
	private final FoldablePool<GraficEffect> pool = Pools.newConcurrentFoldablePool(GraficEffect.class);

	/** модификатор кол-ва частиц эффектов */
	private final float countFactor;

	/** ид шаблона */
	private final int id;

	public GraficEffectTemplate(ServerPacket packet) {
		this.id = packet.readInt();
		this.countFactor = 1F;
	}

	/**
	 * @return модификатор кол-ва частиц эффектов.
	 */
	public float getCountFactor() {
		return countFactor;
	}

	/**
	 * @return тип графического эффекта.
	 */
	public GraficEffectType getEffectType() {
		return null;
	}

	/**
	 * @return ид шаблона эффекта.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Складирование использованного эффекта в пул эффектов.
	 * 
	 * @param effect использованный эффект.
	 */
	public void put(GraficEffect effect) {
		pool.put(effect);
	}

	/**
	 * @return объект эффект.
	 */
	public GraficEffect takeInstance() {

		GraficEffect instance = pool.take();

		if(instance == null) {
			try {

				GraficEffectType effectType = getEffectType();

				Class<? extends GraficEffect> instanceClass = effectType.getInstanceClass();
				Constructor<? extends GraficEffect> constructor = instanceClass.getConstructor(getClass());

				instance = constructor.newInstance(this);

			} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				LOGGER.warning(e);
			}
		}

		return instance;
	}
}
