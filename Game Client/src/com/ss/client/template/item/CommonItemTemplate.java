package com.ss.client.template.item;

import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.item.common.CommonItemType;

import rlib.util.VarTable;

/**
 * Шаблон раличных предметов.
 * 
 * @author Ronn
 */
public class CommonItemTemplate extends ItemTemplate {

	public CommonItemTemplate(final VarTable vars) {
		super(vars);
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return CommonItemType.valueOf(vars.getInteger(TYPE));
	}
}
