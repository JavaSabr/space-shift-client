package com.ss.client.template.ship;

import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerModuleSystem;
import com.ss.client.model.ship.player.PlayerShipClass;

import rlib.util.VarTable;

/**
 * Шаблон корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipTemplate extends ShipTemplate {

	public static final String CLASS = "class";

	/** класс корабля */
	private final PlayerShipClass shipClass;

	public PlayerShipTemplate(final VarTable vars) {
		super(vars);

		this.shipClass = PlayerShipClass.valueOf(vars.getInteger(CLASS));
	}

	@Override
	public Class<? extends ModuleSystem> getModuleSystemClass() {
		return PlayerModuleSystem.class;
	}

	/**
	 * @return класс корабля игрока.
	 */
	public PlayerShipClass getShipClass() {
		return shipClass;
	}
}
