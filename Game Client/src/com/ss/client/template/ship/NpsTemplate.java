package com.ss.client.template.ship;

import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.nps.NpsModuleSystem;
import com.ss.client.table.LangTable;

import rlib.util.VarTable;

/**
 * Шаблон для создания Nps.
 * 
 * @author Ronn
 */
public class NpsTemplate extends ShipTemplate {

	public static final String LEVEL = "level";
	public static final String NAME = "name";

	/** название корабля */
	protected String name;

	/** уровень корабля */
	protected int level;

	public NpsTemplate(VarTable vars) {
		super(vars);

		LangTable langTable = LangTable.getInstance();

		this.name = langTable.getText(vars.getString(NAME));
		this.level = vars.getInteger(LEVEL);
	}

	/**
	 * @return уровень Nps.
	 */
	public int getLevel() {
		return level;
	}

	@Override
	public Class<? extends ModuleSystem> getModuleSystemClass() {
		return NpsModuleSystem.class;
	}

	/**
	 * @return имя Nps.
	 */
	public String getName() {
		return name;
	}
}
