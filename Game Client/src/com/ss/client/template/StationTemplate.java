package com.ss.client.template;

import rlib.util.VarTable;

import com.jme3.scene.Spatial;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.station.SpaceStationType;

/**
 * Шаблон космической станции.
 * 
 * @author Ronn
 */
public class StationTemplate extends GravityObjectTemplate {

	public static final String NAME = "name";

	/** название станции */
	private final String name;

	public StationTemplate(final VarTable vars) {
		super(vars);

		this.name = vars.getString(NAME);
	}

	/**
	 * @return название станции.
	 */
	public final String getName() {
		return name;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return SpaceStationType.valueOf(vars.getInteger(TYPE));
	}

	@Override
	public Spatial loadModel() {
		return assetManager.loadModel(modelKey);
	}

	@Override
	public String toString() {
		return super.toString() + " name = " + name;
	}
}
