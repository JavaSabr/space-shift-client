package com.ss.client.template;

import rlib.util.VarTable;

import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.module.ModuleType;
import com.ss.client.model.module.info.BlasterRayInfo;
import com.ss.client.model.module.info.EngineFireInfo;
import com.ss.client.model.module.info.ForceShieldInfo;
import com.ss.client.model.module.info.RocketShotInfo;

/**
 * Шаблон модуля корабля.
 * 
 * @author Ronn
 */
public class ModuleTemplate extends ObjectTemplate {

	public static final String ROCKET_SHOT_INFO = "rocketShotInfo";
	public static final String FORCE_SHIELD_INFO = "forceShieldIfgo";
	public static final String SKILLS = "skills";
	public static final String ICON = "icon";
	public static final String ENGINE_FIRE_INFO = "engineFireInfo";
	public static final String BLASTER_RAY_INFO = "blasterRayInfo";

	private static final EngineFireInfo[] EMPTY_ENGINE_FIRE_INFOS = new EngineFireInfo[0];
	private static final BlasterRayInfo[] EMPTY_BLASTER_RAY_INFOS = new BlasterRayInfo[0];

	/** скилы модуля */
	private final SkillTemplate[] skills;

	/** шаблон частиц двигателя */
	private final EngineFireInfo[] engineFireInfos;
	/** шаблоны лучей бластеров */
	private final BlasterRayInfo[] blasterRayInfos;
	/** информация о силовом щите модуля */
	private final ForceShieldInfo forceShieldInfo;
	/** информация о ракете модуля */
	private final RocketShotInfo rocketShotInfo;

	/** путь к иконке модуля */
	private final String icon;

	public ModuleTemplate(final VarTable vars) {
		super(vars);

		this.skills = vars.getGeneric(SKILLS, SkillTemplate[].class);
		this.engineFireInfos = vars.get(ENGINE_FIRE_INFO, EngineFireInfo[].class, EMPTY_ENGINE_FIRE_INFOS);
		this.blasterRayInfos = vars.get(BLASTER_RAY_INFO, BlasterRayInfo[].class, EMPTY_BLASTER_RAY_INFOS);
		this.forceShieldInfo = vars.get(FORCE_SHIELD_INFO, ForceShieldInfo.class, null);
		this.rocketShotInfo = vars.get(ROCKET_SHOT_INFO, RocketShotInfo.class, null);
		this.icon = vars.getString(ICON);
	}

	/**
	 * @return шаблоны лучей бластеров.
	 */
	public BlasterRayInfo[] getBlasterRayInfos() {
		return blasterRayInfos;
	}

	/**
	 * @return шаблоны частиц двигателя.
	 */
	public final EngineFireInfo[] getEngineFireInfos() {
		return engineFireInfos;
	}

	/**
	 * @return информация о силовом щите модуля.
	 */
	public ForceShieldInfo getForceShieldInfo() {
		return forceShieldInfo;
	}

	/**
	 * @return путь к иконке модуля.
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @return информация о ракете модуля.
	 */
	public RocketShotInfo getRocketShotInfo() {
		return rocketShotInfo;
	}

	/**
	 * @return набор скилов модуля.
	 */
	public SkillTemplate[] getSkills() {
		return skills;
	}

	/**
	 * @return тип модуля.
	 */
	@Override
	public final ModuleType getType() {
		return (ModuleType) type;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return ModuleType.valueOf(vars.getInteger(TYPE));
	}
}
