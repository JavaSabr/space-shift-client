package com.ss.client.network.packet.server;

import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о статусе корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipStatus extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	/** процент энергии */
	private float energyPercent;
	/** процент прочности */
	private float strengthPercent;

	/** текущее состояние энергии */
	private int currentEnergy;
	/** текущее состояни прочности */
	private int currentStrength;

	@Override
	protected void readImpl() {
		currentEnergy = readInt();
		currentStrength = readInt();
		energyPercent = readFloat();
		strengthPercent = readFloat();
	}

	@Override
	protected void runImpl() {

		GameState state = GAME_STATE.getState();
		GameUtil.waitInitialize(state);

		PlayerShip playerShip = state.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(getClass(), "not found player ship");
			return;
		}

		playerShip.setCurrentEnergy(currentEnergy);
		playerShip.setCurrentStrength(currentStrength);
		playerShip.setEnergyPercent(energyPercent);
		playerShip.setStrengthPercent(strengthPercent);
	}
}
