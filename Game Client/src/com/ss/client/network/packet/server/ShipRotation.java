package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.LocalObjects;

/**
 * Пакет с уведомлением о выключении двигателя у корабля.
 * 
 * @author Ronn
 */
public class ShipRotation extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** текущий разворот корабля */
	private final Quaternion currentRotation;
	/** целевой разворот корабля */
	private final Quaternion targetRotation;

	/** ид корабля */
	private long objectId;

	/** шаг разворота */
	private float step;
	/** выполненность разворота */
	private float done;

	/** бесконечное ли вращение */
	private boolean infinity;
	/** принудительное ли обновление */
	private boolean force;

	public ShipRotation() {
		this.targetRotation = new Quaternion();
		this.currentRotation = new Quaternion();
	}

	/**
	 * @return текущий разворот корабля.
	 */
	public Quaternion getCurrentRotation() {
		return currentRotation;
	}

	/**
	 * @return выполненность разворота.
	 */
	public float getDone() {
		return done;
	}

	/**
	 * @return ид корабля.
	 */
	public long getObjectId() {
		return objectId;
	}

	/**
	 * @return шаг разворота.
	 */
	public float getSpeed() {
		return step;
	}

	/**
	 * @return целевой разворот корабля.
	 */
	public Quaternion getTargetRotation() {
		return targetRotation;
	}

	/**
	 * @return принудительное ли обновление.
	 */
	public boolean isForce() {
		return force;
	}

	/**
	 * @return бесконечное ли вращение.
	 */
	public boolean isInfinity() {
		return infinity;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		step = readFloat();
		done = readFloat();
		currentRotation.set(readFloat(), readFloat(), readFloat(), readFloat());
		targetRotation.set(readFloat(), readFloat(), readFloat(), readFloat());
		infinity = readByte() == 1;
		force = readByte() == 1;
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();
		final PlayerShip playerShip = state.getPlayerShip();

		final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

		if(ship == null || ship == playerShip) {
			return;
		}

		ship.rotate(LocalObjects.get(), currentRotation, targetRotation, step, done, infinity, force);
	}

	@Override
	public String toString() {
		return "ShipRotate [currentCuant=" + currentRotation + ", targetQuant=" + targetRotation + ", shipId=" + objectId + ", rotateSpeed=" + step + "]";
	}
}
