package com.ss.client.network.packet.server;

import com.jme3.math.Vector3f;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.DebugObjectPositionTask;

/**
 * Пакет с положением дебага объекта.
 * 
 * @author Ronn
 */
public class DebugObjectPosition extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** позиция дебага */
	private final Vector3f position;

	/** уникальный ид объекта */
	private long objectId;

	public DebugObjectPosition() {
		this.position = new Vector3f();
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		position.set(readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addGeomTask(DebugObjectPositionTask.getInstance(position, objectId));
	}
}
