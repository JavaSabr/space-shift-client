package com.ss.client.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.skills.Skill;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.SkillListTask;
import com.ss.client.table.SkillTable;
import com.ss.client.template.SkillTemplate;

/**
 * Ответный список скилов корабля.
 * 
 * @author Ronn
 */
public class ResponseSkillList extends ServerPacket {

	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();
	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** список скилов */
	private final Array<Skill> skills;

	/** временный буффер данных */
	private final ByteBuffer saveBuffer;

	public ResponseSkillList() {
		this.skills = Arrays.toArray(Skill.class);
		this.saveBuffer = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		skills.clear();
	}

	/**
	 * @return временный буфер данных.
	 */
	public ByteBuffer getSaveBuffer() {
		return saveBuffer;
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {

		final ByteBuffer saveBuffer = getSaveBuffer();
		final ByteBuffer buffer = getBuffer();

		saveBuffer.clear();
		saveBuffer.put(buffer.array(), buffer.position(), Math.min(buffer.remaining(), saveBuffer.capacity()));
		saveBuffer.flip();
	}

	@Override
	protected void runImpl() {

		setBuffer(getSaveBuffer());

		for(int i = 0, count = readByte(); i < count; i++) {

			final int skillId = readInt();
			final int objectId = readInt();

			final SkillTemplate template = SKILL_TABLE.getTemplate(skillId);

			if(template == null) {
				continue;
			}

			skills.add(template.newInstance(objectId));
		}

		TASK_MANAGER.addUITask(SkillListTask.getInstance(skills));
	}
}
