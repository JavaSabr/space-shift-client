package com.ss.client.network.packet.server;

import rlib.util.VarTable;

import com.jme3.math.Vector3f;
import com.ss.client.model.module.ModuleType;
import com.ss.client.model.module.system.SlotInfo;
import com.ss.client.template.ship.ShipTemplate;

/**
 * Данные о запрашиваемом шаблоне корабля.
 * 
 * @author Ronn
 */
public abstract class ResponseShipTemplate extends ResponseObjectTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		final int count = readInt();

		if(count > 0) {

			final SlotInfo[] slots = new SlotInfo[count];

			for(int i = 0; i < count; i++) {
				final ModuleType type = ModuleType.valueOf(readInt());
				slots[i] = new SlotInfo(type, new Vector3f(readFloat(), readFloat(), readFloat()));
			}

			vars.set(ShipTemplate.SLOTS, slots);
		}
	}
}
