package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.network.ServerPacket;
import com.ss.client.table.SkillTable;
import com.ss.client.template.SkillTemplate;

/**
 * Серверный пакет с инфой о шаблоне скила.
 * 
 * @author Ronn
 */
public class ResponseSkillTemplate extends ServerPacket {

	/** таблица параметров */
	private final VarTable vars;

	public ResponseSkillTemplate() {
		this.vars = VarTable.newInstance();
	}

	@Override
	public void finalyze() {
		vars.clear();
	}

	/**
	 * @return таблица параметров.
	 */
	public VarTable getVars() {
		return vars;
	}

	@Override
	protected void readImpl() {

		final VarTable vars = getVars();

		vars.set(SkillTemplate.ID, readInt());
		vars.set(SkillTemplate.NAME, readString(readInt()));
		vars.set(SkillTemplate.DESCRIPTION, readString(readInt()));
		vars.set(SkillTemplate.ICON, readString(readInt()));
		vars.set(SkillTemplate.SKILL_TYPE, readInt());
	}

	@Override
	protected void runImpl() {

		SkillTable skillTable = SkillTable.getInstance();
		skillTable.addTemplate(new SkillTemplate(getVars()));

		ConcurrentUtils.notifyAll(skillTable);
	}
}
