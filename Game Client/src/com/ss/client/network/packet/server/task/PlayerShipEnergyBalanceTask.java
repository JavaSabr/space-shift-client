package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.panel.status.ShipStatusController;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления энергитического баланса корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipEnergyBalanceTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<PlayerShipEnergyBalanceTask> POOL = Pools.newConcurrentFoldablePool(PlayerShipEnergyBalanceTask.class);

	public static final PlayerShipEnergyBalanceTask getInstance(float weapon, float shield, float engine) {

		PlayerShipEnergyBalanceTask task = POOL.take();

		if(task == null) {
			task = new PlayerShipEnergyBalanceTask();
		}

		task.weapon = weapon;
		task.shield = shield;
		task.engine = engine;

		return task;
	}

	/** распредиление на оружие */
	private float weapon;
	/** распределение на щит */
	private float shield;
	/** распределение на двигатель */
	private float engine;

	public PlayerShipEnergyBalanceTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		GameUIController gameController = GameUIController.getInstance();
		ShipStatusController controller = gameController.findController(ShipStatusController.ID, ShipStatusController.class);

		if(controller != null) {
			controller.update(weapon, shield, engine);
		}

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
