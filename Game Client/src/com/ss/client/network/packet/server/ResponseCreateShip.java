package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.HangarController;
import com.ss.client.gui.controller.HangarController.ScreenState;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.RequestShipList;
import com.ss.client.table.LangTable;

/**
 * Подтверждение подключения к гейм серверу.
 * 
 * @author Ronn
 */
public class ResponseCreateShip extends ServerPacket {

	private static enum ResultType {
		SUCCESSFULL,
		INCORRECT_NAME,
		LIMITED,
		EXEPTION,
	}

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	public static final String EXCEPTION_MESSAGE = LANG_TABLE.getText("@interface:hangarLabelWarningException@");
	public static final String INCORRECT_NAME_MESSAGE = LANG_TABLE.getText("@interface:hangarLabelWarningIncorrectName@");
	public static final String LIMITED_MESSAGE = LANG_TABLE.getText("@interface:hangarLabelWarningLimited@");

	private ResultType result;

	@Override
	protected void readImpl() {
		result = ResultType.values()[readByte()];
	}

	@Override
	protected void runImpl() {

		final HangarController control = HangarController.getInstance();
		final Network network = Network.getInstance();
		final NetServer server = network.getGameServer();

		if(control == null || server == null) {
			return;
		}

		switch(result) {
			case SUCCESSFULL: {
				control.unblocking();
				control.createShipCancel();
				server.sendPacket(RequestShipList.getInstance());

				break;
			}
			case LIMITED: {
				control.warningShow(LIMITED_MESSAGE, ScreenState.WAIT);
				break;
			}
			case INCORRECT_NAME: {
				control.warningShow(INCORRECT_NAME_MESSAGE, null);
				break;
			}
			case EXEPTION: {
				control.warningShow(EXCEPTION_MESSAGE, null);
				break;
			}
		}
	}
}
