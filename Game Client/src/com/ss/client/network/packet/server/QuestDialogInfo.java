package com.ss.client.network.packet.server;

import com.ss.client.network.ServerPacket;

/**
 * Пакет с данными квестового диалога.
 * 
 * @author Ronn
 */
public class QuestDialogInfo extends ServerPacket {

	/** список квестов */
	// private final Array<QuestInfo> quests;

	public QuestDialogInfo() {
		// this.quests = Arrays.toArray(QuestInfo.class);
	}

	@Override
	public void finalyze() {
		// quests.clear();
	}

	// /**
	// * @return квест и указанным ид и названием.
	// */
	// private QuestInfo getQuest(final int id, final String name) {
	// final Array<QuestInfo> quests = getQuests();
	//
	// for(final QuestInfo quest : quests.array()) {
	// if(quest == null)
	// break;
	//
	// if(quest.getId() == id && quest.getName().equals(name))
	// return quest;
	// }
	//
	// final QuestInfo quest = QuestInfo.getInstance();
	// quest.setId(id);
	// quest.setName(name);
	//
	// quests.add(quest);
	//
	// return quest;
	// }

	// /**
	// * @return список загруженных квестов.
	// */
	// public Array<QuestInfo> getQuests() {
	// return quests;
	// }

	@Override
	protected void readImpl() {
		// for(int i = 0, length = readByte(); i < length; i++) {
		// final QuestInfo quest = getQuest(readInt(), readString(readByte()));
		// quest.addButton(readByte(), readString(readByte()),
		// readString(readInt()));
		// }
	}

	@Override
	protected void runImpl() {
		// final Array<QuestInfo> quests = getQuests();
		//
		// for(final QuestInfo quest : quests.array()) {
		// if(quest == null)
		// break;
		//
		// quest.applyLocalization();
		// }
		//
		// final QuestDialogController controller =
		// QuestDialogController.getInstance();
		// controller.addQuests(quests);
		//
		// final GameUIController gameController =
		// GameUIController.getInstance();
		// gameController.showQuestDialog();
	}
}
