package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с информацией об удаленной станции.
 * 
 * @author Ronn
 */
public class ObjectDelete extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** ид удаленной станции */
	private long objectId;

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
	}

	@Override
	protected void runImpl() {

		GAME.syncLock();
		try {

			final SpaceObject object = SPACE_LOCATION.getObject(objectId);

			if(object == null) {
				return;
			}

			object.deleteMe();

		} finally {
			GAME.syncUnlock();
		}
	}
}
