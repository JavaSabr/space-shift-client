package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.manager.ExecutorManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;

import rlib.util.SafeTask;

/**
 * Пакет запроса на отображение объекта.
 * 
 * @author Ronn
 */
public class ObjectShow extends ServerPacket {

	private static final int WAIT_COUNT = 100;
	private static final int WAIT_TIME = 100;

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final Game GAME = Game.getInstance();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();

	/** ид отображаемого объекта */
	private long objectId;

	public long getObjectId() {
		return objectId;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
	}

	@Override
	protected void runImpl() {
		final GameState state = GAME_STATE.getState();

		if(!state.isInitialized())
			return;

		final long objectId = getObjectId();

		GAME.syncLock();
		try {
			final SpaceObject object = SPACE_LOCATION.getObject(objectId);

			if(object == null) {
				final SafeTask task = new SafeTask() {

					private int counter;

					@Override
					protected void runImpl() {
						counter++;

						GAME.syncLock();
						try {
							final SpaceObject object = SPACE_LOCATION.getObject(objectId);

							if(object != null)
								object.show();
							else if(counter < WAIT_COUNT)
								EXECUTOR_MANAGER.scheduleGeneral(this, WAIT_TIME);
						} finally {
							GAME.syncUnlock();
						}
					}
				};

				EXECUTOR_MANAGER.scheduleGeneral(task, WAIT_TIME);
				return;
			}

			object.show();
		} finally {
			GAME.syncUnlock();
		}
	}
}
