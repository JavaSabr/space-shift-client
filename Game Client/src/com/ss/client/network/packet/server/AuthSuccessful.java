package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.RequestShipList;

/**
 * Подтверждение подключения к гейм серверу.
 * 
 * @author Ronn
 */
public class AuthSuccessful extends ServerPacket {

	private static final Game GAME = Game.getInstance();

	@Override
	protected void readImpl() {
	}

	@Override
	protected void runImpl() {

		final Network network = Network.getInstance();
		final NetServer server = network.getGameServer();

		if(server != null) {
			GAME.gotoState(StateId.HANGAR_STATE);
			server.sendPacket(RequestShipList.getInstance());
		}
	}
}
