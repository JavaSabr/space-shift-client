package com.ss.client.network.packet.server;

import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;

/**
 * Ответный пакет для теста коннекта.
 * 
 * @author Ronn
 */
public class LoginServerConnected extends ServerPacket {

	@Override
	protected void readImpl() {
		LOGGER.info(this, "successful connect to Login Server.");
	}

	@Override
	protected void runImpl() {
		final Network network = Network.getInstance();
		network.setLoginConnected(true);
	}
}
