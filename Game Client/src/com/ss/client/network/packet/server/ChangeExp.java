package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.ServerPacket;

/**
 * Пакет в текущим серверным временем, что бы синхронизоваться с клиентом.
 * 
 * @author Ronn
 */
public class ChangeExp extends ServerPacket {

	/** актуальное кол-во опыта */
	private long exp;

	@Override
	protected void readImpl() {
		exp = readLong();
	}

	@Override
	protected void runImpl() {

		GameUIController controller = GameUIController.getInstance();
		PlayerShip ship = controller.getPlayerShip();

		if(ship != null) {
			ship.setExp(exp);
		}
	}
}
