package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.position.ElementPosition;
import com.ss.client.gui.model.InterfaceInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления интерфейса.
 * 
 * @author Ronn
 */
public class ResponseIterfaceTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<ResponseIterfaceTask> POOL = Pools.newConcurrentFoldablePool(ResponseIterfaceTask.class);

	private static final FuncElement<InterfaceInfo> FUNC_ELEMENT = new FuncElement<InterfaceInfo>() {

		@Override
		public void apply(InterfaceInfo info) {
			info.fold();
		}
	};

	public static final ResponseIterfaceTask getInstance(Array<InterfaceInfo> container) {

		ResponseIterfaceTask task = POOL.take();

		if(task == null) {
			task = new ResponseIterfaceTask();
		}

		task.container.addAll(container);

		return task;
	}

	/** контейнер информаций о ячейках */
	private final Array<InterfaceInfo> container;

	public ResponseIterfaceTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.container = Arrays.toArray(InterfaceInfo.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final GameUIController gameController = GameUIController.getInstance();

		final ElementPosition controller = gameController.getInterfacePosition();
		controller.apply(container);

		return true;
	}

	@Override
	public void finalyze() {
		container.apply(FUNC_ELEMENT);
		container.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
