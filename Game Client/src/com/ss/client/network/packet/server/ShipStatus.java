package com.ss.client.network.packet.server;

import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о статусе корабля.
 * 
 * @author Ronn
 */
public class ShipStatus extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** уникальный ид корабля */
	private long objectId;

	/** процент прочности корабля */
	private float strength;
	/** процент энергии корабля */
	private float energy;

	@Override
	protected void readImpl() {
		objectId = readLong();
		strength = readFloat();
		energy = readFloat();
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

		if(ship == null) {
			LOGGER.warning(getClass(), "not found space ship for objectId " + objectId);
			return;
		}

		ship.setStrengthPercent(strength);
		ship.setEnergyPercent(energy);
	}
}
