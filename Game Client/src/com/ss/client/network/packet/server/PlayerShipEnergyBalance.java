package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.PlayerShipEnergyBalanceTask;
import com.ss.client.util.GameUtil;

/**
 * Серверный пакет с текщим состоянием распределния энергии корабля.
 * 
 * @author Ronn
 */
public class PlayerShipEnergyBalance extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** распредиление на оружие */
	private float weapon;
	/** распределение на щит */
	private float shield;
	/** распределение на двигатель */
	private float engine;

	@Override
	protected void readImpl() {
		weapon = readFloat();
		shield = readFloat();
		engine = readFloat();
	}

	@Override
	protected void runImpl() {

		GameUIController gameController = GameUIController.getInstance();

		GameUtil.waitInitialize(gameController);

		PlayerShip playerShip = gameController.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(getClass(), "not found player ship");
			return;
		}

		TASK_MANAGER.addUITask(PlayerShipEnergyBalanceTask.getInstance(weapon, shield, engine));
	}
}
