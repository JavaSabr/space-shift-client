package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.ItemTable;
import com.ss.client.template.item.ItemTemplate;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о новом корабле в космосе.
 * 
 * @author Ronn
 */
public class ItemInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final ItemTable ITEM_TABLE = ItemTable.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция, на которой остался корабль */
	private final Vector3f location;
	/** разворот корабля */
	private final Quaternion rotation;

	/** уникальный ид предмета */
	private long objectId;

	/** кол-во предметов в стопке */
	private long itemCount;

	/** ид шаблона предмета */
	private int templateId;

	public ItemInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {

		objectId = readLong();
		templateId = readInt();
		itemCount = readLong();

		location.setX(readFloat());
		location.setY(readFloat());
		location.setZ(readFloat());

		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		final ItemTemplate template = ITEM_TABLE.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning(this, new Exception("not found template for id " + templateId));
			return;
		}

		final SpaceItem item = template.takeInstance(objectId);
		item.setItemCount(itemCount);

		GAME.syncLock();
		try {
			item.setParentNode(state.getObjectState());
			item.setLocation(location, Vector3f.ZERO);
			item.setRotation(rotation);
			item.spawnMe();
		} finally {
			GAME.syncUnlock();
		}
	}
}
