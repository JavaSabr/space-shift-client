package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.table.StationTable;
import com.ss.client.template.StationTemplate;

/**
 * Серверный пакет с информацией о шаблоне космической станции.
 * 
 * @author Ronn
 */
public class ResponseStationTemplate extends ResponseObjectTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		vars.set(StationTemplate.NAME, readString(readInt()));
	}

	@Override
	protected void runImpl() {

		StationTable stationTable = StationTable.getInstance();
		stationTable.addTemplate(new StationTemplate(getVars()));

		ConcurrentUtils.notifyAll(stationTable);
	}
}
