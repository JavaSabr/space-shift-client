package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.Strings;
import rlib.util.VarTable;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.model.module.info.BlasterRayInfo;
import com.ss.client.model.module.info.EngineFireInfo;
import com.ss.client.model.module.info.ForceShieldInfo;
import com.ss.client.model.module.info.RocketShotInfo;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.SkillTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.SkillTemplate;

/**
 * Серверный пакет с инфой о шаблоне модуля.
 * 
 * @author Ronn
 */
public class ResponseModuleTemplate extends ResponseObjectTemplate {

	private static final int[] EMPTY_INT_ARRAY = new int[0];

	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();

	private BlasterRayInfo readBlasterRayInfo() {

		final VarTable vars = VarTable.newInstance();

		vars.set(BlasterRayInfo.MODEL, readString(readInt()));
		vars.set(BlasterRayInfo.MATERIAL, readString(readInt()));
		vars.set(BlasterRayInfo.SCALE, new Vector3f(readFloat(), readFloat(), readFloat()));
		vars.set(BlasterRayInfo.EXPLOSION, GraficEffectType.newTemplate(this));

		return new BlasterRayInfo(vars);
	}

	private EngineFireInfo readEngineFireInfo() {

		final VarTable vars = VarTable.newInstance();

		vars.set(EngineFireInfo.MODEL, readString(readInt()));
		vars.set(EngineFireInfo.MATERIAL, readString(readInt()));
		vars.set(EngineFireInfo.OFFSET, new Vector3f(readFloat(), readFloat(), readFloat()));
		vars.set(EngineFireInfo.SCALE, new Vector3f(readFloat(), readFloat(), readFloat()));
		vars.set(EngineFireInfo.ROTATE, new Quaternion(readFloat(), readFloat(), readFloat(), readFloat()));

		return new EngineFireInfo(vars);
	}

	private ForceShieldInfo readForceShieldInfo() {

		String modelKey = readString(readByte());
		String textureKey = readString(readByte());

		float effectSize = readFloat();

		ColorRGBA effectColor = new ColorRGBA(readInt() / 255F, readInt() / 255F, readInt() / 255F, 2);

		VarTable vars = VarTable.newInstance();
		vars.set(ForceShieldInfo.MODEL_KEY, modelKey);
		vars.set(ForceShieldInfo.TEXTURE_KEY, textureKey);
		vars.set(ForceShieldInfo.EFFECT_SIZE, effectSize);
		vars.set(ForceShieldInfo.EFFECT_COLOR, effectColor);

		return new ForceShieldInfo(vars);
	}

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();

		vars.set(ModuleTemplate.ICON, readString(readByte()));

		// кол-во скилов у модуля
		int count = readInt();

		if(count > 0) {

			final int[] ids = new int[count];

			for(int i = 0; i < count; i++) {
				ids[i] = readInt();
			}

			vars.set(ModuleTemplate.SKILLS, ids);
		}

		// кол-во двигательных струй у модуля
		count = readInt();

		if(count > 0) {

			final EngineFireInfo[] infos = new EngineFireInfo[count];

			for(int i = 0; i < count; i++) {
				infos[i] = readEngineFireInfo();
			}

			vars.set(ModuleTemplate.ENGINE_FIRE_INFO, infos);
		}

		// кол-во шаблонов бластеров
		count = readInt();

		if(count > 0) {

			final BlasterRayInfo[] infos = new BlasterRayInfo[count];

			for(int i = 0; i < count; i++) {
				infos[i] = readBlasterRayInfo();
			}

			vars.set(ModuleTemplate.BLASTER_RAY_INFO, infos);
		}

		if(readByte() == 1) {
			vars.set(ModuleTemplate.FORCE_SHIELD_INFO, readForceShieldInfo());
		}

		if(readByte() == 1) {
			vars.set(ModuleTemplate.ROCKET_SHOT_INFO, readRocketShotInfo());
		}
	}

	private RocketShotInfo readRocketShotInfo() {

		String model = readString(readByte());

		VarTable vars = VarTable.newInstance();
		vars.set(RocketShotInfo.MODEL, model);
		vars.set(RocketShotInfo.SCALE, new Vector3f(readFloat(), readFloat(), readFloat()));
		vars.set(RocketShotInfo.EXPLOSION, GraficEffectType.newTemplate(this));
		vars.set(RocketShotInfo.ENGINE_INFO, readEngineFireInfo());

		return new RocketShotInfo(vars);
	}

	@Override
	protected void runImpl() {

		final VarTable vars = getVars();

		final int[] ids = vars.getIntegerArray(ModuleTemplate.SKILLS, Strings.EMPTY, EMPTY_INT_ARRAY);

		final SkillTemplate[] skills = new SkillTemplate[ids.length];

		if(ids.length > 0) {
			for(int i = 0, length = ids.length; i < length; i++) {
				skills[i] = SKILL_TABLE.getTemplate(ids[i]);
			}
		}

		vars.set(ModuleTemplate.SKILLS, skills);

		ModuleTable moduleTable = ModuleTable.getInstance();
		moduleTable.addTemplate(new ModuleTemplate(vars));

		ConcurrentUtils.notifyAll(moduleTable);
	}
}
