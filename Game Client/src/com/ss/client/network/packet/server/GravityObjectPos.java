package com.ss.client.network.packet.server;

import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.gravity.GravityObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с информацией о гравитационных объектах.
 * 
 * @author Ronn
 */
public class GravityObjectPos extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция объекта */
	private final Vector3f location;

	/** уникальный ид станции */
	private long objectId;

	public GravityObjectPos() {
		this.location = new Vector3f();
	}

	/**
	 * @return позиция объкта.
	 */
	public Vector3f getLocation() {
		return location;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		location.set(readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		if(!state.isInitialized()) {
			return;
		}

		final Vector3f location = getLocation();

		final GravityObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			return;
		}

		object.setLocation(location, Vector3f.ZERO);
	}
}
