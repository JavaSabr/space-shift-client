package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с информацией об удаленном корабле.
 * 
 * @author Ronn
 */
public class ShipDelete extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** ид удаленного корабля */
	private long objectId;

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
	}

	@Override
	protected void runImpl() {

		GAME.syncLock();
		try {

			final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

			if(ship == null) {
				return;
			}

			ship.deleteMe();

		} finally {
			GAME.syncUnlock();
		}
	}
}
