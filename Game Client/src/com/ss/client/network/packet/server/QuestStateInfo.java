package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.network.ServerPacket;

/**
 * Серверный пакет с новым состоянием квеста.
 * 
 * @author Ronn
 */
public class QuestStateInfo extends ServerPacket {

	private static final Game GAME = Game.getInstance();

	/** состояние квеста */
	// private QuestState state;

	@Override
	public void finalyze() {
		// state = null;
	}

	/**
	 * @return новое состояние квеста.
	 */
	// public QuestState getState() {
	// return state;
	// }

	@Override
	protected void readImpl() {

		// final QuestState state = QuestState.getInstance();
		// state.setQuestId(readInt());
		// state.setName(readString(readByte()));
		// state.setDescr(readString(readByte()));
		//
		// final int count = readByte();
		//
		// for(int i = 0; i < count; i++) {
		// state.addButton(readByte(), readString(readByte()),
		// readString(readInt()));
		// }
		//
		// setState(state);
	}

	@Override
	protected void runImpl() {

		// final QuestState state = getState();
		//
		// if(state == null) {
		// return;
		// }
		//
		// final QuestBookController controller =
		// QuestBookController.getInstance();
		//
		// GameUtil.waitInitialize(controller);
		//
		// state.setStatus(StatusType.IN_PROGRESS);
		// state.applyLocalization();
		//
		// // FIXME
		// // final MainPanelController panel =
		// MainPanelController.getInstance();
		// //
		// // GameUtil.waitInitialize(panel);
		// //
		// // panel.showWindow(ElementId.GAME_WINDOW_QUEST_BOOK);
		//
		// controller.update(state,
		// controller.getQuestState(state.getQuestId()));
		// controller.setSelected(state);
	}

	// private void setState(final QuestState state) {
	// this.state = state;
	// }
}
