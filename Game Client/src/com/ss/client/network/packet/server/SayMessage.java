package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.ChatMessageEvent;
import com.ss.client.gui.controller.game.panel.chat.ChatMessage;
import com.ss.client.gui.model.MessageType;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с уведомлением о выключении двигателя у корабля.
 * 
 * @author Ronn
 */
public class SayMessage extends ServerPacket {

	/** тип сообщения */
	private MessageType type;

	/** отправитель */
	private String sender;
	/** сообщение */
	private String message;

	@Override
	protected void readImpl() {
		type = MessageType.values()[readInt()];
		sender = readString(readByte());
		message = readString(readInt());
	}

	@Override
	protected void runImpl() {

		ChatMessageEvent event = ChatMessageEvent.get();
		event.setMessage(new ChatMessage(type, sender, message));

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(event);
	}
}
