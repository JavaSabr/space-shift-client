package com.ss.client.network.packet.server;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Пакет с инфой о разрушенном объекте.
 * 
 * @author Ronn
 */
public class ObjectDestructed extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** уникальный ид объекта */
	private long objectId;

	@Override
	protected void readImpl() {
		objectId = readLong();
	}

	@Override
	protected void runImpl() {

		GameState gameState = GAME_STATE.getState();

		GameUtil.waitInitialize(gameState);

		SpaceObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			return;
		}

		object.destruct();
	}
}
