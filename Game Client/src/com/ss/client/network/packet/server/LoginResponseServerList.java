package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.LoginController;
import com.ss.client.model.ServerInfo;
import com.ss.client.network.ServerPacket;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

/**
 * Ответный пакет на запрос списка серверов.
 * 
 * @author Ronn
 */
public class LoginResponseServerList extends ServerPacket {

	/** список серверов */
	private final Array<ServerInfo> servers;

	public LoginResponseServerList() {
		this.servers = Arrays.toArray(ServerInfo.class);
	}

	@Override
	public void finalyze() {
		servers.clear();
	}

	@Override
	protected void readImpl() {

		int port = 0;
		int online = 0;

		float loading = 0F;

		String name = null;
		String type = null;
		String host = null;

		for(int i = 0, length = readByte(); i < length; i++) {

			name = readString(readByte());
			type = readString(readByte());
			host = readString(readByte());
			port = readInt();
			loading = readFloat();
			online = readInt();

			servers.add(ServerInfo.newInstance(name, type, host, port, loading, online));
		}
	}

	@Override
	protected void runImpl() {

		final LoginController controller = LoginController.getInstance();

		if(controller == null) {
			return;
		}

		controller.addServers(servers);
	}
}
