package com.ss.client.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.ShipTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.ship.PlayerShipTemplate;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о новом корабле в космосе.
 * 
 * @author Ronn
 */
public class PlayerShipInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final ShipTable SHIP_TABLE = ShipTable.getInstance();
	private static final ModuleTable MODULE_TABLE = ModuleTable.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция, на которой остался корабль */
	private final Vector3f location;
	/** разворот корабля */
	private final Quaternion rotation;

	/** буффер с сохраненными данными */
	private final ByteBuffer saveBuffer;

	public PlayerShipInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
		this.saveBuffer = ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN);
	}

	private final ByteBuffer getSaveBuffer() {
		return saveBuffer;
	}

	@Override
	protected void readImpl() {

		final ByteBuffer saveBuffer = getSaveBuffer();
		ByteBuffer buffer = getBuffer();

		saveBuffer.clear();
		saveBuffer.put(buffer.array(), buffer.position(), Math.min(saveBuffer.remaining(), buffer.remaining()));
		saveBuffer.flip();
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		setBuffer(getSaveBuffer());

		final long objectId = readLong();

		int templateId = readInt();

		final PlayerShipTemplate template = SHIP_TABLE.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning(this, new Exception("not found template for id " + templateId));
			return;
		}

		final PlayerShip ship = template.takeInstance(objectId);
		ship.setName(readString(readByte()));

		final ModuleSystem moduleSystem = ship.getModuleSystem();

		if(moduleSystem == null) {
			LOGGER.warning(this, new Exception("not found module system"));
			return;
		}

		for(int i = 0, length = readByte(); i < length; i++) {

			final long moduleId = readLong();

			templateId = readInt();

			final ModuleTemplate moduleTemplate = MODULE_TABLE.getTemplate(templateId);

			final int index = readByte();

			if(moduleTemplate == null) {
				LOGGER.warning(this, new Exception("not found module for id " + templateId));
				continue;
			}

			moduleSystem.setModule(index, (Module) moduleTemplate.takeInstance(moduleId));
		}

		location.setX(readFloat());
		location.setY(readFloat());
		location.setZ(readFloat());

		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());

		GAME.syncLock();
		try {

			ship.setParentNode(state.getObjectState());
			ship.setLocation(location, Vector3f.ZERO);
			ship.setRotation(rotation);
			ship.spawnMe();

		} finally {
			GAME.syncUnlock();
		}
	}
}
