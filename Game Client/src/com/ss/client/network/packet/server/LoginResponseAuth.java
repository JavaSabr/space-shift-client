package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.LoginController;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.model.NetServer;
import com.ss.client.table.LangTable;

/**
 * Ответный пакет на попытку авторизоваться.
 * 
 * @author Ronn
 */
public class LoginResponseAuth extends ServerPacket {

	private static enum ResultType {
		SUCCESSFUL,
		INCORRECT_LOGIN,
		INCORRECT_PASSWORD,
	}

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	private static final String INCORRECT_PASSWORD = LANG_TABLE.getText("@interface:loginLabeIncorrectPassword@");
	private static final String INCORRECT_LOGIN = LANG_TABLE.getText("@interface:loginLabeNotFoundAccaunt@");

	/** результат */
	private ResultType type;

	@Override
	protected void readImpl() {
		type = ResultType.values()[readByte()];
	}

	@Override
	protected void runImpl() {

		final LoginController controller = LoginController.getInstance();
		final Network network = Network.getInstance();
		final NetServer server = network.getLoginServer();

		if(controller == null || type == null || server == null) {
			return;
		}

		switch(type) {
			case SUCCESSFUL: {
				controller.enterToServer();
				break;
			}
			case INCORRECT_PASSWORD: {
				controller.unblocking();
				controller.warningShow(INCORRECT_PASSWORD, null);
				break;
			}
			case INCORRECT_LOGIN: {
				controller.unblocking();
				controller.warningShow(INCORRECT_LOGIN, null);
			}
		}
	}
}
