package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.table.ShipTable;
import com.ss.client.template.ship.PlayerShipTemplate;

/**
 * Серверный пакет с данными о шаблоне корабля игрока.
 * 
 * @author Ronn
 */
public class ResponsePlayerShipTemplate extends ResponseShipTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		vars.set(PlayerShipTemplate.CLASS, readInt());
	}

	@Override
	protected void runImpl() {

		ShipTable shipTable = ShipTable.getInstance();
		shipTable.addTemplate(new PlayerShipTemplate(getVars()));

		ConcurrentUtils.notifyAll(shipTable);
	}
}
