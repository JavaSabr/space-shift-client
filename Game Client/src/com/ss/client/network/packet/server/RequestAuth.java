package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.model.Account;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.ResponseAuth;

/**
 * Запрос от гейм сервера на получение данных об аккаунте.
 * 
 * @author Ronn
 */
public class RequestAuth extends ServerPacket {

	/** используемый аккаунт */
	private Account account;

	@Override
	protected void readImpl() {
		account = Game.getInstance().getAccount();
	}

	@Override
	protected void runImpl() {

		final Network network = Network.getInstance();
		final NetServer gameServer = network.getGameServer();

		if(gameServer != null) {
			gameServer.sendPacket(ResponseAuth.getInstance(account.getName(), account.getPassword()));
		}
	}
}
