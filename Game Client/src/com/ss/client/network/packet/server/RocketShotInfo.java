package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.shots.RocketShot;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Серверный пакет с информацией о выпущеной ракете.
 * 
 * @author Ronn
 */
public class RocketShotInfo extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final StateId GAME_STATE = StateId.GAME_STATE;
	private static final Game GAME = Game.getInstance();

	/** координаты старта */
	private final Vector3f startLoc;
	/** разворот выстрела */
	private final Quaternion rotation;

	/** скорость ракеты */
	private float speed;
	/** скорость разворота ракеты */
	private float rotationSpeed;

	/** ид стрелявшего корабля */
	private long shipObjectId;
	/** уникальный ид цели */
	private long targetObjectId;
	/** ид стрелявшего модуля */
	private long moduleObjectId;

	/** уникальный ид выстрела */
	private int objectId;
	/** максимальная дистанция выстрела */
	private int maxDistance;
	/** максимальная скорость ракеты */
	private int maxSpeed;
	/** ускорение ракеты */
	private int accel;
	/** индекс ствола модуля */
	private int index;

	public RocketShotInfo() {
		this.startLoc = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	protected void readImpl() {

		shipObjectId = readLong();
		targetObjectId = readLong();
		moduleObjectId = readLong();
		objectId = readInt();
		maxDistance = readInt();
		maxSpeed = readInt();
		index = readInt();
		accel = readInt();

		speed = readFloat();
		rotationSpeed = readFloat();

		startLoc.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		GameState gameState = GAME_STATE.getState();

		GameUtil.waitInitialize(gameState);

		SpaceShip ship = SPACE_LOCATION.getObject(shipObjectId);

		if(ship == null) {
			return;
		}

		SpaceObject target = SPACE_LOCATION.getObject(targetObjectId);

		if(target == null) {
			return;
		}

		ModuleSystem moduleSystem = ship.getModuleSystem();
		Module module = moduleSystem.getModule(moduleObjectId);

		if(module == null || !(module instanceof ShotModule)) {
			LOGGER.warning(getClass(), "not found module for id " + moduleObjectId);
			return;
		}

		ShotModule shotModule = (ShotModule) module;

		RocketShot shot = (RocketShot) shotModule.getNextShot(index);
		shot.setNode(ship.getParentNode());
		shot.setMaxDistance(maxDistance);
		shot.setMaxSpeed(maxSpeed);
		shot.setObjectId(objectId);
		shot.setAccel(accel);
		shot.setTarget(target);
		shot.setShooter(ship);
		shot.setSpeed(speed);
		shot.setRotationSpeed(rotationSpeed);
		shot.setStartLoc(startLoc);
		shot.setRotation(rotation);
		shot.setIndex(index);
		shot.setModule((ShotModule) module);

		GAME.syncLock();
		try {
			shot.start();
		} finally {
			GAME.syncUnlock();
		}
	}
}
