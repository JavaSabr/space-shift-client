package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.window.storage.CellInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.UpdateStorageCellTask;

/**
 * Пакет с информацией для обновления ячейки хранилища.
 * 
 * @author Ronn
 */
public class UpdateStorageCell extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** информация о ячейке */
	private final CellInfo info;

	public UpdateStorageCell() {
		this.info = CellInfo.newInstance();
	}

	@Override
	public void finalyze() {
		info.finalyze();
	}

	@Override
	protected void readImpl() {
		info.setObjectId(readLong());
		info.setTemplateId(readInt());
		info.setIndex(readInt());
		info.setItemCount(readLong());
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addUITask(UpdateStorageCellTask.getInstance(info));
	}
}
