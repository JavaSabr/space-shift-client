package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.network.ServerPacket;

/**
 * Запрос на закрытие квестового диалога.
 * 
 * @author Ronn
 */
public class QuestDialogClose extends ServerPacket {

	@Override
	protected void readImpl() {
	}

	@Override
	protected void runImpl() {
		final GameUIController controller = GameUIController.getInstance();

		while(!controller.isInitialized())
			try {
				Thread.sleep(100);
			} catch(final InterruptedException e) {
				e.printStackTrace();
			}

		controller.hideQuestDialog();
	}
}
