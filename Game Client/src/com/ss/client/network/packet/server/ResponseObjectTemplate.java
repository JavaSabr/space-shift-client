package com.ss.client.network.packet.server;

import rlib.util.VarTable;

import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.network.ServerPacket;
import com.ss.client.template.ObjectTemplate;

/**
 * @author Ronn
 */
public abstract class ResponseObjectTemplate extends ServerPacket {

	/** таблица параметров шаблона */
	private final VarTable vars;

	public ResponseObjectTemplate() {
		this.vars = VarTable.newInstance();
	}

	/**
	 * @return таблица параметров.
	 */
	protected final VarTable getVars() {
		return vars;
	}

	@Override
	protected void readImpl() {

		vars.set(ObjectTemplate.ID, readInt());
		vars.set(ObjectTemplate.SIZE_X, readInt());
		vars.set(ObjectTemplate.SIZE_Y, readInt());
		vars.set(ObjectTemplate.SIZE_Z, readInt());
		vars.set(ObjectTemplate.VISIBLE_RANGE, readInt());
		vars.set(ObjectTemplate.KEY, readString(readInt()));
		vars.set(ObjectTemplate.TYPE, readInt());

		if(readByte() == 1) {
			vars.set(ObjectTemplate.DESTRUCT_EFFECT, GraficEffectType.newTemplate(this));
		}

		LOGGER.info(this, "reading");
	}

	@Override
	public void reinit() {
		vars.clear();
	}
}
