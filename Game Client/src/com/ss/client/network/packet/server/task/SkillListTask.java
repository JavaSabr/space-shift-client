package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateSkillListEvent;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.skills.Skill;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления списка умений.
 * 
 * @author Ronn
 */
public class SkillListTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<SkillListTask> POOL = Pools.newConcurrentFoldablePool(SkillListTask.class);

	public static final SkillListTask getInstance(Array<Skill> skills) {

		SkillListTask task = POOL.take();

		if(task == null) {
			task = new SkillListTask();
		}

		task.skills.addAll(skills);

		return task;
	}

	/** список скилов */
	private final Array<Skill> skills;

	public SkillListTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.skills = Arrays.toArray(Skill.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		UpdateSkillListEvent event = UpdateSkillListEvent.get(skills);

		final GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(event);

		return true;
	}

	@Override
	public void finalyze() {
		skills.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
