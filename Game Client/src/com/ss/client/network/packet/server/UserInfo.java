package com.ss.client.network.packet.server;

import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с информацией о корабле игрока.
 * 
 * @author Ronn
 */
public class UserInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	/** кол-во опыта игрока */
	private long exp;

	/** процент энергии корабля */
	private float energyPercent;
	/** процент прочности корабля */
	private float strengthPercent;

	/** скорость разворота корабля */
	private float rotateSpeed;

	/** уровень корабля */
	private int level;
	/** текущая энергия */
	private int currentEnergy;
	/** максимальное кол-во энергии */
	private int maxEnergy;
	/** скорость регенерации энергии */
	private int energyRegen;
	/** текущая прочность */
	private int currentStrength;
	/** максимальное кол-во прочности */
	private int maxStrength;

	/**
	 * @return скорость разворота корабля.
	 */
	public float getRotateSpeed() {
		return rotateSpeed;
	}

	@Override
	protected void readImpl() {
		exp = readLong();
		rotateSpeed = readFloat();
		energyPercent = readFloat();
		strengthPercent = readFloat();
		level = readInt();
		currentEnergy = readInt();
		maxEnergy = readInt();
		energyRegen = readInt();
		currentStrength = readInt();
		maxStrength = readInt();
	}

	@Override
	protected void runImpl() {

		GameState state = GAME_STATE.getState();

		PlayerShip playerShip = state.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(getClass(), "not found player ship");
			return;
		}

		playerShip.setLevel(level);
		playerShip.setExp(exp);
		playerShip.setRotateSpeed(rotateSpeed);
		playerShip.setCurrentEnergy(currentEnergy);
		playerShip.setMaxEnergy(maxEnergy);
		playerShip.setEnergyRegen(energyRegen);
		playerShip.setCurrentStrength(currentStrength);
		playerShip.setMaxStrength(maxStrength);
		playerShip.setEnergyPercent(energyPercent);
		playerShip.setStrengthPercent(strengthPercent);
	}
}
