package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

/**
 * Пакет с уведомлением о телепортации корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipTeleport extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;
	private static final Game GAME = Game.getInstance();

	/** новая позиция корабля */
	private final Vector3f position;
	/** новый разворот корабля */
	private final Quaternion rotation;

	/** ид новой локации */
	private int locationId;

	public PlayerShipTeleport() {
		this.position = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	protected void readImpl() {

		position.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());

		locationId = readInt();
	}

	@Override
	protected void runImpl() {

		GameState gameState = GAME_STATE.getState();

		GameUtil.waitInitialize(gameState);

		PlayerShip playerShip = gameState.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(getClass(), "not found player ship.");
			return;
		}

		GAME.syncLock();
		try {
			playerShip.teleportTo(position, rotation, LocalObjects.get());
			gameState.gotoLocation(locationId);
		} finally {
			GAME.syncUnlock();
		}
	}
}
