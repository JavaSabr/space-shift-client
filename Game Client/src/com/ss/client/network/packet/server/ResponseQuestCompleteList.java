package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.network.ServerPacket;

/**
 * Ответный список выполненных квестов корабля.
 * 
 * @author Ronn
 */
public class ResponseQuestCompleteList extends ServerPacket {

	private static final Game GAME = Game.getInstance();

	/** список скилов */
	// private final Array<QuestState> quests;

	public ResponseQuestCompleteList() {
		// this.quests = Arrays.toArray(QuestState.class);
	}

	@Override
	public void finalyze() {
		// quests.clear();
	}

	@Override
	protected void readImpl() {

		// for(int i = 0, count = readInt(); i < count; i++) {
		//
		// final QuestState state = QuestState.getInstance();
		// state.setQuestId(readInt());
		// state.setName(readString(readByte()));
		// state.setStatus(readString(readByte()));
		// }
	}

	@Override
	protected void runImpl() {

		// final QuestBookController control =
		// QuestBookController.getInstance();
		//
		// for(final QuestState quest : quests.array()) {
		//
		// if(quest == null) {
		// break;
		// }
		//
		// quest.applyLocalization();
		// control.addQuest(quest);
		// }
	}
}
