package com.ss.client.network.packet.server;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.model.quest.QuestButton;
import com.ss.client.gui.model.quest.QuestCounter;
import com.ss.client.gui.model.quest.QuestState;
import com.ss.client.gui.model.quest.reward.QuestRewardClass;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.QuestListTask;
import com.ss.client.table.LangTable;

/**
 * Серверный пакет с инфой о выполненных и текущих заданиях.
 * 
 * @author Ronn
 */
public class QuestListInfo extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** список задания */
	private final Array<QuestState> quests;

	public QuestListInfo() {
		this.quests = Arrays.toArray(QuestState.class);
	}

	@Override
	public void finalyze() {
		quests.clear();
	}

	@Override
	protected void readImpl() {

		LangTable langTable = LangTable.getInstance();

		int count = readInt();

		for(int i = 0; i < count; i++) {

			final QuestState state = QuestState.newInstance();

			state.setId(readInt());
			state.setObjectId(readInt());
			state.setName(langTable.getText(readString(readByte())));
			state.setDescription(langTable.getText(readString(readByte())));

			int rewards = readByte();

			for(int g = 0; g < rewards; g++) {
				QuestRewardClass rewardClass = QuestRewardClass.valueOf(readByte());
				state.addReward(rewardClass.readFrom(this));
			}

			int counters = readByte();

			for(int g = 0; g < counters; g++) {

				int id = readByte();
				int value = readInt();
				int limit = readInt();

				String description = langTable.getText(readString(readByte()));
				state.addCounter(QuestCounter.newInstance(id, value, limit, description));
			}

			int buttons = readByte();

			for(int g = 0; g < buttons; g++) {
				state.addButton(QuestButton.newInstance(readByte(), langTable.getText(readString(readByte()))));
			}

			quests.add(state);
		}
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addUITask(QuestListTask.getInstance(quests));
	}
}
