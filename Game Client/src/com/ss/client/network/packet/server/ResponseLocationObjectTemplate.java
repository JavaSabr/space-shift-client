package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.table.LocationObjectTable;
import com.ss.client.template.LocationObjectTemplate;

/**
 * Пакет с информацией шаблона локационного объекта.
 * 
 * @author Ronn
 */
public class ResponseLocationObjectTemplate extends ResponseObjectTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		VarTable vars = getVars();
		vars.set(LocationObjectTemplate.SCALE, readFloat());

		System.out.println("read location object");
	}

	@Override
	protected void runImpl() {

		LocationObjectTable objectTable = LocationObjectTable.getInstance();
		objectTable.addTemplate(new LocationObjectTemplate(getVars()));

		ConcurrentUtils.notifyAll(objectTable);
	}
}
