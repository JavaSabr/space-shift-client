package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;


import com.jme3.math.Quaternion;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на изменение вектора полета.
 * 
 * @author Ronn
 */
public final class RequestShipRotate extends ClientPacket {

	private static final RequestShipRotate instance = new RequestShipRotate();

	public static ClientPacket getInstance(final Quaternion start, final Quaternion end, long updateTime, float modiff) {

		final RequestShipRotate packet = instance.newInstance();

		packet.start.set(start);
		packet.end.set(end);
		packet.updateTime = updateTime;
		packet.modiff = modiff;

		return packet;
	}

	/** стартовое направление */
	private final Quaternion start;
	/** конечное направление */
	private final Quaternion end;

	/** время последнего обновления */
	private long updateTime;

	/** модификатор скорости разворота */
	private float modiff;

	public RequestShipRotate() {
		this.start = new Quaternion();
		this.end = new Quaternion();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SHIP_ROTATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		writeFloat(buffer, start.getX());
		writeFloat(buffer, start.getY());
		writeFloat(buffer, start.getZ());
		writeFloat(buffer, start.getW());

		writeFloat(buffer, end.getX());
		writeFloat(buffer, end.getY());
		writeFloat(buffer, end.getZ());
		writeFloat(buffer, end.getW());

		writeLong(buffer, updateTime);
		writeFloat(buffer, modiff);
	}
}
