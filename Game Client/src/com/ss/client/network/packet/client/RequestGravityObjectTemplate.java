package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клинтский пакет с запросом на получение шаблона гравитационного объекта.
 * 
 * @author Ronn
 */
public class RequestGravityObjectTemplate extends ClientPacket {

	private static final RequestGravityObjectTemplate instance = new RequestGravityObjectTemplate();

	public static final ClientPacket getInstance(final int templateId) {
		final RequestGravityObjectTemplate packet = instance.newInstance();

		packet.templateId = templateId;

		return packet;
	}

	/** ид шаблона */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_GRAVITY_OBJECT_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
