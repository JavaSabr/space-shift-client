package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запросом инфы об станции.
 * 
 * @author Ronn
 */
public class RequestStationTemplate extends ClientPacket {

	private static final RequestStationTemplate instance = new RequestStationTemplate();

	public static final ClientPacket getInstance(final int temlateId) {
		final RequestStationTemplate packet = instance.newInstance();

		packet.templateId = temlateId;

		return packet;
	}

	/** ид шаблона */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_STATION_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
