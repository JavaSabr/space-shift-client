package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на авторизацию аккаунта.
 * 
 * @author Ronn
 */
public class LoginRequestAuth extends ClientPacket {

	private static final LoginRequestAuth instance = new LoginRequestAuth();

	public static LoginRequestAuth getInstance(final String login, final String password) {
		final LoginRequestAuth packet = instance.newInstance();

		packet.login = login;
		packet.password = password;

		return packet;
	}

	/** логин аккаунта */
	private String login;
	/** пароль аккаунта */
	private String password;

	@Override
	public void finalyze() {
		login = null;
		password = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_AUTH;
	}

	@Override
	public String toString() {
		return "RequestAuth [login=" + login + ", password=" + password + "]";
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, login.length());
		writeString(buffer, login);
		writeByte(buffer, password.length());
		writeString(buffer, password);
	}
}
