package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.gui.controller.position.ElementType;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на обновление настройки элемента интерфейса.
 * 
 * @author Ronn
 */
public final class RequestInterfaceUpdate extends ClientPacket {

	private static final RequestInterfaceUpdate instance = new RequestInterfaceUpdate();

	public static ClientPacket getInstance(final ElementType type, final int x, final int y, final boolean minimized) {
		final RequestInterfaceUpdate packet = instance.newInstance();

		packet.type = type;
		packet.x = x;
		packet.y = y;
		packet.minimized = minimized;

		return packet;
	}

	/** тип элемента */
	private ElementType type;

	/** координата */
	private int x;
	/** координата */
	private int y;

	/** скрыт ли элемент */
	private boolean minimized;

	@Override
	public void finalyze() {
		type = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_INTERFACE_UPDATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, type.ordinal());
		writeInt(buffer, x);
		writeInt(buffer, y);
		writeByte(buffer, minimized ? 1 : 0);
	}
}
