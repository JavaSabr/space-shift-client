package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.item.SpaceItem;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на погрузку предмета из космоса на склад.
 * 
 * @author Ronn
 */
public class RequestTeleportItem extends ClientPacket {

	private static final RequestTeleportItem instance = new RequestTeleportItem();

	public static RequestTeleportItem getInstance(SpaceItem item) {
		RequestTeleportItem packet = instance.newInstance();
		packet.objectId = item.getObjectId();
		return packet;
	}

	/** уникальный ид предмета */
	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_TELEPORT_ITEM;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
