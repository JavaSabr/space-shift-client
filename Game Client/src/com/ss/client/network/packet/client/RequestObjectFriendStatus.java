package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.SpaceObject;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на текущий статус дружественности объекта.
 * 
 * @author Ronn
 */
public class RequestObjectFriendStatus extends ClientPacket {

	private static final RequestObjectFriendStatus instance = new RequestObjectFriendStatus();

	public static RequestObjectFriendStatus getInstance(SpaceObject object) {

		RequestObjectFriendStatus packet = instance.newInstance();
		packet.objectId = object.getObjectId();

		return packet;
	}

	/** уникальный ид объекта */
	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_OBJECT_FRIEND_STATUS;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
