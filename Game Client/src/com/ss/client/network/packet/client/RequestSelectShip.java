package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на вход в мир указанным кораблем.
 * 
 * @author Ronn
 */
public class RequestSelectShip extends ClientPacket {

	private static final RequestSelectShip instance = new RequestSelectShip();

	public static RequestSelectShip getInstance(final long objectId) {
		final RequestSelectShip packet = instance.newInstance();

		packet.objectId = objectId;

		return packet;
	}

	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SELECT_SHIP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
