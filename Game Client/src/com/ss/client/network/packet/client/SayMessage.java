package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.gui.model.MessageType;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Отправка сообщения.
 * 
 * @author Ronn
 */
public class SayMessage extends ClientPacket {

	private static final SayMessage instance = new SayMessage();

	public static SayMessage getInstance(final MessageType type, final String message) {
		final SayMessage packet = instance.newInstance();

		packet.type = type;
		packet.message = message;

		return packet;
	}

	/** содержание сообщения */
	private String message;

	/** тип сообщения */
	private MessageType type;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.SAY_MESSAGE;
	}

	@Override
	public String toString() {
		return "SayMessage [message=" + message + ", type=" + type + "]";
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, type.ordinal());
		writeByte(buffer, message.length());
		writeString(buffer, message);
	}
}
