package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Уведомление о нажатии на кнопку.
 * 
 * @author Ronn
 */
public final class QuestButtonSelect extends ClientPacket {

	private static final QuestButtonSelect instance = new QuestButtonSelect();

	public static ClientPacket getInstance(final int questId, final int buttonId, final boolean bookButton) {
		final QuestButtonSelect packet = instance.newInstance();

		packet.questId = questId;
		packet.buttonId = buttonId;
		packet.bookButton = bookButton;

		return packet;
	}

	/** ид квеста */
	private int questId;
	/** ид кнопки */
	private int buttonId;

	/** из журнала ли кнопка */
	private boolean bookButton;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.QUEST_BUTTON_SELECT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, questId);
		writeByte(buffer, buttonId);
		writeByte(buffer, bookButton ? 1 : 0);
	}
}
