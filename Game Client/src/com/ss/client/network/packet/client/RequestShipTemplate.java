package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запросом на получение шаблона корабля.
 * 
 * @author Ronn
 */
public class RequestShipTemplate extends ClientPacket {

	private static final RequestShipTemplate instance = new RequestShipTemplate();

	public static ClientPacket getInstance(final int templateId) {
		final RequestShipTemplate packet = instance.newInstance();

		packet.templateId = templateId;

		return packet;
	}

	/** ид интересуемого шаблона */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SHIP_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
