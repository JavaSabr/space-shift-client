package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запросом на получение инфы о локации.
 * 
 * @author Ronn
 */
public class RequestLocationInfo extends ClientPacket {

	private static final RequestLocationInfo instance = new RequestLocationInfo();

	public static ClientPacket getInstance(final int locationId) {
		final RequestLocationInfo packet = instance.newInstance();

		packet.locationId = locationId;

		return packet;
	}

	/** ид запрашиваемой локации */
	private int locationId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_LOCATION_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, locationId);
	}
}
