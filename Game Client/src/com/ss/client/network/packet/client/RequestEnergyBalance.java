package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Пакет с запросом обновления баланса энергии.
 * 
 * @author Ronn
 */
public class RequestEnergyBalance extends ClientPacket {

	private static final RequestEnergyBalance instance = new RequestEnergyBalance();

	public static RequestEnergyBalance getInstance(float weapon, float shield, float engine) {

		RequestEnergyBalance packet = instance.newInstance();
		packet.weapon = weapon;
		packet.shield = shield;
		packet.engine = engine;

		return packet;
	}

	/** распредиление на оружие */
	private float weapon;
	/** распределение на щит */
	private float shield;
	/** распределение на двигатель */
	private float engine;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_ENERGY_BALANCE;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeFloat(buffer, weapon);
		writeFloat(buffer, shield);
		writeFloat(buffer, engine);
	}
}
