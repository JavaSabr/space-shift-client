package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на получение списка скилов корабля.
 * 
 * @author Ronn
 */
public final class RequestSkillList extends ClientPacket {

	private static final RequestSkillList instance = new RequestSkillList();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SKILL_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
