package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на получение настроек интерфейса.
 * 
 * @author Ronn
 */
public final class RequestInterface extends ClientPacket {

	private static final RequestInterface instance = new RequestInterface();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_INTERFACE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
