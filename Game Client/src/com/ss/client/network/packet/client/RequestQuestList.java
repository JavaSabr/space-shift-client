package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на получение инфы о текущем списке заданий игрока.
 * 
 * @author Ronn
 */
public class RequestQuestList extends ClientPacket {

	private static final RequestQuestList instance = new RequestQuestList();

	public static final ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_QUEST_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
