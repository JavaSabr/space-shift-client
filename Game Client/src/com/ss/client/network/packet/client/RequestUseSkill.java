package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;


import com.jme3.math.Vector3f;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на использование скила.
 * 
 * @author Ronn
 */
public class RequestUseSkill extends ClientPacket {

	private static final RequestUseSkill instance = new RequestUseSkill();

	public static RequestUseSkill getInstance(SpaceShip ship, final int objectId, final int skillId) {

		final RequestUseSkill packet = instance.newInstance();

		packet.objectId = objectId;
		packet.skillId = skillId;
		packet.location.set(ship.getLocation());

		return packet;
	}

	/** место, в котором происходит запрос */
	private final Vector3f location;

	/** уникальный ид модуля */
	private int objectId;
	/** ид темплея скила */
	private int skillId;

	public RequestUseSkill() {
		this.location = new Vector3f();
	}

	public Vector3f getLocation() {
		return location;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_USE_SKILL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, skillId);

		Vector3f location = getLocation();

		writeFloat(buffer, location.getX());
		writeFloat(buffer, location.getY());
		writeFloat(buffer, location.getZ());
	}
}
