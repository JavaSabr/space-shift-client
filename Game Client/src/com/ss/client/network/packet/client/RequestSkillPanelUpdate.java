package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.skills.Skill;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на обновление положения скила на панели.
 * 
 * @author Ronn
 */
public final class RequestSkillPanelUpdate extends ClientPacket {

	private static final RequestSkillPanelUpdate instance = new RequestSkillPanelUpdate();

	public static ClientPacket getInstance(final Skill skill, final int oldOrder, final int newOrder) {

		final RequestSkillPanelUpdate packet = instance.newInstance();

		packet.skill = skill;
		packet.oldOrder = oldOrder;
		packet.newOrder = newOrder;

		return packet;
	}

	/** умение на панели */
	private Skill skill;

	/** старое положение на панели */
	private int oldOrder;
	/** новое положение на панели */
	private int newOrder;

	@Override
	public void finalyze() {
		skill = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SKILL_PANEL_UPDATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, skill.getObjectId());
		writeInt(buffer, skill.getId());
		writeByte(buffer, oldOrder);
		writeByte(buffer, newOrder);
	}
}
