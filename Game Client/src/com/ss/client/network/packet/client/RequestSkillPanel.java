package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на получение настроек расположения скилов на панели.
 * 
 * @author Ronn
 */
public final class RequestSkillPanel extends ClientPacket {

	private static final RequestSkillPanel instance = new RequestSkillPanel();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SKILL_PANEL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
