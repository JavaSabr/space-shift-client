package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.ship.player.PlayerShipClass;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Отсылув информации о аккаунте гейм серверу.
 * 
 * @author Ronn
 */
public class RequestCreateShip extends ClientPacket {

	private static final RequestCreateShip instance = new RequestCreateShip();

	public static RequestCreateShip getInstance(final String name, final PlayerShipClass type) {
		final RequestCreateShip packet = instance.newInstance();

		packet.name = name;
		packet.type = type;

		return packet;
	}

	/** логин аккаунта */
	private String name;

	/** пароль аккаунта */
	private PlayerShipClass type;

	@Override
	public void finalyze() {
		name = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_CREATE_SHIP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, name.length());
		writeString(buffer, name);
		writeByte(buffer, type.ordinal());
	}
}
