package com.ss.client.network;

import java.util.HashSet;
import java.util.Set;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Перечисление типов серверных пакетов.
 * 
 * @author Ronn
 */
public enum ClientPacketType {
	/** логин сервер */

	REQUEST_REGISTER(0x1000),
	REQUEST_AUTH(0x1001),
	REQUEST_SERVER_LIST(0x1002),

	/** гейм сервер */

	/** пакеты в рамках ангара */
	RESPONSE_AUTH(0x3000),
	REQUEST_SHIP_LIST(0x3001),
	REQUEST_CREATE_SHIP(0x3002),
	REQUEST_DELETE_SHIP(0x3003),
	REQUEST_SELECT_SHIP(0x3004),
	REQUEST_SPAWN(0x3005),

	/** пакеты в ранкам самой игры */

	/** запрос на обновление положения скила на панели */
	REQUEST_SKILL_PANEL_UPDATE(0x3006),
	/** напрос на смену вектора направления полета */
	REQUEST_SHIP_ROTATE(0x3007),
	/** запрос на отправку сообщения в чат */
	SAY_MESSAGE(0x3008),
	/** запроос на обновление позиции элемента интерфейса */
	REQUEST_INTERFACE_UPDATE(0x3009),
	/** запрос на получение настроек интерфейса */
	REQUEST_INTERFACE(0x3010),
	/** запрос на получение списка скилов */
	REQUEST_SKILL_LIST(0x3011),
	/** запрос на использование скитла */
	REQUEST_USE_SKILL(0x3012),
	/** запрос на получение настроек размещения скилов */
	REQUEST_SKILL_PANEL(0x3013),
	/** запрос на получение диалога ангара */
	REQUEST_HANGAR_DIALOG(0x3014),
	/** уведомление о нажатии кнопки */
	QUEST_BUTTON_SELECT(0x3015),
	/** запрос на получение списка выполненых квестов */
	REQUEST_QUEST_COMPLETE_LIST(0x3016),
	/** запрос на получение шаблона корабля */
	REQUEST_SHIP_TEMPLATE(0x3017),
	/** запрос на получение шаблона модуля */
	REQUEST_MODULE_TEMPLATE(0x3018),
	/** запрос на получение шаблона гравитационного объекта */
	REQUEST_GRAVITY_OBJECT_TEMPLATE(0x3019),
	/** запрос на получение информации о локации */
	REQUEST_LOCATION_INFO(0x3020),
	/** запрос шаблона космической станции */
	REQUEST_STATION_TEMPLATE(0x3021),
	/** запрос на получение шаблона скила */
	REQUEST_SKILL_TEMPLATE(0x3022),
	/** запрос на получение инфы о квестовой книге */
	REQUEST_QUEST_LIST(0x3023),
	/** запрос на получение шаблона предмета */
	REQUEST_ITEM_TEMPLATE(0x3024),
	/** запрос на погрузку предмета в скад */
	REQUEST_TELEPORT_ITEM(0x3025),
	/** запрос информации о содержимом склада */
	REQUEST_STORAGE_INFO(0x3026),
	/** запрос на перемещение предмета в другую ячейку хранилища */
	REQUEST_MOVE_STORAGE_ITEM(0x3027),
	/** пакет с запросом о принудительной синхронизации объекта с сервером */
	REQUEST_FORCE_SYNC_OBJECT(0x3028),
	/** запрос на получение статуса корабля */
	REQUEST_SHIP_STATUS(0x3029),
	/** запрос на обновление распределении энергии */
	REQUEST_ENERGY_BALANCE(0x3030),
	/** запрос текущего баланса распределеия энергии */
	REQUEST_CURRENT_ENERGY_BALANCE(0x3031),
	/** запрос текущего статуса дружелюности объекта */
	REQUEST_OBJECT_FRIEND_STATUS(0x3032),
	/** запрос на выделение цели */
	REQUEST_SELECT_TARGET(0x3033),
	/** запрос на получение шаблона локационного объекта */
	REQUEST_LOCATION_OBJECT_TEMPLATE(0x3034),
	/** запрос на получение информации расположения предметов на панели */
	REQUEST_ITEM_PANEL(0x3035),
	/** запрос на обновление позиции предмета на панели */
	REQUEST_ITEM_PANEL_UPDATE(0x3036), ;

	private static final Logger LOGGER = Loggers.getLogger(ClientPacketType.class);

	/** массив всех типов пакетов */
	public static final ClientPacketType[] VALUES = values();

	/** кол-во всех типов пакетов */
	public static final int SIZE = VALUES.length;

	/**
	 * Инициализация клиентских пакетов.
	 */
	public static final void init() {

		final Set<Integer> set = new HashSet<Integer>();

		for(final ClientPacketType element : VALUES) {

			if(set.contains(element.opcode)) {
				LOGGER.warning("found duplicate opcode " + Integer.toHexString(element.opcode).toUpperCase());
			}

			set.add(element.opcode);
		}

		LOGGER.info("client packets prepared.");
	}

	/** пул пакетов */
	private final FoldablePool<ClientPacket> pool;

	/** опкод пакета */
	private int opcode;

	/**
	 * @param opcode опкод пакета.
	 */
	private ClientPacketType(final int opcode) {
		this.opcode = opcode;
		this.pool = Pools.newConcurrentFoldablePool(ClientPacket.class);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return пул соотвествующего пакета.
	 */
	public FoldablePool<ClientPacket> getPool() {
		return pool;
	}

	/**
	 * @param opcode опкод пакета.
	 */
	public void setOpcode(final int opcode) {
		this.opcode = opcode;
	}
}
