package com.ss.client.network;

import com.ss.client.network.model.NetServer;

import rlib.network.packets.AbstractReadeablePacket;
import rlib.util.pools.FoldablePool;

/**
 * Модель серверного пакета, присылаемого сервером.
 * 
 * @author Ronn
 */
public abstract class ServerPacket extends AbstractReadeablePacket<NetServer> {

	/** типа пакета */
	private ServerPacketType type;

	@Override
	protected final FoldablePool<ServerPacket> getPool() {
		return type.getPool();
	}

	@Override
	public final ServerPacket newInstance() {
		ServerPacket packet = getPool().take();

		if(packet == null) {
			try {
				packet = getClass().newInstance();
			} catch(InstantiationException | IllegalAccessException e) {
				LOGGER.warning(this, e);
			}

			packet.type = type;
		}

		return packet;
	}

	/**
	 * Чтение 8х байтов в виде double из буфера.
	 */
	protected final double readDouble() {
		return buffer.getDouble();
	}

	/**
	 * @param type тип пакета.
	 */
	public final void setPacketType(final ServerPacketType type) {
		this.type = type;
	}
}
