package com.ss.client.network;

import java.net.InetSocketAddress;

import com.ss.client.Config;
import com.ss.client.network.model.GameConnectHandler;
import com.ss.client.network.model.LoginConnectHandler;
import com.ss.client.network.model.NetServer;
import com.ss.client.util.ReflectionMethod;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.network.NetworkFactory;
import rlib.network.client.ClientNetwork;

/**
 * Сеть клиента.
 * 
 * @author Ronn
 */
public final class Network {

	private static final Logger log = Loggers.getLogger(Network.class);

	private static Network instance;

	@ReflectionMethod
	public static Network getInstance() {
		if(instance == null)
			instance = new Network();

		return instance;
	}

	/** селектор для подключения к логин серверу */
	private final ClientNetwork loginNetwork;
	/** селектор для подключения к гейм сервере */
	private final ClientNetwork gameNetwork;

	/** логин сервер */
	private NetServer loginServer;
	/** гейм сервер */
	private NetServer gameServer;

	/** флаг завершенного конекта к логин серверу */
	private boolean loginConnected;
	/** флаг завершенного конекта к гейм серверу */
	private boolean gameConnected;

	private Network() {
		InitializeManager.valid(getClass());

		ServerPacketType.init();
		ClientPacketType.init();

		loginNetwork = NetworkFactory.newDefaultAsynchronousClientNetwork(Config.NETWORK_CONFIG, LoginConnectHandler.getInstance());
		gameNetwork = NetworkFactory.newDefaultAsynchronousClientNetwork(Config.NETWORK_CONFIG, GameConnectHandler.getInstance());

		connectToLogin();

		log.info("initialized.");
	}

	/**
	 * Запусть процесс подключения к игровому серверу.
	 */
	public void connectToGame(final InetSocketAddress adress) {
		log.info("connect to game server " + adress);

		gameNetwork.connect(adress);
	}

	/**
	 * Запусть процесс подключения к логин серверу.
	 */
	public void connectToLogin() {
		loginNetwork.connect(Config.LOCAL_MODE ? Config.LOGIN_LOCAL_HOST : Config.LOGIN_HOST);
	}

	public ClientNetwork getGameNetwork() {
		return gameNetwork;
	}

	/**
	 * @return гейм сервер.
	 */
	public NetServer getGameServer() {
		return gameServer;
	}

	public ClientNetwork getLoginNetwork() {
		return loginNetwork;
	}

	/**
	 * @return логин сервер.
	 */
	public NetServer getLoginServer() {
		return loginServer;
	}

	/**
	 * @return флаг подключения к гейм серверу.
	 */
	public final boolean isGameConnected() {
		return gameConnected;
	}

	/**
	 * @return есть ли коннект к логину.
	 */
	public boolean isLoginConnected() {
		return loginConnected;
	}

	/**
	 * Отправка пакета игровому серверу.
	 * 
	 * @param packet отправляемый пакет.
	 */
	public void sendPacketToGameServer(final ClientPacket packet) {
		final NetServer server = getGameServer();

		if(server == null)
			throw new RuntimeException("not found Game Server.");

		server.sendPacket(packet);
	}

	/**
	 * Отправка пакета логин серверу.
	 * 
	 * @param packet отправляемый пакет.
	 */
	public void sendPacketToLoginServer(final ClientPacket packet) {
		final NetServer server = getLoginServer();

		if(server == null)
			throw new RuntimeException("not found Login Server.");

		server.sendPacket(packet);
	}

	/**
	 * @param gameConnected флаг подключения к гейм серверу.
	 */
	public synchronized final void setGameConnected(final boolean gameConnected) {
		this.gameConnected = gameConnected;

		if(!gameConnected && gameServer != null)
			gameServer.close();
	}

	/**
	 * @param gameServer гейм сервер.
	 */
	public void setGameServer(final NetServer gameServer) {
		this.gameServer = gameServer;

		if(gameServer == null)
			log.info("close game server");
	}

	/**
	 * @param loginConnected есть ли коннект к логину.
	 */
	public synchronized void setLoginConnected(final boolean loginConnected) {
		this.loginConnected = loginConnected;

		if(!loginConnected && loginServer != null)
			loginServer.close();
	}

	/**
	 * @param loginServer логин сервер.
	 */
	public void setLoginServer(final NetServer loginServer) {
		this.loginServer = loginServer;
		this.loginConnected = loginServer != null;
	}
}
