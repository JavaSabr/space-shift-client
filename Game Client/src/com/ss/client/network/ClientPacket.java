package com.ss.client.network;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;

import com.ss.client.network.model.NetServer;

import rlib.network.packets.AbstractSendablePacket;
import rlib.util.pools.FoldablePool;

/**
 * Базовая модель отправляемого пакета серверам.
 * 
 * @author Ronn
 */
public abstract class ClientPacket extends AbstractSendablePacket<NetServer> {

	/** тип пакета */
	private final ClientPacketType type;

	/** получаем конструктор пакета */
	private Constructor<? extends ClientPacket> constructor;

	public ClientPacket() {
		this.type = getPacketType();

		try {
			this.constructor = getClass().getConstructor();
		} catch(NoSuchMethodException | SecurityException e) {
			LOGGER.warning(this, e);
		}
	}

	@Override
	public void complete() {
		// ложим в пул
		getPool().put(this);
	}

	/**
	 * @return тип пакета.
	 */
	public abstract ClientPacketType getPacketType();

	/**
	 * @return получение пула для соотвествующего пакета.
	 */
	protected FoldablePool<ClientPacket> getPool() {
		return type.getPool();
	}

	@Override
	public boolean isSynchronized() {
		return false;
	}

	/**
	 * @return новый экземпляр пакета.
	 */
	@SuppressWarnings("unchecked")
	public final <T extends ClientPacket> T newInstance() {
		// получаем пакет из пула
		ClientPacket packet = getPool().take();

		// если его нету
		if(packet == null)
			try {
				// создаем новый
				packet = constructor.newInstance();
			} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.warning(this, e);
			}

		// возвращаем пакет
		return (T) packet;
	}

	@Override
	public final void writeHeader(final ByteBuffer buffer, final int length) {
		buffer.putShort(0, (short) length);
	}

	/**
	 * Запись опкода пакета.
	 */
	protected final void writeOpcode() {
		writeShort(type.getOpcode());
	}

	/**
	 * Запись опкода пакета.
	 */
	protected final void writeOpcode(final ByteBuffer buffer) {
		writeShort(buffer, type.getOpcode());
	}

	@Override
	public final void writePosition(final ByteBuffer buffer) {
		buffer.position(2);
	}
}
