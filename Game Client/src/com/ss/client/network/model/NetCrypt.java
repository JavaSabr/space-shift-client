package com.ss.client.network.model;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.network.GameCrypt;
import rlib.util.SymmetryCrypt;

/**
 * Модель игрового криптора пакетов.
 * 
 * @author Ronn
 */
public final class NetCrypt implements GameCrypt {

	private static final Logger log = Loggers.getLogger(NetCrypt.class);

	/** сам криптор */
	private SymmetryCrypt crypter;

	public NetCrypt() {
		try {
			crypter = new SymmetryCrypt(":qYx5GE?");
		} catch(InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException e) {
			log.warning(e);

			System.exit(0);
		}
	}

	@Override
	public void decrypt(final byte[] data, final int offset, final int length) {
		try {
			crypter.decrypt(data, offset, length, data);
		} catch(ShortBufferException | IllegalBlockSizeException | BadPaddingException e) {
			log.warning(e);
		}
	}

	@Override
	public void encrypt(final byte[] data, final int offset, final int length) {
		try {
			crypter.encrypt(data, offset, length, data);
		} catch(ShortBufferException | IllegalBlockSizeException | BadPaddingException e) {
			log.warning(e);
		}
	}
}
