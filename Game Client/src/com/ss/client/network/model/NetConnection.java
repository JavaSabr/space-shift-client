package com.ss.client.network.model;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import rlib.network.client.ClientNetwork;
import rlib.network.client.server.AbstractServerConnection;
import rlib.util.Util;

import com.ss.client.Config;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.ServerPacketType;

/**
 * Модель подключения к срверу.
 * 
 * @author Ronn
 */
public class NetConnection extends AbstractServerConnection<NetServer, ServerPacket, ClientPacket> {

	public NetConnection(final ClientNetwork network, final AsynchronousSocketChannel channel, final Class<ClientPacket> sendableType) {
		super(network, channel, sendableType);
	}

	/**
	 * Получение пакета из буфера.
	 */
	private ServerPacket getPacket(final ByteBuffer buffer, final NetServer server) {

		if(buffer.remaining() < 2) {
			return null;
		}

		ServerPacket packet = ServerPacketType.getPacketForOpcode(buffer.getShort() & 0xFFFF);

		if(packet != null) {
			// LOGGER.info("find packet " + packet.getName());
		}

		return packet;
	}

	/**
	 * Получение размера следующего пакета в буфере.
	 * 
	 * @param buffer буфер с данными пакетов.
	 * @return размер пакета.
	 */
	private final int getPacketSize(ByteBuffer buffer) {
		return buffer.getShort() & 0xFFFF;
	}

	@Override
	protected void movePacketToBuffer(final ClientPacket packet, final ByteBuffer buffer) {

		final NetServer server = getServer();

		buffer.clear();
		packet.writePosition(buffer);
		packet.write(buffer);
		buffer.flip();

		packet.writeHeader(buffer, buffer.limit());

		if(Config.DEV_DEBUG_CLIENT_PACKETS) {
			LOGGER.info("Client packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}

		server.encrypt(buffer, 2, buffer.limit() - 2);

		// System.out.println("write " + packet.getName() + ", length " +
		// buffer.limit());

		if(Config.DEV_DEBUG_CLIENT_PACKETS) {
			System.out.println("Client crypt packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}
	}

	@Override
	protected void onWrited(final ClientPacket packet) {
		packet.complete();
	}

	@Override
	protected void readPacket(final ByteBuffer buffer) {

		final NetServer server = getServer();

		if(Config.DEV_DEBUG_SERVER_PACKETS) {
			System.out.println("Server dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}

		for(int i = 0, limit = 0, size = 0; buffer.remaining() > 2 && i < 1000; i++) {

			size = getPacketSize(buffer);
			limit += size;

			server.decrypt(buffer, buffer.position(), size - 2);
			server.readPacket(getPacket(buffer, server), buffer);

			if(limit > buffer.limit()) {
				break;
			}

			buffer.position(limit);
		}
	}
}
