package com.ss.client.network.model;

import java.net.ConnectException;
import java.nio.channels.AsynchronousSocketChannel;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.Network;

import rlib.logging.Loggers;
import rlib.network.client.ConnectHandler;

/**
 * обработчик подключения к логин серверу.
 * 
 * @author Ronn
 */
public class LoginConnectHandler extends ConnectHandler {

	private static LoginConnectHandler instance;

	public static LoginConnectHandler getInstance() {
		if(instance == null)
			instance = new LoginConnectHandler();

		return instance;
	}

	private LoginConnectHandler() {
		super();
	}

	@Override
	protected void onConnect(final AsynchronousSocketChannel channel) {
		// получаем сеть
		final Network network = Network.getInstance();

		// создаем подключение к логин серверу
		final NetConnection connection = new NetConnection(network.getLoginNetwork(), channel, ClientPacket.class);

		// создаем обертку для общения с сервером
		final NetServer server = new NetServer(connection, ServerType.LOGIN_SERVER, null);

		// запоминаем логие сервер
		network.setLoginServer(server);

		// запоминаем за коннектом
		connection.setServer(server);

		// ставим в режим ожидания пакетов
		connection.startRead();
	}

	@Override
	protected void onFailed(final Throwable exc) {
		// сообщаем об ошибке
		if(exc instanceof ConnectException)
			Loggers.info(this, "невозможно подключиться.");
		else
			Loggers.warning(this, new Exception(exc));

		try {
			Thread.sleep(5000);
		} catch(final InterruptedException e) {
			Loggers.warning(this, e);
		}

		// получаем сеть
		final Network network = Network.getInstance();

		// запускаем реконект
		network.connectToLogin();
	}
}
