package com.ss.client.network.model;

import rlib.network.client.server.AbstractServer;
import rlib.network.packets.ReadeablePacket;

import com.ss.client.manager.ExecutorManager;
import com.ss.client.model.ServerInfo;
import com.ss.client.network.Network;

/**
 * Модель сервера.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public class NetServer extends AbstractServer<NetConnection, NetCrypt> {

	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();

	/** тип сервера */
	private final ServerType type;

	/** информация о сервере */
	private ServerInfo info;

	public NetServer(final NetConnection connection, final ServerType type, final ServerInfo info) {
		super(connection, new NetCrypt());

		this.type = type;
		this.info = info;
	}

	@Override
	public void close() {
		super.close();

		final Network network = Network.getInstance();

		if(type == ServerType.LOGIN_SERVER) {
			network.setLoginServer(null);
			network.connectToLogin();
		} else if(type == ServerType.GAME_SERVER) {
			network.setGameServer(null);
		}

		LOGGER.info(this, "close " + type + ".");
	}

	@Override
	protected void execute(final ReadeablePacket packet) {

		if(packet.isSynchronized()) {
			EXECUTOR_MANAGER.synExecute(packet);
		} else {
			EXECUTOR_MANAGER.asynExecute(packet);
		}
	}

	/**
	 * @return инфа об сервере.
	 */
	public final ServerInfo getInfo() {
		return info;
	}

	/**
	 * @param info инфа об сервере.
	 */
	public final void setInfo(final ServerInfo info) {

		if(info == null) {
			new Exception("set null server info").printStackTrace();
		}

		this.info = info;
	}

	@Override
	public String toString() {
		return "NetServer [type=" + type + "]";
	}
}
