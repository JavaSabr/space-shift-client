package com.ss.client.document;

import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ss.client.template.StationTemplate;

import rlib.data.AbstractStreamDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

/**
 * Парсер моделей станций с xml.
 * 
 * @author Ronn
 * @deprecated
 */
@Deprecated
public final class DocumentStations extends AbstractStreamDocument<Array<StationTemplate>> {

	public DocumentStations(final InputStream stream) {
		super(stream);
	}

	@Override
	protected Array<StationTemplate> create() {
		return Arrays.toArray(StationTemplate.class);
	}

	@Override
	protected void parse(final Document doc) {
		for(Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
			if("list".equals(list.getNodeName()))
				for(Node temp = list.getFirstChild(); temp != null; temp = temp.getNextSibling())
					if("template".equals(temp.getNodeName())) {
						final VarTable vars = VarTable.newInstance(temp);

						final StationTemplate template = new StationTemplate(vars);

						result.add(template);
					}
	}
}
