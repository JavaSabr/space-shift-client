package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestItemTemplate;
import com.ss.client.template.item.ItemTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица шаблонов предметов.
 * 
 * @author Ronn
 */
public final class ItemTable {

	private static final Logger LOGGER = Loggers.getLogger(ItemTable.class);

	private static final int WAIT_TIME = 100000;

	private static ItemTable instance;

	@ReflectionMethod
	public static ItemTable getInstance() {

		if(instance == null) {
			instance = new ItemTable();
		}

		return instance;
	}

	/** таблица шаблонов предметов */
	private final Table<IntKey, ItemTemplate> table;

	private ItemTable() {
		table = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона модуля в таблицу.
	 * 
	 * @param template новый шаблон модуля.
	 */
	public void addTemplate(final ItemTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ItemTemplate> T getTemplate(final int templateId) {

		ItemTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return (T) template;
	}

	/**
	 * Загрузка с сервера шаблона модуля.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private ItemTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestItemTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
