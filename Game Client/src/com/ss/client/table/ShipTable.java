package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestShipTemplate;
import com.ss.client.template.ship.ShipTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица моделей кораблей.
 * 
 * @author Ronn
 */
public final class ShipTable {

	private static final Logger LOGGER = Loggers.getLogger(ShipTable.class);

	private static final int WAIT_TIME = 100000;

	private static ShipTable instance;

	@ReflectionMethod
	public static ShipTable getInstance() {

		if(instance == null) {
			instance = new ShipTable();
		}

		return instance;
	}

	/** таблица темплейтов моделей кораблей */
	private final Table<IntKey, ShipTemplate> table;

	private ShipTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		LOGGER.info("linitialized.");
	}

	/**
	 * Добавление нового шаблона корабля в таблицу.
	 * 
	 * @param template новый шаблон корабля.
	 */
	public void addTemplate(final ShipTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	@SuppressWarnings("unchecked")
	public final <T extends ShipTemplate> T getTemplate(final int templateId) {

		ShipTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return (T) template;
	}

	/**
	 * Загрузка с сервера шаблона корабля.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private ShipTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestShipTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
