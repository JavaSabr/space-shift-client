package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.model.skills.Skill;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestSkillTemplate;
import com.ss.client.template.SkillTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица скилов модулей.
 * 
 * @author Ronn
 */
public final class SkillTable {

	private static final Logger LOGGER = Loggers.getLogger(SkillTable.class);

	private static final int WAIT_TIME = 100000;

	private static SkillTable instance;

	@ReflectionMethod
	public static SkillTable getInstance() {

		if(instance == null) {
			instance = new SkillTable();
		}

		return instance;
	}

	/** таблица шаблонов модулей кораблей */
	private final Table<IntKey, SkillTemplate> table;
	/** таблица инстансов скилов */
	private final Table<IntKey, Skill> instances;

	private SkillTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		instances = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона скила в таблицу.
	 * 
	 * @param template новый шаблон скила.
	 */
	public void addTemplate(final SkillTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @param id ид инстанса скила.
	 * @return инстанс скил.
	 */
	public Skill getInstance(final int id) {

		Skill instance = instances.get(id);

		if(instance == null) {
			instance = loadInstance(id);
		}

		return instance;
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public SkillTemplate getTemplate(final int templateId) {

		SkillTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return template;
	}

	/**
	 * Подгрузка инстанса скила.
	 * 
	 * @param id скила.
	 * @return возвращает инстанс скила.
	 */
	public Skill loadInstance(final int id) {

		final SkillTemplate template = getTemplate(id);

		if(template != null) {
			final Skill instance = template.newInstance(0);
			instances.put(id, instance);
			return instance;
		}

		return null;
	}

	/**
	 * Загрузка с сервера шаблона скила.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private SkillTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestSkillTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
