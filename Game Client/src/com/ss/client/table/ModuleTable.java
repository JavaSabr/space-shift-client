package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestModuleTemplate;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица модулей кораблей.
 * 
 * @author Ronn
 */
public final class ModuleTable {

	private static final Logger LOGGER = Loggers.getLogger(ModuleTable.class);

	private static final int WAIT_TIME = 100000;

	private static ModuleTable instance;

	@ReflectionMethod
	public static ModuleTable getInstance() {

		if(instance == null) {
			instance = new ModuleTable();
		}

		return instance;
	}

	/** таблица темплейтов модулей кораблей */
	private final Table<IntKey, ModuleTemplate> table;

	private ModuleTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона модуля в таблицу.
	 * 
	 * @param template новый шаблон модуля.
	 */
	public void addTemplate(final ModuleTemplate template) {

		LOGGER.info("load template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public <T extends ModuleTemplate> T getTemplate(final Class<T> type, final int templateId) {
		return type.cast(getTemplate(templateId));
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public ModuleTemplate getTemplate(final int templateId) {

		ModuleTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return template;
	}

	/**
	 * Загрузка с сервера шаблона модуля.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private ModuleTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestModuleTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
