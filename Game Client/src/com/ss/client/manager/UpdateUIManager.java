package com.ss.client.manager;

import rlib.concurrent.ConcurrentUtils;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.tasks.UpdateUIElementTask;
import com.ss.client.util.ReflectionMethod;

/**
 * Менедж по асинхроному обновлению элементов UI.
 * 
 * @author Ronn
 */
public class UpdateUIManager {

	public static final int MAX_CONTAINER_LOAD = 100;
	public static final int START_CONTAINERS_COUNT = 0;

	private static UpdateUIManager instance;

	@ReflectionMethod
	public static UpdateUIManager getInstance() {

		if(instance == null) {
			instance = new UpdateUIManager();
		}

		return instance;
	}

	/** контейнеры обновляемых элементов UI */
	private volatile Array<UpdateUIElement>[] containers;
	/** список задач по обновлению элементов в контейнерах */
	private volatile UpdateUIElementTask[] updateTasks;

	/** индекс последнего использованного контейнера */
	private int index;

	@SuppressWarnings("unchecked")
	public UpdateUIManager() {
		InitializeManager.valid(getClass());

		containers = new Array[START_CONTAINERS_COUNT];
		updateTasks = new UpdateUIElementTask[START_CONTAINERS_COUNT];

		for(int i = 0, length = containers.length; i < length; i++) {
			containers[i] = Arrays.toArray(UpdateUIElement.class);
			updateTasks[i] = new UpdateUIElementTask(containers[i], i, false);
		}
	}

	/**
	 * Добавление на обнвление нового UI элемента.
	 * 
	 * @param element новый обновляемый элемент.
	 */
	public void addElement(UpdateUIElement element) {

		Array<UpdateUIElement>[] containers = getContainers();
		Array<UpdateUIElement> target = null;

		if(!element.isComplexElement()) {

			int index = getIndex();

			// фаза 1, мы пробуем найти не занятый контейнер в списке после
			// последнего используемого
			if(index < containers.length - 1) {
				for(int i = index + 1, length = containers.length; i < length; i++) {

					Array<UpdateUIElement> container = containers[i];

					if(container.size() < MAX_CONTAINER_LOAD) {
						target = container;
						index = i;
						break;
					}
				}

			}

			// фаза 2, если еще не нашли контейнер, пробуем еще раз поискать с
			// самого начала списка
			if(target == null) {
				for(int i = 0, length = containers.length; i < length; i++) {

					Array<UpdateUIElement> container = containers[i];

					if(container.size() < MAX_CONTAINER_LOAD) {
						target = container;
						index = i;
						break;
					}
				}
			}
		}

		// фаза 3, если досихпор не нашли контейнер, создаем новый
		if(target == null) {
			target = createContainer(containers.length, element.isComplexElement());
			index = containers.length;
		}

		synchronized(target) {

			target.add(element);

			if(target.size() == 1) {
				ConcurrentUtils.notifyAllInSynchronize(target);
			}
		}

		element.bind(target);
		setIndex(index);
	}

	/**
	 * Расширение списка контейнеров.
	 * 
	 * @param currentLength текущее кол-во контейнеров.
	 * @return новый контейнер.
	 */
	private synchronized Array<UpdateUIElement> createContainer(int currentLength, boolean complex) {

		if(complex) {

			Array<UpdateUIElement> container = Arrays.toArray(UpdateUIElement.class);

			UpdateUIElementTask task = new UpdateUIElementTask(container, -1, complex);
			task.start();

			return container;

		} else {

			Array<UpdateUIElement>[] containers = getContainers();

			// если уже было произведено расширение в другом потоке
			if(containers.length > currentLength) {
				return containers[currentLength];
			}

			containers = Arrays.copyOf(containers, 1);
			containers[currentLength] = Arrays.toArray(UpdateUIElement.class);

			UpdateUIElementTask[] updateTasks = getUpdateTasks();
			updateTasks = Arrays.copyOf(updateTasks, 1);
			updateTasks[currentLength] = new UpdateUIElementTask(containers[currentLength], currentLength, false);
			updateTasks[currentLength].start();

			setContainers(containers);
			setUpdateTasks(updateTasks);

			return containers[currentLength];
		}
	}

	/**
	 * @return контейнера обновляемых элементов.
	 */
	public Array<UpdateUIElement>[] getContainers() {
		return containers;
	}

	/**
	 * @return индекс контейнера, в который был последний добавлен элемент.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return задачи по обновлению элементов в контейнерах.
	 */
	public UpdateUIElementTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * @param containers контейнера обновляемых элементов.
	 */
	public void setContainers(Array<UpdateUIElement>[] containers) {
		this.containers = containers;
	}

	/**
	 * @param index индекс контейнера, в который был последний добавлен элемент.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param updateTasks задачи по обновлению элементов в контейнерах.
	 */
	public void setUpdateTasks(UpdateUIElementTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}
}
