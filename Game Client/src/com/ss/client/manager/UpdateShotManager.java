package com.ss.client.manager;

import rlib.concurrent.ConcurrentUtils;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.model.shots.Shot;
import com.ss.client.tasks.UpdateShotsTask;
import com.ss.client.util.ReflectionMethod;

/**
 * Менедж по обновлению состояния выстрелов.
 * 
 * @author Ronn
 */
public class UpdateShotManager {

	public static final int MAX_SHOT_LOAD = 30;
	public static final int START_CONTAINERS_COUNT = 1;

	private static UpdateShotManager instance;

	@ReflectionMethod
	public static UpdateShotManager getInstance() {

		if(instance == null) {
			instance = new UpdateShotManager();
		}

		return instance;
	}

	/** контейнеры изсполняемых выстрелов */
	private volatile Array<Shot>[] containers;
	/** список задач по обновлению выстрлов в контейнерах */
	private volatile UpdateShotsTask[] updateTasks;

	/** индекс последнего использованного контейнера */
	private int index;

	@SuppressWarnings("unchecked")
	public UpdateShotManager() {
		InitializeManager.valid(getClass());

		containers = new Array[START_CONTAINERS_COUNT];
		updateTasks = new UpdateShotsTask[START_CONTAINERS_COUNT];

		for(int i = 0, length = containers.length; i < length; i++) {
			containers[i] = Arrays.toArray(Shot.class);
			updateTasks[i] = new UpdateShotsTask(containers[i], i);
		}
	}

	/**
	 * Добавление на выполнение новоо выстрела.
	 * 
	 * @param shot новый выстрел.
	 */
	public void addShot(Shot shot) {

		Array<Shot>[] containers = getContainers();
		Array<Shot> target = null;

		int index = getIndex();

		// фаза 1, мы пробуем найти не занятый контейнер в списке после
		// последнего используемого
		if(index < containers.length - 1) {
			for(int i = index + 1, length = containers.length; i < length; i++) {

				Array<Shot> container = containers[i];

				if(container.size() < MAX_SHOT_LOAD) {
					target = container;
					index = i;
					break;
				}
			}

		}

		// фаза 2, если еще не нашли контейнер, пробуем еще раз поискать с
		// самого начала списка
		if(target == null) {
			for(int i = 0, length = containers.length; i < length; i++) {

				Array<Shot> container = containers[i];

				if(container.size() < MAX_SHOT_LOAD) {
					target = container;
					index = i;
					break;
				}
			}
		}

		// фаза 3, если досихпор не нашли контейнер, создаем новый
		if(target == null) {
			target = createContainer(containers.length);
			index = containers.length;
		}

		synchronized(target) {

			target.add(shot);

			if(target.size() == 1) {
				ConcurrentUtils.notifyAllInSynchronize(target);
			}
		}

		shot.bind(target);
		setIndex(index);
	}

	/**
	 * Расширение списка контейнеров.
	 * 
	 * @param currentLength текущее кол-во контейнеров.
	 * @return новый контейнер.
	 */
	private synchronized Array<Shot> createContainer(int currentLength) {

		Array<Shot>[] containers = getContainers();

		// если уже было произведено расширение в другом потоке
		if(containers.length > currentLength) {
			return containers[currentLength];
		}

		containers = Arrays.copyOf(containers, 1);
		containers[currentLength] = Arrays.toArray(Shot.class);

		UpdateShotsTask[] updateTasks = getUpdateTasks();
		updateTasks = Arrays.copyOf(updateTasks, 1);
		updateTasks[currentLength] = new UpdateShotsTask(containers[currentLength], currentLength);

		setContainers(containers);
		setUpdateTasks(updateTasks);

		return containers[currentLength];
	}

	/**
	 * @return контейнера обновляемых выстрелов.
	 */
	public Array<Shot>[] getContainers() {
		return containers;
	}

	/**
	 * @return индекс контейнера, в который был последний добавлен выстрел.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return задачи по обновлению выстрелов в контейнерах.
	 */
	public UpdateShotsTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * @param containers контейнера обновляемых выстрелов.
	 */
	public void setContainers(Array<Shot>[] containers) {
		this.containers = containers;
	}

	/**
	 * @param index индекс контейнера, в который был последний добавлен выстрел.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param updateTasks задачи по обновлению выстрелов в контейнерах.
	 */
	public void setUpdateTasks(UpdateShotsTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}
}
