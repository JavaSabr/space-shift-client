package com.ss.client.manager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.ss.client.GameThread;
import com.ss.client.util.ReflectionMethod;

import rlib.concurrent.GroupThreadFactory;
import rlib.logging.Logger;
import rlib.logging.Loggers;

/**
 * Менеджер пула потоков.
 * 
 * @author Ronn
 */
public final class ExecutorManager {

	private static final Logger LOGGER = Loggers.getLogger(ExecutorManager.class);

	private static ExecutorManager instance;

	@ReflectionMethod
	public static ExecutorManager getInstance() {

		if(instance == null) {
			instance = new ExecutorManager();
		}

		return instance;
	}

	/** исполнитель основных заданий */
	private final ScheduledExecutorService generalExecutor;
	/** исполнитель задач по обновлению выстрелов */
	private final ScheduledExecutorService updateShotExecutor;
	/** исполнитель задач по обновлению UI */
	private final ScheduledExecutorService updateUIExecutor;
	/** исполнитель задач по обновлению графических эффектов */
	private final ScheduledExecutorService updateGraficEffectExecutor;
	/** исполнитель задач по обновлению различных объектов */
	private final ScheduledExecutorService updateObjectExecutor;

	/** исполнитель асинхронных действий */
	private final ExecutorService asynExecutor;
	/** исполнитель синхроных задач */
	private final ExecutorService synExecutor;

	private ExecutorManager() {

		generalExecutor = Executors.newScheduledThreadPool(3, new GroupThreadFactory("GeneralExecutor", GameThread.class, Thread.NORM_PRIORITY));
		updateShotExecutor = Executors.newScheduledThreadPool(1, new GroupThreadFactory("UpdateShotExecutor", GameThread.class, Thread.NORM_PRIORITY + 2));
		updateUIExecutor = Executors.newScheduledThreadPool(1, new GroupThreadFactory("UpdateUIExecutor", GameThread.class, Thread.NORM_PRIORITY + 2));
		updateGraficEffectExecutor = Executors.newScheduledThreadPool(1, new GroupThreadFactory("UpdateGraficEffectExecutor", GameThread.class, Thread.NORM_PRIORITY + 2));
		updateObjectExecutor = Executors.newScheduledThreadPool(1, new GroupThreadFactory("UpdateObjectExecutor", GameThread.class, Thread.NORM_PRIORITY + 2));

		synExecutor = Executors.newFixedThreadPool(1, new GroupThreadFactory("SynExecutor", GameThread.class, Thread.MAX_PRIORITY));
		asynExecutor = Executors.newFixedThreadPool(3, new GroupThreadFactory("AsynExecutor", GameThread.class, Thread.MAX_PRIORITY));
	}

	/**
	 * Выполнение задания в параллельном потоке.
	 * 
	 * @param runnable задание, которое надо выполнить.
	 */
	public void asynExecute(final Runnable runnable) {
		asynExecutor.execute(runnable);
	}

	/**
	 * Создание отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед выполнением.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneral(final T runnable, long delay) {
		try {

			if(delay < 0) {
				delay = 0;
			}

			return (ScheduledFuture<T>) generalExecutor.schedule(runnable, delay, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Создание периодического отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед первым запуском.
	 * @param interval интервал между выполнением таска.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneralAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) generalExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Отправка на периодическое исполнение задач по обновлению графических
	 * эффектов.
	 * 
	 * @param runnable задача по обновлению UI элементов.
	 * @param delay задержка перед выполненим задачи.
	 * @param interval интервал выполнений задачи.
	 * @return ссылка на задачу.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleUpdateGraficEffectAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) updateGraficEffectExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Отправка на периодическое исполнение задач по обновлению объектов.
	 * 
	 * @param runnable задача по обновлению UI элементов.
	 * @param delay задержка перед выполненим задачи.
	 * @param interval интервал выполнений задачи.
	 * @return ссылка на задачу.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleUpdateObjectAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) updateObjectExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Отправка на периодическое исполнение задач по обновлению выстрелов.
	 * 
	 * @param runnable задача по обновлению выстрелов.
	 * @param delay задержка перед выполненим задачи.
	 * @param interval интервал выполнений задачи.
	 * @return ссылка на задачу.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleUpdateShotAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) updateShotExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Отправка на периодическое исполнение задач по обновлению UI элементов.
	 * 
	 * @param runnable задача по обновлению UI элементов.
	 * @param delay задержка перед выполненим задачи.
	 * @param interval интервал выполнений задачи.
	 * @return ссылка на задачу.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleUpdateUIAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) updateUIExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	public void synExecute(final Runnable runnable) {
		synExecutor.execute(runnable);
	}
}
