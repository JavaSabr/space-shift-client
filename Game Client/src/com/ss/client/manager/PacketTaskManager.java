package com.ss.client.manager;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.GameThread;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LocalObjects;
import com.ss.client.util.PacketTask;
import com.ss.client.util.ReflectionMethod;

/**
 * Менеджер по исполнению отложенных задач пакетов.
 * 
 * @author Ronn
 */
public class PacketTaskManager {

	private static final Logger LOGGER = Loggers.getLogger(PacketTaskManager.class);

	private static final Game GAME = Game.getInstance();

	public static final int EXECUTE_LIMIT = 100;

	private static final FuncElement<PacketTask> FINISH_TASK_FUNC = new FuncElement<PacketTask>() {

		@Override
		public void apply(PacketTask packetTask) {

			if(packetTask instanceof FoldablePacketTask) {
				((FoldablePacketTask) packetTask).fold();
			}
		}
	};

	private static PacketTaskManager instance;

	@ReflectionMethod
	public static PacketTaskManager getInstance() {

		if(instance == null) {
			instance = new PacketTaskManager();
		}

		return instance;
	}

	/** очередь исполняемых задач */
	private final Array<PacketTask> syncTasks;
	/** очередь исполняемых задач по геометрии */
	private final Array<PacketTask> geomTasks;
	/** очередь исполняемых задач по UI */
	private final Array<PacketTask> uiTasks;

	public PacketTaskManager() {
		InitializeManager.valid(getClass());

		this.syncTasks = Arrays.toArray(PacketTask.class);
		this.geomTasks = Arrays.toArray(PacketTask.class);
		this.uiTasks = Arrays.toArray(PacketTask.class);

		GameThread thread = new GameThread() {

			@Override
			public void run() {

				Array<PacketTask> execute = Arrays.toArray(PacketTask.class);
				Array<PacketTask> executed = Arrays.toArray(PacketTask.class);
				Array<PacketTask> syncTasks = getSyncTasks();

				LocalObjects local = LocalObjects.get();

				while(true) {

					executed.clear();
					execute.clear();

					synchronized(syncTasks) {

						if(syncTasks.isEmpty()) {
							ConcurrentUtils.waitInSynchronize(syncTasks);
						}

						execute.addAll(syncTasks);
					}

					GAME.syncLock();
					try {

						long currentTime = System.currentTimeMillis();

						for(PacketTask task : execute.array()) {

							if(task == null) {
								break;
							}

							if(task.execute(local, currentTime)) {
								executed.add(task);
							}
						}

						if(!executed.isEmpty()) {

							synchronized(syncTasks) {
								syncTasks.removeAll(executed);
							}

							executed.apply(FINISH_TASK_FUNC);
						}

					} catch(Exception e) {
						LOGGER.warning(e);
					} finally {
						GAME.syncUnlock();
					}
				}
			}
		};
		thread.setName("SyncPacketTaskExecutor");
		thread.setDaemon(true);
		thread.start();

		thread = new GameThread() {

			@Override
			public void run() {

				Array<PacketTask> execute = Arrays.toArray(PacketTask.class);
				Array<PacketTask> executed = Arrays.toArray(PacketTask.class);
				Array<PacketTask> geomTasks = getGeomTasks();

				LocalObjects local = LocalObjects.get();

				while(true) {

					executed.clear();
					execute.clear();

					synchronized(geomTasks) {

						if(geomTasks.isEmpty()) {
							ConcurrentUtils.waitInSynchronize(geomTasks);
						}

						execute.addAll(geomTasks);
					}

					GAME.updateGeomStart();
					try {

						long currentTime = System.currentTimeMillis();

						for(PacketTask task : execute.array()) {

							if(task == null) {
								break;
							}

							if(task.execute(local, currentTime)) {
								executed.add(task);
							}
						}

						if(!executed.isEmpty()) {

							synchronized(geomTasks) {
								geomTasks.removeAll(executed);
							}

							executed.apply(FINISH_TASK_FUNC);
						}

					} catch(Exception e) {
						LOGGER.warning(e);
					} finally {
						GAME.updateGeomEnd();
					}
				}
			}
		};
		thread.setName("GeomPacketTaskExecutor");
		thread.setDaemon(true);
		thread.start();

		thread = new GameThread() {

			@Override
			public void run() {

				Array<PacketTask> execute = Arrays.toArray(PacketTask.class);
				Array<PacketTask> executed = Arrays.toArray(PacketTask.class);
				Array<PacketTask> uiTaks = getUiTasks();

				LocalObjects local = LocalObjects.get();

				while(true) {

					executed.clear();
					execute.clear();

					synchronized(uiTaks) {

						if(uiTaks.isEmpty()) {
							ConcurrentUtils.waitInSynchronize(uiTaks);
						}

						execute.addAll(uiTaks);
					}

					GAME.updateUIStart();
					try {

						long currentTime = System.currentTimeMillis();

						for(PacketTask task : execute.array()) {

							if(task == null) {
								break;
							}

							if(task.execute(local, currentTime)) {
								executed.add(task);
							}
						}

						if(!executed.isEmpty()) {

							synchronized(uiTaks) {
								uiTaks.removeAll(executed);
							}

							executed.apply(FINISH_TASK_FUNC);
						}

					} catch(Exception e) {
						LOGGER.warning(e);
					} finally {
						GAME.updateUIEnd();
					}
				}
			}
		};
		thread.setName("UIPacketTaskExecutor");
		thread.setDaemon(true);
		thread.start();
	}

	/**
	 * Добавление на обработку пакетной задачи по обновлению геометрии.
	 * 
	 * @param task пакетная задача.
	 */
	public void addGeomTask(PacketTask task) {

		Array<PacketTask> geomTasks = getGeomTasks();

		synchronized(geomTasks) {

			boolean needNotify = geomTasks.isEmpty();

			geomTasks.add(task);

			if(needNotify) {
				ConcurrentUtils.notifyAllInSynchronize(geomTasks);
			}
		}
	}

	/**
	 * Добавление на обработку пакетной задачи.
	 * 
	 * @param task пакетная задача.
	 */
	public void addSyncTask(PacketTask task) {

		Array<PacketTask> syncTasks = getSyncTasks();

		synchronized(syncTasks) {

			boolean needNotify = syncTasks.isEmpty();

			syncTasks.add(task);

			if(needNotify) {
				ConcurrentUtils.notifyAllInSynchronize(syncTasks);
			}
		}
	}

	/**
	 * Добавление на обработку пакетной задачи по обновлению UI.
	 * 
	 * @param task пакетная задача.
	 */
	public void addUITask(PacketTask task) {

		Array<PacketTask> uiTasks = getUiTasks();

		synchronized(uiTasks) {

			boolean needNotify = uiTasks.isEmpty();

			uiTasks.add(task);

			if(needNotify) {
				ConcurrentUtils.notifyAllInSynchronize(uiTasks);
			}
		}
	}

	/**
	 * @return очередь исполняемых задач по геометрии.
	 */
	public Array<PacketTask> getGeomTasks() {
		return geomTasks;
	}

	/**
	 * @return
	 */
	public Array<PacketTask> getSyncTasks() {
		return syncTasks;
	}

	/**
	 * @return очередь исполняемых задач по UI.
	 */
	public Array<PacketTask> getUiTasks() {
		return uiTasks;
	}
}
