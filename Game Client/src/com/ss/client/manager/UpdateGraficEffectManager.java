package com.ss.client.manager;

import rlib.concurrent.ConcurrentUtils;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.model.effect.GraficEffect;
import com.ss.client.tasks.UpdateGraficEffectTask;
import com.ss.client.util.ReflectionMethod;

/**
 * Менедж по обновлению графических эффетков.
 * 
 * @author Ronn
 */
public class UpdateGraficEffectManager {

	public static final int START_CONTAINERS_COUNT = 1;
	public static final int MAX_SHOT_LOAD = 100;

	private static UpdateGraficEffectManager instance;

	@ReflectionMethod
	public static UpdateGraficEffectManager getInstance() {

		if(instance == null) {
			instance = new UpdateGraficEffectManager();
		}

		return instance;
	}

	/** контейнеры обновляемых графических эффектов */
	private volatile Array<GraficEffect>[] containers;
	/** список задач по обновлению эффектов в контейнерах */
	private volatile UpdateGraficEffectTask[] updateTasks;

	/** индекс последнего использованного контейнера */
	private int index;

	@SuppressWarnings("unchecked")
	public UpdateGraficEffectManager() {
		InitializeManager.valid(getClass());

		containers = new Array[START_CONTAINERS_COUNT];
		updateTasks = new UpdateGraficEffectTask[START_CONTAINERS_COUNT];

		for(int i = 0, length = containers.length; i < length; i++) {
			containers[i] = Arrays.toArray(GraficEffect.class);
			updateTasks[i] = new UpdateGraficEffectTask(containers[i], 0);
		}
	}

	/**
	 * Добавление на обновление графического эффекта.
	 * 
	 * @param graficEffect графический эффект.
	 */
	public void addGraficEffect(GraficEffect graficEffect) {

		Array<GraficEffect>[] containers = getContainers();
		Array<GraficEffect> target = null;

		int index = getIndex();

		// фаза 1, мы пробуем найти не занятый контейнер в списке после
		// последнего используемого
		if(index < containers.length - 1) {
			for(int i = index + 1, length = containers.length; i < length; i++) {

				Array<GraficEffect> container = containers[i];

				if(container.size() < MAX_SHOT_LOAD) {
					target = container;
					index = i;
					break;
				}
			}

		}

		// фаза 2, если еще не нашли контейнер, пробуем еще раз поискать с
		// самого начала списка
		if(target == null) {
			for(int i = 0, length = containers.length; i < length; i++) {

				Array<GraficEffect> container = containers[i];

				if(container.size() < MAX_SHOT_LOAD) {
					target = container;
					index = i;
					break;
				}
			}
		}

		// фаза 3, если досихпор не нашли контейнер, создаем новый
		if(target == null) {
			target = createContainer(containers.length);
			index = containers.length;
		}

		synchronized(target) {

			target.add(graficEffect);

			if(target.size() == 1) {
				ConcurrentUtils.notifyAllInSynchronize(target);
			}
		}

		graficEffect.bind(target);
		setIndex(index);
	}

	/**
	 * Расширение списка контейнеров.
	 * 
	 * @param currentLength текущее кол-во контейнеров.
	 * @return новый контейнер.
	 */
	private synchronized Array<GraficEffect> createContainer(int currentLength) {

		Array<GraficEffect>[] containers = getContainers();

		// если уже было произведено расширение в другом потоке
		if(containers.length > currentLength) {
			return containers[currentLength];
		}

		containers = Arrays.copyOf(containers, 1);
		containers[currentLength] = Arrays.toArray(GraficEffect.class);

		UpdateGraficEffectTask[] updateTasks = getUpdateTasks();
		updateTasks = Arrays.copyOf(updateTasks, 1);
		updateTasks[currentLength] = new UpdateGraficEffectTask(containers[currentLength], currentLength);

		setContainers(containers);
		setUpdateTasks(updateTasks);

		return containers[currentLength];
	}

	/**
	 * @return контейнера обновляемых эффектов.
	 */
	public Array<GraficEffect>[] getContainers() {
		return containers;
	}

	/**
	 * @return индекс контейнера, в который был последний добавлен выстрел.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return задачи по обновлению выстрелов в контейнерах.
	 */
	public UpdateGraficEffectTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * @param containers контейнера обновляемых эффектов.
	 */
	public void setContainers(Array<GraficEffect>[] containers) {
		this.containers = containers;
	}

	/**
	 * @param index индекс контейнера, в который был последний добавлен выстрел.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param updateTasks задачи по обновлению выстрелов в контейнерах.
	 */
	public void setUpdateTasks(UpdateGraficEffectTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}
}
