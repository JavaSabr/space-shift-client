package com.ss.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.VarTable;

import com.jme3.asset.AssetEventListener;
import com.jme3.asset.AssetKey;
import com.jme3.asset.TextureKey;
import com.jme3.input.KeyInput;
import com.jme3.system.AppSettings;
import com.ss.client.document.DocumentConfig;
import com.ss.client.gui.model.Language;
import com.ss.client.gui.model.ScreenSize;
import com.ss.client.table.LangTable;

/**
 * Набор настроек, изменяемых пользователем во время игры.
 * 
 * @author Ronn
 */
public final class GameConfig implements AssetEventListener {

	private static final Logger LOGGER = Loggers.getLogger(GameConfig.class);

	private static final File FILE = new File(Config.PROGECT_PATH + "/game_config.xml");

	private static final GameConfig instance = new GameConfig();

	/** язык интерфейса */
	public static Language LANG;

	/** используемое разрешение экрана */
	public static ScreenSize SCREEN_SIZE;

	/** громкость музыки */
	public static float MUSIC_VOLUME;

	public static float EFFECT_VOLUME;
	public static float VIEW_DISTANCE;

	public static int SWITCH_SELECTOR_KEY;

	public static GameConfig getInstance() {
		return instance;
	}

	public static AppSettings loadSettings() {

		final VarTable vars = FILE.exists() && FILE.canRead() ? new DocumentConfig(FILE).parse() : VarTable.newInstance();
		final LangTable langTable = LangTable.getInstance();

		LANG = langTable.langOf(vars.getString("LANG", "РУССКИЙ"));

		SCREEN_SIZE = ScreenSize.sizeOf(vars.getString("SCREEN_SIZE", "800x600"));

		MUSIC_VOLUME = vars.getFloat("MUSIC_VOLUME", 1F);
		EFFECT_VOLUME = vars.getFloat("EFFECT_VOLUME", 1F);
		VIEW_DISTANCE = vars.getFloat("VIEW_DISTANCE", 1000000F);

		SWITCH_SELECTOR_KEY = vars.getInteger("SWITCH_SELECTOR_KEY", KeyInput.KEY_TAB);

		final AppSettings settings = new AppSettings(true);

		settings.setRenderer(AppSettings.LWJGL_OPENGL3);
		settings.setTitle("Space Shift");
		settings.setFullscreen(false);
		settings.setResolution(SCREEN_SIZE.getWidth(), SCREEN_SIZE.getHeight());
		settings.setFrameRate(-1);

		return settings;
	}

	/**
	 * Сохранения настроек.
	 */
	public static void save() {

		if(!FILE.exists()) {
			try {
				FILE.createNewFile();
			} catch(final IOException e) {
				LOGGER.warning(e);
			}
		}

		if(!FILE.canWrite()) {
			return;
		}

		try(PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(FILE), "UTF-8"))) {

			out.println("<?xml version='1.0' encoding='utf-8'?>");
			out.println("<list>");

			final Field[] fields = GameConfig.class.getFields();

			for(final Field field : fields) {
				out.println("	<set name=\"" + field.getName() + "\" value=\"" + field.get(null) + "\" />");
			}
			out.println("</list>");

		} catch(FileNotFoundException | IllegalArgumentException | IllegalAccessException | UnsupportedEncodingException e) {
			LOGGER.warning(e);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void assetDependencyNotFound(final AssetKey parentKey, final AssetKey dependentAssetKey) {
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void assetLoaded(final AssetKey key) {
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void assetRequested(final AssetKey key) {

		if(key instanceof TextureKey) {
			((TextureKey) key).setAnisotropy(16);
		}
	}
}
