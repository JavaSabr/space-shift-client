package com.ss.client.jme.post.effect;

import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.DepthOfFieldFilter;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.LocalObjects;

/**
 * Реализацияч эффекта с фокусом и размытием.
 * 
 * @author Ronn
 */
public class DepthOfFieldEffect implements PostEffect {

	public static final int MIN_FOCUS_RANGE = 500;
	public static final int MAX_FOCUS_RANGE = 15000;

	private static DepthOfFieldEffect instance;

	public static PostEffect bind(FilterPostProcessor processor) {
		instance = new DepthOfFieldEffect(processor);
		return instance;
	}

	public static DepthOfFieldEffect getInstance() {
		return instance;
	}

	/** фильтр фокуса и резкости */
	private DepthOfFieldFilter filter;

	/** корабль игрока */
	private PlayerShip playerShip;

	public DepthOfFieldEffect(FilterPostProcessor processor) {

		DepthOfFieldFilter filter = new DepthOfFieldFilter();
		filter.setBlurScale(2.5F);
		filter.setFocusDistance(50);
		filter.setFocusRange(MAX_FOCUS_RANGE);
		filter.setEnabled(false);

		processor.addFilter(filter);
		setFilter(filter);
	}

	@Override
	public void activate(LocalObjects local) {
		filter.setEnabled(true);
	}

	@Override
	public void deactivate(LocalObjects local) {
		filter.setEnabled(false);
	}

	/**
	 * @param filter фильтр фокуса и резкости.
	 */
	public void setFilter(DepthOfFieldFilter filter) {
		this.filter = filter;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	@Override
	public void update(LocalObjects local) {
	}
}
