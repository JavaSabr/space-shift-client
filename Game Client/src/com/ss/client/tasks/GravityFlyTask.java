package com.ss.client.tasks;

import java.util.concurrent.atomic.AtomicBoolean;


import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.gravity.GravityObject;

/**
 * Модель обработки гравитации объекта.
 * 
 * @author Ronn
 */
public final class GravityFlyTask {

	/** гравитационный объект */
	private final GravityObject object;

	/** разворот орбиты */
	private final Quaternion rotation;
	/** флаг активности */
	private final AtomicBoolean running;
	/** вектор разворота */
	private final Vector3f rotateVector;
	/** вектор изменения позиции */
	private final Vector3f changed;

	/** время последнего действия */
	private long lastTime;

	/** выполненый градус оборота */
	private double done;

	public GravityFlyTask(final GravityObject object) {
		this.object = object;
		this.rotation = object.getOrbitalRotation();
		this.rotateVector = new Vector3f();
		this.changed = new Vector3f();
		this.running = new AtomicBoolean();
	}

	/**
	 * @return вектор изменения позиции.
	 */
	public Vector3f getChanged() {
		return changed;
	}

	/**
	 * @return степень выполненности.
	 */
	public double getDone() {
		return done;
	}

	/**
	 * @return время последней обработки.
	 */
	public long getLastTime() {
		return lastTime;
	}

	/**
	 * @return гравитационных объект.
	 */
	public GravityObject getObject() {
		return object;
	}

	/**
	 * @return вектор разворота.
	 */
	public Vector3f getRotateVector() {
		return rotateVector;
	}

	/**
	 * @return запущена ли задача полета.
	 */
	public boolean isRunning() {
		return running.get();
	}

	/**
	 * @param done степень выполненности.
	 */
	public void setDone(final double done) {
		this.done = done;
	}

	/**
	 * @param lastTime время последней обработки.
	 */
	public void setLastTime(final long lastTime) {
		this.lastTime = lastTime;
	}

	/**
	 * @param running запущена ли задача полета.
	 */
	public void setRunning(boolean running) {
		this.running.set(running);
	}

	/**
	 * Запуск задачи.
	 */
	public void start() {
		synchronized(this) {
			setLastTime(Game.getCurrentTime());
			setDone(0);
			setRunning(true);
		}
	}

	/**
	 * Остановка задачи.
	 */
	public void stop() {
		synchronized(this) {
			setDone(0);
			setRunning(false);
		}
	}

	/**
	 * Обновление позиции объекта.
	 * 
	 * @param done процент выполнености оборота.
	 */
	public void update(double done) {
		synchronized(this) {
			setLastTime(Game.getCurrentTime());
			setDone(done);
		}
	}

	/**
	 * Обновление позиции.
	 * 
	 * @param currentTime текущее время.
	 */
	public void update(final long currentTime) {

		if(!isRunning()) {
			return;
		}

		try {

			final GravityObject object = getObject();

			if(object.getTurnAroundTime() < 1) {
				return;
			}

			final GravityObject parent = object.getParent();

			if(parent == null) {
				return;
			}

			synchronized(this) {

				final long lastTime = getLastTime();
				final long turnAroundTime = object.getTurnAroundTime();

				final double diff = currentTime - lastTime;
				double done = getDone();

				done += 360 * diff / turnAroundTime;

				final double radians = done * 3.141592653D / 180D;

				final Vector3f center = parent.getLocation();

				final int distance = object.getDistance();

				final double newX = distance * Math.cos(radians);
				final double newZ = distance * Math.sin(radians);

				final Vector3f rotateVector = getRotateVector();
				final Vector3f changed = getChanged();

				rotateVector.set((float) newX, 0, (float) newZ);

				rotation.multLocal(rotateVector);

				rotateVector.addLocal(center);
				rotateVector.subtract(object.getLocation(), changed);

				object.setLocation(rotateVector, changed);

				setDone(done);
			}

		} finally {
			setLastTime(currentTime);
		}
	}
}
