package com.ss.client.tasks;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.GameThread;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.util.LocalObjects;

/**
 * Реализация задачи по обновлению графических эффектов в контейнерах.
 * 
 * @author Ronn
 */
public class UpdateGraficEffectTask extends GameThread {

	private static final Logger LOGGER = Loggers.getLogger(UpdateGraficEffectTask.class);
	private static final Game GAME = Game.getInstance();

	private static final FuncElement<GraficEffect> FINISH_EFFECT_FUNC = new FuncElement<GraficEffect>() {

		@Override
		public void apply(GraficEffect element) {
			element.finish();
		}
	};

	/** контейнер обновляемых выстрелов */
	private final Array<GraficEffect> container;

	public UpdateGraficEffectTask(Array<GraficEffect> container, int order) {
		this.container = container;
		setDaemon(true);
		setName(getClass().getSimpleName() + "-" + order);
		start();
	}

	/**
	 * @return контейнер обновляемых графических эффектов.
	 */
	public Array<GraficEffect> getContainer() {
		return container;
	}

	@Override
	public void run() {

		Array<GraficEffect> update = Arrays.toArray(GraficEffect.class);
		Array<GraficEffect> finished = Arrays.toArray(GraficEffect.class);
		Array<GraficEffect> container = getContainer();

		LocalObjects local = LocalObjects.get();

		while(true) {

			update.clear();
			finished.clear();

			try {

				synchronized(container) {

					if(container.isEmpty()) {
						ConcurrentUtils.waitInSynchronize(container);
					}

					update.addAll(container);
				}

				GAME.updateGeomStart();
				try {

					long currentTime = System.currentTimeMillis();

					for(GraficEffect effect : update.array()) {

						if(effect == null) {
							break;
						}

						if(effect.update(local, currentTime)) {
							finished.add(effect);
						}
					}

				} finally {
					GAME.updateGeomEnd();
				}

				if(!finished.isEmpty()) {

					synchronized(container) {
						container.removeAll(finished);
					}

					finished.apply(FINISH_EFFECT_FUNC);
				}

			} catch(Exception e) {
				LOGGER.warning(e);
			}
		}
	}
}
