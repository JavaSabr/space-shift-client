package com.ss.client.tasks;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.GameThread;
import com.ss.client.model.shots.Shot;
import com.ss.client.util.LocalObjects;

/**
 * Рефлизация задачи по обновлению выстрелов в контейнерах.
 * 
 * @author Ronn
 */
public class UpdateShotsTask extends GameThread {

	private static final Logger LOGGER = Loggers.getLogger(UpdateShotsTask.class);
	private static final Game GAME = Game.getInstance();

	private static final FuncElement<Shot> FINISH_SHOT_FUNC = new FuncElement<Shot>() {

		@Override
		public void apply(Shot shot) {
			shot.finish();
		}
	};

	/** контейнер обновляемых выстрелов */
	private final Array<Shot> container;

	public UpdateShotsTask(Array<Shot> container, int order) {
		this.container = container;
		setDaemon(true);
		setName(getClass().getSimpleName() + "-" + order);
		start();
	}

	/**
	 * @return контейнер обновляемых выстрелов.
	 */
	public Array<Shot> getContainer() {
		return container;
	}

	@Override
	public void run() {

		Array<Shot> update = Arrays.toArray(Shot.class);
		Array<Shot> finished = Arrays.toArray(Shot.class);
		Array<Shot> container = getContainer();

		LocalObjects local = LocalObjects.get();

		while(true) {

			update.clear();
			finished.clear();

			try {

				synchronized(container) {

					if(container.isEmpty()) {
						ConcurrentUtils.waitInSynchronize(container);
					}

					update.addAll(container);
				}

				GAME.updateGeomStart();
				try {

					long currentTime = System.currentTimeMillis();

					for(Shot shot : update.array()) {

						if(shot == null) {
							break;
						}

						if(shot.update(currentTime, local)) {
							finished.add(shot);
						}
					}

				} catch(Exception e) {
					LOGGER.warning(e);
				} finally {
					GAME.updateGeomEnd();
				}

				if(!finished.isEmpty()) {

					synchronized(container) {
						container.removeAll(finished);
					}

					GAME.syncLock();
					try {
						finished.apply(FINISH_SHOT_FUNC);
					} finally {
						GAME.syncUnlock();
					}
				}

			} catch(Exception e) {
				LOGGER.warning(e);
			}
		}
	}
}
