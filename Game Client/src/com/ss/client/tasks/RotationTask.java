package com.ss.client.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

import com.jme3.math.Quaternion;
import com.ss.client.Config;
import com.ss.client.Game;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.util.LocalObjects;

/**
 * Модель разворота корабля.
 * 
 * @author Ronn
 */
public class RotationTask {

	/** флаг активности */
	private final AtomicBoolean running;
	/** разворачиваемый корабль */
	private final SpaceShip spaceShip;

	/** конечный разворот */
	private final Quaternion endRotation;
	/** начальный разворот */
	private final Quaternion startRotation;
	/** итоговый текущий разворот */
	private final Quaternion resultRotation;
	/** буферный разворот */
	private final Quaternion bufferRotation;

	/** время последнего обновления модели */
	private long lastUpdate;
	/** время последнего обновления вьюхи объекта */
	private long lastViewUpdate;

	/** процент выполненности разворота */
	private double done;
	/** шаг разворота при обновлении */
	private double step;

	/** бесконечно ли идет поворот */
	private boolean infinity;

	public RotationTask(final SpaceShip spaceShip) {
		this.spaceShip = spaceShip;
		this.endRotation = new Quaternion();
		this.startRotation = new Quaternion();
		this.resultRotation = new Quaternion();
		this.bufferRotation = new Quaternion();
		this.running = new AtomicBoolean();
	}

	/**
	 * @return буферный разворот.
	 */
	public Quaternion getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return процент выполненности разворота.
	 */
	public final double getDone() {
		return done;
	}

	/**
	 * @return начальный разворот.
	 */
	public Quaternion getEndRotation() {
		return endRotation;
	}

	/**
	 * @return время последнего обновления модели.
	 */
	public final long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return время последнего обновления вьюхи.
	 */
	public long getLastViewUpdate() {
		return lastViewUpdate;
	}

	/**
	 * @return итоговый текущий разворот.
	 */
	public Quaternion getResultRotation() {
		return resultRotation;
	}

	/**
	 * @return разворачиваемый корабль.
	 */
	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	/**
	 * @return конечный разворот.
	 */
	public Quaternion getStartRotation() {
		return startRotation;
	}

	/**
	 * @return шаг разворота при обновлении.
	 */
	public final double getStep() {
		return step;
	}

	/**
	 * @return бесконечно ли идет поворот.
	 */
	public boolean isInfinity() {
		return infinity;
	}

	/**
	 * @return в процессе разворота ли.
	 */
	public boolean isRotation() {
		return done < 1D;
	}

	/**
	 * @return запущена ли задача разворота.
	 */
	public boolean isRunning() {
		return running.get();
	}

	/**
	 * Реинициализация.
	 */
	public void reinit(long currentTime) {
		setLastUpdate(currentTime);
		setRunning(true);
		setDone(1);
	}

	/**
	 * @param done процент выполнения таска.
	 */
	public final void setDone(final double done) {
		this.done = done;
	}

	/**
	 * @param rotation конечный разворот.
	 */
	public void setEndRotation(Quaternion rotation) {
		this.endRotation.set(rotation);
	}

	/**
	 * @param infinity бесконечно ли идет поворот.
	 */
	public void setInfinity(boolean infinity) {
		this.infinity = infinity;
	}

	/**
	 * @param last время последнего обновления модели.
	 */
	public final void setLastUpdate(final long last) {
		this.lastUpdate = last;
	}

	/**
	 * @param lastViewUpdate время последнего обновления вьюхи объекта.
	 */
	public void setLastViewUpdate(long lastViewUpdate) {
		this.lastViewUpdate = lastViewUpdate;
	}

	/**
	 * @param running запущена ли задача разворота.
	 */
	public void setRunning(boolean running) {
		this.running.set(running);
	}

	/**
	 * @param rotation начальный разворот.
	 */
	public void setStartRotaton(Quaternion rotation) {
		this.startRotation.set(rotation);
	}

	/**
	 * @param step шаг выполнения.
	 */
	public final void setStep(final double step) {
		this.step = step;
	}

	/**
	 * Остановка вращения.
	 */
	public void stop() {
		synchronized(this) {
			setDone(1D);
			setRunning(false);
		}
	}

	public boolean update(LocalObjects local, long currentTime) {

		double done = getDone();

		try {

			if(done >= 1D) {
				return false;
			}

			if(!isRunning()) {
				return false;
			}

			final SpaceShip ship = getSpaceShip();
			final SpaceShipView view = ship.getView();

			Quaternion current = view.getRotation();
			Quaternion start = getStartRotation();
			Quaternion end = getEndRotation();
			Quaternion result = getResultRotation();

			if(current.equals(end) || getStep() == 0D) {
				done = 1D;
				return false;
			}

			final long diff = currentTime - getLastViewUpdate();

			if(diff < 1) {
				return false;
			}

			done += getStep() * diff / 1000F;

			if(done >= 1D) {

				if(!isInfinity()) {
					result = end;
				} else {

					Quaternion buffer = getBufferRotation();
					buffer.set(end);
					buffer.subtractLocal(start);

					start.addLocal(buffer);
					start.normalizeLocal();

					end.addLocal(buffer);
					end.normalizeLocal();

					done = Math.min(0.99F, done -= 1F);
				}
			}

			if(result != end) {

				Quaternion buffer = getBufferRotation();
				buffer.set(end);

				result = result.slerp(start, buffer, (float) done);
			}

			synchronized(this) {
				if(isRunning()) {

					view.setRotation(result);

					if(currentTime - getLastUpdate() > Config.WORLD_UPDATE_OBJECT_INTERVAL) {
						ship.setRotation(result);
						setLastUpdate(currentTime);
					}
				}
			}

			return false;
		} finally {
			setLastViewUpdate(currentTime);
			setDone(done);
		}
	}

	/**
	 * Обновление обработки разворота.
	 * 
	 * @param local контейнер локальных объектв.
	 * @param start стартовый разворот.
	 * @param target целевой развор получаем владельца
	 * @param step шаг разворота.
	 * @param done выполненность разворота.
	 * @param infinity бесконечно ли разворачиваться.
	 * @param force принудительная ли синхронизация.
	 */
	public void update(LocalObjects local, Quaternion start, final Quaternion target, float step, float done, boolean infinity, boolean force) {

		synchronized(this) {

			if(done >= 1D) {
				setDone(done);
				return;
			}

			float dot = Math.abs(start.dot(target));

			if(dot >= 0.99999F) {
				setEndRotation(target);
				setDone(1D);
				return;
			}

			setStartRotaton(start);
			setEndRotation(target);
			setDone(done);
			setStep(step);
			setInfinity(infinity);
			setLastUpdate(Game.getCurrentTime());
			setLastViewUpdate(Game.getCurrentTime());
		}
	}

	/**
	 * Запуск бесконечного разворота по указанному направлению.
	 * 
	 * @param target целевой развор получаем владельца.
	 * @param modiff модификатор скорости разворота.
	 */
	public void update(final Quaternion target, float modiff) {

		SpaceShip ship = getSpaceShip();

		if(!ship.isActiveEngine()) {
			return;
		}

		SpaceShipView view = ship.getView();

		if(view == null) {
			return;
		}

		long currentTime = Game.getCurrentTime();

		synchronized(this) {

			Quaternion start = view.getRotation();

			float dot = Math.abs(start.dot(target));

			if(dot > 0.99999F) {
				setDone(1D);
				return;
			}

			final double radians = Math.acos(dot);

			double speed = ship.getRotateSpeed();
			speed = Math.min(speed * modiff, speed);

			if(speed < 0.001F) {
				setDone(1D);
				return;
			}

			final double count = radians / speed;
			final double step = 1 / count;

			setStartRotaton(start);
			setEndRotation(target);
			setLastUpdate(currentTime);
			setLastViewUpdate(currentTime);
			setDone(0F);
			setStep(step);
			setInfinity(true);
		}
	}
}
