package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;

/**
 * Пример создания ландшавта.
 */
public class HelloTerrain extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloTerrain app = new HelloTerrain();
		app.start();
	}

	private TerrainQuad terrain;

	private Material mat_terrain;

	@Override
	public void simpleInitApp() {
		// устанавливаем скорость камеры
		flyCam.setMoveSpeed(50);

		// создаем материал, на основе которого делается текстура ландавта
		mat_terrain = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
		// добавляем альфо текстуру
		mat_terrain.setTexture("Alpha", assetManager.loadTexture("Textures/Terrain/splat/alpha11.png"));
		// загружаем текстуру травы
		final Texture grass = assetManager.loadTexture("Textures/Terrain/splat/grass.jpg");
		// ставим нужный тип заполнения
		grass.setWrap(WrapMode.Repeat);
		// применяем под именем Текс1 на материал
		mat_terrain.setTexture("Tex1", grass);
		// определеям размер чейки текстуры
		mat_terrain.setFloat("Tex1Scale", 64f);
		// тоже саоме
		final Texture dirt = assetManager.loadTexture("Textures/Terrain/splat/dirt.jpg");
		dirt.setWrap(WrapMode.Repeat);
		mat_terrain.setTexture("Tex2", dirt);
		mat_terrain.setFloat("Tex2Scale", 32f);
		// тоже саоме
		final Texture rock = assetManager.loadTexture("Textures/Terrain/splat/road.jpg");
		rock.setWrap(WrapMode.Repeat);
		mat_terrain.setTexture("Tex3", rock);
		mat_terrain.setFloat("Tex3Scale", 128f);

		// переменная модели ландшавта
		AbstractHeightMap heightmap = null;
		// согружаем чернобелую текстуру, описывающую высоты ландавта
		final Texture heightMapImage = assetManager.loadTexture("Textures/Terrain/splat/mountains512.png");
		// создаем модель ландшавта на основе ее
		heightmap = new ImageBasedHeightMap(heightMapImage.getImage());
		// загружаем
		heightmap.load();

		/*
		 * пример генерации ландшавта HillHeightMap.NORMALIZE_RANGE = 100; //
		 * optional try { heightmap = new HillHeightMap(513, 1000, 50, 100,
		 * (byte) 3); // byte 3 is a random seed } catch (Exception ex) {
		 * ex.printStackTrace(); }
		 */

		// определяем размер плитки в ландавте
		final int patchSize = 65;

		// создаем ландшавт с указанным размером плитки, общим размером блока
		// ландшавта и картой высот
		terrain = new TerrainQuad("my terrain", patchSize, 513, heightmap.getHeightMap());
		// применяем материал
		terrain.setMaterial(mat_terrain);
		// устанавливаем его позицию
		terrain.setLocalTranslation(0, -100, 0);
		// маштабируем
		terrain.setLocalScale(2f, 1f, 2f);
		// добавляем в мир
		rootNode.attachChild(terrain);

		// контролер детализации ландшавта
		final TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
		// добавляем его в ландшавт
		terrain.addControl(control);
	}
}