package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 * Пример реализации движухи в сцене.
 */
public class HelloLoop extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloLoop app = new HelloLoop();
		app.start();
	}

	/** сслыка на геометрию коробки */
	protected Geometry boxGeom;

	@Override
	public void simpleInitApp() {
		// создаем коробку размерами 1х1х1
		final Box box = new Box(Vector3f.ZERO, 1, 1, 1);
		// создаем геометрию для коробки
		boxGeom = new Geometry("blue cube", box);
		// создаем материал для геометрии
		final Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// применяем материалу синий цвет
		mat.setColor("Color", ColorRGBA.Blue);
		// применяем к геометрии материал
		boxGeom.setMaterial(mat);
		// добавляем коробку в мир
		rootNode.attachChild(boxGeom);
	}

	/**
	 * Этот метод вызывается при каждом обновлении кадра, т.е. при 400ФПС, 400
	 * раз в секунду )
	 */
	@Override
	public void simpleUpdate(final float tpf) {
		// переменная тпф, это время в кадре, т.е. все нажи
		// смешения/развороты/прочая хуйня
		// должна на нее умножаться, что бы при любом фпс все двигалось с
		// одинаковой скоростью
		boxGeom.rotate(0, 2 * tpf, 0);
	}
}