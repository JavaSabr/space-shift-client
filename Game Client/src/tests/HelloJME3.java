package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 * Аналог Hello Word только для этог движка ))
 */
public class HelloJME3 extends SimpleApplication {

	public static void main(final String[] args) {
		// создаем экземпляр игры
		final HelloJME3 app = new HelloJME3();
		// запускаем игру
		app.start(); // start the game
	}

	/**
	 * Этот метод выполняется перед запуском игры
	 */
	@Override
	public void simpleInitApp() {
		// создаем коробку размерами 1х1х1
		final Box box = new Box(Vector3f.ZERO, 1, 1, 1);
		// создаем геометрию для коробки
		final Geometry geom = new Geometry("Box", box);
		// создаем материал для коробки
		final Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// к материалу применяем цвет синий
		material.setColor("Color", ColorRGBA.Blue);
		// применяем материал к геометрии
		geom.setMaterial(material);
		// добавляем геометрию в мир
		rootNode.attachChild(geom);
	}
}