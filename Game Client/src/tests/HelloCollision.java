package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * Ъкземпляр интегрирования сцены в движек и придание камере физику игрока от
 * первого лица
 */
public class HelloCollision extends SimpleApplication implements ActionListener {

	public static void main(final String[] args) {
		final HelloCollision app = new HelloCollision();
		app.start();
	}

	/** модель сцены */
	private Spatial sceneModel;
	/** используется для придание физики миру */
	private BulletAppState bulletAppState;
	/** используется для придание твердости модели города */
	private RigidBodyControl landscape;
	/** контролер перемещения? */
	private CharacterControl player;

	/** точка перемещения */
	private Vector3f walkDirection;
	/** флаги? */
	private boolean left;
	private boolean right;
	private boolean up;

	private boolean down;

	/**
	 * Обработка действия игрока.
	 */
	@Override
	public void onAction(final String binding, final boolean value, final float tpf) {
		if(binding.equals("Left"))
			left = value;
		else if(binding.equals("Right"))
			right = value;
		else if(binding.equals("Up"))
			up = value;
		else if(binding.equals("Down"))
			down = value;
		else if(binding.equals("Jump"))
			player.jump();
	}

	/**
	 * Инициализация клавишь управления.
	 */
	private void setUpKeys() {
		// укзаываем какая кнопка что делает
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
		inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
		// добавляем на прослушку указанных действий
		inputManager.addListener(this, "Left", "Right", "Up", "Down", "Jump");
	}

	/**
	 * Инициализация освещение в сцене
	 */
	private void setUpLight() {
		// постоянное общее освещение
		final AmbientLight al = new AmbientLight();
		// устанавливаем его оттенок
		al.setColor(ColorRGBA.White.mult(1.3f));
		// добавляем в мир
		rootNode.addLight(al);

		// направленное освещение
		final DirectionalLight dl = new DirectionalLight();
		// устанавливаем его цвет
		dl.setColor(ColorRGBA.White);
		// устанавливаем направление
		dl.setDirection(new Vector3f(2.8f, -2.8f, -2.8f).normalizeLocal());
		// добавляем в мир
		rootNode.addLight(dl);
	}

	@Override
	public void simpleInitApp() {
		// создаем объект точки перемещения
		walkDirection = new Vector3f();

		// создаем модель физики мира
		bulletAppState = new BulletAppState();
		// применяем к этой сцене
		stateManager.attach(bulletAppState);

		// ставим цвет фона голубой
		viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
		// ставим скорость камеры
		flyCam.setMoveSpeed(200);
		// настраиваем клавиши
		setUpKeys();
		// настраиваем освещение
		setUpLight();

		// заливаем в оссент менеджер архив со сценой
		assetManager.registerLocator("town.zip", ZipLocator.class);
		// загружаем модель сцены
		sceneModel = assetManager.loadModel("main.scene");
		// маштабируем сцену
		sceneModel.setLocalScale(2f);

		// создаем модель расчета столкновений, данный вариант оптимален для
		// статичных объектов
		final CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(sceneModel);
		// создаем физицу сцены и применяем модель физики с нулевой массой(сцена
		// веса не имеет)
		landscape = new RigidBodyControl(sceneShape, 0);
		// добавляем обработчика в саму сцену
		sceneModel.addControl(landscape);

		// теперь создаем физику игрока
		final CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 6f, 1);
		// создаем контролер игрока
		player = new CharacterControl(capsuleShape, 0.05f);
		// устанавливаем скорсоть прыжка
		player.setJumpSpeed(30);
		// скорость падения
		player.setFallSpeed(50);
		// силу гравитации
		player.setGravity(40);
		// устанавливаем исходную позицию
		player.setPhysicsLocation(new Vector3f(0, 10, 0));

		// добавляем сцену в мир
		rootNode.attachChild(sceneModel);
		// добавлям в физ. движек сцену и игрока
		bulletAppState.getPhysicsSpace().add(landscape);
		bulletAppState.getPhysicsSpace().add(player);
	}

	@Override
	public void simpleUpdate(final float tpf) {
		// получаем направление камеры
		final Vector3f camDir = cam.getDirection().clone();
		// получаем направление от камеры влево
		final Vector3f camLeft = cam.getLeft().clone();

		// зануляем направление перемещения
		walkDirection.set(0, 0, 0);

		if(left)
			// если двигаемся влево, берем значения направления в лево
			walkDirection.addLocal(camLeft);

		if(right)
			// если вправо, то берем обратное направления влево
			walkDirection.addLocal(camLeft.negate());

		if(up)
			// если вперед, то берем направление камеры
			walkDirection.addLocal(camDir);
		if(down)
			// если назад, берем обратное направление камеры
			walkDirection.addLocal(camDir.negate());

		// начинаем движение
		player.setWalkDirection(walkDirection);
		// ставим позицию камеры на позицию игрока
		cam.setLocation(player.getPhysicsLocation());
	}
}