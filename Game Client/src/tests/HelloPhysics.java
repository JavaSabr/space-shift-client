package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.TextureKey;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.font.BitmapText;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.scene.shape.Sphere.TextureMode;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;

/**
 * Пример создания полноценной физики физици.
 */
public class HelloPhysics extends SimpleApplication {

	/** контантные экземпляры объектов */
	private static final Box box;
	private static final Sphere sphere;
	private static final Box floor;

	public static void main(final String args[]) {
		final HelloPhysics app = new HelloPhysics();
		app.start();
	}

	/** фундаментальная физика сцены */
	private BulletAppState bulletAppState;

	/** материалы объектов */
	private Material wall_mat;
	private Material stone_mat;
	private Material floor_mat;

	/** описание габаритов кирпича */
	private static final float brickLength = 0.48f;
	private static final float brickWidth = 0.24f;
	private static final float brickHeight = 0.12f;

	static {
		// создаем ядро
		sphere = new Sphere(32, 32, 0.4f, true, false);
		// ставим ей нужный режим заливки текстуры
		sphere.setTextureMode(TextureMode.Projected);
		// создаем кирпич
		box = new Box(Vector3f.ZERO, brickLength, brickHeight, brickWidth);
		// маштабирование текстуры
		box.scaleTextureCoordinates(new Vector2f(1f, .5f));
		// создаем пол
		floor = new Box(Vector3f.ZERO, 10f, 0.1f, 5f);
		// маштабирование текстуры
		floor.scaleTextureCoordinates(new Vector2f(3, 6));
	}

	/**
	 * Прослушиватель выстрелов
	 */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {
			if(name.equals("shoot") && !keyPressed)
				// запускаем мячик
				makeCannonBall();
		}
	};

	/**
	 * Инициализация прицела.
	 */
	protected void initCrossHairs() {
		guiNode.detachAllChildren();
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		final BitmapText ch = new BitmapText(guiFont, false);
		ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
		ch.setText("+"); // fake crosshairs :)
		ch.setLocalTranslation(
		// center
				settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 2, settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
		guiNode.attachChild(ch);
	}

	/**
	 * Инициализация пола.
	 */
	public void initFloor() {
		// создаем геометрию пола
		final Geometry floor_geo = new Geometry("Floor", floor);
		// применяем материал
		floor_geo.setMaterial(floor_mat);
		// указываем положение
		floor_geo.setLocalTranslation(0, -0.1f, 0);
		// добавляем в мир
		rootNode.attachChild(floor_geo);
		// создаем физику с нулевой массой
		final RigidBodyControl floor_phy = new RigidBodyControl(0.0f);
		// добавляем в геометрию физику
		floor_geo.addControl(floor_phy);
		// добавляем в фундаментальную модель физику пола
		bulletAppState.getPhysicsSpace().add(floor_phy);
	}

	/**
	 * Инициализация материалов.
	 */
	public void initMaterials() {
		// создаем материал кирпича
		wall_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// хуйня какая-то
		final TextureKey key = new TextureKey("Textures/Terrain/BrickWall/BrickWall.jpg");
		key.setGenerateMips(true);
		// загружаем текстуру
		final Texture tex = assetManager.loadTexture(key);
		// применяем текстуру на материал
		wall_mat.setTexture("ColorMap", tex);

		stone_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		final TextureKey key2 = new TextureKey("Textures/Terrain/Rock/Rock.PNG");
		key2.setGenerateMips(true);
		final Texture tex2 = assetManager.loadTexture(key2);
		stone_mat.setTexture("ColorMap", tex2);

		floor_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		final TextureKey key3 = new TextureKey("Textures/Terrain/Pond/Pond.jpg");
		key3.setGenerateMips(true);
		final Texture tex3 = assetManager.loadTexture(key3);
		tex3.setWrap(WrapMode.Repeat);
		floor_mat.setTexture("ColorMap", tex3);
	}

	/**
	 * Создание стены из кирпичей
	 */
	public void initWall() {
		float startpt = brickLength / 4;
		float height = 0;

		// создаем 15 кирпичей в высоту
		for(int j = 0; j < 15; j++) {
			// 6 в ширину
			for(int i = 0; i < 6; i++)
				makeBrick(new Vector3f(i * brickLength * 2 + startpt, brickHeight + height, 0));

			startpt = -startpt;
			height += 2 * brickHeight;
		}
	}

	/**
	 * Метод создания кирпича в указаной точке
	 */
	public void makeBrick(final Vector3f loc) {
		// созаемгеометрию кирпича
		final Geometry brick_geo = new Geometry("brick", box);
		// применяем материал кирпича
		brick_geo.setMaterial(wall_mat);
		// добавляем в мир
		rootNode.attachChild(brick_geo);
		// ставим на нужную позицию
		brick_geo.setLocalTranslation(loc);
		// создаем физику кирпича
		final RigidBodyControl brick_phy = new RigidBodyControl(2f);
		// добавляем физику в геометрию
		brick_geo.addControl(brick_phy);
		// добавляем в фундаментальную модель физику кирпича
		bulletAppState.getPhysicsSpace().add(brick_phy);
	}

	/**
	 * Кидание ядром
	 */
	public void makeCannonBall() {
		// создаем геометрию шара
		final Geometry ball_geo = new Geometry("cannon ball", sphere);
		// применяем к нему материал
		ball_geo.setMaterial(stone_mat);
		// добавляем в мир
		rootNode.attachChild(ball_geo);
		// указываем позицию камеры
		ball_geo.setLocalTranslation(cam.getLocation());
		// создаем модел физики
		final RigidBodyControl ball_phy = new RigidBodyControl(1f);
		// добавляем ее к геометрии
		ball_geo.addControl(ball_phy);
		// добавляем в фундаментальную физическую модель
		bulletAppState.getPhysicsSpace().add(ball_phy);
		// применить толчок силой 25 в направлении камеры
		ball_phy.setLinearVelocity(cam.getDirection().mult(25));
	}

	@Override
	public void simpleInitApp() {
		// создаем фундаментальную модель физики
		bulletAppState = new BulletAppState();
		// применяем ее на эту сцену
		stateManager.attach(bulletAppState);

		// размещаем камеру в указанной позиции
		cam.setLocation(new Vector3f(0, 4f, 6f));
		// указываем смотреть на нужную точку
		cam.lookAt(new Vector3f(2, 2, 0), Vector3f.UNIT_Y);

		// добавляем выстрел на кнопку мыши
		inputManager.addMapping("shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		// добавляем прослушку выстрела
		inputManager.addListener(actionListener, "shoot");

		// инициализируем материалы
		initMaterials();
		//
		initWall();
		// инициализируем пол
		initFloor();
		// инициализируем прицел
		initCrossHairs();
	}
}