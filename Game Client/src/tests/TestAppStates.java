package tests;

import java.util.logging.Level;

import com.jme3.app.Application;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;

public class TestAppStates extends Application {

	public static void main(final String[] args) {
		java.util.logging.Logger.getLogger("").setLevel(Level.SEVERE);
		final TestAppStates app = new TestAppStates();
		app.start();
	}

	@Override
	public void destroy() {
		super.destroy();

		System.out.println("Destroy");
	}

	@Override
	public void initialize() {
		super.initialize();

		System.out.println("Initialize");

		final RootNodeState state = new RootNodeState();
		// viewPort.attachScene(state.getRootNode());
		stateManager.attach(state);

		final Spatial model = assetManager.loadModel("Models/Teapot/Teapot.obj");
		model.scale(3);
		model.setMaterial(assetManager.loadMaterial("Interface/Logo/Logo.j3m"));
		state.getRootNode().attachChild(model);

		final NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
		// niftyDisplay.getNifty().fromXml("Interface/Nifty/HelloJme.xml",
		// "start");
		guiViewPort.addProcessor(niftyDisplay);

		final Thread thread = new Thread() {

			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch(final InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("detach");

				stateManager.detach(state);
			}
		};

		thread.start();
	}

	@Override
	public void start(final JmeContext.Type contextType) {
		final AppSettings settings = new AppSettings(true);
		settings.setResolution(1024, 768);
		setSettings(settings);

		super.start(contextType);
	}

	@Override
	public void update() {
		super.update();

		// do some animation
		final float tpf = timer.getTimePerFrame();

		stateManager.update(tpf);
		stateManager.render(renderManager);

		// render the viewports
		renderManager.render(tpf, context.isRenderable());
	}
}