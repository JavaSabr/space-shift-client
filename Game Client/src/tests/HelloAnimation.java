package tests;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/**
 * Пример использования анимации.
 */
public class HelloAnimation extends SimpleApplication implements AnimEventListener {

	public static void main(final String[] args) {
		final HelloAnimation app = new HelloAnimation();
		app.start();
	}

	private AnimChannel channel;
	private AnimControl control;
	private Node player;

	/** анонимный слушатель событий */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {
			// если событие "движение" и кнопка не зажата
			if(name.equals("Walk") && !keyPressed)
				// несли текущая анимация не равна запускаемой
				if(!channel.getAnimationName().equals("Walk")) {
					// запускаем анимацию хотьбы
					channel.setAnim("Walk", 0.50f);
					// в повторяющемся режиме
					channel.setLoopMode(LoopMode.Cycle);
				}
		}
	};

	/**
	 * Инициализация прослушки клавишь
	 */
	private void initKeys() {
		// добавляем событие при нажатии на пробел
		inputManager.addMapping("Walk", new KeyTrigger(KeyInput.KEY_SPACE));
		// добавляем слушатель событий
		inputManager.addListener(actionListener, "Walk");
	}

	@Override
	public void onAnimChange(final AnimControl control, final AnimChannel channel, final String animName) {
		// unused
	}

	/**
	 * Вызывается при выполнении какой-то анимации.
	 */
	@Override
	public void onAnimCycleDone(final AnimControl control, final AnimChannel channel, final String animName) {
		// если это была анимаия хотьбы
		if(animName.equals("Walk")) {
			// устанавливаем переключение на "стоять" и указываем сколько
			// времени идет переход между анимаций, что была запущена к этой
			// анимации
			channel.setAnim("stand", 0.50f);
			// режим повтора, циклично делать или один раз
			channel.setLoopMode(LoopMode.DontLoop);
			// модификатор скорости анимации
			channel.setSpeed(1f);
		}
	}

	@Override
	public void simpleInitApp() {
		// ставим цвет фона
		viewPort.setBackgroundColor(ColorRGBA.LightGray);
		// инициализируем прослушку клавишь
		initKeys();

		// создаем источник света, нужен как минимум для отображения текстуры
		final DirectionalLight dl = new DirectionalLight();
		// устанавливаем его позицию
		dl.setDirection(new Vector3f(-0.1f, -1f, -1).normalizeLocal());
		// добавляем в мир
		rootNode.addLight(dl);

		// загружаем моделт робота
		player = (Node) assetManager.loadModel("Models/Oto/Oto.mesh.xml");
		// маштабируем
		player.setLocalScale(0.5f);
		// добавляем в мир
		rootNode.attachChild(player);

		// получаем контроль анимпации робота, он дает доступ к набору анимаций
		// модели
		control = player.getControl(AnimControl.class);
		// добавляем прослушку
		control.addListener(this);
		// создаем канал набора анимаций
		channel = control.createChannel();
		// привести моделт в дефолтную позицию
		channel.setAnim("stand");
	}
}